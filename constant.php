<?php
class Constant
{
  public static $KEY_ADMIN_STATE = "BDS_ADMIN_STATE";
  public static $KEY_ADMIN_MESSAGE = 'BDS_KEY_ADMIN_MSG';

  public static $KEY_USER_STATE = 'BDS_USER_STATE';
  public static $KEY_MESSAGE = 'BDS_KEY_MESSAGE';
  public static $KEY_LANG = "BDS_LANG_STATE";
  public static $LIMIT_PIC_UPLOAD = 5120;
  public static $ALLOW_PIC_UPLOAD = 'jpg|png|jpeg';

  public static $PER_ROW_PAGE = 15;
  public static $PER_ROW_PAGE_ADMIN = 50;
  public static $NUM_LINK_PAGE = 4;
  public static $NUM_STAGE = 10;
  public static $LANGUAGES = array('vi' => 'vietnamese', 'en' => 'english');
  public static $DEFAULT_LANG = 'vi';
  public static $ITEM_TYPE = array(
    array('id' => 1, 'code' => 'itemtype_for_rent', 'name' => 'Cần cho thuê', 'default_days' => 30),
    array('id' => 2, 'code' => 'itemtype_rent', 'name' => 'Cần thuê', 'default_days' => 30),
    array('id' => 3, 'code' => 'itemtype_for_sale', 'name' => 'Cần bán', 'default_days' => 90),
    array('id' => 4, 'code' => 'itemtype_buy', 'name' => 'Cần mua', 'default_days' => 90)
  );

  public static $ORIENTATION_HOUSE = array(
    array('id' => 1, 'name' => 'Đông', 'code' => 'oh_east'),
    array('id' => 2, 'name' => 'Tây', 'code' => 'oh_west'),
    array('id' => 3, 'name' => 'Nam', 'code' => 'oh_south'),
    array('id' => 4, 'name' => 'Bắc', 'code' => 'oh_north'),
    array('id' => 5, 'name' => 'Đông Bắc', 'code' => 'oh_east_north'),
    array('id' => 6, 'name' => 'Đông Nam', 'code' => 'oh_east_south'),
    array('id' => 7, 'name' => 'Tây Bắc', 'code' => 'oh_west_north'),
    array('id' => 8, 'name' => 'Tây Nam', 'code' => 'oh_west_south'),
  );
  public static $RANGE_PINK_BOOK = array(
    array('', '--Chọn sổ hồng--'),
    array('1', 'Có'),
    array('2', 'Không')
  );
  public static $RANGE_RED_BOOK = array(
    array('', '--Chọn sổ đỏ--'),
    array('1', 'Có'),
    array('2', 'Không')
  );
  public static $RANGE_AREA = array(
    array('', '--Chọn diện tích--'),
    array('<=30', '<= 30 m²'),
    array('30-50', '30 - 50 m²'),
    array('50-80', '50 - 80 m²'),
    array('80-100', '80 - 100 m²'),
    array('100-150', '100 - 150 m²'),
    array('150-200', '150 - 200 m²'),
    array('200-250', '200 - 250 m²'),
    array('250-300', '250 - 300 m²'),
    array('300-500', '300 - 500 m²'),
    array('>=500', '>= 500 m²')
  );

  public static $RANGE_PRICE = array(
    array('', '-- Chọn mức giá --'),
    array('0', 'Thỏa thuận'),
    array('<500', '< 500 triệu'),
    array('500-800', '500 - 800 triệu'),
    array('800-1000', '800 triệu - 1 tỷ'),
    array('1000-2000', '1 - 2 tỷ'),
    array('2000-3000', '2 - 3 tỷ'),
    array('3000-5000', '3 - 5 tỷ'),
    array('5000-7000', '5 - 7 tỷ'),
    array('7000-10000', '7 - 10 tỷ'),
    array('10000-20000', '10 - 20 tỷ'),
    array('20000-30000', '20 - 30 tỷ'),
    array('>30000', '> 30 tỷ')
  );

  public static $RANGE_NUM_BEDROOMS = array(
    array('', '--Chọn số phòng ngủ--'),
    array('', 'Không xác định'),
    array('=1', '1+'),
    array('=2', '2+'),
    array('=3', '3+'),
    array('=4', '4+'),
    array('=5', '5+')
  );
  public static $RANGE_BATH_ROOM = array(
    array('', '--Chọn số phòng tắm, vệ sinh--'),
    array('', 'Không xác định'),
    array('=1', '1+'),
    array('=2', '2+'),
    array('=3', '3+'),
    array('=4', '4+'),
    array('=5', '5+')
  );
  public static $RES_IMG = array('width' => 900, 'height' => 675);
  public static $MIN_RES_IMG = array('width' => 500, 'height' => 375);
  public static $RANGE_TOTAL_AREA = array(
    array('', '--Chọn tổng điện tích đất--'),
    array('<1000', '0-1.000m²'),
    array('1000-5000', '1.000-5.000m²'),
    array('5000-10000', '5.000-1ha'),
    array('10000-50000', '1-5ha'),
    array('100000-500000', '10-50ha'),
    array('>50000', '>50ha')
  );
  public static $RANGE_BUILDING_DENSITY = array(
    array('', '--Chọn mật độ xây dựng--'),
    array('<25', '0-25%'),
    array('25-60', '25-60%'),
    array('60-80', '60-80%'),
    array('80-100', '80-100%')
  );
  public static $RANGE_NUM_FLOOR = array(
    array('', '--Chọn số tầng--'),
    array('<5', '0-5 tầng'),
    array('5-10', '5-10 tầng'),
    array('10-25', '10-25 tầng'),
    array('>25', '>25 tầng')
  );
  public static $RANGE_TOTAL_AREA_BUILDING = array(
    array('', '--Chọn tổng điện tích xây dựng--'),
    array('<100', '0-100m²'),
    array('100-200', '100-200m²'),
    array('200-500', '200-500m²'),
    array('500-1000', '500-1.000m²'),
    array('>1000', '>1.000m²')
  );
  // --Chọn tổng điện tích đất--
  public static $FILTER_2 = array(
    array('', '--Chọn tổng điện tích đất--'),
    array('<1000', '0-1.000m²'),
    array('1000-5000', '1.000-5.000m²'),
    array('5000-10000', '5.000-1ha'),
    array('10000-50000', '1-5ha'),
    array('100000-500000', '10-50ha'),
    array('>50000', '>50ha')
  );
  // -- Chọn tổng điện tích đất --
  public static $FILTER_3 = array(
    array('', '-- Chọn tổng điện tích đất --'),
    array('<100', '0-100m²'),
    array('100-200', '100-200m²'),
    array('200-500', '200-500m²'),
    array('500-1000', '500-1.000m²'),
    array('>1000', '>1.000m²')
  );
  // --Chọn tổng điện tích đất--
  public static $FILTER_4 = array(
    array('', ' --Chọn tổng điện tích đất--'),
    array('<500', '0-500m²'),
    array('500-1000', '500-1.000m²'),
    array('1000-5000', '1.000-5.000m²'),
    array('>5000', '>5.000m²')
  );
  // --Chọn mật độ xây dựng--
  public static $FILTER_5 = array(
    array('', '--Chọn mật độ xây dựng--'),
    array('<25', '0-25%'),
    array('25-60', '25-60%'),
    array('60-80', '60-80%'),
    array('80-100', '80-100%')
  );
  // --Chọn số tầng--
  public static $FILTER_6 = array(
    array('', '--Chọn số tầng--'),
    array('<5', '0-5 tầng'),
    array('5-10', '5-10 tầng'),
    array('10-25', '10-25 tầng'),
    array('>25', '>25 tầng')
  );
  // --Chọn số tầng--
  public static $FILTER_7 = array(
    array('', '--Chọn số tầng--'),
    array('<3', '1-3 tầng'),
    array('>3', '>3 tầng')
  );
  // --Chọn tổng điện tích xây dựng--
  public static $FILTER_8 = array(
    array('', '--Chọn tổng điện tích xây dựng--'),
    array('<100', '0-100m²'),
    array('100-200', '100-200m²'),
    array('200-500', '200-500m²'),
    array('500-1000', '500-1.000m²'),
    array('>1000', '>1.000m²')
  );
  // --Chọn tổng điện tích xây dựng--
  public static $FILTER_9 = array(
    array('', '--Chọn tổng điện tích xây dựng--'),
    array('<500', '0-500m²'),
    array('500-1000', '500-1.000m²'),
    array('1000-5000', '1.000-5.000m²'),
    array('>5000', '>5.000m²')
  );
  // --Chọn phòng tắm, vệ sinh--
  public static $FILTER_10 = array(
    array('', '--Chọn phòng tắm, vệ sinh--'),
    array('=1', '1'),
    array('=2', '2'),
    array('=3', '3'),
    array('=4', '4'),
    array('=5', '5'),
    array('>5', '>5')
  );
  //--Chọn phòng ngủ--
  public static $FILTER_11 = array(
    array('', '--Chọn phòng ngủ--'),
    array('=1', '1'),
    array('=2', '2'),
    array('=3', '3'),
    array('=4', '4'),
    array('=5', '5'),
    array('>5', '>5')
  );
}
