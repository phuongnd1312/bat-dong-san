var arrTitle = arrDesc = [];
var title = $('span.title');
var description = $('div.description');
title.each(function( index ) {
	arrTitle[index] = $(this).text();
});
title.each(function( index ) {
		var number = $(this).width()/10;
		if($(this).height()>32)
		{
		    var text=arrTitle[index].substring(number*2-5,0)+'... ';
		    $(this).text(text);

		}
	});
description.each(function( index ) {
  arrDesc[index] = $(this).text();
});
description.each(function( index ) {
	
		var number2 = $(this).width()/6;
		var text2=arrDesc[index].substring(number2*2,0);
		if(text2!=arrDesc[index])
			text2=text2+'...';
		$(this).text(text2);
	
		
});
$( window ).resize(function() {
	title.each(function( index ) {
		var number = $(this).width()/10;
		if($(this).height()>32)
		{
		    var text=arrTitle[index].substring(number*2-5,0)+'... ';
		    $(this).text(text);

		}
	});

	description.each(function( index ) {
		
			var number2 = $(this).width()/6;
			var text2=arrDesc[index].substring(number2*2,0);
			if(text2!=arrDesc[index])
				text2=text2+'...';
			$(this).text(text2);
		
	});
	
});


var save_item_second = false;
function onSaveItem(item_id){
	if(save_item_second ==  true) return;
	save_item_second =  true;
	API.updateUserItem(item_id, function(resp){
		if(resp.error_code == 0){
			var user_item = $('#user_item_'+item_id);
			user_item.find('.labeltext').html(resp.data.lbl);

			var link = user_item.find('a.btn_item');
			if(resp.data.class == 'save')
				link.removeClass('remove');
			else
				link.removeClass('save');

			link.addClass(resp.data.class);
		}else{

		}
		save_item_second = false;
	});
}