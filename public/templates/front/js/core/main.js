/*API*/
var API = {
  userLogin: function(params, callback) {
    Util.httpRequest(params, "/front/users/login", "POST", function(
      data,
      textStatus
    ) {
      callback(data);
    });
  },
  removePhotoItem: function(params, callback) {
    Util.httpRequest(params, "/front/items/remove_photo", "post", function(
      data,
      textStatus
    ) {
      callback(data);
    });
  },
  getDistrictByCity: function(id, callback) {
    Util.httpRequest(
      "",
      "/front/ajax/get_district_by_city/" + id,
      "get",
      function(data, textStatus) {
        callback(data);
      }
    );
  },
  getWardByDistrict: function(id, callback) {
    Util.httpRequest(
      "",
      "/front/ajax/get_ward_by_district/" + id,
      "get",
      function(data, textStatus) {
        callback(data);
      }
    );
  },
  getStreetByDistrict: function(id, callback) {
    Util.httpRequest(
      "",
      "/front/ajax/get_street_by_district/" + id,
      "get",
      function(data, textStatus) {
        callback(data);
      }
    );
  },
  getStreetByWards: function(ids, callback) {
    Util.httpRequest(
      "",
      "/front/ajax/get_street_by_wards/" + ids,
      "get",
      function(data, textStatus) {
        callback(data);
      }
    );
  },
  getTypeItem: function(params, callback) {
    Util.httpRequest(params, "/front/ajax/get_type_item/", "get", function(
      data,
      textStatus
    ) {
      callback(data);
    });
  },
  getProjectProperties: function(params, callback) {
    Util.httpRequest(params, "/front/ajax/project_properties/", "get", function(
      data,
      textStatus
    ) {
      callback(data);
    });
  },

  getFilterProjectProperties: function(params, callback) {
    Util.httpRequest(
      params,
      "/front/ajax/filter_project_properties/",
      "get",
      function(data, textStatus) {
        callback(data);
      }
    );
  },
  getLocationItems: function(params, callback) {
    Util.httpRequest(
      params,
      "/front/ajax/get_location_items/",
      "post",
      function(data, textStatus) {
        callback(data);
      }
    );
  },
  updateItemStatus: function(params, callback) {
    Util.httpRequest(
      params,
      "/front/items/update_item_status/",
      "post",
      function(data, textStatus) {
        callback(data);
      }
    );
  },
  updatePostStatus: function(params, callback) {
    Util.httpRequest(
      params,
      "/front/items/update_post_status/",
      "post",
      function(data, textStatus) {
        callback(data);
      }
    );
  },
  checkCaptcha: function(params, callback) {
    Util.httpRequest(params, "/front/ajax/check_captcha/", "post", function(
      data,
      textStatus
    ) {
      callback(data);
    });
  },
  updateUserItem: function(item_id, callback) {
    Util.httpRequest(
      "",
      "/front/user_items/update_item/" + item_id,
      "get",
      function(data, textStatus) {
        callback(data);
      }
    );
  },
  getProjects: function(params, callback) {
    Util.httpRequest(params, "/front/ajax/get_projects/", "get", function(
      data,
      textStatus
    ) {
      callback(data);
    });
  }
};

function onUserLogin() {
  $("input.lfrequired").each(function(index) {
    var val = $(this).val();
    if ($.trim(val) == "") $(this).addClass("lferror");
    else $(this).removeClass("lferror");
  });

  var login_message = $("#login_message");
  login_message.hide();
  if ($("input.lferror").length == 0) {
    API.userLogin($("#frm_user_login").serialize(), function(resp) {
      if (resp.error_code == 0) location.reload();
      else {
        login_message.html(resp.message);
        login_message.show();
      }
    });
  }
}
function isScrolledIntoView(elem) {
  var $elem = $(elem);
  var $window = $(".main_content");

  var docViewTop = $window.scrollTop();
  var docViewBottom = docViewTop + $window.height();
  var elemTop = $elem.offset().top;
  var elemBottom = elemTop + $elem.height();

  return elemBottom <= docViewBottom && elemTop >= docViewTop;
}

jQuery.fn.ForceNumericOnly = function() {
  return this.each(function() {
    $(this).keydown(function(e) {
      var key = e.charCode || e.keyCode || 0;
      // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
      // home, end, period, and numpad decimal
      return (
        key == 8 ||
        key == 9 ||
        key == 13 ||
        key == 46 ||
        key == 110 ||
        key == 190 ||
        (key >= 35 && key <= 40) ||
        (key >= 48 && key <= 57) ||
        (key >= 96 && key <= 105)
      );
    });
  });
};
Number.prototype.formatMoney = function formatMoney(c, d, t) {
  var n = this,
    c = isNaN((c = Math.abs(c))) ? 2 : c,
    d = d == undefined ? "," : d,
    t = t == undefined ? "." : t,
    s = n < 0 ? "-" : "",
    i = parseInt((n = Math.abs(+n || 0).toFixed(c))) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
  return (
    s +
    (j ? i.substr(0, j) + t : "") +
    i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) +
    (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "")
  );
};

$.fn.serializeObject = function() {
  var o = {};
  var a = this.serializeArray();
  $.each(a, function() {
    if (o[this.name] !== undefined) {
      if (!o[this.name].push) {
        o[this.name] = [o[this.name]];
      }
      o[this.name].push(this.value || "");
    } else {
      o[this.name] = this.value || "";
    }
  });
  return o;
};

$(document).ready(function() {
  $(".sidebar-navigation a").each(function() {
    var currentLink = window.location.href;
    if (currentLink.slice(-1) == "/") {
      currentLink = currentLink.substring(0, currentLink.length - 1);
    }

    var link = $(this).prop("href");

    if (link.slice(-1) == "/") {
      link = link.substring(0, link.length - 1);
    }
    if (link == currentLink) {
      $(this).closest("li").addClass("active");
      return false;
    }
  });

  $("input.type_number").ForceNumericOnly();
  $("#field_pass_login").on("keydown", function(event) {
    if (event.which == 13) {
      onUserLogin();
    }
  });
  $("#frm_filter_item").blur(function() {
    if ($("#frm_filter_item").height() > $(".main_content").height())
      $("#frm_filter_item").css("overflow-y", "scroll");
  });
  $("#btn_toggle").click(function() {
    var toggle = $(".bound_slidebar_left");
    var main = $(".main_content");
    if (toggle.css("display") == "none") {
      main.width("75%");
      toggle.toggle();
    } else {
      toggle.toggle();
      main.width("100%");
    }
  });
  $(".cb-hd").closest(".ez-checkbox").addClass("hidden");
});

/*
//Crawl Street from BDS
function httpRequest(params, callback) {
	$.ajax({
		url : 'http://localhost/bdsvn/crawl/save',
		type : 'POST',
		data : params,
		success : function(data, textStatus, jqXHR) {
			callback(jQuery.parseJSON(data), textStatus);
		},
		error : function(jqXHR, textStatus, errorThrown) {
			console.log('callNetwork failed ' + errorThrown);
			callback(null, textStatus);
		}
	});
}
function delay(ms) {
	var cur_d = new Date();
	var cur_ticks = cur_d.getTime();
	var ms_passed = 0;
	while(ms_passed < ms) {
		var d = new Date();  // Possible memory leak?
		var ticks = d.getTime();
		ms_passed = ticks - cur_ticks;
		// d = null;  // Prevent memory leak?
	}
}

function crawl(){
	var status = 0;
	$('#ddlDistrict option').each(function(idt){
		if(idt != 0){
			$(this).prop('selected', true);
			$('#ddlDistrict').trigger('change');
			delay(1000);
			var city = $('#ddlCity option:selected').html();
			var district = $(this).html();
			var data = {city: city, district: district, streets: []};
			$('#ddlStreet option').each(function(){
				if($(this).val() != 0)
				{
					data.streets.push($(this).html());
				}
			});
			if(data.streets.length > 0){
				httpRequest(data, function(resp){
					console.log(resp);
				});
			}else{
				console.log(district+' No street');
			}
			console.log('crawling...');
			status = 1;
		}
	});
}*/
/*if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
window.onmousewheel = document.onmousewheel = wheel;

function wheel(event) {
    var delta = 0;
    if (event.wheelDelta) delta = event.wheelDelta / 120;
    else if (event.detail) delta = -event.detail / 3;

    handle(delta);
    if (event.preventDefault) event.preventDefault();
    event.returnValue = false;
}

function handle(delta) {
    var time = 10;
	var distance = 300;
    var time1 = 100;
	var distance1 = 1000;
    $('.select2-results__options').stop().animate({
        scrollTop: $('.select2-results__options').scrollTop() - (distance * delta)
    }, time );
     $('body').stop().animate({
        scrollTop: $('window').scrollTop() - (distance1 * delta)
    }, time1 );
}*/
