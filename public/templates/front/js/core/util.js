var Util = {
	validateName: function(name) {
	    var regularExpression  = /[a-zA-Z0-9]$/;

	    if(!regularExpression.test(name)) {
	        return false;
	    }
	    return true;
	},
	validatePassword: function(newPassword) {
	    var minNumberofChars = 6;
	    var maxNumberofChars = 32;
	    var regularExpression  = /^(?=.*)[a-zA-Z0-9]{6,32}$/;
	  
	    if(newPassword.length < minNumberofChars || newPassword.length > maxNumberofChars){
	        return false;
	    }
	    /*if(!regularExpression.test(newPassword)) {
	        return false;
	    }*/
	    return true;
	},
	isEmpty: function(str){
	    if($.trim(str) == "")
	        return true;
	    else 
	        return false;
	},
	validateEmail: function(email) {
	    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	    return re.test(email);
	},
	validatePhone: function(phone) {
		phone=phone.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi,'');
	
	    var re = /^(01\d{9})|(09\d{8})$/;
	    return re.test(phone);
	},
	validateDeskphone: function(phone) {
		phone=phone.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi,'');
	    var re = /^(0\d{9})$/;
	    return re.test(phone);
	},
	isNumber: function(email){
		if(isNaN(email) == true)
			return false;
		else
			return true;
	},
	httpRequest: function(params, action, method, callback) {
		$.ajax({
			url : BASE_URL+action,
			type : method,
			data : params,
			success : function(data, textStatus, jqXHR) {
				
				callback(jQuery.parseJSON(data), textStatus);
			},
			error : function(jqXHR, textStatus, errorThrown) {

				callback(null, textStatus);
			}
		});
	},
	scrollTo: function(element){
		$('html, body').animate({
	        scrollTop: element.offset().top
	    }, 2000);
	},
	fullformatCurrency: function(price, lang){
		price =Number(price);
		if (price <= 1)
			return "Thỏa thuận";
		else if (price < 1000000){
			tmp = price/ 1000;
			var df = new DecimalFormat("#");
			return df.format(tmp)+" nghìn";
		}
		else if(price < 1000000000) {
			tmp = price/ 1000000;
			var df = new DecimalFormat("#.##");
			return df.format(tmp).replace(".", ",")+" triệu";
		}else {
			tmp = price / 1000000000;
			var df = new DecimalFormat("#.##");
			return df.format(tmp).replace(".", ",")+" tỉ";
		}
	},
	isObjEmpty: function(map) {
	   for(var key in map) {
	      if (map.hasOwnProperty(key)) {
	         return false;
	      }
	   }
	   return true;
	},
	validate: function(evt) {
		   var charCode = (evt.which) ? evt.which : event.keyCode
		    if (charCode > 31 && (charCode < 48 || charCode > 57))
		        return false;
		    return true;
	},
	numberWithCommas: function(x) {
		  var val = Number(x.replace(/\./g,""));
			return val.formatMoney(0);
	},
	getSearchParameters: function() {
      var prmstr = window.location.search.substr(1);
      return prmstr != null && prmstr != "" ? Util.transformToAssocArray(prmstr) : {};
	},
	transformToAssocArray: function ( prmstr ) {
	    var params = {};
	    var prmarr = prmstr.split("&");
	    for ( var i = 0; i < prmarr.length; i++) {
	        var tmparr = prmarr[i].split("=");
	        params[tmparr[0]] = tmparr[1];
	    }
	    return params;
	},

}


var Item = {
	getArea: function(item){
		if(item.total_area)
			return Number(item.total_area);
		else
			return Number(item.total_area_building);
	},
}

var Num2Text = {
	mangso: ['không','một','hai','ba','bốn','năm','sáu','bảy','tám','chín'],
	dochangchuc: function(so,daydu)
	{
	    var chuoi = "";
	    chuc = Math.floor(so/10);
	    donvi = so%10;
	    if (chuc>1) {
	        chuoi = " " + this.mangso[chuc] + " mươi";
	        if (donvi==1) {
	            chuoi += " mốt";
	        }
	    } else if (chuc==1) {
	        chuoi = " mười";
	        if (donvi==1) {
	            chuoi += " một";
	        }
	    } else if (daydu && donvi>0) {
	        chuoi = " lẻ";
	    }
	    if (donvi==5 && chuc>=1) {
	        chuoi += " lăm";
	    } else if (donvi>1||(donvi==1&&chuc==0)) {
	        chuoi += " " + this.mangso[ donvi ];
	    }
	    return chuoi;
	},
	docblock: function(so,daydu)
	{
	    var chuoi = "";
	    tram = Math.floor(so/100);
	    so = so%100;
	    if (daydu || tram>0) {
	        chuoi = " " + this.mangso[tram] + " trăm";
	        chuoi += this.dochangchuc(so,true);
	    } else {
	        chuoi = this.dochangchuc(so,false);
	    }
	    return chuoi;
	},
	dochangtrieu: function(so,daydu)
	{
	    var chuoi = "";
	    trieu = Math.floor(so/1000000);
	    so = so%1000000;
	    if (trieu>0) {
	        chuoi = this.docblock(trieu,daydu) + " triệu";
	        daydu = true;
	    }
	    nghin = Math.floor(so/1000);
	    so = so%1000;
	    if (nghin>0) {
	        chuoi += this.docblock(nghin,daydu) + " nghìn";
	        daydu = true;
	    }
	    if (so>0) {
	        chuoi += this.docblock(so,daydu);
	    }
	    return chuoi;
	},
	docso: function(so)
	{
	        if (so==0) return mangso[0];
	    var chuoi = "", hauto = "";
	    do {
	        ty = so%1000000000;
	        so = Math.floor(so/1000000000);
	        if (so>0) {
	            chuoi = this.dochangtrieu(ty,true) + hauto + chuoi;
	        } else {
	            chuoi = this.dochangtrieu(ty,false) + hauto + chuoi;
	        }
	        hauto = " tỷ";
	    } while (so>0);
	    return chuoi+ ' đồng';
	}  
}