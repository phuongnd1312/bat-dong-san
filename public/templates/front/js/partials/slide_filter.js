var changeLocation = true;
var sbl_form_item = $("#sbl_form_item");
var sbl_project_type = $("#sbl_project_type");
var sbl_select_area = $("#sbl_select_area");
var sbl_select_price = $("#sbl_select_price");
var sbl_num_beds = $("#sbl_num_beds");
var sbl_orientation_house = $("#sbl_orientation_house");
var sbl_project = $("#sbl_project");

Location.select_city = $("#sbl_select_city");
Location.select_district = $("#sbl_select_district");
Location.select_ward = $("#sbl_select_ward");
Location.select_street = $("#sbl_select_street");

Location.select_city.change(function() {
  Location.getDistrict(Location.select_city.val());
  changeLocation = true;
  getProjects();
});
Location.select_district.change(function() {
  Location.getWards(Location.select_district.val());
  Location.getStreets(Location.select_district.val());
  changeLocation = true;
  getProjects();
});

$("select.advance_combobox").select2({
  language: "vi"
});

function onSearchData(opt) {
  var formObject = $("#frm_filter_item").serializeObject();
  var tmp = {};
  for (var prop in formObject) {
    var key = prop.replace("[]", "");
    if (typeof formObject[prop] == "object") {
      tmp[key] = formObject[prop].join("-");
    } else if (formObject[prop] != "") {
      tmp[key] = formObject[prop];
    }
  }

  if (CMap.attr.map == null || opt != null) {
    if (opt == 0)
      url = $(".bound_ctr_map_list .nav_menu:not(.active)").attr("href");
    else url = $(".bound_ctr_map_list .nav_menu.active").attr("href");
    if (!url) url = BASE_URL + "/danh-sach";
    if (Object.keys(tmp).length > 0) url += "?" + $.param(tmp);
    //alert(window.location);
    window.location = url;
  } else {
    searchMap(tmp);
  }
}
function searchMap(filter) {
  var address = "";
  var location = null;
  var select_wards = $("#sbl_select_ward").val();

  if (select_wards != null && select_wards[0] != "") {
    var tmp = $("#sbl_select_ward").find(":selected").first();
    location = { lat: Number(tmp.attr("lat")), lng: Number(tmp.attr("lng")) };
    CMap.md.zoom = MapZoom.ward;
  } else if (Location.select_district.val() != "") {
    var tmp = Location.select_district.find(":selected");
    location = { lat: Number(tmp.attr("lat")), lng: Number(tmp.attr("lng")) };
    CMap.md.zoom = MapZoom.district;
  } else if (Location.select_city.val() != "") {
    address = Location.select_city.find(":selected").html() + ", Vietnamese";
    CMap.md.zoom = MapZoom.city;
  }

  var select_streets = $("#sbl_select_street").val();
  CMap.md.filter = filter;
  if (location != null) {
    CMap.attr.map.setCenter(location);
    if (CMap.attr.map.zoom != CMap.md.zoom) {
      CMap.attr.map.setZoom(CMap.md.zoom);
    }
    CMap.loadItems();
  } else if (address != "") {
    var objFilter = new Object();
    objFilter.address = address;
    CMap.geocoder(objFilter);
    changeLocation = false;
  } else CMap.loadItems();
  //khong search lan 2 khi chon filter address
  if (!Util.isEmpty(filter.address)) CMap.md.allow_search = false;
  else CMap.md.allow_search = true;
}

function onResetSearch() {
  select2_ward.val(null).trigger("change");
  select2_street.val(null).trigger("change");
  select2_common.val(null).trigger("change");
  //onSearchData();
}

function onAdvancedSearch() {
  // var group_advanced_search = $("#group_advanced_search");
  // var show_hide_advanced_seach = $("#show_hide_advanced_seach");

  // if (group_advanced_search.hasClass("active")) {
  //   group_advanced_search.removeClass("active");
  //   show_hide_advanced_seach.val(jlang.js_search_advanced);
  // } else {
  //   group_advanced_search.add("active");
  //   show_hide_advanced_seach.val(jlang.js_ignore_search_advanced);
  // }

  if (
    document
      .getElementById("group_advanced_search")
      .classList.contains("active")
  ) {
    document.getElementById("show_hide_advanced_seach").innerHTML =
      jlang.js_search_advanced;
    document.getElementById("group_advanced_search").classList.remove("active");
  } else {
    document.getElementById("group_advanced_search").classList.add("active");
    document.getElementById("show_hide_advanced_seach").innerHTML =
      jlang.js_ignore_search_advanced;
  }
}

function getProjectProperties() {
  var search_params =
    typeof properties_pro_filter != "undefined"
      ? properties_pro_filter
      : Util.getSearchParameters();
  var params = {
    id: sbl_project_type.val(),
    item_id: sbl_form_item.val(),
    city: Location.select_city.val(),
    district: Location.select_district.val(),
    search_params: search_params
  };

  API.getFilterProjectProperties(params, function(resp) {
    $("#adv_filter_more").html(resp.data);
    $("select.advance_combobox").select2({
      language: "vi"
    });
    $("#sel_belong_project").select2();
    Location.select_ward.select2({
      placeholder: "--Chọn phường/xã--"
    });
    Location.select_street.select2({
      placeholder: "--Chọn đường--"
    });
    if (resp.data !== "" && resp.data !== null) {
      $("#adv_filter").removeClass("hidden");
    } else {
      $("#adv_filter").addClass("hidden");
    }
  });
}

function getProjects() {
  var sel_belong_project = $("#sel_belong_project");
  if (sel_belong_project != null) {
    var params = {
      city: Location.select_city.val(),
      district: Location.select_district.val()
    };
    API.getProjects(params, function(resp) {
      if (resp.error_code == 0) {
        sel_belong_project.html(resp.data).trigger("change");
      }
    });
  }
}

sbl_project_type.change(function() {
  getProjectProperties();
});

$(document).ready(function() {
  console.log("vao day ko");
  $(".bound_ctr_map_list .nav_menu").click(function(e) {
    e.preventDefault();
    if ($(this).hasClass("active")) return;

    onSearchData(0);
  });

  getProjectProperties();
  $("#wrap_scroll_filter").mCustomScrollbar({ theme: "minimal-dark" });
});
