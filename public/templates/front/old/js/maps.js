var BoundMap = {
	ne_lat: 17.9,
	ne_lng: 108.7,
	sw_lat: 10.6,
	sw_lng: 105.7
}
var MyPosition = {
	lat: 10.8230989,
	lng: 106.6296638,
	zoom: 13,
}
var MapZoom = {
	city: 13,
	district: 14,
	ward:15,
}
/*-----------------------------Map--------------------------------*/

		
var CMap = {
	md: {
		items: [],
		markers: [],
		filter: {},
		zoom: 13,
		allow_search: true,
	},
	attr: {
		map: null,
		geocoder: null,
		mapView: null,
		popup: null,
		windowinfo: null,
		markerCluster: null,
	},
	init: function(){
		//set time load init
		setTimeout(function(){ onSearchData(); },100);

		// this.attr.mapView = $('#map_canvas');
		// this.attr.geocoder = new google.maps.Geocoder();
		// this.attr.map = new google.maps.Map(document.getElementById('map_canvas'), {
		// 	center: {lat: MyPosition.lat, lng: MyPosition.lng},
		// 	zoom: MyPosition.zoom,
		// 	mapTypeId 	: google.maps.MapTypeId.ROADMAP,
		// 	mapTypeControl: true,
		// 	panControl  : false,
		// 	streetViewControl:false,
		// 	draggable	: true,
			
		// });
		this.attr.windowinfo = new google.maps.InfoWindow({
			content: ''
		});
		CMap.attr.markerCluster = new MarkerClusterer(CMap.attr.map, CMap.md.markers.filter(function(obj) {return obj.map != null;}));
		//drap map
		google.maps.event.addListener(CMap.attr.map, 'drag', function() {
			//dragging = true;
		});
		google.maps.event.addListener(CMap.attr.map, 'dragend', function() {
			CMap.changeLocation();
		});

		//load map
		google.maps.event.addListenerOnce(CMap.attr.map, 'idle', function() {
			CMap.changeLocation();
		});

		//zoom map
		google.maps.event.addListener(CMap.attr.map, 'zoom_changed', function() {	
			CMap.changeLocation();
		});
		
		//click map
		google.maps.event.addListener(CMap.attr.map, 'click', function() {
			
		});
		var myMarker = new google.maps.Marker({
		  map: CMap.attr.map,
		  animation: google.maps.Animation.DROP,
		});
		CMap.addYourLocationButton(CMap.attr.map, myMarker);
		$('#map_canvas').on('mouseenter', '.marker', function(){
			var itemid = $(this).attr('data-marker-id');
			CMap.renderPopup(itemid, true);
		});
		$('#map_canvas').on('mouseleave','.marker', function () {
			if (CMap.attr.popup) {CMap.attr.popup.setMap(null);};
		});

	},
	changeLocation: function(){
		CMap.changeMyLocation();
		var bounds = CMap.attr.map.getBounds();
		if (!bounds) return;

		BoundMap.ne_lat = bounds.getNorthEast().lat();
		BoundMap.ne_lng = bounds.getNorthEast().lng();
		BoundMap.sw_lat = bounds.getSouthWest().lat();
		BoundMap.sw_lng = bounds.getSouthWest().lng();
		//Map.changeLocationSearch();
		CMap.loadItems();
		
	},
	addYourLocationButton:function (map, marker) 
	{
    var controlDiv = document.createElement('div');

    var firstChild = document.createElement('button');
    firstChild.style.backgroundColor = '#fff';
    firstChild.style.border = 'none';
    firstChild.style.outline = 'none';
    firstChild.style.width = '28px';
    firstChild.style.height = '28px';
    firstChild.style.borderRadius = '2px';
    firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
    firstChild.style.cursor = 'pointer';
    firstChild.style.marginRight = '10px';
    firstChild.style.padding = '0';
    firstChild.title = 'Your Location';
    controlDiv.appendChild(firstChild);

    var secondChild = document.createElement('div');
    secondChild.style.margin = '5px';
    secondChild.style.width = '18px';
    secondChild.style.height = '18px';
    secondChild.style.backgroundImage = 'url(https://maps.gstatic.com/tactile/mylocation/mylocation-sprite-2x.png)';
    secondChild.style.backgroundSize = '180px 18px';
    secondChild.style.backgroundPosition = '0 0';
    secondChild.style.backgroundRepeat = 'no-repeat';
    firstChild.appendChild(secondChild);

    google.maps.event.addListener(map, 'center_changed', function () {
        secondChild.style['background-position'] = '0 0';
    });

    firstChild.addEventListener('click', function () {
        var imgX = '0',
            animationInterval = setInterval(function () {
                imgX = imgX === '-18' ? '0' : '-18';
                secondChild.style['background-position'] = imgX+'px 0';
            }, 500);

        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
            	
                var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                map.setCenter(latlng);
                clearInterval(animationInterval);
                secondChild.style['background-position'] = '-144px 0';
            });
        } else {
            clearInterval(animationInterval);
            secondChild.style['background-position'] = '0 0';
        }
    });

    controlDiv.index = 1;
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(controlDiv);
},
	changeMyLocation: function(){
		var center = CMap.attr.map.center;
		MyPosition = {lat: center.lat(), lng: center.lng(), zoom: CMap.attr.map.zoom};
	},
	loadItems: function(){
		if(this.md.allow_search == false)
			return;
		
		var params = {};
		params.location = BoundMap;
		params.filter = this.md.filter;
		API.getLocationItems(params, function(resp){
			if(resp.error_code == 0){
				CMap.clearMarkers();
				var markerids = [];
				var markers = CMap.md.markers;
				var num_marker = markers.length;
				for(var j = 0; j < num_marker; j++) 
					markerids.push(markers[j]._id);
				var data = resp.data;
				if(data.length>1){
					var item=data[0];
					var arr=CMap.groupBy(data, function(item)
					{
					  return [item.lat, item.lng];
					});
					var data=[];
					for(i = 0; i < arr.length; i++){
						if(arr[i].length>1){
							var point=0;
							for(j = 0; j < arr[i].length; j++){
								arr[i][j].lat=parseFloat(arr[i][j].lat)+parseFloat(point);
								arr[i][j].lng=parseFloat(arr[i][j].lng)+parseFloat(point);
								data.push(arr[i][j]);
								point+=0.0001;
							}
						}
						else
							data.push(arr[i][0]);
					}
				}
				length = data.length;
				for(i = 0; i < length; i++){

					data[i].photos = data[i].photos.split(";");
					data[i].lat = Number(data[i].lat);
					data[i].lng = Number(data[i].lng);
					data[i].price_total = Number(data[i].price_total);
					data[i].total_area = Number(data[i].total_area);
					data[i].total_area_building = Number(data[i].total_area_building);
		
					// check marker is exist ?
					if ($.inArray(data[i].id, markerids) >= 0) { continue; };
					CMap.renderMarker(data[i]);
					markerids.push(data[i].id);
				}
				CMap.md.items = data;
			}else{
				CMap.attr.items = [];
				CMap.attr.markerCluster.clearMarkers();
			}
			CMap.showMarkers();
			//console.log(resp);

		});
	},
	renderMarker: function(item){
		/*
		var marker = new CustomMarker(
            new google.maps.LatLng(item.lat, item.lng)
            , Map.attr.map, 
            { 
            	markerId: item.id, className: 'map_label', pr: '', 
	            infoTemplate: '',
	            mkType: 'R', viewed: (item.viewed === true ? true : false), 
	            saved: (item.saved === true ? true : false)
	        }
            );
        marker._id = item.id;*/
  		var position =  {lat:  item.lat, lng: item.lng};

		var marker = new MarkerWithLabel({
			position : position,
			labelID : item.id,
			labelContent : Util.fullformatCurrency(item.price_total),
			labelAnchor : new google.maps.Point(20, 25),
			labelClass : "marker_label",
			labelInBackground: false,
			icon : ' ',
		});

		marker._id = item.id;
        CMap.md.markers.push(marker);
        google.maps.event.addListener(marker, 'mouseover', function() {
		    this.set('labelClass', 'marker_label hover');
			CMap.renderPopup(marker._id, true);
		});
		google.maps.event.addListener(marker, 'mouseout', function() {
		    this.set('labelClass', 'marker_label hover');
		    if (CMap.attr.popup) {CMap.attr.popup.setMap(null);};
		});

		google.maps.event.addListener(marker, 'click', function() {
			this.set('labelClass', 'marker_label selected');
			var item = CMap.md.items.filter(function(obj) {return obj.id == marker._id;});
			item = item.length > 0 ? item[0] : null;
			//console.log(item);
			if(item != null){
				CMap.attr.windowinfo.setContent(CMap.renderInfoWindow(item));
				CMap.attr.windowinfo.open(CMap.attr.map, marker);
			}
		});
	},
	renderInfoWindow: function(item) {
		var pr = Util.fullformatCurrency(item.price_total, LANG);
		if(item.type_item==1 || item.type_item==2)
			var month='/tháng';
		else
			month='';
		var LANG = '<?php echo App::ins()->getLangCode(); ?>';
		var html = "<div class='map_infowindow'>";
		html+="<div class='title'><a href='"+item.link+"'>" + item.title + "</a></div>";
		html+="<div class='img'><a href='"+item.link+"'><img src='"+BASE_URL+"/public"+item.photos[0]+"'/></a></div>";
		html+="<div class='content'><div class='price'>Giá: <span>" + pr +month+ "</span></div>";
		html+="<div class='area'>Diện tích: <span>" + Item.getArea(item, true)+" m²</span></div>";
		html+="<div class='address'><span>" + item.full_address+"</span></div>";
		html+="</div><div class='clear'></div>";
		html+="<div class='control'></div></div>";
		return html;
	},
	renderInfoPopup: function(item) {
		if(item.type_item==1 || item.type_item==2)
			var month='/tháng';
		else
			month='';
		var pr = Util.fullformatCurrency(item.price_total);
		var html = "<div class='map_popup'>";
		html+="<div class='img'><img src='"+BASE_URL+"/public"+item.photos[0]+"'/></div>";
		html+="<div class='price'>" + pr +month+ "</div>";
		html+="<div class='area'>Diện tích: <span>" + Item.getArea(item) + " m²</span></div></div>";
		html+="<div class='address'>" + item.full_address + "</div>";

		return html;
	},
	renderPopup: function(itemid, pad) {
		var item = CMap.md.items.filter(function(obj) {return obj.id == itemid;});
		item = item.length > 0 ? item[0] : null;
		pad = pad || false;
		if (CMap.attr.popup) {
			CMap.attr.popup.setMap(null);
		}
		
		var p = new CustomMarker(
            new google.maps.LatLng(item.lat, item.lng)
            , CMap.attr.map, 
            { 
            	markerId: item.id,
	            infoTemplate: CMap.renderInfoPopup(item),
	            isp : true,
	            pad: pad
	        }
            );
        p._id = item.id;
        CMap.attr.popup = p;
	},
	setMapOnAll: function (map) {
		// Sets the map on all markers in the array.
		var showIds = [];
		var num = CMap.md.items.length;
		for(var j = 0; j < num; j++) 
			showIds.push(CMap.md.items[j].id);

		num = CMap.md.markers.length;
		for (var i = 0; i < num; i++) {
			if (showIds.indexOf(CMap.md.markers[i]._id) >= 0) {
				CMap.md.markers[i].setMap(map);
			} else {
				CMap.md.markers[i].setMap(null);
			}
		}
		CMap.attr.markerCluster.addMarkers(CMap.md.markers.filter(function(obj) {return obj.map != null;}));
	},
	clearMarkers: function () {
		// Removes the markers from the map, but keeps them in the array.
		CMap.setMapOnAll(null);
		CMap.attr.markerCluster.clearMarkers();
	},
	// Shows any markers currently in the array.
	showMarkers: function () {
		CMap.setMapOnAll(CMap.attr.map);
	},
	// Deletes all markers in the array by removing references to them.
	deleteMarkers: function () {
		CMap.md.markers = [];
		CMap.md.items = [];
		CMap.md.clearMarkers();
	},
	//
	geocoder: function(objFilter){
		CMap.attr.geocoder.geocode(objFilter, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				CMap.attr.map.setCenter(results[0].geometry.location);
			} else {
				console.log('Geocode was not successful for the following reason: ' + status);
			}
		});
	},
	onSearch: function(){
		var address = '';
		if(Location.select_city.val() != ''){
			address=Location.select_city.find(':selected').html()+', Vietnamese';
			//Map.attr.map.setZoom(MapZoom.city);
		}
		if(Location.select_district.val() != ''){
			address=$.trim(Location.select_district.find(':selected').html())+', '+address;
			//Map.attr.map.setZoom(MapZoom.district);
		}
		var select_wards = $('#sbl_select_ward').val();
		if(select_wards != null){
			address=$.trim(select_wards.find(':selected').first().html())+', '+address;
			//Map.attr.map.setZoom(MapZoom.wards);
		}
		var filter = {};
		filter.type = type;
		filter.area = area;
		filter.price = price;
		CMap.md.filter = filter;
		if(address != '' && changeLocation){
			var objFilter = new Object();
			objFilter.address = address;
			CMap.geocoder(objFilter);
			changeLocation = false;
		}
		else
			CMap.loadItems();
	},
	groupBy: function ( array , f )
	{
	  var groups = {};
	  array.forEach( function( o )
	  {
	    var group = JSON.stringify( f(o) );
	    groups[group] = groups[group] || [];
	    groups[group].push( o );  
	  });
	  return Object.keys(groups).map( function( group )
	  {
	    return groups[group]; 
	  })
	}
}