var field_latitude = $('#field_latitude');
var field_longitude = $('#field_longitude');
var field_full_address = $('#field_full_address');
var field_num_house = $('#field_num_house');
var leb_price_total=$('#leb_price_total');

var position = {lat: ($.trim(field_latitude.val()) == '') ? 10.771473 : Number(field_latitude.val()),
				lng: ($.trim(field_longitude.val()) == '') ? 106.65302399999996 : Number(field_longitude.val())};

var geocoder = new google.maps.Geocoder();
var map = new google.maps.Map(document.getElementById('map_canvas'), {

					center: position,
					zoom: 15
				});
				console.log("​item_form -> map", map)
var marker = new google.maps.Marker({
    position: position,
    map: map,
    draggable: true,
	animation: google.maps.Animation.DROP
});
google.maps.event.addListener(marker, 'dragend', function() {
	geocodePosition(marker.getPosition());
});
var infowindow = new google.maps.InfoWindow({
	content: field_full_address.val()
});

function geocodePosition(pos) {
  	geocoder.geocode({
		latLng: pos
  	}, function(responses) {
	    if (responses && responses.length > 0) {
	    	infowindow.setContent(responses[0].formatted_address);
	    	infowindow.open(map, marker);
			map.setCenter(marker.getPosition());
	    } else {
	      	console.log('Cannot determine address at this location.');
	    }
 	});
}

function renderUploadItem(error_code, param){
	var html = '<div class="bound_item_file photo"><div class="bound_middle">';
	if(error_code == 0)
		html+='<img class="photo_item" src="'+BASE_URL+'/public'+param+'" url="'+param+'" />';
	else
		html+='<span class="error_msg">'+param+'</span>';
	html+='</div><a href="javascript:void(0)" onclick="removePhoto(this, \''+param+'\')" class="remove"></a></div>';
	html+='</div>';
	$('#control_upload').before($(html));
}

function removePhoto(obj, path){
	if(confirm(jlang.js_do_want_remove_item)){
		$(obj).parent().remove();
	}
}

function renderAddress(){
	var address = [];
	if($.trim(field_num_house.val()) != '')
		address.push(field_num_house.val());

	if(Location.select_street.val() != '')
		address.push('đường '+Location.select_street.find('option:selected').html());

	if(Location.select_ward.val() != '')
		address.push(Location.select_ward.find('option:selected').html());
	if(Location.select_district.val() != '')
		address.push(Location.select_district.find('option:selected').html());


	if(Location.select_city.val() != '')
		address.push(Location.select_city.find('option:selected').html());

	field_full_address.val(address.join(', '));
	geocoderAddress();
}

function geocoderAddress(){
	var objFilter = new Object();
	objFilter.address = field_full_address.val();
	if($.trim(objFilter.address) == '')
		return;

	geocoder.geocode(objFilter, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			infowindow.setContent(results[0].formatted_address);
			infowindow.open(map, marker);
			map.setCenter(results[0].geometry.location);
			marker.setPosition(results[0].geometry.location);
		} else {
			console.log('Geocode was not successful for the following reason: ' + status);
		}
	});
}

function setlatlng(){
	field_latitude.val(marker.getPosition().lat());
	field_longitude.val(marker.getPosition().lng());
}

function setPhoto(){
	var tmp = [];
	var photo_default = null;
	$('img.photo_item').each(function(index){
		if($(this).parent().parent().hasClass('default'))
			photo_default = $(this).attr('url');
		else
			tmp.push($(this).attr('url'));
	});
	if(photo_default != null)
		tmp.unshift(photo_default);

	$('#field_photos').val(tmp.join(';'));
}

function onSubmitItem(opt){
	setlatlng();
	setPhoto();

	$('.required .form-control').each(function(index){
		var val = $(this).val();
		if($.trim(val) == '')
			$(this).addClass('error');
		else
			$(this).removeClass('error');
	});
	if($('#wrap_post_item .form-control.error').length == 0){
		if(opt == 1)
			$('#field_draft').attr('checked', true);
		else if(opt == 2)
			$('#field_approval').attr('checked', true);

		if($('#field_input_captcha').length){
			API.checkCaptcha({captcha: $('#field_input_captcha').val()}, function(resp){
				if(resp.error_code == 0){
					$('#captcha_message').html('');
					$('#field_token').val(resp.data);
					$('#frm_post_item').submit();
				}
				else
				{
					onRefreshCaptcha();
					$('#captcha_message').html(resp.message);
				}
			})
		}
		else
			$('#frm_post_item').submit();
	}else
		Util.scrollTo($('#wrap_post_item .form-control.error').first());
	return false;
}

function onRefreshCaptcha(){
	document.getElementById('siimage').src = BASE_URL+'/front/ajax/show_captcha?sid=' + Math.random();
}
function getProjectProperties(){
	var params = {id: $('#field_project_type').val(), item_id: $('#field_item_id').val(),
					city: Location.select_city.val(), district: Location.select_district.val()};
	API.getProjectProperties(params, function(resp){
		$('#project_properties').html(resp.data);
		$('input.type_number').ForceNumericOnly();
	});
}

function getTypeItem(type){
	API.getTypeItem({type:type}, function(resp){
		if(resp.error_code == 0){
			$('#field_project_type').html(resp.data);
		}
	});
}

function getProjects(){
	var sel_belong_project = $('#sel_belong_project');
	if(sel_belong_project != null){
		var params = {city: Location.select_city.val(), district: Location.select_district.val()};
		API.getProjects(params, function(resp){
			if(resp.error_code == 0){
				sel_belong_project.html(resp.data).trigger("change");
			}

		});
	}

}



var CropImage = {
	md: {
		src_img: null,
		index: 0,
	},
	edit: function(item_id, index, url){
		var d = new Date();
		this.md.src_img = url;
		this.md.index = index;
		$('#iframe_crop_photo').attr('src', BASE_URL+'/front/items/edit_image/item_id?url='+url+"&d="+d.getTime());
		$('#popup_crop_image').modal('show');
	},
	close: function(){
		var d = new Date();
		var photo_item = $("img#photo_item_"+this.md.index);
		photo_item.attr("src", photo_item.attr('src')+"?"+d.getTime());
	}
}
/*----------Init--------------*/

Location.select_city = $('#select_city');
Location.select_district = $('#select_district');
Location.select_ward = $('#select_ward');
Location.select_street  = $('#select_street');

Location.select_city.change(function(){
	Location.getDistrict(Location.select_city.val());
    Location.select_district.html(Location.option_district).trigger("change");
    Location.select_ward.html(Location.option_ward).trigger("change");
    Location.select_street.html(Location.option_street).trigger("change");
    field_num_house.val('');
	field_full_address.html(renderAddress());

	getProjects();
});
Location.select_district.change(function(){
	Location.getWards(Location.select_district.val());
	Location.getStreets(Location.select_district.val());
    Location.select_ward.html(Location.option_ward).trigger("change");
    Location.select_street.html(Location.option_street).trigger("change");

    field_num_house.val('');
 	field_full_address.html(renderAddress());
 	getProjects();
});
Location.select_ward.change(function(){
	field_num_house.val('');
	field_full_address.html(renderAddress());
});
Location.select_street.change(function(){
	field_full_address.html(renderAddress());
	field_num_house.val('');
});
field_num_house.change(function(){
	field_full_address.html(renderAddress());
});

function getTotalArea(){
	var who_main_area = $('#who_main_area');
	var total_area = 1;
	if(who_main_area.length){
		total_area = $('#field_'+who_main_area.val()).val();
	}
	return total_area;
}

var thousands_sep = '.';
var field_price_total = $('#field_price_total');
var field_price_total_string=$('#field_price_total_string');
var field_price_square = $('#field_price_square');
field_price_total.blur(function(){

	var total_area = getTotalArea();
	if(!total_area || total_area==0)
		total_area=1;
	else
		total_area = Number(total_area.replace(/\./g,""));

	var val = Number($(this).val().replace(/\./g,""));
	var tmp = Math.round(val/total_area/1000)*1000;
	if(tmp!=0)
		field_price_square.val(tmp.formatMoney(0));
	else
		field_price_square.val(0);
});
field_price_total.keyup(function(){
	var total_area = getTotalArea();
	if(!total_area)
		total_area=1;
	else
		total_area =Number(total_area.replace(/\./g,""));
	var val = Number($(this).val().replace(/\./g,""));
	var tmp = Math.round(val/total_area/1000)*1000;
	if(tmp!=0)
		field_price_square.val(tmp.formatMoney(0));
	else
		field_price_square.val(0);
	var val = $(this).val().replace(/\./g,"");
	field_price_total_string.html(Num2Text.docso(val));
	var tmp = Number(val).formatMoney(0);
	$(this).val(tmp);
});
/*field_price_square.blur(function(){
	var total_area = $('input.cal_equivalent_base').val();
	var val = Number($(this).val().replace(/\./g,""));
	field_price_total.val(Math.round(val*total_area).formatMoney(0));
});*/
field_price_square.keyup(function(){
	var val = $(this).val().replace(/\./g,"");
	var tmp = Number(val).formatMoney(0);
	$(this).val(tmp);
});


$('#field_project_type').change(function(){
	getProjectProperties();
});

$('#control_upload').click(function(){
	$('#field_file_upload').trigger("click");
});

$('#field_type_item').change(function(){
	$('#field_time_to_expire').val($(this).find('option:selected').attr('placeholder'));
	getTypeItem($(this).val());
});

$('#field_title').keyup(function(e){
	var length = $(this).val().length;
	$('#num_of_characters').html(length+"/99");
});
$('#num_of_characters').html($('#field_title').val().length+"/99");

$('#field_file_upload').change(function() {
    var files = $(this).prop('files');

    var length = files.length;
    for (var i = 0; i < length; i++) {
    	 var form_data = new FormData();
    	 form_data.append('file', files[i]);
    	$.ajax({
			url: $('#field_file_upload').attr('url'),
			dataType: 'text',
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			success: function(resp){
				resp = jQuery.parseJSON(resp);
				renderUploadItem(resp.error_code, resp.data);
			}
	     });
    };


    $('#field_file_upload').val('');
});
$('#picker_date_started').datepicker();
$('#picker_date_started').datepicker( "option", "dateFormat", 'dd/mm/yy').datepicker('setDate', 'today');

$(document).on("click", "span.remove_photo_item", function() {
 	removePhoto($(this).parent().index());
});
$(document).on("click", "div.bound_item_file.photo", function() {
	if($(this).find('.photo_item').length > 0){
		$('div.bound_item_file.photo').removeClass('default');
 		$(this).addClass('default');
	}

});
// $('select.advance_custom').select2();

//*Auto call*/
getProjectProperties();

