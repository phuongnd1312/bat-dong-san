var Location = {
	select_city: null,
	select_district: null,
	select_ward: null,
	select_street: null,
	full_address: '',
	option_district: '<option value="">--Quận/Huyện--</option>',
	option_ward: '<option value="">--Phường/Xã--</option>',
	option_street: '<option value="">--Đường/Phố--</option>',

	getDistrict: function (id){
		console.log("​id", id);
		API.getDistrictByCity(id, function(resp){
			var data = resp.data;
			var length = data.length;
			var html = Location.option_district;
			for(i = 0; i < length; i++){
				html+='<option value="'+data[i].id+'" lat="'+data[i].lat+'" lng="'+data[i].lng+'">'+data[i].full_name+'</option>';
			}
			console.log("​html", html);
			Location.select_district.html(html).trigger("change");
			
		});
	},
	getWards: function(id){
		API.getWardByDistrict(id, function(resp){
			if(resp.error_code == 0){
				var data = resp.data;
				var length = data.length;
				var html = Location.option_ward;
				for(i = 0; i < length; i++){
					html+='<option value="'+data[i].id+'" lat="'+data[i].lat+'" lng="'+data[i].lng+'">'+data[i].full_name+'</option>';
				}
				Location.select_ward.html(html).trigger("change");
			}
		});
	},
	getStreets: function(id){
		API.getStreetByDistrict(id, function(resp){
			if(resp.error_code == 0){
				var data = resp.data;
				var length = data.length;
				var html = Location.option_street;
				for(i = 0; i < length; i++){
					html+='<option value="'+data[i].id+'">'+data[i].name+'</option>';
				}
				Location.select_street.html(html).trigger("change");
			}
		});
	}
}

