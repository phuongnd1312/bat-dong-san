/*JAVASCRIPT*/
var Util = {
	
	isEmpty: function(str){
	    if($.trim(str) == "")
	        return true;
	    else 
	        return false;
	},
	validateEmail: function(email) {
	    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	    return re.test(email);
	},
	httpRequest: function(params, action, method, callback) {
		$.ajax({
			url : BASE_URL+action,
			type : method,
			data : params,
			success : function(data, textStatus, jqXHR) {
				callback(jQuery.parseJSON(data), textStatus);
			},
			error : function(jqXHR, textStatus, errorThrown) {
				//console.log('callNetwork failed ' + errorThrown);
				callback(null, textStatus);
			}
		});
	}
}

/*API*/
var API = {
	removePhotoItem: function(params, callback){
		Util.httpRequest(params, '/admin/item/remove_photo', 'post', function(data, textStatus){
			callback(data);
		});
	},
	getDistrictByCity: function(id, callback){
		Util.httpRequest('', '/admin/ajax/get_district_by_city/'+id, 'get', function(data, textStatus){
			callback(data);
		});
	},
	getWardByDistrict: function(id, callback){
		Util.httpRequest('', '/admin/ajax/get_ward_by_district/'+id, 'get', function(data, textStatus){
			callback(data);
		});
	},
	
}
