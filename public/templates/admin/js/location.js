var Loation = {
  select_city: null,
  select_district: null,
  select_ward: null,
  full_address: "",
  option_district: '<option value="">--Chọn quận/huyện--</option>',
  option_ward: '<option value="">--Chọn phường/xã--</option>'
};

function getDistrict(id) {
  API.getDistrictByCity(id, function(resp) {
    var data = resp.data;
    var length = data.length;
    var html = Loation.option_district;
    for (i = 0; i < length; i++) {
      html +=
        '<option value="' + data[i].id + '">' + data[i].name + "</option>";
    }
    Loation.select_district.html(html);
  });
}
function getWards(id) {
  API.getWardByDistrict(id, function(resp) {
    var data = resp.data;
    var length = data.length;
    var html = Loation.option_ward;
    for (i = 0; i < length; i++) {
      html +=
        '<option value="' + data[i].id + '">' + data[i].name + "</option>";
    }
    Loation.select_ward.html(html);
  });
}
function renderAddress() {
  var address = [];
  //if($.trim(field_add_street.val()) != '')
  //	address.push(field_add_street.val());

  if (Loation.select_ward.val() != "")
    address.push(select_ward.find("option:selected").html());
  if (Loation.select_district.val() != "")
    address.push(select_district.find("option:selected").html());
  if (Loation.select_city.val() != "") {
    address.push(select_city.find("option:selected").html());
    geocoderAddress();
  }
  Loation.full_address = address.join(", ");
}
