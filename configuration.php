<?php 
class Configuration{

	static $investment_certificate = array('lbl_all','lbl_no_info', 'lbl_during_discuss_investment', 'lbl_agreement_location',
		'lbl_have_investment_certificate', 'lbl_other');
	
	static $implementation_planning = array('lbl_all','lbl_no_info', 'lbl_design_planning', 'lbl_show_planning', 'lbl_approve_planning',
		'lbl_have_planning_certificate', 'lbl_other');

	static $clearance = array('lbl_all','lbl_no_info', 'lbl_during_compensation', 'lbl_done_compensation', 'lbl_done_red_book', 'lbl_other');

	static $land_fees = array('lbl_all','lbl_no_info', 'lbl_during_estimate', 'lbl_notify_pay_fee', 'lbl_done_fee', 'lbl_other');

	static $done_red_book = array('lbl_all','lbl_no_info', 'lbl_no_reb_book', 'lbl_have_red_book', 'lbl_other');

	static $under_construction = array('lbl_all','lbl_no_info', 'lbl_not_yet_start', 'lbl_building_infrastructure', 'lbl_building_foundation',
	'lbl_raw_construction', 'lbl_done_building', 'lbl_other');
	
	static $on_marketing = array('lbl_all','lbl_no_info', 'lbl_during_contributions', 'lbl_selling', 'lbl_other');
	static $advance_filter = array('red_book', 'pink_book','orientation_house','belong_project', 'investment_certificate', 'implementation_planning','clearance', 'land_fees', 'done_red_book', 'under_construction', 'on_marketing');

	static $advance_filter1 = array('total_area', 'building_density', 'num_floor', 'total_area_building', 'num_bedrooms', 'bath_room');
	
}

class UserRole{
	public static $REGISTER = 1;
	public static $MANAGER = 2;
	public static $ActPermit = array('',
			array('items/manage', 'items/form', 'items/update_item_status', 
					'items/update_post_status','users/edit','users/profile', 'users/change_password'),
			
			array('items/manage', 'items/form', 'items/manage_bds', 'items/form_bds', 'items/form_bds', 'items/form_bds_test', 'items/update_item_status', 
				'items/update_post_status', 'items/remove','users/edit','users/profile', 'users/change_password')
			
		); 
	static $Detail = array('', 'role_register', 'role_manager');
}

class Document{
	private $title = 'BDSVN';
	private $description='Bất động sản Việt Nam';

	private static $intance = null;
	public function Document(){
	}
	public static function ins(){
		if(self::$intance == null)
			self::$intance = new Document();
		return self::$intance;
	}
	public function setTitle($title){
		$this->title = 'BDSVN :: '.$title;
	}
	public function getTitle(){
		return $this->title;
	}

	public function setDesc($description){
		$this->description = $description;
	}
	public function getDesc(){
		return $this->description;
	}
}

class App{
	private $lang = null;
	private $lang_name = null;
	public $dec_point = ',';
	public $thousands_sep = '.';
	private static $intance = null;
	public function App(){
		$this->init();
	}
	public static function ins(){
		if(self::$intance == null)
			self::$intance = new App();
		return self::$intance;
	}
	private function init(){

		if(isset($_COOKIE["lang"]) && $this->in_langs($_COOKIE["lang"], Constant::$LANGUAGES))
			$this->lang = $_COOKIE["lang"];
		else 
			$this->lang = Constant::$DEFAULT_LANG;

		$this->lang_name = Constant::$LANGUAGES[$this->lang];
	}
	private function in_langs($lang){
		foreach (Constant::$LANGUAGES as $key => $value) {
			if($lang == $key)
				return true;
		}
		return false;
	}

	public function setLang($code){
		if(isset(Constant::$LANGUAGES[$code])){
			$this->lang = $code;
			$this->lang_name = Constant::$LANGUAGES[$this->lang];

			setcookie("lang", $code, 0, '/');
		}
	}

	public function getLangTrans(){
		return $this->ci->lang->line($this->lang);
	}

	public function getLangName(){
		return $this->lang_name;
	}

	public function getLangCode(){
		return $this->lang;
	}

}

class ItemStatus{
	static $New = 1;
	static $Draft = 2;
	static $Approved = 3;
	static $Disable = 4;
	static $Detail = array('', 'is_checking', 'is_draft', 'is_approved', 'is_disable');
}

class PostStatus{
	static $Available = 1;
	static $Leased = 2;
	static $Sold = 3;
	static $Detail = array('','ps_available', 'ps_leased', 'ps_sold');
}

class Gender{
	static $Male = 2;
	static $Female = 1;
	static $Detail = array('','gender_female', 'gender_male');
}

class Link{
	static $list = '/danh-sach';
	static $map = '/ban-do';  		

	static function city($cityName, $cityId){
		return $cityId ;
		return 'bds-'.Util::stringURLSafe($cityName).'-cdct'.$cityId;
	}
	static function district($districtName, $districtId, $cityId){
		return 'bds-'.Util::stringURLSafe($districtName).'-cddt'.$districtId.'-'.$cityId;
	}
	static function type($typeName, $typeId){
		return 'bds-'.Util::stringURLSafe($typeName).'-cdtp'.$typeId;
	}
}