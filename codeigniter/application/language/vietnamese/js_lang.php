<?php
$lang['js_username_not_include_spec_char'] = 'Username không chứa ký tự đặc biệt.';
$lang['js_email_not_correct'] = 'Email không đúng định dạng';
$lang['js_phone_not_correct'] = 'Số điện thoại không đúng định dạng';
$lang['js_password_not_match'] = 'Mật khẩu không trùng khớp';
$lang['js_password_validate_wrong'] = 'Mật khẩu phải có ít nhất 6 ký tự.';
$lang['js_do_want_remove_item'] = "Bạn có muốn xóa thành phần này không?";
$lang['js_are_you_sure'] = "Bạn chắc chắn muốn thực hiện tác vụ này?";
$lang['js_search_advanced'] = "Tìm nâng cao";
$lang['js_ignore_search_advanced'] = "Bỏ tìm nâng cao";