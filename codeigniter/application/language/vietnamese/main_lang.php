<?php
$lang['vi'] = "Tiếng Việt";
$lang['en'] = 'Tiếng Anh';
$lang['warning_permission'] = 'Bạn không có quyền truy cập trang này';
$lang['nav_previous'] = 'Lùi';
$lang['nav_next'] = 'Tiếp';
$lang['lbl_cancel'] = 'Hủy';
/*------------------- Register account -----------------------*/
$lang['lbl_sign_up'] = 'Đăng ký';
$lang['lbl_sign_in'] = 'Đăng nhập';
$lang['lbl_sign_out'] = 'Đăng xuất';
$lang['msg_sign_up_success'] = 'Đăng ký thành công';
$lang['msg_sign_up_unsuccess'] = 'Đăng ký thất bại';
$lang['msg_mobile_exists'] = 'Số điện thoại này đã tồn tại trong hệ thống';
$lang['msg_email_exists'] = 'Email này đã tồn tại trong hệ thống';
$lang['msg_email_activation'] = 'Email kích hoạt đã được gửi tới bạn. Vui lòng bấm vào liên kết trong email của bạn để kích hoạt tài khoản của bạn';
$lang['msg_sms_activation'] = 'Mã kích hoạt đã được gửi tới số điện thoại của bạn. Vui lòng bấm vào liên kết trong sms của bạn để kích hoạt tài khoản của bạn';
$lang['msg_cant_send_email'] = 'Không thể gửi mail. Vui lòng liên hệ quản trị website.';
$lang['msg_cant_send_sms'] = 'Hệ thống không thể gửi sms. Vui lòng liên hệ quản trị website.';
$lang['msg_code_activation_invalid'] = 'Mã kích hoạt của bạn không hợp lệ';
$lang['msg_your_account_activated'] = 'Tài khoản của bạn đã được kích hoạt';
$lang['msg_your_account_not_activated'] = 'Tài khoản của bạn chưa được kích hoạt';
$lang['msg_your_loign_failed'] = 'Đăng nhập không thành công';
$lang['txt_forgot_password'] = 'Quên mật khẩu';
$lang['lbl_forgot_password'] = 'Quên mật khẩu';
$lang['lbl_submit_forgot'] = 'Gửi';
$lang['lbl_email_active'] = 'Email kích hoạt tài khoản';
$lang['lbl_email_reset_pass'] = 'Đổi mật khẩu';
$lang['msg_email_reset_pass'] = 'Email đổi mật khẩu đã được gửi tới bạn. Vui lòng bấm vào liên kết trong email để đổi lại mật khẩu của bạn.';
$lang['msg_email_not_existed'] = 'Email này không tồn tại trong hệ thống. Bạn vui lòng thử lại với email khác.';
$lang['msg_email_phone_not_existed'] = 'Email hoặc số điện thoại này không tồn tại trong hệ thống. Bạn vui lòng thử lại với email khác.';
$lang['msg_code_reset_pass_not_right'] = 'Mã dùng để đổi mật khẩu không chính xác.';
$lang['msg_password_reset_success'] = 'Mật khẩu của bạn đã được thay đổi.';
$lang['msg_warning_miss_info'] ='Vui lòng điền đầy đủ thông tin yêu cầu.';


$lang['lbl_update_profile'] = 'Cập nhật hồ sơ';
$lang['lbl_profile'] = 'Hồ sơ';
$lang['lbl_change_password'] = 'Đổi mật khẩu';

$lang['lbl_email'] = 'Tên đăng nhập';
$lang['lbl_password'] = 'Mật khẩu';
$lang['txt_dont_have_account'] = "Chưa có tài khoản";
$lang['txt_enter_email_or_phone'] = 'Nhập email hoặc số điện thoại';
$lang['txt_enter_email_or_phone_user'] = 'Nhập email hoặc số điện thoại';
$lang['txt_enter_email'] = 'Nhập email của bạn';
$lang['txt_enter_password'] = 'Nhập mật khẩu';
$lang['lbl_old_password'] = 'Mật khẩu cũ';
$lang['lbl_repassword'] = 'Mật khẫu gõ lại';
$lang['txt_enter_old_password'] = 'Nhập mật khẩu cũ';
$lang['txt_enter_repassword'] = 'Nhập lại mật khẩu';
$lang['txt_enter_password_new'] = 'Nhập mật khẩu mới';
$lang['txt_enter_repassword_new'] = 'Gõ lại mật khẩu mới';
$lang['lbl_update'] = 'Cập nhật';
$lang['msg_password_not_match'] = 'Mật khẫu cũ của bạn không trùng khớp';
$lang['warn_new_old_password_is_match'] = 'Mật khẩu mới phải khác mật khẩu cũ';
$lang['msg_change_pass_success'] = 'Đổi mật khẩu thành công';
$lang['msg_change_pass_unsuccess'] = 'Đổi mật khẩu thất bại';
$lang['gender_male'] = 'Nam';
$lang['gender_female'] = 'Nữ';
$lang['lbl_user_profile'] = 'Tài khoản';
$lang['msg_update_profile_success'] = 'Cập nhật thông tin thành công.';
$lang['warning_captcha'] = 'Captcha không hợp lệ.';
/*-------------------- Items ---------------------------------------*/
$lang['lbl_post_item'] = 'Đăng tin';
$lang['total_area'] = "Tổng diện tích đất";
$lang['building_density'] = "Mật độ xây dựng";
$lang['num_floor'] = "Tổng số tầng";
$lang['total_area_building'] = "Tổng diện tích xây dựng";
$lang['num_bedrooms'] = "Số phòng ngủ";
$lang['bath_room'] = "Phòng tắm, nhà vệ sinh";
$lang['orientation_house'] = "Hướng nhà";
$lang['belong_project'] = "Thuộc dự án";
$lang['red_book'] = "Sổ đỏ";
$lang['pink_book'] = "Sổ hồng";
$lang['investment_certificate'] = "Chủ trương đầu tư";
$lang['implementation_planning'] = "Quy hoạch";
$lang['clearance'] = "Giải phóng mặt bằng";
$lang['land_fees'] = "Tiền sử dụng đất";
$lang['done_compensation'] = "Xong đền bù";
$lang['done_red_book'] = "Sổ đỏ";
$lang['under_construction'] = "Xây dựng";
$lang['on_marketing'] = "Đang bán hàng";
$lang['lbl_manage_items'] = 'Quản lý tin';
$lang['lbl_manage_items_bdsvn'] = 'Quản lý tin BDSVN';
$lang['lbl_user_manage_items'] = 'Quản lý tin của bạn';
$lang['msg_item_save_successfully'] = 'Đã lưu thành công.';
$lang['msg_item_save_unsuccessfully'] = 'Lưu thất bại. Vui lòng thử lại.';
$lang['msg_remove_item_successfully'] = 'Tin đăng của bạn đã được xóa.';
$lang['lbl_your_items'] = 'Tin đã lưu';

/*Type of projct*/
$lang['lbl_all'] ='Tất cả';
$lang['lbl_no_info'] = 'Không có thông tin'; 
$lang['lbl_during_discuss_investment'] = 'Đang trong quá trình thỏa thuận địa điểm, chủ trương đầu tư'; 
$lang['lbl_agreement_location'] = 'Đã có thỏa thuận địa điểm đầu tư'; 
$lang['lbl_have_investment_certificate'] = 'Đã có giấy chứng nhận đầu tư'; 
$lang['lbl_other'] = 'Khác';

$lang['lbl_design_planning'] = 'Đang thiết kế quy hoạch';
$lang['lbl_show_planning'] = 'Đang trình phê duyệt quy hoạch';
$lang['lbl_approve_planning'] = 'Đã phê duyệt quy hoạch';
$lang['lbl_have_planning_certificate'] = 'Đã có chứng chỉ quy hoạch';

$lang['lbl_during_compensation'] = 'Đang trong quá trình đền bù';
$lang['lbl_done_compensation'] = 'Xong đền bù';
$lang['lbl_done_red_book'] = 'Xong sổ đỏ';

$lang['lbl_during_estimate'] = 'Trong quá trình định giá tiền sử dụng đất';
$lang['lbl_notify_pay_fee'] = 'Đã có thông báo nộp tiền sử dụng đất';
$lang['lbl_done_fee'] = 'Đã nộp tiền sử dụng đất';

$lang['lbl_no_reb_book'] = 'Chưa có sô đỏ';
$lang['lbl_have_red_book'] = 'Đã có sổ đỏ';

$lang['lbl_not_yet_start'] = 'Chưa khỏi công';
$lang['lbl_building_infrastructure'] = 'Đang xây dựng hạ tầng';
$lang['lbl_building_foundation'] = 'Đang xây dựng móng';
$lang['lbl_raw_construction'] = 'Đã xong xây dựng thô';
$lang['lbl_done_building'] = 'Đã xong xây dựng hoàn thiện';

$lang['lbl_during_contributions'] = 'Đang nhận hợp đồng góp vốn mua';
$lang['lbl_selling'] = 'Đang bán hàng';

$lang['lbl_search_advanced'] = "Tìm nâng cao";
$lang['lbl_ignore_search_advanced'] = "Bỏ tìm nâng cao";

/* Kind of Project*/
$lang['project'] = 'Dự án';
$lang['housing'] = "Nhà mặt đất";
$lang['apartment'] = "Căn hộ chung cư";
$lang['land'] = "Đất";
$lang['factory'] = "Nhà xưởng, khu công nghiệp";
$lang['office'] = "Văn phòng";
$lang['store_kiosks'] = "Ki ốt cửa hàng, mặt bằng bán lẻ";
$lang['other'] = "Loại khác";

/* Item Status */
$lang['is_checking'] = 'Đang chờ duyệt';
$lang['is_approved'] = "Đang hiển thị";
$lang['is_draft'] = "Tin nháp";
$lang['is_disable'] = "Không hiển thị";

/* Post Status */
$lang['ps_available'] = 'Đang giao dịch';
$lang['ps_leased'] = "Kết thúc giao dịch";
$lang['ps_sold'] = "Kết thúc giao dịch";

/*User Role */
$lang['role_manager'] = 'Manager';
$lang['role_register'] = 'Register';

/*Item Type*/
$lang['itemtype_for_rent'] = 'Cần cho thuê';
$lang['itemtype_rent'] = 'Cần thuê';
$lang['itemtype_for_sale'] = 'Cần bán';
$lang['itemtype_buy'] = 'Cần mua';

/*ORIENTATION_HOUSE*/
$lang['oh_east'] = 'Đông';
$lang['oh_west'] = 'Tây';
$lang['oh_south'] = 'Nam';
$lang['oh_north'] = 'Bắc';
$lang['oh_east_north'] = 'Đông Bắc';
$lang['oh_east_south'] = 'Đông Nam';
$lang['oh_west_north'] = 'Tây Bắc';
$lang['oh_west_south'] = 'Tây Nam';