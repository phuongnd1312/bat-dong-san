<?php
$lang['js_username_not_include_spec_char'] = 'Username not include special characters';
$lang['js_email_not_correct'] = 'Email formatting is not correct';
$lang['js_phone_not_correct'] = 'Phone formatting is not correct';
$lang['js_password_not_match'] = 'Password and repassword are not match';
$lang['js_password_validate_wrong'] = 'Password have to have less 6 characters';
$lang['js_do_want_remove_item'] = "Do you want to remove item?";
$lang['js_are_you_sure'] = "Are you sure?";