<?php
$lang['vi'] = 'Vietnamese';
$lang['en'] = 'English';
$lang['warning_permission'] = "You don't have permission to access this page";
$lang['nav_previous'] = 'Previous';
$lang['nav_next'] = 'Next';
/*------------------- Register account -----------------------*/
$lang['lbl_sign_up'] = 'Sign Up';
$lang['lbl_sign_in'] = 'Login';
$lang['lbl_sign_out'] = 'Logout';
$lang['msg_sign_up_success'] = 'Sign up successfully';
$lang['msg_sign_up_unsuccess'] = 'Sign up unsuccessfully';
$lang['msg_email_exists'] = 'Email already exists';
$lang['msg_email_activation'] = 'Email activation was sent to you. Please click link in your email to activate your account';
$lang['msg_cant_send_email'] = 'Can not send message. Please contact administrator';
$lang['msg_code_activation_invalid'] = 'Your activation code is invalid.';
$lang['msg_your_account_activated'] = 'Your account has activated.';
$lang['msg_your_account_not_activated'] = 'Your account is not activated.';
$lang['msg_your_loign_failed'] = 'Your login is failed.';
$lang['txt_forgot_password'] = 'Forgot password.';
$lang['lbl_forgot_password'] = 'Forgot password';
$lang['lbl_submit_forgot'] = 'Submit';
$lang['lbl_email_active'] = 'Activation Mail';
$lang['lbl_email_reset_pass'] = 'Reset password';
$lang['msg_email_reset_pass'] = 'A message was sent to your email. Please click link in your email to reset your password';
$lang['msg_email_not_existed'] = 'This email does not exist in system.';
$lang['msg_code_reset_pass_not_right'] = 'Your code is not correct.';
$lang['msg_password_reset_success'] = 'Your password was reset';
$lang['msg_warning_miss_info'] ='Please, enter fileds required.';

$lang['lbl_update_profile'] = 'Update Profile';
$lang['lbl_profile'] = 'Profile';

$lang['lbl_email'] = 'Email';
$lang['lbl_password'] = 'Password';
$lang['txt_dont_have_account'] = "Don't have an account.";

$lang['txt_enter_email'] = 'Nhập email của bạn';
$lang['txt_enter_password'] = 'Nhập mật khẩu';
$lang['lbl_change_password'] = 'Change Password';
$lang['lbl_old_password'] = 'Old Password';
$lang['lbl_repassword'] = 'Re Password';
$lang['txt_enter_old_password'] = 'Enter your old password';
$lang['txt_enter_repassword'] = 'Enter your repassword';
$lang['txt_enter_password_new'] = 'Enter new password';
$lang['txt_enter_repassword_new'] = 'Retype new password';

$lang['lbl_update'] = 'Update';
$lang['msg_password_not_match'] = 'Your old password not match';
$lang['warn_new_old_password_is_match'] = 'New password must be difference old password';
$lang['msg_change_pass_success'] = 'Change password successfully';
$lang['msg_change_pass_unsuccess'] = 'Change password unsuccessfully';

/*-------------------- Items ---------------------------------------*/
$lang['lbl_post_item'] = 'Đăng tin';
$lang['total_area'] = "Tổng diện tích đất";
$lang['building_density'] = "Mật độ xây dựng (số phần trăm)";
$lang['num_floor'] = "Tổng số tầng";
$lang['total_area_building'] = "Tổng diện tích xây dựng";
$lang['num_bedrooms'] = "Số phòng ngủ";
$lang['orientation_house'] = "Hướng nhà";
$lang['belong_project'] = "Thuộc dự án";
$lang['red_book'] = "Sổ đỏ";
$lang['pink_book'] = "Sổ hồng";
$lang['investment_certificate'] = "Chứng nhận đầu tư, chủ trương đầu tư";
$lang['implementation_planning'] = "Đang trong quá trình thực hiện quy hoạch 1/500";
$lang['finished_planning'] = "Xong quy hoạch 1/500";
$lang['during_compensation'] = "Đang trong quá trình đền bù";
$lang['done_compensation'] = "Xong đền bù";
$lang['done_red_book'] = "Xong sổ đỏ";
$lang['done_obligation_finance'] = "Xong nộp tiền thực hiện nghĩa vụ tài chính với nhà nước";
$lang['under_construction'] = "Đang trong quá trình xây dựng hạ tầng";
$lang['done_building'] = "Xong xây dựng hạ tầng";
$lang['on_marketing'] = "Đang bán hàng";
$lang['lbl_manage_items'] = 'Quản lý tin';
$lang['lbl_user_manage_items'] = 'Quản lý tin của bạn';
$lang['msg_item_save_successfully'] = 'Your item was saved successfully.';
$lang['msg_item_save_unsuccessfully'] = 'Your item was saved unsuccessfully.';

/* Kind of Project*/
$lang['project'] = 'Dự án';
$lang['housing'] = "Nhà mặt đất";
$lang['apartment'] = "Căn hộ chung cư";
$lang['land'] = "Đất";
$lang['factory'] = "Nhà xưởng, khu công nghiệp";
$lang['office'] = "Văn phòng";
$lang['store_kiosks'] = "Ki ốt cửa hàng, mặt bằng bán lẻ";
$lang['other'] = "Loại khác";

/* Item Status */
$lang['is_checking'] = 'Đang duyệt';
$lang['is_approved'] = "Đang hiển thị";
$lang['is_draft'] = "Tin nháp";
$lang['is_disable'] = "Không hiển thị";

/* Post Status */
$lang['ps_available'] = 'Sẵn sàng';
$lang['ps_leased'] = "Đã cho thuê";
$lang['ps_sold'] = "Đã bán";