<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'front/main/map';
$route['404_override'] = '';
$route['translate_uri_dashes'] = false;
$route['ban-do'] = 'front/main/map';
$route['danh-sach'] = 'front/main/index/';
$route['danh-sach/(:any)'] = 'front/main/index/0/$1';
$route['bds-(:any)-cd(:any)'] = 'front/main/index/$2/0';
$route['bds-(:any)-cd(:any)/(:num)'] = 'front/main/index/$2/$3';
$route['bds-tim-kiem'] = 'front/main/search/';
$route['account/(:any)'] = 'front/users/$1';
$route['account/(:any)/(:any)'] = 'front/users/$1/$2';
$route['tin-da-luu'] = 'front/user_items/index';
$route['dang-tin'] = 'front/items/form';
$route['dang-tin/(:any)'] = 'front/items/form/$1';
$route['quan-tri-tin'] = 'front/items/manage';

//seach
//$route['bds-(:any)-sh(:num)'] = 'main/index/$2/0';
$route['bds-(:any)-sh(:num)/(:num)/(:num)/(.*)'] = 'front/main/filter/$2/$3/$4';

$route['chi-tiet/(:any)-(:num)'] = 'front/items/detail/$2';

//admin
$route['admin'] = 'admin/authen';