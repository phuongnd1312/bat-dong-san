<?php
require_once 'facebook.php';

class Auth_Facebook
{
	private static $facebook = null;

	function __construct()
	{
		if(self::$facebook == null)
		{
			if(ENVIRONMENT == 'development'){
				self::$facebook = new Facebook(array(
				  'appId'  => Constant::$DEV_FAPP_ID,
				  'secret' => Constant::$DEV_FAPP_SECRET
				));
			}else{
				self::$facebook = new Facebook(array(
				  'appId'  => Constant::$PRO_FAPP_ID,
				  'secret' => Constant::$PRO_FAPP_SECRET
				));
			}
		
			
		}
	}
	public function getUrlLogin()
	{
		$perms = array('scope' => 'email');
		return self::$facebook->getLoginUrl($perms);
	}
	public function getUser(&$user_profile)
	{
		$user = self::$facebook->getUser();
		if ($user) 
		{
			  try 
			  {
			    	// Proceed knowing you have a logged in user who's authenticated.
			    	$user_profile = self::$facebook->api('/me');
			  } 
			  catch (FacebookApiException $e) 
			  {
			    	$user = null;
			  }
		}
		return $user;
	}
}