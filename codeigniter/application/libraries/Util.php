<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Util
{
    public static function loadImg($path)
    {
        return base_url('public') . $path;
    }

    public static function formatDate($date)
    {
        return date('d/m/Y', strtotime($date));
    }

    public static function response($error_code, $message, $data = null)
    {
        $resp = array('error_code' => $error_code, 'message' => $message, 'data' => $data);
        echo json_encode($resp);
        exit;
    }

    public static function slug($string, $slug = '-')
    {
        return strtolower(trim(preg_replace('~[^0-9a-z]+~i', $slug, $string), $slug));
    }

    public static function stringURLSafe($string)
    {
        $trans = array(
            "đ" => "d", "ă" => "a", "â" => "a", "á" => "a", "à" => "a", "ả" => "a", "ã" => "a", "ạ" => "a",
            "ấ" => "a", "ầ" => "a", "ẩ" => "a", "ẫ" => "a", "ậ" => "a",
            "ắ" => "a", "ằ" => "a", "ẳ" => "a", "ẵ" => "a", "ặ" => "a",
            "é" => "e", "è" => "e", "ẻ" => "e", "ẽ" => "e", "ẹ" => "e",
            "ế" => "e", "ề" => "e", "ể" => "e", "ễ" => "e", "ệ" => "e",
            "í" => "i", "ì" => "i", "ỉ" => "i", "ĩ" => "i", "ị" => "i",
            "ư" => "u", "ô" => "o", "ơ" => "o", "ê" => "e",
            "Ư" => "u", "Ô" => "o", "Ơ" => "o", "Ê" => "e",
            "ú" => "u", "ù" => "u", "ủ" => "u", "ũ" => "u", "ụ" => "u",
            "ứ" => "u", "ừ" => "u", "ử" => "u", "ữ" => "u", "ự" => "u",
            "ó" => "o", "ò" => "o", "ỏ" => "o", "õ" => "o", "ọ" => "o",
            "ớ" => "o", "ờ" => "o", "ở" => "o", "ỡ" => "o", "ợ" => "o",
            "ố" => "o", "ồ" => "o", "ổ" => "o", "ỗ" => "o", "ộ" => "o",
            "ú" => "u", "ù" => "u", "ủ" => "u", "ũ" => "u", "ụ" => "u",
            "ứ" => "u", "ừ" => "u", "ử" => "u", "ữ" => "u", "ự" => "u", 'ý' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y', 'Ý' => 'Y', 'Ỳ' => 'Y', 'Ỷ' => 'Y', 'Ỹ' => 'Y', 'Ỵ' => 'Y',
            "Đ" => "D", "Ă" => "A", "Â" => "A", "Á" => "A", "À" => "A", "Ả" => "A", "Ã" => "A", "Ạ" => "A",
            "Ấ" => "A", "Ầ" => "A", "Ẩ" => "A", "Ẫ" => "A", "Ậ" => "A",
            "Ắ" => "A", "Ằ" => "A", "Ẳ" => "A", "Ẵ" => "A", "Ặ" => "A",
            "É" => "E", "È" => "E", "Ẻ" => "E", "Ẽ" => "E", "Ẹ" => "E",
            "Ế" => "E", "Ề" => "E", "Ể" => "E", "Ễ" => "E", "Ệ" => "E",
            "Í" => "I", "Ì" => "I", "Ỉ" => "I", "Ĩ" => "I", "Ị" => "I",
            "Ư" => "U", "Ô" => "O", "Ơ" => "O", "Ê" => "E",
            "Ư" => "U", "Ô" => "O", "Ơ" => "O", "Ê" => "E",
            "Ú" => "U", "Ù" => "U", "Ủ" => "U", "Ũ" => "U", "Ụ" => "U",
            "Ứ" => "U", "Ừ" => "U", "Ử" => "U", "Ữ" => "U", "Ự" => "U",
            "Ó" => "O", "Ò" => "O", "Ỏ" => "O", "Õ" => "O", "Ọ" => "O",
            "Ớ" => "O", "Ờ" => "O", "Ở" => "O", "Ỡ" => "O", "Ợ" => "O",
            "Ố" => "O", "Ồ" => "O", "Ổ" => "O", "Ỗ" => "O", "Ộ" => "O",
            "Ú" => "U", "Ù" => "U", "Ủ" => "U", "Ũ" => "U", "Ụ" => "U",
            "Ứ" => "U", "Ừ" => "U", "Ử" => "U", "Ữ" => "U", "Ự" => "U",
        );

        //remove any '-' from the string they will be used as concatonater
        $str = str_replace('-', ' ', $string);
        $str = strtr($str, $trans);


        // remove any duplicate whitespace, and ensure all characters are alphanumeric
        $str = preg_replace(array('/\s+/', '/[^A-Za-z0-9\-]/'), array('-', ''), $str);
        // $str = preg_replace(array('/\s+/', '/\./'), array('-', '_'), $str);
        //$str = utf8_accents_to_ascii($str);
        // lowercase and trim
        $str = trim(strtolower($str));
        return $str;
    }

    public static function parsePhotos($str_photos)
    {
        if (empty($str_photos))
            return array();
        return explode(';', $str_photos);
    }

    public static function baseUrlNoSlash()
    {
        return preg_replace("/\/$/", "", base_url());
    }

    public static function createDirectoryDate($path, $date)
    {
        $arr_date = explode('/', $date);
        foreach ($arr_date as $val) {
            $path .= '/' . $val;
            if (!Util::createDirectory($path))
                return false;
        }
        return true;
    }

    public static function createDirectory($path)
    {
        if (!file_exists($path)) {
            return mkdir($path);
        }
        return true;
    }

    public static function formatDateYMD($strDate, $determine = '/')
    {
        $arrDate = explode($determine, $strDate);
        if (count($arrDate) == 3)
            return $arrDate[2] . '-' . $arrDate[1] . '-' . $arrDate[0];
        else
            return null;
    }

    public static function formatDateDMY($strDate, $determine = '-')
    {
        $arrDate = explode($determine, $strDate);
        if (count($arrDate) == 3)
            return $arrDate[2] . '/' . $arrDate[1] . '/' . $arrDate[0];
        else
            return null;
    }

    public static function parseEndDate($start_date, $distance)
    {
        $tmp = strtotime($start_date) + (86400 * intval($distance));
        return date('Y-m-d', $tmp);
    }

    public static function distance2Days($date_end, $date_start)
    {
        $start = strtotime($date_start);

        $end = strtotime($date_end);

        if ($start && $end)
            return round(($end - $start) / 86400);
        else
            return 0;
    }

    public static function gpost($key, $defaul = null)
    {
        if (!isset($_POST[$key]))
            return $defaul;
        else
            return $_POST[$key];
    }

    public static function gget($key, $defaul = null)
    {
        if (!isset($_GET[$key]))
            return $defaul;
        else
            return $_GET[$key];
    }

    public static function removeSegment($segments, $index)
    {
        $arr = explode('/', $segments);
        if (isset($arr[$index])) {
            unset($arr[$index]);
            return implode('/', $arr);
        }
        return $segments;
    }

    public static function parse2url($params)
    {
        if (count($params)) {
            $tmp = array();
            foreach ($params as $key => $val) {
                if (is_array($val))
                    $tmp[] = $key . '=' . implode('-', $val);
                else
                    $tmp[] = $key . '=' . $val;
            }
            return implode('&', $tmp);
        }
        return '';
    }

    public static function formatPhone($phone)
    {
        $length = strlen($phone);
        if ($length >= 10)
            return "" . substr($phone, 0, 4) . "-" . substr($phone, 4, 3) . "-" . substr($phone, 7);
        return $phone;
    }
    public static function truncateWords($input, $line,  $numwords, $padding = "")
    {
        $array = explode("\n", $input);
        $input = null;
        array_filter($array);
        for ($i = 0; $i < count($array); $i++) {
            if (strlen($array[$i]) != 1)
                $input .= trim($array[$i]) . ' ';
        }
        $output = strtok($input, " \n");
        while (--$numwords > 0) $output .= " " . strtok(" \n");
        if ($output != $input) $output = trim($output) . $padding;
        return $output;
    }

    static function composite_photo($path_name)
    {
        //header('Content-type: image/jpeg');
        $ext = strtolower(pathinfo($path_name, PATHINFO_EXTENSION));
        $path_name = PUBLIC_URL . $path_name;

        if (!file_exists($path_name))
            return false;

        $logo = imagecreatefrompng('public/images/stories/composite_logo.png');
        $w_logo  = imagesx($logo);
        $h_logo = imagesy($logo);

        if ($ext == 'png')
            $jpg_image = imagecreatefrompng($path_name);
        else
            $jpg_image = imagecreatefromjpeg($path_name);

        $w_img  = imagesx($jpg_image);
        $h_img = imagesy($jpg_image);

        imagecopy($jpg_image, $logo, $w_img - $w_logo - 10, $h_img - $h_logo - 10, 0, 0, $w_logo, $h_logo);


        //background logo 
        $bg_logo = imagecreatefrompng('public/images/stories/bg_logo.png');
        $w_bg_logo  = imagesx($bg_logo);
        $h_bg_logo = imagesy($bg_logo);


        imagecopy($jpg_image, $bg_logo, round(($w_img - $w_bg_logo) / 2), round(($h_img - $h_bg_logo) / 2), 0, 0, $w_bg_logo, $h_bg_logo);

        // Send Image to Browser
        if ($ext == 'png')
            imagepng($jpg_image, $path_name);
        else
            imagejpeg($jpg_image, $path_name); //write to file

        imagedestroy($jpg_image);
    }

    static function zero2Null($num)
    {
        $tmp = intval($num);
        if ($tmp == 0)
            return null;
        else
            return $tmp;
    }
    public static function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '', $string); // Replaces multiple hyphens with single one.
    }

    static function addQueryString($arr)
    {
        $url = parse_url($_SERVER['REQUEST_URI']);
        $params = array();
        if (isset($url['query']))
            parse_str($url['query'], $params);
        foreach ($arr as $key => $val) {
            $params[$key] = $val;
        }

        return http_build_query($params);
    }

    static function removeParamsQueryString($pkey)
    {
        $url = parse_url($_SERVER['REQUEST_URI']);
        $params = array();
        if (isset($url['query'])) {
            parse_str($url['query'], $params);
            foreach ($params as $key => $val) {
                if ($key == $pkey || $val == '')
                    unset($params[$key]);
            }
        }

        return http_build_query($params);
    }

    static function sendSMS($phone, $content)
    {

        $APIKey = "D0A331AD93F99DD4167D5D576FB320";
        $SecretKey = "4545789AD1FABF00FC406C9BAF5CA9";
        $ch = curl_init();
        $SampleXml = "<RQST>"
            . "<APIKEY>" . $APIKey . "</APIKEY>"
            . "<SECRETKEY>" . $SecretKey . "</SECRETKEY>"
            . "<CONTENT>" . $content . "</CONTENT>"
            . "<SMSTYPE>2</SMSTYPE>"
            . "<BRANDNAME>SweetHome</BRANDNAME>"
            . "<CONTACTS>"
            .     "<CUSTOMER>"
            .         "<PHONE>" . $phone . "</PHONE>"
            .     "</CUSTOMER>"
            . "</CONTACTS>"
            . "</RQST>";
        curl_setopt($ch, CURLOPT_URL,            "http://api.esms.vn/MainService.svc/xml/SendMultipleSMSBrandname/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST,           1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $SampleXml);
        curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: text/plain'));

        $result = curl_exec($ch);
        $xml = simplexml_load_string($result);

        if ($xml === false) {
            return false;
        }
        if ($xml->CodeResult == '100') {
            return true;
        } else
            return false;
    }

    static function generatePass($str)
    {
        return substr(md5($str), 0, 4);
    }
}