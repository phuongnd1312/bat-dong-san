<?php
$class = isset($home) ? 'homepage' : 'subpage';
?>
<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Aviators - byaviators.com">

    <!-- Css input search select combobox -->
    <link rel='stylesheet' type='text/css' href="<?php echo $document['css_url']; ?>lib/select2.min.css?v=1" />
    <!-- Css style scroll -->
    <link rel="stylesheet" href="<?php echo $document['css_url']; ?>lib/jquery.mCustomScrollbar.css" />

    <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'> -->
    <link rel="shortcut icon" href="<?php echo $document['template_url']; ?>/img/favicon.png" type="image/png">
    <link rel="stylesheet" href="<?php echo $document['template_url']; ?>/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $document['template_url']; ?>/css/bootstrap-responsive.css" type="text/css">
    <!-- <link rel="stylesheet" href="<?php echo $document['template_url']; ?>/libraries/chosen/chosen.css" type="text/css"> -->
    <link rel="stylesheet" href="<?php echo $document['template_url']; ?>/libraries/bootstrap-fileupload/bootstrap-fileupload.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $document['template_url']; ?>/libraries/jquery-ui-1.10.2.custom/css/ui-lightness/jquery-ui-1.10.2.custom.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo $document['template_url']; ?>/css/realia-blue-google.css" type="text/css" id="color-variant-default">
    <link rel="stylesheet" href="<?php echo $document['template_url']; ?>/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="#" type="text/css" id="color-variant">

    <title>Realia | HTML Template</title>


    <script type="text/javascript" src="<?php echo $document['template_url']; ?>/js/jquery.js"></script>
    <!-- Get  Google MAP -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACk-SDgIJne6DgNHWnaGRsYK-cBOmYBls&libraries=places,geometry"></script>
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>lib/markerwithlabel.1.1.9.js"></script>

    <script type="text/javascript" src="<?php echo $document['js_url']; ?>lib/libs.js?v=1.0"></script>
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>lib/vn.bdsvn.storage.js"></script>
    <!-- Js input search select combobox -->
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>lib/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>lib/i18n/vi.js"></script>
    <!-- Js style scroll using: [slide_filter]-->
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>lib/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Js define Location using in search form -->
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>core/location.js"></script>
    <!-- Define API function  -->
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>core/main.js?v=1.2"></script>
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>core/util.js?v=1.2"></script>


    <script type="text/javascript" src="<?php echo $document['js_url']; ?>core/maps.js?v=1.5"></script>
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

    <script type="text/javascript">
        var BASE_URL = '<?php echo Util::baseUrlNoSlash(); ?>';
        var LANG = '<?php echo App::ins()->getLangCode(); ?>';
    </script>
    <?php MHtml::jslang(); ?>
    <link rel="stylesheet" href="<?php echo $document['template_url']; ?>/css/custom.css" type="text/css">
</head>

<body>
    <div id="wrapper-outer">
        <div id="wrapper">
            <div id="wrapper-inner">
                <?php echo $navigation; ?>
                <!-- CONTENT -->
                <?php echo $contents; ?>
            </div><!-- /#wrapper-inner -->

            <?php echo Mhtml::loadView('front/partials/footer'); ?>

        </div><!-- /#wrapper -->
    </div><!-- /#wrapper-outer -->


    <script type="text/javascript" src="<?php echo $document['template_url']; ?>/js/carousel.js"></script>
    <!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=true"></script> -->
    <!-- <script type="text/javascript" src="<?php echo $document['template_url']; ?>/js/jquery.ezmark.js"></script> -->
    <script type="text/javascript" src="<?php echo $document['template_url']; ?>/js/jquery.currency.js"></script>
    <script type="text/javascript" src="<?php echo $document['template_url']; ?>/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo $document['template_url']; ?>/js/retina.js"></script>
    <script type="text/javascript" src="<?php echo $document['template_url']; ?>/js/bootstrap.min.js"></script>
    <!-- <script type="text/javascript" src="<?php echo $document['template_url']; ?>/js/gmap3.min.js"></script> -->
    <!-- <script type="text/javascript" src="<?php echo $document['template_url']; ?>/js/gmap3.infobox.min.js"></script> -->
    <script type="text/javascript" src="<?php echo $document['template_url']; ?>/libraries/jquery-ui-1.10.2.custom/js/jquery-ui-1.10.2.custom.min.js"></script>
    <!-- <script type="text/javascript" src="<?php echo $document['template_url']; ?>/libraries/chosen/chosen.jquery.min.js"></script> -->
    <script type="text/javascript" src="<?php echo $document['template_url']; ?>/libraries/iosslider/_src/jquery.iosslider.min.js"></script>
    <script type="text/javascript" src="<?php echo $document['template_url']; ?>/libraries/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="<?php echo $document['template_url']; ?>/js/realia.js"></script>
    <!-- <script type="text/javascript" src="<?php echo $document['template_url']; ?>/js/util.js"></script>
<script type="text/javascript" src="<?php echo $document['template_url']; ?>/js/main.js"></script> -->
</body>

</html>