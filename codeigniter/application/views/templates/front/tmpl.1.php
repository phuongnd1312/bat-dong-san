<?php 
$class = isset($home) ? 'homepage' : 'subpage';
?>
<html lang=''>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	<?php Meta::ins()->showMeta(); ?>
	<link rel="stylesheet" href="<?php echo $document['template_url']; ?>/bootstrap-3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo $document['css_url']; ?>font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo $document['template_url']; ?>/bootstrap-3.3.5/css/datepicker3.css" />
	<link rel='stylesheet' type='text/css' href="<?php echo $document['css_url']; ?>jcarousel.basic.css?v=1.0" />
  	<link rel='stylesheet' type='text/css' href="<?php echo $document['css_url']; ?>select2.min.css?v=1" />
  	<link rel="stylesheet" href="<?php echo $document['css_url']; ?>jquery.mCustomScrollbar.css" />
	<link rel='stylesheet' type='text/css' href="<?php echo $document['css_url']; ?>template.css?v=1.2" />


	<script type="text/javascript">
		var BASE_URL = '<?php echo Util::baseUrlNoSlash(); ?>';
		var LANG = '<?php echo App::ins()->getLangCode(); ?>';
	</script>

	<?php MHtml::jslang(); ?>
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>lib/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>lib/jquery.jcarousel.min.js"></script>
    <script src="<?php echo $document['template_url']; ?>/bootstrap-3.3.5/js/bootstrap-datepicker.js"></script>  
    <script src="<?php echo $document['template_url']; ?>/bootstrap-3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACk-SDgIJne6DgNHWnaGRsYK-cBOmYBls&libraries=places,geometry"></script> 
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>lib/markerwithlabel.1.1.9.js"></script>
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>jquery.imgareaselect.pack.js"></script>
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>lib/vn.bdsvn.storage.js"></script>
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>location.js"></script>
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>lib/select2.min.js"></script> 
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>lib/i18n/vi.js"></script> 
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>main.js?v=1.2"></script>
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>lib/libs.js?v=1.0"></script>
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>util.js?v=1.2"></script>  
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>maps.js?v=1.5"></script>  
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>lib/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</head>
<body class=''>
	<div id='wrap_all'>
		<div class='page-wrapper'>
			<div id='header'>
				<?php echo $navigation; ?>
			</div>
			<div id="container">
				<?php echo $contents; ?>
			</div>
		</div>
    	<div id="footer">
    		<?php echo Mhtml::loadView('front/partials/footer'); ?>
    	</div>
	</div>
</body>
</html>