<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Quản trị website</title>
	<link rel="stylesheet" href="<?php echo $document['template_url']; ?>/bootstrap-3.3.5/css/bootstrap.min.css">
	<link href="<?php echo $document['template_url']; ?>/font_aws/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel='stylesheet' type='text/css' href="<?php echo $document['css_url']; ?>sb-admin.css" />
	<link rel="stylesheet" href="<?php echo $document['template_url']; ?>/bootstrap-3.3.5/css/datepicker3.css" />
	<link rel='stylesheet' type='text/css' href="<?php echo $document['css_url']; ?>template.css" />
	<script type="text/javascript">
		var BASE_URL = '<?php echo Util::baseUrlNoSlash(); ?>';
	</script>
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>jquery-1.11.3.min.js"></script>
    <script src="<?php echo $document['template_url']; ?>/bootstrap-3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>tinymce.4.2.8/tinymce.min.js"></script>
    <script src="<?php echo $document['template_url']; ?>/bootstrap-3.3.5/js/bootstrap-datepicker.js"></script> 
    
    <script type="text/javascript" src="<?php echo $document['js_url']; ?>main.js"></script>    
</head>
<body>

	<?php 
	if(!Admin_Helper::ins()->isLogin()):
		echo $contents;
	else: ?>
	<div id="wrapper">
       	<div id='navigation'>
       		<?php echo $navigation; ?>
       	</div>
        <div id="page-wrapper">
            <div class="container-fluid">

                <?php 
                echo Admin_Helper::ins()->getMessage();

                echo $contents; 
                ?>
            </div>
        </div>
    </div>
	<?php endif; ?>
</body>
</html>