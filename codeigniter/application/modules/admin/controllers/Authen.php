<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authen extends MY_Controller {

	public function __construct() {

		parent::__construct ('admin');
		$this->load->helper('admin_helper');
		$this->load->model ('admin_model');
	}

	public function index(){

		if(Admin_Helper::ins()->adminLogin())
			redirect(base_url('admin/main/index'));

		$this->template->load('admin/login');
	}

	public function login(){

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$user = $this->admin_model->signin($username, $password);
		
		if(count($user))
		{
			$this->session->set_userdata(Constant::$KEY_ADMIN_STATE, $user);
			redirect(base_url('admin/main/index'));
		}	
		else
		{
			$resp = array();
			if($username != false || $password != false){
				$resp = array('error'=>'Login unsuccessfully');
			}
		
			$this->template->load('admin/login', $resp);
		}
	}

	public function logout(){
		$this->session->set_userdata(Constant::$KEY_ADMIN_STATE, false);
		redirect(base_url('admin/authen/login'));
	}
}