<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Streets extends MY_Controller {

	public function __construct() {
		parent::__construct ('admin');
		$this->load->helper('admin_helper');
		Admin_Helper::ins()->adminLogin();
		$this->load->model ('street_model');
		$this->load->model ('city_model');
		$this->load->model ('district_model');
		$this->load->model ('ward_model');
	}

	public function index(){
		$field = $this->gget('field', 'id');
		$sort = $this->gget('sort', 'desc');
		$order = 's.'.$field.' '.$sort;

		$filter['city'] = $this->gget('city', 0);
		$filter['district'] = $this->gget('district', 0);
		$filter['ward'] = $this->gget('ward', 0);

		$str_filter = '';
		if($filter['city'] != 0 || $filter['district'] != 0){
			Util::addQueryString($filter);
		}

		$query = Util::removeParamsQueryString('per_page');
		$query = (trim($query) != '') ? '?'.$query : '';

		$cfg_page = $this->load_pagination("admin/streets".$query);
		$cfg_page['page_query_string'] = TRUE;
		$cfg_page['per_page'] = CONSTANT::$PER_ROW_PAGE_ADMIN;
		$page = $this->gget('per_page', 0);

		$resp['streets'] = $this->street_model->getListWP($filter, $order, $cfg_page["per_page"], $page, $cfg_page["total_rows"]);
		$resp['count'] = $cfg_page["total_rows"];
		$this->pagination->initialize($cfg_page);
		$resp['pagination'] = $this->pagination->create_links();
		$resp['cities'] = $this->city_model->getList();

		$sel_city = $this->gget('city', 0);
		$sel_district = $this->gget('district', 0);
		$districts = array();
		if($sel_city)
			$districts = $this->district_model->getByCity($sel_city);
		$resp['districts'] = $districts;
		$this->template->load('admin/street/streets', $resp);
	}

	public function form($pid = 0){
	
		if(count($_POST)){
			$pid = $this->project_save();
			if($pid)
				Admin_Helper::ins()->setMessage('Your item was saved successfully.', 'success');
			else
				Admin_Helper::ins()->setMessage('Your item was saved unsuccessfully.', 'danger');
			redirect(base_url('/admin/streets/form/'.$pid));
		}

		$street = $this->street_model->getById($pid);

		if($pid != 0 && $street == null)
			show_404();
		if($street == null)
			$street = new street_model();


		
		$districts = array();
		if($street != null){
			$districts = $this->district_model->getByCity($street->lvl1_id);
		}

		$resp['cities'] = $this->city_model->getList();
		$resp['districts'] = $districts;
		$resp['street'] = $street;

		$this->template->load('admin/street/form', $resp);
	}

	private function project_save(){
		$params['id'] = $this->gpost('id', null);
		$params['name'] = $this->gpost('name', '');
		$params['lvl1_id'] = $this->gpost('lvl1_id', 0);
		$params['lvl2_id'] = $this->gpost('lvl2_id', 0);


		$id = $this->street_model->save($params);
		return $id;
	}
	
	public function remove($id){
		$this->street_model->delete($id);
		Admin_Helper::ins()->setMessage('Your item was removed successfully.', 'success');
		
		redirect(base_url('/admin/streets/'));
	}
}