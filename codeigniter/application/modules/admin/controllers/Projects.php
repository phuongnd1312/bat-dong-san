<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends MY_Controller {

	public function __construct() {
		parent::__construct ('admin');
		$this->load->helper('admin_helper');
		Admin_Helper::ins()->adminLogin();
		$this->load->model ('project_model');
		$this->load->model ('city_model');
		$this->load->model ('district_model');
		$this->load->model ('ward_model');
	}

	public function index(){
		$sel_city = $this->gget('city', 0);
		$sel_district = $this->gget('district', 0);

		$field = $this->gget('field', 'id');
		$sort = $this->gget('sort', 'desc');
		$order = 'p.'.$field.' '.$sort;

		$str_filter = '';
		if($sel_city != 0 || $sel_district != 0){
			Util::addQueryString(array('city'=>$sel_city, 'district'=>$sel_district));
		}

		$districts = array();
		if($sel_city)
			$districts = $this->district_model->getByCity($sel_city);
		
		
		$filter['city'] = $sel_city;
		$filter['district'] = $sel_district;
	
		$query = Util::removeParamsQueryString('per_page');
		$query = (trim($query) != '') ? '?'.$query : '';

		$cfg_page = $this->load_pagination("admin/projects".$query);
		$cfg_page['page_query_string'] = TRUE;
		$cfg_page['per_page'] = CONSTANT::$PER_ROW_PAGE_ADMIN;
		$page = $this->gget('per_page', 0);

		$resp['districts'] = $districts;
		$resp['projects'] = $this->project_model->getListWP($filter, $order, $cfg_page["per_page"], $page, $cfg_page["total_rows"]);
		$resp['count'] = $cfg_page["total_rows"];
		$this->pagination->initialize($cfg_page);
		$resp['pagination'] = $this->pagination->create_links();

		$resp['cities'] = $this->city_model->getList();



		$this->template->load('admin/project/projects', $resp);
	}

	public function form($pid = 0){
	
		if(count($_POST)){
			$pid = $this->project_save();
			if($pid)
				Admin_Helper::ins()->setMessage('Your item was saved successfully.', 'success');
			else
				Admin_Helper::ins()->setMessage('Your item was saved unsuccessfully.', 'danger');
			redirect(base_url('/admin/projects/form/'.$pid));
		}

		$project = $this->project_model->getById($pid);

		if($pid != 0 && $project == null)
			show_404();
		if($project == null)
			$project = new project_model();

	
		$resp['cities'] = $this->city_model->getList();
		$resp['districts'] = $this->district_model->getByCity($project->city_id);
	
		if($resp['districts']  == null)
			$resp['districts'] = array();


		$resp['project'] = $project;

		$this->template->load('admin/project/form', $resp);
	}

	private function project_save(){
		$params['id'] = $this->gpost('id', null);
		$params['name'] = $this->gpost('name', '');
		$params['description'] = $this->gpost('description', '');
		$params['address'] = $this->gpost('address', '');
		$params['city_id'] = $this->gpost('city_id', 0);

		$params['district_id'] = $this->gpost('district_id', 0);
		$params['phone'] = $this->gpost('phone', '');
		$params['website'] = $this->gpost('website', '');
		$params['email'] = $this->gpost('email', '');
		$params['fax'] = $this->gpost('fax', '');
		//$params['photo'] = $this->gpost('photo', '');
		$params['infrastructure'] = $this->gpost('infrastructure', '');

		$params['invest_address'] = $this->gpost('invest_address', '');
		$params['invest_phone'] = $this->gpost('invest_phone', '');
		$params['invest_fax'] = $this->gpost('invest_fax', '');
		$params['invest_website'] = $this->gpost('invest_website', '');
		$params['invest_email'] = $this->gpost('invest_email', '');

		//$params['invest_photo'] = $this->gpost('invest_photo', '');
		$id = $this->project_model->save($params);
		if($id){
			$params_photo = array();
			if(isset($_FILES['photo']) && $_FILES['photo']['size'] > 0){
				$_FILES['userfile'] = &$_FILES['photo'];
				$rlt = $this->do_upload_pricture($id);
				if(isset($rlt['upload_data']))
					$params_photo['photo'] = $rlt['path'].'/'.$rlt['upload_data']['file_name'];
				else
					Admin_Helper::ins()->setMessage($rlt['error'], 'danger');
			}

			if(isset($_FILES['invest_photo']) && $_FILES['invest_photo']['size'] > 0){
				$_FILES['userfile'] = &$_FILES['invest_photo'];
				$rlt = $this->do_upload_pricture($id);
				if(isset($rlt['upload_data'])){
					$params_photo['invest_photo'] = $rlt['path'].'/'.$rlt['upload_data']['file_name'];
				}else
					Admin_Helper::ins()->setMessage($rlt['error'], 'danger');
			}

			if(count($params_photo)){
				$params_photo['id'] = $id;
				$this->project_model->save($params_photo);
			}
		}
		
		return $id;
	}
	
	public function remove($id){
		$this->project_model->delete($id);
		Admin_Helper::ins()->setMessage('Your item was removed successfully.', 'success');
		
		redirect(base_url('/admin/projects/'));
	}

	private function do_upload_pricture($id)
	{
		$image_path = '/images/projects/'.$id;
		$path = 'public'.$image_path;
		if(!Util::createDirectory($path))
			return array('error' => 'Can not create directory.');

		$config['upload_path'] = $path;
		$config['allowed_types'] = Constant::$ALLOW_PIC_UPLOAD;
		$config['max_size']	= Constant::$LIMIT_PIC_UPLOAD;
		$config['encrypt_name'] = true;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
			return array('error' => $this->upload->display_errors());
		else
			return array('upload_data' => $this->upload->data(), 'path' => $image_path);
	}
}