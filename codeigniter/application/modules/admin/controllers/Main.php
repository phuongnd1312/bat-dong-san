<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	public function __construct() {
		parent::__construct ('admin');
		$this->load->helper('admin_helper');
		Admin_Helper::ins()->adminLogin();
		$this->load->model ('admin_model');
	}

	public function index(){
		$this->load->model ('crawl_model');
		$resp['num_crawl'] = $this->crawl_model->getNumCrawl();
		$resp['num_crawl_approved'] = $this->crawl_model->getNumCrawlApprove();
		$this->template->load('admin/index', $resp);
	}
}