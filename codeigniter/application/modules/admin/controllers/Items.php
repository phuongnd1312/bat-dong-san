<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items extends MY_Controller {

	public function __construct() {
		parent::__construct ('admin');
		$this->load->helper('admin_helper');
		Admin_Helper::ins()->adminLogin();
		$this->load->model ('admin_model');
		$this->load->model ('item_model');
		$this->load->model ('city_model');
		$this->load->model ('district_model');
		$this->load->model ('ward_model');
	}

	public function index(){
		$keywords = '';
		$uri_segment_page = 4;
		$cfg_page = $this->load_pagination("admin/items/index/", $uri_segment_page);
		$page = ($this->uri->segment($uri_segment_page)) ? $this->uri->segment($uri_segment_page) : 0;

		$resp['items'] = $this->item_model->getListWP($keywords, $cfg_page["per_page"], $page, $cfg_page["total_rows"]);
		$this->pagination->initialize($cfg_page);


		$resp['pagination'] = $this->pagination->create_links();

		$this->template->load('admin/item/items', $resp);
	}

	public function form($pid = 0){
		
		$this->load->helper('item_helper');
		//$this->load->model('item_type_model');
		$this->load->model('properties_proportion_model');
		$this->load->model ('ward_model');
		$this->load->model ('district_model');
	
		if(count($_POST)){

			$pid = $this->item_save();
			if($pid)
				Admin_Helper::ins()->setMessage('Your item was saved successfully.', 'success');
			else
				Admin_Helper::ins()->setMessage('Your item was saved unsuccessfully.', 'danger');
			redirect(base_url('/admin/items/form/'.$pid));
		}

		$item = $this->item_model->getById($pid);

		if($pid != 0 && $item == null)
			show_404();
		if($item == null)
			$item = new item_model();
		

		$resp['properties_proportion'] = $this->properties_proportion_model->getList();
		$resp['type_items'] =  Constant::$ITEM_TYPE;
		$resp['cities'] = $this->city_model->getList();
		$resp['districts'] = $this->district_model->getByCity($item->add_level_1);
		$resp['wards'] = $this->ward_model->getByDistrict($item->add_level_2);

		$resp['item'] = $item;

		$this->template->load('admin/item/item_form', $resp);
	}

	private function item_save(){
		$params['id'] = $this->gpost('id', null);
		$params['title'] = $this->gpost('title', '');
		$params['date_started'] = Util::formatDateYMD($this->gpost('date_started', 0));
		$params['date_ended'] = Util::formatDateYMD($this->gpost('date_ended', 0));
		$params['description'] = $this->gpost('description', '');
		$params['type_item'] = $this->gpost('type_item', 0);
		$params['add_level_1'] = $this->gpost('add_level_1', 0);
		$params['add_level_2'] = $this->gpost('add_level_2', 0);
		$params['add_level_3'] = $this->gpost('add_level_3', 0);
		$params['project_type'] = $this->gpost('project_id', 0);
		$params['price_total'] = $this->gpost('price_total', 0);
		$params['project_type'] = $this->gpost('project_type', 0);

		//$params['add_street'] = $this->gpost('add_street', '');
		$params['area'] = $this->gpost('area', 0);
		$params['full_address'] = $this->gpost('full_address', '');
		$params['contact_name'] = $this->gpost('contact_name', '');
		$params['contact_phone'] = $this->gpost('contact_phone', '');
		$params['contact_company'] = $this->gpost('contact_company', '');

		$params['contact_email'] = $this->gpost('contact_email', '');
		$params['contact_zalo'] = $this->gpost('contact_zalo', '');
		$params['contact_company'] = $this->gpost('contact_company', '');

		$params['lat'] = $this->gpost('lat', 0);
		$params['lng'] = $this->gpost('lng', 0);
		$params['photos'] = $this->gpost('photos', '');


		$id = $this->item_model->save($params);
		return $id;
	}
	
	public function remove($id){
		$this->item_model->delete($id);
		Admin_Helper::ins()->setMessage('Your item was removed successfully.', 'success');
		
		redirect(base_url('/admin/items/index'));
	}

	public function upload_img(){
	
		if(isset($_FILES['file']) && $_FILES['file']['size'] > 0){
			$_FILES['userfile'] = &$_FILES['file'];
		
			$upload = $this->do_upload_pricture();
			if(isset($upload['upload_data']))
			{

				$photo = $upload['path'].'/'.$upload['upload_data']['file_name'];
				
				Util::response(0, 'Success', $photo);
				
			}else{
				Util::response(1, 'UnSuccess', $upload['error']);
			}	
		}
		exit;
	}


	private function do_upload_pricture()
	{
		$path = 'public/images/items';
		$date = date('Y/m/d');
		$full_path = $path.'/'.$date;
		$image_path = '/images/items'.'/'.$date;

		if(!file_exists($full_path)){
			if(!Util::createDirectoryDate($path, $date))
				return array('error' => 'Can not make directory '.$path);
		}

		$config['upload_path'] = $full_path;
		$config['allowed_types'] = Constant::$ALLOW_PIC_UPLOAD;
		$config['max_size']	= Constant::$LIMIT_PIC_UPLOAD;
		$config['encrypt_name'] = true;


		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
			return array('error' => $this->upload->display_errors());
		else
			return array('upload_data' => $this->upload->data(), 'path' => $image_path);
	}

	public function remove_photo(){
		$path_file = $this->gpost('path_file','');

		if(is_file($path_file)){
			unlink($path_file);
			Util::response(0, 'Remove file successfully');
		}else
			Util::response(1, 'No file');
	}

}