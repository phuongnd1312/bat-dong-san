<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ajax extends MY_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  private function response($error_code, $message, $data = null)
  {
    $resp = array('error_code' => $error_code, 'message' => $message, 'data' => $data);
    echo json_encode($resp);
    exit;
  }

  public function get_district_by_city($city = 0)
  {
    $this->load->model('district_model');
    $districts = $this->district_model->getByCity($city, " ");
    $this->response(0, 'Success', $districts);
  }

  public function get_ward_by_district($district = 0)
  {
    $this->load->model('ward_model');
    $wards = $this->ward_model->getByDistrict(($district, " ");
    $this->response(0, 'Success', $wards);
  }

  public function get_street_by_district($district = 0)
  {
    $this->load->model('street_model');
    $streets = $this->street_model->getByDistrict(($district, " ");
    $this->response(0, 'Success', $streets);
  }

  public function show_captcha()
  {
    $this->load->helper('captcha_helper');
    Captcha_Helper::show();
    exit();
  }

  public function project_properties()
  {
    $id = $this->gget('id', 0);
    $item_id = $this->gget('item_id', 0);
    $this->load->model('properties_proportion_model');
    $this->load->model('item_properties_model');
    $this->load->model('project_model');


    $properties = $this->properties_proportion_model->getById($id);

    $html = '';
    if ($properties != null) {
      $item_properties = $this->item_properties_model->getById($item_id);
      if ($item_properties == null)
        $item_properties = new item_properties_model();

      $resp['orientation_houses'] = Constant::$ORIENTATION_HOUSE;
      $resp['projects'] = $this->project_model->getList();
      $resp['properties'] = $properties;
      $resp['item_properties'] = $item_properties;
      $html = MHtml::loadView('/front/item/partials/project_properties', $resp, true);
    }

    $this->response(0, 'Success', $html);
  }

  /*------------------------- Items ---------------------------*/
  public function get_location_items()
  {

    $this->load->helper('item_helper');
    $this->load->model('item_model');

    $location = $this->gpost('location', null);
    $filter = $this->gpost('filter', null);
    if ($location == null)
      $this->response(1, 'No filter', array());

    $general_filter = array();
    if (!empty($filter['address'])) {
      $filter_address = $filter['address'];
      if (!empty($filter['city']))
        $general_filter[] = 'add_level_1 = ' . intval($filter_address['city']);

      if (!empty($filter_address['district']))
        $general_filter[] = 'add_level_2 = ' . intval($filter_address['district']);

      if (!empty($filter_address['wards']))
        $general_filter[] = 'add_level_3 IN (' . implode(',', $filter_address['wards']) . ')';

      if (!empty($filter_address['streets']))
        $general_filter[] = 'add_level_4 IN (' . implode(',', $filter_address['streets']) . ')';
    }

    //filter by bound location
    if (count($general_filter) == 0) {
      $ne_lat = floatval($location['ne_lat']);
      $ne_lng = floatval($location['ne_lng']);
      $sw_lng = floatval($location['sw_lng']);
      $sw_lat = floatval($location['sw_lat']);

      $scaleX = 1 / 20;
      $scaleY = 1 / 20;
      $ne_lat1 = $ne_lat + ($sw_lat - $ne_lat) * $scaleX;
      $sw_lat1 = $ne_lat + ($sw_lat - $ne_lat) * (1 - $scaleX);

      $ne_lng1 = $ne_lng + ($sw_lng - $ne_lng) * $scaleY;
      $sw_lng1 = $ne_lng + ($sw_lng - $ne_lng) * (1 - $scaleY);

      // ____ location filter
      $general_filter[] = "lat > $sw_lat1";
      $general_filter[] = "lat < $ne_lat1";
      $general_filter[] = "lng > $sw_lng1";
      $general_filter[] = "lng < $ne_lng1";
    }


    //filter
    if (!empty($filter['type'])) {
      $general_filter[] = 'type_item = ' . intval($filter['type']);
    }

    if (!empty($filter['price'])) {
      $filter_price = Item::getFilterPrice($filter['price']);
      if (!empty($filter_price)) {
        $general_filter[] = 'price_total' . $filter_price;
      }
    }

    if (!empty($filter['area'])) {
      $filter_area = Item::getFilterArea($filter['area']);
      if (!empty($filter_area)) {
        $general_filter[] = 'area' . $filter_area;
      }
    }
    //chi show item da dc approvel
    $general_filter['status'] = 'status=' . ItemStatus::$Approved;

    $items = $this->item_model->getByLocation($general_filter);

    $length = count($items);
    for ($i = 0; $i < $length; $i++) {
      $items[$i]->link = Item::getLink($items[$i]);
    }
    $this->response(0, 'Success', $items);
  }
}
