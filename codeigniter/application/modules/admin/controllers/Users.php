<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

	public function __construct() {
		parent::__construct ('admin');
		$this->load->helper('admin_helper');
		Admin_Helper::ins()->adminLogin();
		$this->load->model('user_model');
	}
	public function registers(){
		$this->index(UserRole::$REGISTER);
	}
	public function managers(){
		$this->index(UserRole::$MANAGER);
	}
	public function index($role = 1){

		$field = $this->gget('field', 'id');
		$sort = $this->gget('sort', 'desc');
		$order = 'u.'.$field.' '.$sort;

		$this->load->model ('city_model');
		$this->load->model ('item_model');
		$resp['cities'] = $this->city_model->getList();
		

		$filter = array();
		$filter[] = 'user_role = '.intval($role);
		$sub_link = "admin/users";
		if($role == UserRole::$MANAGER)
			$sub_link = "admin/managers";

		$query = Util::removeParamsQueryString('per_page');
		$query = (trim($query) != '') ? '?'.$query : '';

		$cfg_page = $this->load_pagination($sub_link.$query);
		$cfg_page['page_query_string'] = TRUE;
		$cfg_page['per_page'] = CONSTANT::$PER_ROW_PAGE_ADMIN;
		$page = $this->gget('per_page', 0);

		$resp['role'] = $role;
		$resp['users'] = $this->user_model->getListWP($filter,  $order, $cfg_page["per_page"], $page, $cfg_page["total_rows"]);
		$resp['count'] = $cfg_page["total_rows"];
		$this->pagination->initialize($cfg_page);
		$resp['pagination'] = $this->pagination->create_links();

		if(UserRole::$MANAGER == $role && count($resp['users'])){
			$items_approved_month = $this->item_model->itemsApprovedPerMonth();
			$items_approved = $this->item_model->itemsApproved();

			$user_items = array();
			if(count($items_approved)){
				
				foreach ($items_approved as $key => $item) {
					$user_items[$item->approval_id] = array('num_month'=>0, 'total'=>$item->total);
				}

				if(count($items_approved_month)){
					foreach ($items_approved_month as $key => $item) {
						$user_items[$item->approval_id]['num_month']=$item->total;
					}
				}

			}
			$resp['user_items'] = $user_items;
			
		}

		$this->template->load('admin/user/list', $resp);
	}

	public function form($id = 0){
		$this->load->model ('city_model');
		if(count($_POST)){
			$pid = $this->user_save();
			if($pid)
				Admin_Helper::ins()->setMessage('Info was saved successfully.', 'success');
			else
				Admin_Helper::ins()->setMessage('Info item was saved unsuccessfully.', 'danger');
			redirect(base_url('/admin/users/form/'.$id));
		}

		$user = $this->user_model->getById($id);

		if($id != 0 && $user == null)
			show_404();
		if($user == null)
			$user = new user_model();
		$resp['cities'] = $this->city_model->getList();
		$user->locations = explode(',', $user->locations);
		$tmp = array();
		foreach ($user->locations as $key => $val) {
			$tmp[$val] = $val;
		}
		$user->locations = $tmp;
		
		$resp['user'] = $user;
		$this->template->load('admin/user/form', $resp);
	}

	private function user_save(){
		$params['id'] = $this->gpost('id', null);
		$params['full_name'] = $this->gpost('full_name', '');
		$params['email'] = $this->gpost('email', '');
		$params['address'] = $this->gpost('address', '');
		$params['user_role'] = $this->gpost('user_role', 0);
		$locations = $this->gpost('location', null);
		$params['locations'] = implode(',', $locations);
		$id = $this->user_model->save($params);
		return $id;
	}
	
	public function remove($id){
		$this->user_model->delete($id);
		Admin_Helper::ins()->setMessage('User was removed successfully.', 'success');
		
		redirect(base_url('/admin/users/'));
	}
}