<?php
class Street_model extends CI_Model{

	protected $_name = 'bds_add_level_4';
	var $id;
	var $name;
	var $lvl1_id;
	var $lvl2_id;
	var $lvl3_id;

	function Street_model(){
		parent::__construct();
		$this->load->database();
	}

	function getById($id){
		$sql = "SELECT * FROM {$this->_name} WHERE id = ".$this->db->escape($id);
		$query = $this->db->query($sql);
		return $query->row();
	}

	function getList(){
		$sql = "SELECT * FROM {$this->_name} ORDER BY name asc";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function getListWP($filter, $order, $limit = 0, $start = 0, &$total=0){
		if($limit){
			$select = 'count(s.id) as total';
			$query = $this->db->query($this->queryItems($select, $filter));
			$total = $query->row()->total;
		}

		$select = 's.*, d.name as district';
		$query = $this->db->query($this->queryItems($select, $filter, $order, array($start, $limit)));
		return $query->result();
	}

	function queryItems($select, $filter, $order = null, $pagination = null){

		$sql = "SELECT {$select} FROM {$this->_name} as s 
				LEFT JOIN bds_add_level_2 as d on d.id = s.lvl2_id";
		if($filter['district'])
			$sql.=" WHERE s.lvl2_id = ".intval($filter['district']);
		else if($filter['city'])
			$sql.=" WHERE s.lvl1_id = ".intval($filter['city']);

		if($pagination[1] != 0){
			$sql.=" order by {$order}  limit {$pagination[0]}, {$pagination[1]}";
		}
		return $sql;
	}

	function save($params = null){
		
		if(empty($params['id'])){
			$this->db->insert($this->_name, $params);
			return $this->db->insert_id();
		}else{
			$this->db->where('id', $params['id']);
			if($this->db->update($this->_name, $params)) 
				return $params['id'];
			else
				return 0;
		}
	}

	function delete($id){
		return $this->db->delete($this->_name, array('id' => $id)); 
	}

	function getByDistrict($district){
		$sql = "SELECT * FROM {$this->_name} WHERE lvl2_id = %s ORDER BY name asc";
		$sql = sprintf($sql, intval($district));
		$query = $this->db->query($sql);
		return $query->result();
	}

	function getByWards($wards){
		$sql = "SELECT * FROM {$this->_name} WHERE lvl3_id IN (%s) ORDER BY name asc";
		$sql = sprintf($sql, $wards);
		$query = $this->db->query($sql);
		return $query->result();
	}
}