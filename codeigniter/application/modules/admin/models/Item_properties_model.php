<?php
class Item_properties_model extends CI_Model{
	protected $_name = 'bds_item_properties';
	var $item_id;
	var $total_area;
	var $building_density;
	var $num_floor;
	var $total_area_building;
	var $num_bedrooms;
	var $orientation_house;
	var $belong_project;
	var $red_book;
	var $pink_book;
	var $investment_certificate;
	var $implementation_planning;
	var $finished_planning;
	var $during_compensation;
	var $done_compensation;
	var $done_red_book;
	var $done_obligation_finance;
	var $under_construction;
	var $done_building;
	var $on_marketing;

	function Item_properties_model(){
		parent::__construct();
		$this->load->database();
	}

	function getById($id){
		$sql = "SELECT * FROM {$this->_name} WHERE item_id = ".intval($id);
		$query = $this->db->query($sql);
		return $query->row();
	}

	function save($item_id){
		$params = $this->getPostParams();
		$params['item_id'] = $item_id;

		if($this->getById($params['item_id']) == null){
			$this->db->insert($this->_name, $params);
			return $this->db->insert_id();
		}else
		{
			$this->db->where('item_id', $params['item_id']);
			if($this->db->update($this->_name, $params)) 
				return $params['item_id'];
			else
				return 0;
		}
	}

	private function getPostParams(){
		$params = array();

		$params['total_area'] = Util::gpost('total_area', null);
		$params['building_density'] = Util::gpost('building_density', null);
		$params['num_floor'] = Util::gpost('num_floor', null);
		$params['total_area_building'] = Util::gpost('total_area_building', null);
		$params['num_bedrooms'] = Util::gpost('num_bedrooms', null);
		$params['orientation_house'] = Util::gpost('orientation_house', null);
		$params['belong_project'] = Util::gpost('belong_project', null);
		$params['red_book'] = Util::gpost('red_book', null);
		$params['pink_book'] = Util::gpost('pink_book', null);
		$params['investment_certificate'] = Util::gpost('investment_certificate', null);
		$params['implementation_planning'] = Util::gpost('implementation_planning', null);
		$params['finished_planning'] = Util::gpost('finished_planning', null);
		$params['during_compensation'] = Util::gpost('during_compensation', null);
		$params['done_compensation'] = Util::gpost('done_compensation', null);
		$params['done_red_book'] = Util::gpost('done_red_book', null);
		$params['done_obligation_finance'] = Util::gpost('done_obligation_finance', null);
		$params['under_construction'] = Util::gpost('under_construction', null);
		$params['done_building'] = Util::gpost('done_building', null);
		$params['on_marketing'] = Util::gpost('on_marketing', null);

		return $params;
	}
}