<?php
class Admin_model extends CI_Model{
	
	protected $_name = 'bds_admin';

	function Admin_model(){
		parent::__construct();
		$this->load->database();
	}

	function signin($username, $password){
	
		$sql = "SELECT * FROM {$this->_name} where username=".$this->db->escape($username)."";

		$query = $this->db->query($sql);
		$user = $query->row();
		if($user != null && password_verify($password, $user->password))
			return $user;
		else
			return null;
	
	}
}