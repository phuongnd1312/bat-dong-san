<?php
class User_model extends CI_Model{
	
	protected $_name = 'bds_users';
	var $id;
	var $full_name;
	var $name_alias;
	var $birthday;
	var $gender;
	var $address;
	var $username;
	var $password;
	var $user_role;
	var $email;
	var $mobile;
	var $locations;

	function User_model(){
		parent::__construct();
		$this->load->database();
	}

	function getById($id){
		$sql = "SELECT * FROM {$this->_name} where id=".$this->db->escape($id);

		$query = $this->db->query($sql);
		return $query->row();
	}

	function save($params = null){
		
		if(empty($params['id'])){
			$this->db->insert($this->_name, $params);
			return $this->db->insert_id();
		}else{
			$this->db->where('id', $params['id']);

			return $this->db->update($this->_name, $params); 
		}
		
	}

	function getByCode($code){
		$sql = "SELECT * FROM {$this->_name} where activation_code=".$this->db->escape($code);

		$query = $this->db->query($sql);
		return $query->row();
	}


	function getListWP($filter, $order, $limit = 0, $start = 0, &$total=0){
		if($limit){
			$select = 'count(u.id) as total';
			$query = $this->db->query($this->queryItems($select, $filter));
			$total = $query->row()->total;
		}

		$select = 'u.*';
		$query = $this->db->query($this->queryItems($select, $filter, $order, array($start, $limit)));
		return $query->result();
	}

	function queryItems($select, $filter, $order = null, $pagination = null){

		$sql = "SELECT {$select} FROM {$this->_name} as u ";

		if(count($filter)){
			$where = array();
			foreach ($filter as $key => $value) {
				$where[] = $value;
			}
			$sql.=" WHERE ".implode(" AND ", $where);
		}

		if($pagination[1] != 0){
			$sql.=" order by {$order}  limit {$pagination[0]}, {$pagination[1]}";
		}
		return $sql;
	}

	function delete($id){
		return $this->db->delete($this->_name, array('id' => $id)); 
	}
}