<?php
class City_model extends CI_Model{

	protected $_name = 'bds_add_level_1';
	var $id;

	function City_model(){
		parent::__construct();
		$this->load->database();
	}

	function getById($id){
		$sql = "SELECT * FROM {$this->_name} WHERE id = ".$this->db->escape($id);
		$query = $this->db->query($sql);
		return $query->row();
	}

	function getList(){
		$sql = "SELECT * FROM {$this->_name} ORDER BY ordering asc, name asc";
		$query = $this->db->query($sql);
		return $query->result();
	}
}