<?php
class Item_type_model extends CI_Model{

	protected $_name = 'bds_item_type';
	var $id;

	function Item_type_model(){
		parent::__construct();
		$this->load->database();
	}

	function getList(){
		$sql = "SELECT * FROM {$this->_name}";
		$query = $this->db->query($sql);
		return $query->result();
	}
}