<?php
class Project_model extends CI_Model
{

	protected $_name = 'bds_projects';
	var $id;
	var $type_project;
	var $name;
	var $description;
	var $address;
	var $city_id;
	var $district_id;

	var $phone;
	var $website;
	var $email;
	var $fax;
	var $photo;
	var $infrastructure;

	var $invest_address;
	var $invest_phone;
	var $invest_fax;
	var $invest_website;
	var $invest_email;
	var $invest_photo;

	function Project_model()
	{
		parent::__construct();
		$this->load->database();
	}

	function getById($id)
	{
		$sql = "SELECT * FROM {$this->_name} WHERE id = " . $this->db->escape($id);
		$query = $this->db->query($sql);
		return $query->row();
	}

	function getList()
	{
		$sql = "SELECT * FROM {$this->_name}";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function getListWP($filter, $order, $limit = 0, $start = 0, &$total = 0)
	{
		if ($limit) {
			$select = 'count(p.id) as total';
			$query = $this->db->query($this->queryItems($select, $filter));
			$total = $query->row()->total;
		}

		$select = 'p.*, d.name as district, c.name as city';
		$query = $this->db->query($this->queryItems($select, $filter, $order, array($start, $limit)));
		return $query->result();
	}

	function queryItems($select, $filter, $order = null, $pagination = null)
	{

		$sql = "SELECT {$select} FROM {$this->_name} as p 
				LEFT JOIN bds_add_level_1 as c on c.id = p.city_id
				LEFT JOIN bds_add_level_2 as d on d.id = p.district_id";
		$where = array();
		if ($filter['city'])
			$where[] = "p.city_id = " . $filter['city'];
		if ($filter['district'])
			$where[] = "p.district_id = " . $filter['district'];

		if (count($where))
			$sql .= ' WHERE ' . implode(' AND ', $where);

		if ($pagination[1] != 0) {
			$sql .= " order by {$order}  limit {$pagination[0]}, {$pagination[1]}";
		}
		return $sql;
	}

	function save($params = null)
	{

		if (empty($params['id'])) {
			$this->db->insert($this->_name, $params);
			return $this->db->insert_id();
		} else {
			$this->db->where('id', $params['id']);
			if ($this->db->update($this->_name, $params))
				return $params['id'];
			else
				return 0;
		}
	}

	function delete($id)
	{
		return $this->db->delete($this->_name, array('id' => $id));
	}
}
