<?php
class Orientation_house_model extends CI_Model{

	protected $_name = 'bds_orientation_house';
	var $id;

	function Orientation_house_model(){
		parent::__construct();
		$this->load->database();
	}

	function getList(){
		$sql = "SELECT * FROM {$this->_name}";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function getById($id){
		$sql = "SELECT * FROM {$this->_name} WHERE id = ".intval($id);
		$query = $this->db->query($sql);
		return $query->row();
	}
}