<?php
class Crawl_model extends CI_Model{

	function Crawl_model(){
		parent::__construct();
		$this->load->database();
	}

	function getDistrict($city, $district){
		$sql = "SELECT d.id as district, c.id as city from bds_add_level_2 as d
				INNER JOIN bds_add_level_1 as c on c.id = d.lvl1_id
				WHERE c.name = %s AND d.name = %s";
		$sql = sprintf($sql, $this->db->escape($city), $this->db->escape(trim($district)));
		$query = $this->db->query($sql);

		return $query->row();
	}

	function insertStreet($district, $city, $streets){
		$sql = "SELECT id FROM bds_add_level_4 WHERE lvl2_id = $district";
		$query = $this->db->query($sql);
		if($query->row() != null)
			return -1;
		$data = array();
		foreach ($streets as $key => $street) {
			$data[] = array('name'=>$street, 'lvl1_id'=>$city, 'lvl2_id'=>$district);
		}

		return $this->db->insert_batch('bds_add_level_4', $data);
	}

	function log($city, $district, $success, $msg){
		$params = array();
		$params['city'] = $city;
		$params['district'] = $district;
		$params['success'] = $success;
		$params['message'] = $msg;
		$this->db->insert('bds_logs', $params);
	}

	function getNumCrawl(){
		$sql = 'SELECT count(id) as total FROM bds_items WHERE bds_id != 0';
		$query = $this->db->query($sql);
		return $query->row()->total;
	}

	function getNumCrawlApprove(){
		$sql = 'SELECT count(id) as total FROM bds_items WHERE bds_id != 0 AND status != 0';
		$query = $this->db->query($sql);
		return $query->row()->total;
	}
}