<?php
class Properties_proportion_model extends CI_Model{
	protected $_name = 'bds_properties_proportion';
	var $id;

	function Properties_proportion_model(){
		parent::__construct();
		$this->load->database();
	}

	function getList(){
		$sql = "SELECT * FROM {$this->_name}";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function getById($id){
		$sql = "SELECT * FROM {$this->_name} WHERE id = ".intval($id);
		$query = $this->db->query($sql);
		return $query->row();
	}
}