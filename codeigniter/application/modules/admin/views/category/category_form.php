<?php 
function showCategories($categories, $parent_id = 0, $ignore = null, $cur_parent = null, $char = '')
{
    foreach ($categories as $key => $item)
    {
        if ($item->parent_id == $parent_id && $item->id != $ignore)
        {   
        	$selected = '';
        	if($cur_parent == $item->id)
        		$selected = ' selected="selected" ';
        	?>
            <option <?php echo $selected; ?> value="<?php echo $item->id; ?>">
                <?php echo $char.$item->name_vi; ?>
            </option>
        <?php     
            unset($categories[$key]);
            showCategories($categories, $item->id, $ignore, $cur_parent, $char.'|---');
        }
    }
}

$subhead = ($category->id == null) ? 'Create New' : 'Edit' ?>
<div id='wrap_category_form'>
    <div class="row">
        <div class="col-lg-12">
			<h1 class="page-header">
			    Category Form
			    <small><?php echo $subhead; ?></small>
			</h1>
			<ol class="breadcrumb">
                <li>
                    <i class="fa fa-fw fa-table"></i>  <a href="<?php echo base_url('/admin_man/categories'); ?>">Categories</a>
                </li>
                <li class="active">
                    <i class="fa fa-fw fa-edit"></i> Form
                </li>
            </ol>


			<form action='' method='post' id='frm_category'  enctype="multipart/form-data">
				<div class='row'>
					<div class='col-sm-12'>
						<div class='form-group'>
							<label>Name(vi)</label>
							<input class='form-control' type="text" name='name_vi' value="<?php echo $category->name_vi; ?>" required/>
						</div>

						<div class='form-group'>
							<label>Name(en)</label>
							<input class='form-control' type="text" name='name_en' value="<?php echo $category->name_en; ?>"/>
						</div>

						<div class='form-group'>
							<label>Parent</label>
							<select class='form-control' name='parent_id' required>
								<option value='0'>--Select parent--</option>
								<?php showCategories($categories, 0, $category->id, $category->parent_id); ?>
							</select>	
						</div>
					</div>
					
				</div>
				

				<div class='form-group text-center'>
					<input type='reset' value='Reset' class='btn btn-default' /> 
					<input type='submit' value='Submit' class='btn btn-primary'  name='submit' /> 
				</div>
				<?php if($category->id != null){ ?>
					<input type="hidden" value='<?php echo $category->id; ?>' name='id' />
				<?php
				} ?>
				
			</form>	
		</div>	
	</div>
</div>

 <script type="text/javascript">


</script>