<?php

function showCategories($categories, $parent_id = 0, $char = '')
{
    foreach ($categories as $key => $item)
    {
        if ($item->parent_id == $parent_id)
        {   ?>
            <tr>
                <td width="10px"><?php echo $item->id; ?></td>
                <td><a href='<?php echo base_url('/admin_man/category/'.$item->id); ?>'>
                    <?php echo $char.$item->name_vi; ?></a>
                </td>
                <td><a href='<?php echo base_url('/admin_man/category/'.$item->id); ?>'>
                    <?php echo $item->name_en; ?></a>
                </td>
                <td align='center'>
                    <a href='<?php echo base_url('/admin_man/category_remove/'.$item->id); ?>' onclick='return onRemove();'>
                        <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
                    </a>
                </td>
            </tr>
        <?php     
            unset($categories[$key]);
            showCategories($categories, $item->id, $char.'|---');
        }
    }
}
?>

<div id='wrap_competitions'>
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Categories
            </h1>
        </div>  
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class='bound_control'>
                <a href='<?php echo base_url('/admin_man/category/'); ?>' class='btn btn-info'>Create New</a>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name(vi)</th>
                            <th>Name(en)</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                         <?php showCategories($categories); ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class='wrap_pagination' style='text-align:center;'>
            <div class='pagination' >
                <?php //echo $pagination; ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function onRemove(){
        return confirm("Do you want to remove it?");
    }
</script>