<?php
$subhead = ($item->id == null) ? 'Create New' : 'Edit' ?>
<div id='wrap_item_form'>
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">
        Item Form
        <small><?php echo $subhead; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li class='control text-right'>
          <a href='<?php echo base_url('/admin_items'); ?>' class='btn btn-danger'>Back</a>
          <a href='<?php echo base_url('/admin_items/form'); ?>' class='btn btn-primary'>Create New</a>
        </li>
      </ol>


      <form action='' method='post' id='frm_item' enctype="multipart/form-data">
        <div class="panel panel-info">
          <div class="panel-heading">Thông tin mô tả</div>
          <div class="panel-body">
            <div class='date_post_item text-center'>
              <table style="display:inline-block">
                <tr>
                  <td>
                    <label>Ngày bắt đầu *</label>
                    <input type='text' name='date_started' id='picker_date_started' value='<?php echo Util::formatDateDMY($item->date_started); ?>' required />
                  </td>
                  <td width="20px"></td>
                  <td>
                    <label>Ngày kết thúc *</label>
                    <input type='text' name='date_ended' id='picker_date_ended' value='<?php echo Util::formatDateDMY($item->date_ended); ?>' required />
                  </td>
                </tr>
              </table>
            </div>

            <div class='form-group'>
              <label>Tiêu đề *</label>
              <input name='title' class='form-control' value='<?php echo $item->title; ?>' required />
            </div>

            <div class='form-group'>
              <label>Nội dung tin đăng *</label>
              <textarea name='description' rows="8" class='form-control' required><?php echo $item->description; ?></textarea>
            </div>

            <div class='photos'>
              <label>Hình ảnh</label>
              <input id="field_file_upload" type="file" name="file_upload" url='<?php echo base_url('/admin_item/upload_img/'); ?>' />
              <div class="bound_photos">
                <?php
                $photos = Item::photos($item);
                $length = count($photos);
                for ($i = 0; $i < $length; $i++) {

                  ?>
                  <div class='bound_item_file photo'>
                    <img class='photo_item' src='<?php echo Util::loadImg($photos[$i]); ?>' url='<?php echo $photos[$i]; ?>' />
                    <a href="javascript:;" onclick="removePhoto(this, '<?php echo $photos[$i]; ?>');" class="remove"></a>
                  </div>
                <?php
              }
              ?>
                <div class='bound_item_file' id='control_upload'>
                  <i class="fa fa-plus"></i>
                </div>
                <div class='clear'></div>
              </div>
            </div>
          </div>
        </div>

        <div class="panel panel-info">
          <div class="panel-heading">Thông tin cơ bản</div>
          <div class="panel-body row">
            <div class='col-xs-6'>
              <div class='form-group'>
                <label>Hình thức *</label>
                <select name='type_item' class='form-control' required>
                  <option value=''>--Chọn hình thức--</option>
                  <?php foreach ($type_items as $val) {
                    $select = ($val['id'] == $item->type_item) ? ' selected="selected" ' : '';
                    ?>
                    <option <?php echo $select; ?> value='<?php echo $val['id']; ?>' placeholder='<?php echo $val['default_days']; ?>'>
                      <?php echo $this->lang->line($val['code']); ?>
                    </option>
                  <?php
                } ?>
                </select>
              </div>

              <div class='form-group'>
                <label>Tỉnh/Thành phố *</label>
                <select name='add_level_1' class='form-control' id='select_city' required>
                  <option value="">--Chọn tỉnh/thành--</option>
                  <?php foreach ($cities as $city) :
                    $select = ($city->id == $item->add_level_1) ? ' selected="selected" ' : '';
                    ?>
                    <option <?php echo $select; ?> value="<?php echo $city->id; ?>"><?php echo $city->name; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>

              <div class='form-group'>
                <label>Phường/Xã *</label>
                <select name='add_level_3' class='form-control' id='select_ward' required>
                  <option value="">--Chọn phường/xã--</option>
                  <?php foreach ($wards as $ward) :
                    $select = ($ward->id == $item->add_level_3) ? ' selected="selected" ' : '';
                    ?>
                    <option <?php echo $select; ?> value="<?php echo $ward->id; ?>"><?php echo $ward->name; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>

              <div class='form-group'>
                <label>Giá *</label>
                <input type="number" name='price_total' class='form-control' value='<?php echo $item->price_total; ?>' required />
              </div>


            </div>

            <div class='col-xs-6'>
              <div class='form-group'>
                <label>Loại *</label>
                <select name='project_type' class='form-control' required>
                  <option value=''>--Chọn loại--</option>
                  <?php foreach ($properties_proportion as $val) {
                    $select = ($val->id == $item->project_type) ? ' selected="selected" ' : '';
                    ?>
                    <option <?php echo $select; ?>value='<?php echo $val->id; ?>'><?php echo $val->type; ?></option>
                  <?php
                } ?>
                </select>
              </div>

              <div class='form-group'>
                <label>Quận/Huyện *</label>
                <select name='add_level_2' class='form-control' id='select_district' required>
                  <option value="">--Chọn phường/xã--</option>
                  <?php foreach ($districts as $district) :
                    $select = ($district->id == $item->add_level_2) ? ' selected="selected" ' : '';
                    ?>
                    <option <?php echo $select; ?> value="<?php echo $district->id; ?>"><?php echo $district->name; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>

              <!--<div class='form-group'>
								<label>Đường phố</label>
								<input type="text" name='add_street' class='form-control' value='<?php echo $item->add_street; ?>' id='field_add_street' />
							</div>-->

              <div class='form-group'>
                <label>Diện tích/m²</label>
                <input type="text" name='area' value='<?php echo $item->area; ?>' class='form-control' />
              </div>

            </div>
            <div class='col-xs-12'>
              <div class='form-group'>
                <label>Địa chỉ *</label>
                <input type="text" name='full_address' value='<?php echo $item->full_address; ?>' class='form-control' id='field_full_address' required />
              </div>
            </div>
          </div>
        </div>

        <div class="panel panel-info">
          <div class="panel-heading">Thông tin liên hệ</div>
          <div class="panel-body row">
            <div class='col-xs-6'>
              <div class='form-group'>
                <label>Tên liên hệ *</label>
                <input type="text" name='contact_name' value='<?php echo $item->contact_name; ?>' class='form-control' required />
              </div>

              <div class='form-group'>
                <label>Điện thoại *</label>
                <input type="text" name='contact_phone' value='<?php echo $item->contact_phone; ?>' class='form-control' required />
              </div>

              <div class='form-group'>
                <label>Công ty</label>
                <input type="text" name='contact_company' value='<?php echo $item->contact_company; ?>' class='form-control' />
              </div>
            </div>

            <div class='col-xs-6'>
              <div class='form-group'>
                <label>Email *</label>
                <input type="email" name='contact_email' value='<?php echo $item->contact_email; ?>' class='form-control' required />
              </div>

              <div class='form-group'>
                <label>Zalo</label>
                <input type="text" name='contact_zalo' value='<?php echo $item->contact_zalo; ?>' class='form-control' />
              </div>
            </div>
          </div>
        </div>

        <div class="panel panel-info">
          <div class="panel-heading">Bản đồ</div>
          <div class="panel-body">
            <p>
              Để tăng độ tin cậy và tin rao được nhiều người quan tâm hơn, hãy sửa vị trí tin rao của bạn trên bản đồ bằng cách kéo icon location tới đúng vị trí của tin rao.
            </p>
            <div id='map_canvas' style="height: 350px;"></div>
          </div>
        </div>

        <div class='form-group text-right'>
          <input type="hidden" name='lat' id='field_latitude' value='<?php echo $item->lat; ?>' />
          <input type="hidden" name='lng' id='field_longitude' value='<?php echo $item->lng; ?>' />
          <input type='hidden' name='photos' id='field_photos' value='' />
          <input type='reset' value='Reset' class='btn btn-default' />
          <input type='submit' value='Submit' class='btn btn-primary' onclick="return onSubmitItem();" />
        </div>
        <?php if ($item->id != null) { ?>
          <input type="hidden" value='<?php echo $item->id; ?>' id='item_id' name='id' />
        <?php
      } ?>
      </form>

    </div>
  </div>
</div>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false&libraries=places,geometry"></script>
<script type="text/javascript">
  var field_latitude = $('#field_latitude');
  var field_longitude = $('#field_longitude');
  var select_city = $('#select_city');
  var select_district = $('#select_district');
  var select_ward = $('#select_ward');
  var field_full_address = $('#field_full_address');
  var field_add_street = $('#field_add_street');
  var option_district = '<option value="">--Chọn quận/huyện--</option>';
  var option_ward = '<option value="">--Chọn phường/xã--</option>';

  var position = {
    lat: ($.trim(field_latitude.val()) == '') ? 10.771473 : Number(field_latitude.val()),
    lng: ($.trim(field_longitude.val()) == '') ? 106.65302399999996 : Number(field_longitude.val())
  };

  var geocoder = new google.maps.Geocoder();
  var map = new google.maps.Map(document.getElementById('map_canvas'), {
    center: position,
    zoom: 15
  });
  var marker = new google.maps.Marker({
    position: position,
    map: map,
    draggable: true,
    animation: google.maps.Animation.DROP
  });
  google.maps.event.addListener(marker, 'dragend', function() {
    geocodePosition(marker.getPosition());
  });
  var infowindow = new google.maps.InfoWindow({
    content: field_full_address.val()
  });

  function geocodePosition(pos) {
    geocoder.geocode({
      latLng: pos
    }, function(responses) {
      if (responses && responses.length > 0) {
        infowindow.setContent(responses[0].formatted_address);
        map.setCenter(marker.getPosition());
      } else {
        console.log('Cannot determine address at this location.');
      }
    });
  }

  function renderUploadItem(error_code, param) {
    var html = '<div class="bound_item_file photo">';
    if (error_code == 0)
      html += '<img class="photo_item" src="' + BASE_URL + '/public' + param + '" url="' + param + '" />';
    else
      html += '<span class="error_msg">' + param + '</span>';
    html += '<a href="javascript:;" onclick="removePhoto(this, \'' + param + '\')" class="remove"></a></div>';
    html += '</div>';
    $('#control_upload').before($(html));
  }

  function removePhoto(obj, path) {
    if (confirm("Do you want to remove item")) {
      $(obj).parent().remove();
      /*API.removePhotoItem({path_file: path}, function(resp){
      	console.log(resp);
      });*/
    }
  }


  function getDistrict(id) {
    API.getDistrictByCity(id, function(resp) {
      var data = resp.data;
      var length = data.length;
      var html = option_district;
      for (i = 0; i < length; i++) {
        html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
      }
      select_district.html(html);
    });
  }

  function getWards(id) {
    API.getWardByDistrict(id, function(resp) {
      var data = resp.data;
      var length = data.length;
      var html = option_ward;
      for (i = 0; i < length; i++) {
        html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
      }
      select_ward.html(html);
    });
  }

  function renderAddress() {
    var address = [];
    if ($.trim(field_add_street.val()) != '')
      address.push(field_add_street.val());

    if (select_ward.val() != '')
      address.push(select_ward.find('option:selected').html());
    if (select_district.val() != '')
      address.push(select_district.find('option:selected').html());
    if (select_city.val() != '') {
      address.push(select_city.find('option:selected').html());
      geocoderAddress();
    }
    field_full_address.val(address.join(', '));
  }

  function geocoderAddress() {
    var objFilter = new Object();
    objFilter.address = field_full_address.val();
    if ($.trim(objFilter.address) == '')
      return;

    geocoder.geocode(objFilter, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        infowindow.setContent(results[0].formatted_address);
        map.setCenter(results[0].geometry.location);
        marker.setPosition(results[0].geometry.location);
      } else {
        console.log('Geocode was not successful for the following reason: ' + status);
      }
    });
  }

  function setlatlng() {
    field_latitude.val(marker.getPosition().lat());
    field_longitude.val(marker.getPosition().lng());
  }

  function setPhoto() {
    var tmp = [];
    $('img.photo_item').each(function(index) {
      tmp.push($(this).attr('url'));
    });
    $('#field_photos').val(tmp.join(';'));
  }


  function onSubmitItem() {
    setlatlng();
    setPhoto();
    return true;
  }

  $('#control_upload').click(function() {
    $('#field_file_upload').trigger("click");
  });

  $('#field_file_upload').change(function() {
    var file_data = $(this).prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
      url: $('#field_file_upload').attr('url'), // point to server-side PHP script 
      dataType: 'text', // what to expect back from the PHP script, if anything
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      success: function(resp) {
        //console.log(resp);
        resp = jQuery.parseJSON(resp);
        renderUploadItem(resp.error_code, resp.data);
      }
    });
    $('#field_file_upload').val('');
  });

  /*----------Init--------------*/
  infowindow.open(map, marker);

  $('#picker_date_started').datepicker({
    format: "dd/mm/yyyy",
    startDate: "+0d",
  });
  $('#picker_date_ended').datepicker({
    format: "dd/mm/yyyy",
    startDate: "+30d",
  });

  $(document).on("click", "span.remove_photo_item", function() {
    removePhoto($(this).parent().index());
  });

  select_city.change(function() {
    getDistrict(select_city.val());
    select_ward.html(option_ward);
    select_district.html(option_ward);
    renderAddress();
  });
  select_district.change(function() {
    getWards(select_district.val());
    select_ward.html(option_ward);
    renderAddress();
  });
  select_ward.change(function() {
    renderAddress();
  });
  field_add_street.change(function() {
    renderAddress();
  });
</script>