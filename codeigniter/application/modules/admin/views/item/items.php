<div id='wrap_competitions'>
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
            <?php $length = count($items);?>
                Items(<span><?php echo $length;?></span>)
            </h1>
        </div>  
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class='bound_control' style="margin-bottom: 20px;">
                <a href='<?php echo base_url('/admin/items/form/'); ?>' class='btn btn-info'>Create New</a>
            </div>
            <form id='frm_filter_item' action='' method="get">
                <ol class="breadcrumb">
                     <li class='control'>
                        <select name='city' class='form-control' id='select_city'>
                            <option value="">--Status--</option>
                        <?php
                            foreach (PostStatus::$Detail as $key => $val) { 
                               // $selected = ($key == $user->gender) ? ' selected="selected" ' : '';
                            ?>
                                <option <?php echo //$selected; ?> value='<?php echo $key; ?>'><?php echo $this->lang->line(Gender::$Detail[$key]); ?></option>
                        <?php
                            }
                        ?>
                        </select>
                    </li>

                     <li class='control'>
                        <select name='district' class='form-control' id='select_district'>
                            <option value="">--Need help--</option>
                            <?php 
                                $sel_district = Mhtml::gget('district', 0);
                                foreach($districts as $district):
                                $select = ($district->id == $sel_district) ? ' selected="selected" ' : '';
                            ?>
                                <option <?php echo $select; ?> value="<?php echo $district->id; ?>"><?php echo $district->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </li>

                </ol>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Address</th>
                            <th>Start</th>
                            <th>End</th>
                            <th>Approved</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php 
                   
                            for($i=0; $i < $length; $i++){?>
                            <tr>
                                <td width="10px"><?php echo $items[$i]->id; ?></td>
                                <td><a href='<?php echo base_url('/admin/items/form/'.$items[$i]->id); ?>'><?php echo $items[$i]->title; ?></a></td>
                                <td><?php echo $items[$i]->full_address; ?></td>
                                <td><?php echo $items[$i]->date_started; ?></td>
                                <td><?php echo $items[$i]->date_ended; ?></td>
                                <td align='center'>
                                    <a href='<?php echo base_url('/admin/items/remove/'.$items[$i]->id); ?>' onclick='return onRemove();'>
                                        <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php
                            }
                         ?>
                        
                    </tbody>
                </table>
            </div>

            <div class='wrap_pagination' style='text-align:center;'>
                <div class='pagination' >
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function onRemove(){
        return confirm("Do you want to remove it?");
    }
</script>