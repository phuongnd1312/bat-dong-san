<?php 
echo Mhtml::tinymce();
$subhead = ($project->id == null) ? 'Create New' : 'Edit';
 ?>
<div id='wrap_item_form'>
    <div class="row">
        <div class="col-lg-12">
			<h1 class="page-header">
			    Project Form
			    <small><?php echo $subhead; ?></small>
			</h1>
			<ol class="breadcrumb">
                 <li class='control text-right'>
	            	<a href='<?php echo base_url('/admin/projects'); ?>' class='btn btn-danger'>Back</a>
	            	<a href='<?php echo base_url('/admin/projects/form'); ?>' class='btn btn-primary'>Create New</a>
	            </li>
            </ol>


			<form action='' method='post' id='frm_project' enctype="multipart/form-data">
				<div class='row'>
					<div class='col-sm-5'>
						<div class='form-group'>
							<label>Tỉnh/Thành phố *</label>
							<select name='city_id' class='form-control' id='select_city' required>
								<option value="">--Chọn tỉnh/thành--</option>
								<?php foreach($cities as $city):
									$select = ($city->id == $project->city_id) ? ' selected="selected" ' : '';
								?>
									<option <?php echo $select; ?> value="<?php echo $city->id; ?>"><?php echo $city->name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>

						<div class='form-group'>
							<label>Quận/Huyện *</label>
							<select name='district_id' class='form-control' id='select_district' required>
								<option value="">--Chọn quận/huyện--</option>
								<?php foreach($districts as $district):
									$select = ($district->id == $project->district_id) ? ' selected="selected" ' : '';
								?>
									<option <?php echo $select; ?> value="<?php echo $district->id; ?>"><?php echo $district->name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">Thông tin dự án</div>
							<div class="panel-body">
								<div class='form-group'>
									<label>Địa chỉ</label>
									<input class='form-control' type="text" name='address' value="<?php echo $project->address; ?>" required/>
								</div>

								<div class='form-group'>
									<label>Điện thoại</label>
									<input class='form-control' type="text" name='phone' value="<?php echo $project->phone; ?>"/>
								</div>

								<div class='form-group'>
									<label>Fax</label>
									<input class='form-control' type="text" name='fax' value="<?php echo $project->fax; ?>"/>
								</div>

								<div class='form-group'>
									<label>Email</label>
									<input class='form-control' type="text" name='email' value="<?php echo $project->email; ?>"/>
								</div>

								<div class='form-group'>
									<label>Website</label>
									<input class='form-control' type="text" name='website' value="<?php echo $project->website; ?>"/>
								</div>

								<div class='form-group'>
									<label>Photo</label>
									<?php 
									$src = '';
									if(!empty($project->photo)){
										$src = Util::loadImg($project->photo);
									}?>
									<br />
									<img id='project_photo' src='<?php echo $src; ?>' style='max-width:100%'/>
									<input class='form-control' type="file" accept="image/*" name='photo' value="<?php echo $src; ?>"  onchange="loadFile(event, 'project_photo')"/>
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">Chủ đầu tư</div>
							<div class="panel-body">
								<div class='form-group'>
									<label>Địa chỉ</label>
									<input class='form-control' type="text" name='invest_address' value="<?php echo $project->invest_address; ?>" required/>
								</div>

								<div class='form-group'>
									<label>Điện thoại</label>
									<input class='form-control' type="text" name='invest_phone' value="<?php echo $project->invest_phone; ?>"/>
								</div>

								<div class='form-group'>
									<label>Fax</label>
									<input class='form-control' type="text" name='invest_fax' value="<?php echo $project->invest_fax; ?>"/>
								</div>

								<div class='form-group'>
									<label>Email</label>
									<input class='form-control' type="text" name='invest_email' value="<?php echo $project->invest_email; ?>"/>
								</div>

								<div class='form-group'>
									<label>Website</label>
									<input class='form-control' type="text" name='invest_website' value="<?php echo $project->invest_website; ?>"/>
								</div>

								<div class='form-group'>
									<label>Photo</label>
									<?php 
									$src = '';
									if(!empty($project->invest_photo)){
										$src = Util::loadImg($project->invest_photo);
									}?>
									<br />
									<img id='invest_photo' src='<?php echo $src; ?>' style='max-width:100%'/>
									<input class='form-control' type="file" accept="image/*" name='invest_photo' value="<?php echo $src; ?>"  onchange="loadFile(event, 'invest_photo')"/>
								</div>
							</div>
						</div>

					</div>

					<div class='col-sm-7'>
						<div class='form-group'>
							<label>Tên</label>
							<input class='form-control' type="text" name='name' value="<?php echo $project->name; ?>" required/>
						</div>

						<div class='form-group'>
							<label>Mô tả</label>
							<textarea class='form-control' rows="20" name='description' id='edit_description'><?php echo $project->description; ?></textarea>
						</div>


						<div class='form-group'>
							<label>Hạ tầng - Quy hoạch</label>
							<textarea class='form-control' rows="20" name='infrastructure' id='edit_infrastructure'><?php echo $project->infrastructure; ?></textarea>
						</div>
					</div>
				</div>
				

				<div class='form-group text-center'>
					<input type='reset' value='Reset' class='btn btn-default' /> 
					<input type='submit' value='Submit' class='btn btn-primary'  name='submit' /> 
				</div>
				<?php if($project->id != null){ ?>
					<input type="hidden" value='<?php echo $project->id; ?>' name='id' />
				<?php
				} ?>
				
			</form>	
		
		</div>	
	</div>
</div>


<script type="text/javascript" src="<?php echo base_url('/public/templates/admin/js'); ?>/location.js"></script> 
<script type="text/javascript">
Editor.basic('edit_description');
Editor.basic('edit_infrastructure');

Loation.select_city = $('#select_city');
Loation.select_district = $('#select_district');

Loation.select_city.change(function(){
	getDistrict(Loation.select_city.val());
	Loation.select_ward.html(Loation.option_ward);
	Loation.select_district.html(Loation.option_ward);
});
var loadFile = function(event, id) {
	var output = document.getElementById(id);
    output.src = URL.createObjectURL(event.target.files[0]);
};
</script>
