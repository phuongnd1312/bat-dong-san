
<div id='wrap_competitions' class='list_items'>
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                Projects (<span><?php echo $count;?></span>)
            </h3>
        </div>  
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class='bound_control' style="margin-bottom: 20px;">
                <a href='<?php echo base_url('/admin/projects/form/'); ?>' class='btn btn-info'>Create New</a>
            </div>

            <form id='frm_filter_location' action='' method="get">
                <ol class="breadcrumb">
                     <li class='control'>
                        <select name='city' class='form-control' id='select_city'>
                            <option value="">--Chọn tỉnh/thành--</option>
                            <?php 
                            $sel_city = Mhtml::gget('city', 0);
                            foreach($cities as $city):
                                $select = ($city->id == $sel_city) ? ' selected="selected" ' : '';
                            ?>
                                <option <?php echo $select; ?> value="<?php echo $city->id; ?>"><?php echo $city->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </li>

                     <li class='control'>
                        <select name='district' class='form-control' id='select_district'>
                            <option value="">--Chọn quận/huyện--</option>
                            <?php 
                                $sel_district = Mhtml::gget('district', 0);
                                foreach($districts as $district):
                                $select = ($district->id == $sel_district) ? ' selected="selected" ' : '';
                            ?>
                                <option <?php echo $select; ?> value="<?php echo $district->id; ?>"><?php echo $district->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </li>
                </ol>
            </form>

            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th width="50"><?php Mhtml::sorting('ID','id'); ?></th>
                            <th><?php Mhtml::sorting('Name','name'); ?></th>
                            <th>Address</th>
                            <th  width="150">District</th>
                            <th  width="150">City</th>
                            <th  width="50">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php 
                            $length = count($projects);
                            for($i=0; $i < $length; $i++){?>
                            <tr>
                                <td align="center"><?php echo $projects[$i]->id; ?></td>
                                <td><a href='<?php echo base_url('/admin/projects/form/'.$projects[$i]->id); ?>'><?php echo $projects[$i]->name; ?></a></td>
                                <td><?php echo $projects[$i]->address; ?></td>
                                <td><?php echo $projects[$i]->district; ?></td>
                                <td><?php echo $projects[$i]->city; ?></td>
                                <td align='center'>
                                    <a href='<?php echo base_url('/admin/projects/remove/'.$projects[$i]->id); ?>' onclick='return onRemove();'>
                                        <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php
                            }
                         ?>
                        
                    </tbody>
                </table>
            </div>

            <div class='wrap_pagination' style='text-align:center;'>
                <div class='pagination' >
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('/public/templates/admin/js'); ?>/location.js"></script> 
<script type="text/javascript">
function onRemove(){
    return confirm("Do you want to remove it?");
}
Loation.select_city = $('#select_city');
Loation.select_district = $('#select_district');

Loation.select_city.change(function(){
    Loation.select_district.html(Loation.option_district);
    $('#frm_filter_location').submit();
});
Loation.select_district.change(function(){
    $('#frm_filter_location').submit();
});

</script>