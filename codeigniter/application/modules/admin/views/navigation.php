<?php 
    $menuItems = array(array('title'=>'Dashboard', 'link'=>'/admin/main', 'action'=>'index', 'ic'=>'fa-dashboard'),
                    array('title'=>'Projects', 'link'=>'/admin/projects', 'action'=>'index', 'ic'=>'fa-table'),
                    array('title'=>'Streets', 'link'=>'/admin/streets', 'action'=>'index', 'ic'=>'fa-table'),
                    array('title'=>'Managers', 'link'=>'/admin/users/managers', 'action'=>'index', 'ic'=>'fa-table'),
                    array('title'=>'Users', 'link'=>'/admin/users/registers', 'action'=>'index', 'ic'=>'fa-table'));
?>
 <!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo base_url('/admin/main'); ?>">Administrator</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Admin <b class="caret"></b></a>
            <ul class="dropdown-menu">
              
                <li>
                    <a href="<?php echo base_url('/admin/authen/logout'); ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <?php 
                foreach ($menuItems as $val) { 
                    $class = '';//($val['action'] == $action) ? 'active' : '';
                ?>
                    <li class='item <?php echo $class; ?>'>
                        <?php if(isset($val['child'])): ?>
                             <a href="javascript:;" data-toggle="collapse" data-target="#collapse_<?php echo $val['action'];?>">
                                <i class="fa fa-fw fa-arrows-v"></i>
                                <?php echo $val['title']; ?> <i class="fa fa-fw fa-caret-down"></i>
                             </a>
                             <ul id="collapse_<?php echo $val['action'];?>" class="collapse">
                             <?php foreach ($val['child'] as $item) { ?>
                                <li>
                                    <a href="<?php echo base_url($item['link']); ?>"><?php echo $item['title']; ?></a>
                                </li>
                             <?php
                             } ?>
                             </ul>
                        <?php else: ?>
                            <a href='<?php echo base_url($val['link']); ?>'>
                                <i class="fa fa-fw <?php echo $val['ic']; ?>"></i><?php echo $val['title']; ?>
                            </a>
                        <?php endif; ?>
                    </li>
            <?php } ?>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>
