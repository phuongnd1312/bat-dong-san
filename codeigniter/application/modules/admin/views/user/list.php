<div id='wrap_users' class='list_items'>
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                Users (<span><?php echo $count;?></span>)
            </h3>
        </div>  
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class='bound_control' style="margin-bottom: 20px;">
                <a href='<?php echo base_url('/admin/users/form/'); ?>' class='btn btn-info'>Create New</a>
            </div>
            <?php if($role == UserRole::$MANAGER){ ?>
            <div class="row" style="margin-left: 10px;">
                <div class='col-sm-12'>
                    <div class='row'>
                        <?php 
                        $locations='';
                        foreach ($users as $k=> $v) {
                            if($v->locations)
                            $locations.=','.$v->locations;
                        }
                        $locations=explode(',', $locations);
                            echo '<h4>Địa điểm chưa được quản lý</h4>';
                            foreach ($cities as $key => $value) {
                                if(!array_search($value->id, $locations)){
                                ?>
                                <div class='col-xs-3'>
                                    <p><?php echo $value->name; ?></p>
                                </div>
                        <?php
                            }
                            }                 
                        ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php /*
            <form id='frm_filter_user_role' action='' method="get">
                <ol class="breadcrumb">
                     <li class='control'>
                        <select name='user_role' class='form-control' id='select_user_role'>
                            
                            <?php 
                            $sel_role = Mhtml::gget('user_role', null);

                            foreach(UserRole::$Detail as $key=>$val):
                                $select = (is_numeric($sel_role) && $key == $sel_role) ? ' selected ' : '';
                                if($key == 0)
                                    echo '<option value="">--Select role--</option>';
                                else{
                            ?>
                                    <option <?php echo $select; ?> value="<?php echo $key; ?>"><?php echo $this->lang->line($val); ?></option>
                            <?php 
                                }
                            endforeach; ?>
                        </select>
                    </li>
                </ol>
            </form>
            */ 
            ?>

            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th><?php Mhtml::sorting('Id','id'); ?></th>
                            <th><?php Mhtml::sorting('Name','full_name'); ?></th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <?php
                            if(UserRole::$MANAGER == $role){?>
                            <th>Approved/Month</th>
                            <th>Approved/All</th>
                            <th>Provincial Administration</th>
                            <?php } ?>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php 
                            $length = count($users);
                            for($i=0; $i < $length; $i++){
                                $link = base_url('/admin/users/form/'.$users[$i]->id); ?>
                            <tr>
                                <td width="10px">
                                    <a href='<?php echo $link; ?>'>
                                        <?php echo $users[$i]->id; ?>
                                    </a>
                                </td>
                                <td><a href='<?php echo $link; ?>'><?php echo $users[$i]->full_name; ?></a></td>
                                <td><?php echo $users[$i]->email; ?></td>
                               
                                <td><?php echo $users[$i]->mobile; ?></td>
                        
                                <?php 
                                if(UserRole::$MANAGER == $role){
                                    if(isset($user_items[$users[$i]->id])){?>
                                        <td><?php echo $user_items[$users[$i]->id]['num_month']; ?></td>
                                        <td><?php echo $user_items[$users[$i]->id]['total']; ?></td>
                                        <?php 
                                        }else{
                                            echo '<td>0</td><td>0</td>';
                                        }
                                        ?>
                                    <td>
                                    <?php
                                    $locations=explode(',',$users[$i]->locations );
                                    foreach ($cities as $key => $value){
                                        foreach ($locations as $k => $v) {
                                            if($v==$value->id)
                                                 echo $value->name.', '; 
                                        }
                                    }
                                    ?>
                                    </td>
                                <?php } ?>
                    
                                <td align='center'>
                                    <a href='<?php echo base_url('/admin/users/remove/'.$users[$i]->id); ?>' onclick='return onRemove();'>
                                        <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php
                            }
                         ?>
                        
                    </tbody>
                </table>
            </div>

            <div class='wrap_pagination' style='text-align:center;'>
                <div class='pagination' >
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function onRemove(){
        return confirm("Do you want to remove it?");
    }
    $('#select_user_role').change(function(){
        $('#frm_filter_user_role').submit();
    });
</script>