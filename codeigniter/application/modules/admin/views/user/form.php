<?php
$subhead = 'Create New';
$action_back = 'registers';
if($user->id != null){
	$subhead = 'Edit';
	if($user->user_role == UserRole::$MANAGER)
		$action_back = 'managers';
}
	
 ?>
<div id='wrap_item_form'>
    <div class="row">
        <div class="col-lg-12">
			<h1 class="page-header">
			    User Form
			    <small><?php echo $subhead; ?></small>
			</h1>
			<ol class="breadcrumb">
                 <li class='control text-right'>
	            	<a href='<?php echo base_url('/admin/users/'.$action_back); ?>' class='btn btn-danger'>Back</a>
	            </li>
            </ol>


			<form action='' method='post' id='frm_street'>
				<div class='row'>
					<div class='col-sm-6'>
						<div class='form-group'>
							<label>Full name</label>
							<input class='form-control' type="text" name='full_name' value="<?php echo $user->full_name; ?>" required/>
						</div>

						<div class='form-group'>
							<label>Email</label>
							<input class='form-control' type="text" name='email' value="<?php echo $user->email; ?>" required/>
						</div>

						<div class='form-group'>
							<label>Mobile</label>
							<input class='form-control' type="text" name='mobile' value="<?php echo $user->mobile; ?>" required/>
						</div>

						<div class='form-group'>
							<label>Address</label>
							<input class='form-control' type="text" name='address' value="<?php echo $user->address; ?>" required/>
						</div>

						<div class='form-group'>
							<label>Role</label>
							<select name='user_role' class='form-control' id='select_user_role' required>
	                            
	                            <?php 
	                            foreach(UserRole::$Detail as $key=>$val):
	                                $select = ($key == $user->user_role) ? ' selected ' : '';
	                            	if($key == 0)
	                            		echo '<option value="">--Select role--</option>';
	                            	else{
	                            ?>
	                                	<option <?php echo $select; ?> value="<?php echo $key; ?>"><?php echo $this->lang->line($val); ?></option>
	                            <?php 
	                            	}
	                            endforeach; ?>
	                        </select>
						</div>

						<div class='form-group text-center'>
							<input type='reset' value='Reset' class='btn btn-default' /> 
							<input type='submit' value='Submit' class='btn btn-primary' name='submit' id='btn_submit' />
						</div>
					</div>

					<div class='col-sm-6'>
						<div class='row'>
							<?php 
							if(UserRole::$MANAGER == $user->user_role){
								echo '<h4>Locations</h4>';
								foreach ($cities as $key => $value) {
									$select = isset($user->locations[$value->id]) ? ' checked ' : '';
									?>
									<div class='col-xs-4'>
										<label><input type='checkbox' name='location[]' <?php echo $select; ?> value='<?php echo $value->id; ?>'/><?php echo $value->name; ?></label>
									</div>
							<?php
								}
							}
							?>
						</div>
					</div>
				</div>
				
				<div class='form-group text-center'>
					<?php if($user->id != null){ ?>
						<input type="hidden" value='<?php echo $user->id; ?>' name='id' id='field_id' />
					<?php
					} ?>
				</div>
			</form>	
		
		</div>	
	</div>
</div>