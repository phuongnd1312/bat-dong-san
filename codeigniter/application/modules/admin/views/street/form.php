<?php 
$subhead = ($street->id == null) ? 'Create New' : 'Edit';
 ?>
<div id='wrap_item_form'>
    <div class="row">
        <div class="col-lg-12">
			<h1 class="page-header">
			    Street Form
			    <small><?php echo $subhead; ?></small>
			</h1>
			<ol class="breadcrumb">
                 <li class='control text-right'>
	            	<a href='<?php echo base_url('/admin/streets'); ?>' class='btn btn-danger'>Back</a>
	            	<a href='<?php echo base_url('/admin/streets/form'); ?>' class='btn btn-primary'>Create New</a>
	            </li>
            </ol>


			<form action='' method='post' id='frm_street'>
				<div class='row'>
					<div class='col-sm-12'>
						<div class='form-group'>
							<label>Tỉnh/Thành phố *</label>
							<select name='lvl1_id' class='form-control' id='select_city' required>
								<option value="">--Chọn tỉnh/thành--</option>
								<?php foreach($cities as $city):
									$select = ($city->id == $street->lvl1_id) ? ' selected="selected" ' : '';
								?>
									<option <?php echo $select; ?> value="<?php echo $city->id; ?>"><?php echo $city->name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>

						<div class='form-group'>
							<label>Quận/Huyện *</label>
							<select name='lvl2_id' class='form-control' id='select_district' required>
								<option value="">--Chọn quận/huyện--</option>
								<?php foreach($districts as $district):
									$select = ($district->id == $street->lvl2_id) ? ' selected="selected" ' : '';
								?>
									<option <?php echo $select; ?> value="<?php echo $district->id; ?>"><?php echo $district->name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>

						<div class='form-group'>
							<label>Tên</label>
							<input class='form-control' type="text" name='name' value="<?php echo $street->name; ?>" required/>
						</div>

					</div>

				</div>
				

				<div class='form-group text-center'>
					<input type='reset' value='Reset' class='btn btn-default' /> 
					<input type='submit' value='Submit' class='btn btn-primary' name='submit' id='btn_submit' /> 
					<?php if($street->id != null){ ?>
						<input type="hidden" value='<?php echo $street->id; ?>' name='id' id='field_id' />
						<input type='button' value='Submit New' class='btn btn-danger' name='submit_new' onclick="onSubmitNew();" /> 
					<?php
					} ?>
				</div>
			</form>	
		
		</div>	
	</div>
</div>


<script type="text/javascript" src="<?php echo base_url('/public/templates/admin/js'); ?>/location.js"></script> 
<script type="text/javascript">
Loation.select_city = $('#select_city');
Loation.select_district = $('#select_district');
Loation.select_ward = $('#select_ward');

Loation.select_city.change(function(){
	getDistrict(Loation.select_city.val());
	Loation.select_ward.html(Loation.option_ward);
	Loation.select_district.html(Loation.option_ward);
});
Loation.select_district.change(function(){
	getWards(Loation.select_district.val());
	Loation.select_ward.html(Loation.option_ward);
});

function onSubmitNew(){
	$('#field_id').val('');
	$('#btn_submit').trigger('click');
}
</script>
