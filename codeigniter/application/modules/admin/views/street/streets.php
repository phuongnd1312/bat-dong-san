<div id='wrap_streets' class='list_items'>
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                Streets (<span><?php echo $count;?></span>)
            </h3>
        </div>  
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class='bound_control' style="margin-bottom: 20px;">
                <a href='<?php echo base_url('/admin/streets/form/'); ?>' class='btn btn-info'>Create New</a>
            </div>
            <form id='frm_filter_location' action='' method="get">
                <ol class="breadcrumb">
                     <li class='control'>
                        <select name='city' class='form-control' id='select_city'>
                            <option value="">--Chọn tỉnh/thành--</option>
                            <?php 
                            $sel_city = Mhtml::gget('city', 0);
                            foreach($cities as $city):
                                $select = ($city->id == $sel_city) ? ' selected="selected" ' : '';
                            ?>
                                <option <?php echo $select; ?> value="<?php echo $city->id; ?>"><?php echo $city->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </li>

                     <li class='control'>
                        <select name='district' class='form-control' id='select_district'>
                            <option value="">--Chọn quận/huyện--</option>
                            <?php 
                                $sel_district = Mhtml::gget('district', 0);
                                foreach($districts as $district):
                                $select = ($district->id == $sel_district) ? ' selected="selected" ' : '';
                            ?>
                                <option <?php echo $select; ?> value="<?php echo $district->id; ?>"><?php echo $district->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </li>

                </ol>
            </form>

            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th><?php Mhtml::sorting('Id','id'); ?></th>
                            <th><?php Mhtml::sorting('Name','name'); ?></th>
                            <th>District</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php 
                            $length = count($streets);
                            for($i=0; $i < $length; $i++){?>
                            <tr>
                                <td width="10px"><?php echo $streets[$i]->id; ?></td>
                                <td><a href='<?php echo base_url('/admin/streets/form/'.$streets[$i]->id); ?>'><?php echo $streets[$i]->name; ?></a></td>
                                <td><?php echo $streets[$i]->district; ?></td>
                                <td align='center'>
                                    <a href='<?php echo base_url('/admin/streets/remove/'.$streets[$i]->id); ?>' onclick='return onRemove();'>
                                        <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php
                            }
                         ?>
                        
                    </tbody>
                </table>
            </div>

            <div class='wrap_pagination' style='text-align:center;'>
                <div class='pagination' >
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('/public/templates/admin/js'); ?>/location.js"></script> 
<script type="text/javascript">
function onRemove(){
    return confirm("Do you want to remove it?");
}

Loation.select_city = $('#select_city');
Loation.select_district = $('#select_district');
Loation.select_ward = $('#select_ward');

Loation.select_city.change(function(){
    Loation.select_district.html(Loation.option_district);
    $('#frm_filter_location').submit();
});
Loation.select_district.change(function(){
    Loation.select_ward.html(Loation.option_ward);
    $('#frm_filter_location').submit();
});
Loation.select_ward.change(function(){
    $('#frm_filter_location').submit();
});


</script>