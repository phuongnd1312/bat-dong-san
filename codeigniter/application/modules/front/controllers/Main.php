<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->helper('item_helper');
		$this->load->model('item_model');
		$this->load->model('crawl_model');
		$this->load->model('city_model');
		$this->load->model('district_model');
		$this->load->model('ward_model');
		$this->load->model('street_model');
		$this->load->model('user_item_model');
		$this->load->model('properties_proportion_model');
		$this->load->model('filter_properties_model');
		$this->load->model('project_model');
	}
	public function index($cd = 0, $page = 0)
	{
		if (count($_GET)) {
			$this->filter();
		} else {
			$options = array();
			$filter = array();

			$link_page = $this->uri->segment(1);
			if (preg_match("/^ct(\d+)/", $cd, $matches)) {//city
				$options['city'] = intval($matches[1]);
				$filter[] = 'add_level_1 = ' . $options['city'];
			} else if (preg_match("/dt(\d+)/", $cd, $matches)) { //district
				$arr = explode('-', str_replace('dt', '', $cd));
				$options['district'] = $arr[0];
				$filter[] = 'add_level_2 = ' . $options['district'];
				$options['city'] = $arr[1];
				$filter[] = 'add_level_1 = ' . $options['city'];
			} else if (preg_match("/^tp(\d+)/", $cd, $matches)) { //type of project
				$options['type'] = intval($matches[1]);
				$filter[] = 'project_type = ' . $options['type'];
			} else
				$link_page = Link::$list;

			$uri_segment_page = 2;
			$this->loadDataList($filter, $link_page, $options, $uri_segment_page);
		}

	}


	public function filter($type = 0, $city = 0, $district = 0)
	{
		$options = array();
		$filter = array();

		$form = $this->gget('form', 0);
		$type = $this->gget('type', 0);
		$city = $this->gget('city', 0);
		$district = $this->gget('district', 0);
		$wards = $this->gget('wards', null);
		$streets = $this->gget('streets', null);

		$area = $this->gget('area', 0);
		$price = $this->gget('price', 0);
		$num_beds = $this->gget('num_beds', 0);
		$orientation = $this->gget('orientation', 0);
		$project = $this->gget('project', 0);
		//project properties filter
		$has_advance_filter = false;
		foreach (Configuration::$advance_filter as $key => $val) {
			$tmp = $this->gget($val, null);
			if (is_numeric($tmp)) {
				$options[$val] = $tmp;
				$filter[$val] = $val . ' = ' . intval($options[$val]);
				$has_advance_filter = true;
			}
		}

		foreach (Configuration::$advance_filter1 as $key => $val) {
			$tmp = $this->gget($val, null);
			$arr = explode('-', $tmp);
			if (isset($arr[1]) && $arr[1] > 1) {
				$options[$val] = $tmp;

				$f = 'filter_' . $val;
				$$f = Item::select_func($arr[0], $arr[1], $val);

				if (!empty($$f)) {
					$filter[$val] = $$f;
				}

				$has_advance_filter = true;
			}
		}

		if ($form != 0) {
			$options['form'] = $form;
			$filter['type_item'] = 'type_item = ' . intval($options['form']);
		}

		if ($type != 0) {
			$options['type'] = $type;
			$filter['project_type'] = 'project_type = ' . intval($options['type']);
		}

		if ($city != 0) {
			$options['city'] = intval($city);
			$filter['add_level_1'] = 'add_level_1 = ' . $options['city'];
		}

		if ($district != 0) {
			$options['district'] = intval($district);
			$filter['add_level_2'] = 'add_level_2 = ' . $options['district'];
		}

		if ($price != 0)
			$options['price'] = $price;
		$filter_price = Item::getFilterPrice($price);
		if (!empty($filter_price)) {
			$filter['price_total'] = $filter_price;
		}

		if ($area != 0)
			$options['area'] = $area;
		$filter_area = Item::getFilterArea($area);
		if (!empty($filter_area)) {
			$filter['area'] = 'area' . $filter_area;

		}
		if ($wards != null) {
			$options['wards'] = explode('-', $wards);
			$filter['wards'] = 'add_level_3 IN (' . implode(',', $options['wards']) . ')';
			$has_advance_filter = true;
		}

		if ($streets != null) {
			$options['streets'] = explode('-', $streets);
			$filter['streets'] = 'add_level_4 IN (' . implode(',', $options['streets']) . ')';
			$has_advance_filter = true;
		}

		if ($num_beds != 0) {
			$filter['num_beds'] = 'num_bedrooms = ' . $num_beds;
			$options['num_beds'] = $num_beds;
		}

		$uri_segment_page = null;
		$query_string = $this->input->server('QUERY_STRING');
		$link_page = Link::$list;
		if (count($options))
			$link_page .= '?' . Util::parse2url($options);;
		$this->loadDataList($filter, $link_page, $options, $uri_segment_page, $has_advance_filter);
	}

	private function loadDataList($filter, $link_page, $options = array(), $uri_segment_page, $has_advance_filter = false)
	{
		//chi show item da dc approvel
		$filter['status'] = 'status=' . ItemStatus::$Approved;

		$cfg_page = $this->load_pagination($link_page, $uri_segment_page);
		$cfg_page["per_page"] = 8;

		if ($uri_segment_page == null) {
			$cfg_page['page_query_string'] = true;
			$page = $this->gget('per_page', 0);
		} else
			$page = ($this->uri->segment($uri_segment_page)) ? $this->uri->segment($uri_segment_page) : 0;

		$items = $this->item_model->getListItems($filter, $cfg_page["per_page"], $page, $cfg_page["total_rows"]);
		$this->pagination->initialize($cfg_page);

		$districts = array();
		if (isset($options['city']))
			$districts = $this->district_model->getByCity($options['city']);

		$wards = array();
		$streets = array();
		if (isset($options['district'])) {
			$wards = $this->ward_model->getByDistrict($options['district']);
			$streets = $this->street_model->getByDistrict($options['district']);
		}
		$user = User_Helper::ins()->get();
		if ($user != null)
			$resp['user_items'] = $this->user_item_model->getByUser($user->id);

		$resp['pagination'] = $this->pagination->create_links();
		$resp['properties_proportion'] = $this->properties_proportion_model->getList();
		$resp['projects'] = $this->project_model->getList();
		$resp['type_items'] = Constant::$ITEM_TYPE;
		$resp['cities'] = $this->city_model->getList();
		$resp['districts'] = $districts;
		$resp['wards'] = $wards;
		$resp['streets'] = $streets;
		$resp['items'] = $items;
		$resp['filter'] = $options;
		$resp['params_search'] = Util::parse2url($options);
		$resp['has_advance_filter'] = $has_advance_filter;
		//debug($resp['items']);
		$this->template->load('front/main/list', $resp);
	}

	public function map()
	{
		$options = array();
		parse_str($this->input->server('QUERY_STRING'), $options);

		$districts = array();
		if (isset($options['city']))
			$districts = $this->district_model->getByCity($options['city']);

		$streets = $wards = array();
		if (isset($options['district'])) {
			$wards = $this->ward_model->getByDistrict($options['district']);
			$streets = $this->street_model->getByDistrict($options['district']);
		}

		if (isset($options['wards'])) {
			$options['wards'] = explode('-', $options['wards']);
		}

		if (isset($options['streets'])) {
			$options['streets'] = explode('-', $options['streets']);
		}

		$has_advance_filter = false;
		foreach (Configuration::$advance_filter as $key => $val) {
			$tmp = $this->gget($val, null);
			if (is_numeric($tmp)) {
				$has_advance_filter = true;
			}
		}

		foreach (Configuration::$advance_filter1 as $key => $val) {
			$tmp = $this->gget($val, null);
			$arr = explode('-', $tmp);
			if (isset($arr[1]) && $arr[1] > 1) {
				$has_advance_filter = true;
			}
		}

		//chi show item da dc approvel
		$filter['status'] = 'status=' . ItemStatus::$Approved;

		$cfg_page = $this->load_pagination($link_page, $uri_segment_page);
		$cfg_page["per_page"] = 6;

		if ($uri_segment_page == null) {
			$cfg_page['page_query_string'] = true;
			$page = $this->gget('per_page', 0);
		} else
			$page = ($this->uri->segment($uri_segment_page)) ? $this->uri->segment($uri_segment_page) : 0;

		$items = $this->item_model->getNewsList($filter, $cfg_page["per_page"], 1, $cfg_page["total_rows"]);
		$this->pagination->initialize($cfg_page);

		$itemsAll = $this->item_model->getNewsList($filter, 20, 6, $cfg_page["total_rows"]);
		$this->pagination->initialize($cfg_page);

		$agents = $this->crawl_model->getUsersByRole(2, 2);


		$resp['properties_proportion'] = $this->properties_proportion_model->getList();
		$resp['projects'] = $this->project_model->getList();
		$resp['type_items'] = Constant::$ITEM_TYPE;
		$resp['cities'] = $this->city_model->getList();
		$resp['districts'] = $districts;
		$resp['wards'] = $wards;
		$resp['streets'] = $streets;
		$resp['filter'] = $options;
		$resp['params_search'] = '';
		$resp['has_advance_filter'] = $has_advance_filter;
		$resp['items'] = $items;
		$resp['itemsAll'] = $itemsAll;
		$resp['agents'] = $agents;

		$this->template->load('front/main/map', $resp);

	}

	public function search()
	{
		$keywords = $this->gget('keywords', '');

		$items = null;
		if (strlen($keywords) >= 3) {
			$uri_segment_page = 2;
			$cfg_page = $this->load_pagination('/bds-tim-kiem?keywords=' . $keywords, $uri_segment_page);
			$cfg_page["per_page"] = 8;
			$cfg_page['page_query_string'] = true;
			$page = $this->gget('per_page', 0);

			$items = $this->item_model->searchItems($keywords, $cfg_page["per_page"], $page, $cfg_page["total_rows"]);
			$this->pagination->initialize($cfg_page);
			$resp['pagination'] = $this->pagination->create_links();
		} else
			$resp['message'] = 'Keywords phải lớn hơn bằng 3 ký tự';

		$resp['type_items'] = Constant::$ITEM_TYPE;
		$resp['cities'] = $this->city_model->getList();
		$resp['districts'] = null;
		$resp['wards'] = null;
		$resp['streets'] = null;
		$resp['filter'] = null;
		$resp['params_search'] = '';
		$resp['items'] = $items;
		//debug($resp['items']);
		$this->template->load('front/main/list', $resp);
	}
}