<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Items extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('item_model');
		$this->load->helper('item_helper');
		$this->load->model('properties_proportion_model');
		$this->load->model('filter_properties_model');
		$this->load->model('user_model');
		$this->load->model('city_model');
		$this->load->model('district_model');

		$this->load->model('ward_model');
		$this->load->model('street_model');
		$this->load->model('project_model');
		$this->load->model('item_properties_model');
	}

	public function form($iid = 0)
	{
		$this->load->helper('captcha_helper');
		User_Helper::ins()->checkPermission();
		$user = User_Helper::ins()->get();

		if (count($_POST)) {

			if ($this->session->userdata('token', '') == $this->gpost('token')) {
				$iid = Item::item_save($this);
				if ($iid) {
					User_Helper::ins()->setMessage($this->lang->line('msg_item_save_successfully'), 'success');
					redirect(Links::manageItem());
				} else
					User_Helper::ins()->setMessage($this->lang->line('msg_item_save_unsuccessfully'), 'danger');
				redirect(Links::pushItem($iid));
			} else {
				User_Helper::ins()->setMessage($this->lang->line('warning_captcha'), 'warning');
				redirect(Links::pushItem($iid));
			}
		}

		$item = $this->item_model->getById($iid);

		if ($iid != 0 && $item == null)
			show_404();
		if ($item == null) {
			$item = new item_model();
			$item->date_started = date('Y-m-d');
			$item->contact_name = $user->full_name;
			$item->contact_phone = $user->mobile;
			$item->contact_email = $user->email;
		}

		$resp['user'] = $user;
		$resp['properties_proportion'] = $this->properties_proportion_model->getList();
		$resp['type_items'] = Constant::$ITEM_TYPE;
		$resp['cities'] = $this->city_model->getList();
		$resp['districts'] = $this->district_model->getByCity($item->add_level_1);
		$resp['wards'] = $this->ward_model->getByDistrict($item->add_level_2);
		$resp['streets'] = $this->street_model->getByDistrict($item->add_level_2);
		$resp['projects'] = $this->project_model->getList();

		$resp['item'] = $item;

		$this->template->load('front/item/item_form', $resp);
	}

	public function remove($id)
	{
		User_Helper::ins()->checkPermission();
		$this->item_model->delete($id);
		User_Helper::ins()->setMessage($this->lang->line('msg_remove_item_successfully'), 'success');
		redirect(Links::manageItem());
	}
	public function upload_img($item_id = 0)
	{

		if (isset($_FILES['file']) && $_FILES['file']['size'] > 0) {
			$_FILES['userfile'] = &$_FILES['file'];

			$item = $this->item_model->getById($item_id);
			$upload = Item::do_upload_pricture($item, $this);
			if (isset($upload['upload_data'])) {

				$data = &$upload['upload_data'];
				//debug($data);
				if (!($data['image_width'] >= Constant::$MIN_RES_IMG['width'] ||
					$data['image_height'] >= Constant::$MIN_RES_IMG['height'])) {
					$msg = 'Kích thước hình ảnh phải có chiều rộng >= %spx và chiều cao >= %spx.';
					$msg = sprintf($msg, Constant::$MIN_RES_IMG['width'], Constant::$MIN_RES_IMG['height']);
					Util::response(1, 'UnSuccess', $msg);
				} else {
					$photo = $upload['path'] . '/' . $upload['upload_data']['file_name'];
					Item::format_picture($this, $photo);
					Util::composite_photo($photo);
					Util::response(0, 'Success', $photo);
				}
			} else {
				Util::response(1, 'UnSuccess', $upload['error']);
			}
		}
		exit;
	}

	public function remove_photo()
	{
		$path_file = 'public' . $this->gpost('path_file', '');
		if (is_file($path_file) && unlink($path_file)) {
			Util::response(0, 'Remove file successfully');
		} else
			Util::response(1, 'No file');
	}

	public function manage()
	{
		$filter = array();
		User_Helper::ins()->checkPermission();

		$keywords = $this->gget('keywords', '');
		if (!empty($keywords)) {
			$filter[] = '(i.title like "%' . $keywords . '%" OR u.full_name like "%' . $keywords . '%" OR u.email like "%' . $keywords . '%")';
		}


		$user = User_Helper::ins()->get();

		if ($user->user_role == UserRole::$MANAGER) {
			$need_help = $this->gget('need_help', null);
			$status = $this->gget('status', null);
			/*if($need_help=='on')
				$filter[]='need_help = 1';*/
			if ($status !== null) {
				$this->session->set_userdata('field_status', $status);
			} else {
				$status = $this->session->userdata('field_status', null);
			}
			$filter[] = 'status > 0';
			if ($status != null)
				$filter[] = 'status = ' . $status;

			if (trim($user->locations) != '')
				$filter[] = 'add_level_1 IN (' . $user->locations . ')';
			$resp['status'] = $status;
		} else
			$filter[] = 'user_id = ' . $user->id;

		$uri_segment_page = 4;
		$cfg_page = $this->load_pagination("front/items/manage", $uri_segment_page);
		$cfg_page["per_page"] = 15;
		$page = ($this->uri->segment($uri_segment_page)) ? $this->uri->segment($uri_segment_page) : 0;
		$resp['items'] = $this->item_model->getItemByUser($filter, $cfg_page["per_page"], $page, $cfg_page["total_rows"]);
		$this->pagination->initialize($cfg_page);
		$resp['pagination'] = $this->pagination->create_links();

		if ($user->user_role == UserRole::$MANAGER)
			$this->template->load('front/item/man_manage', $resp);
		else
			$this->template->load('front/item/user_manage', $resp);
	}


	public function detail($id = 0)
	{
		$this->load->model('user_item_model');
		$this->load->model('crawl_model');
		$item = $this->item_model->getDetailById($id);
		if ($item == null)
			show_404();

		$item->photos = Item::photos($item);
		$user = User_Helper::ins()->get();
		if ($user != null)
			$resp['item_saved'] = ($this->user_item_model->getItem($user->id, $id) != null);
		$params['id'] = $item->id;
		$params['hits'] = $item->hits + 1;
		$this->item_model->save($params);
		$resp['item'] = $item;
		$districts = $streets = $wards = array();
		$options = array();
		$options['city'] = $item->add_level_1;
		$options['district'] = $item->add_level_2;
		$options['wards'] = array($item->add_level_3);
		$options['streets'] = array($item->add_level_4);
		$options['form'] = $item->type_item;
		$options['type'] = $item->project_type;
		$options['price'] = Item::getRangePrice($item->price_total);
		$options['area'] = Item::getRangeArea($item->area);

		//set meta 
		$item_photo = null;
		if (count($item->photos))
			$item_photo = Util::loadImg($item->photos[0]);
		Meta::ins()->setMeta($item->title, null, $item_photo);
		$ppfa_val = array();
		foreach (Configuration::$advance_filter  as $key => $val) {
			if (isset($item->$val)) {
				$ppfa_val[$val] = $item->$val;
			}
		}

		$advance_number = $this->filter_properties_model->getById($item->project_type);
		if ($advance_number != null) {
			foreach (Configuration::$advance_filter1 as $key => $val) {
				if (isset($item->$val)) {
					$opt = 'lbli_' . $val;
					if ($advance_number->$opt > 1) {
						$t = Item::getRangeFilter($item->$val, $advance_number->$opt);
						$ppfa_val[$val] = $advance_number->$opt . '-' . $t;
					}
				}
			}
		}

		if (isset($options['city']))
			$districts = $this->district_model->getByCity($options['city']);

		if (isset($options['district'])) {
			$wards = $this->ward_model->getByDistrict($options['district']);
			$streets = $this->street_model->getByDistrict($options['district']);
		}

		$agents = $this->crawl_model->getUsersByRole(2, 2);

		$resp['properties_proportion'] = $this->properties_proportion_model->getList();
		$resp['projects'] = $this->project_model->getList();
		$resp['type_items'] = Constant::$ITEM_TYPE;
		$resp['cities'] = $this->city_model->getList();
		$resp['districts'] = $districts;
		$resp['wards'] = $wards;
		$resp['streets'] = $streets;
		$resp['filter'] = $options;
		$resp['ppfa_val'] = $ppfa_val;
		$resp['has_advance_filter'] = true;
		$resp['agents'] = $agents;
		//debug($resp['filter']);
		$this->template->load('front/item/detail', $resp);
	}

	public function update_item_status()
	{
		User_Helper::ins()->checkPermission();

		$item_id = $this->gpost('item_id', 0);
		$status = $this->gpost('status', 0);

		$item = $this->item_model->getById($item_id);
		if (!($item != null && $item->status >= 2))
			Util::response(0, 'Change status unsuccessfully');

		$params['id'] = $item_id;
		$params['status'] = $status;
		$rlt = $this->item_model->save($params);
		if ($rlt)
			Util::response(0, 'Change status successfully');
		else
			Util::response(0, 'Change status unsuccessfully');
	}

	public function update_post_status()
	{
		User_Helper::ins()->checkPermission();

		$item_id = $this->gpost('item_id', 0);
		$status = $this->gpost('status', 0);

		$params['id'] = $item_id;
		$params['post_status'] = $status;
		$rlt = $this->item_model->save($params);
		if ($rlt)
			Util::response(0, 'Change post status successfully');
		else
			Util::response(0, 'Change post status unsuccessfully');
	}

	/*------------------------BDSVN Items -----------------------*/
	public function manage_bds()
	{

		$filter = array();

		$keywords = $this->gget('keywords', '');
		if (!empty($keywords)) {
			$filter[] = '(i.title like "%' . $keywords . '%" OR u.full_name like "%' . $keywords . '%" OR u.email like "%' . $keywords . '%")';
		}

		User_Helper::ins()->checkPermission();
		$user = User_Helper::ins()->get();


		if ($user->user_role == UserRole::$MANAGER) {
			$need_help = $this->gget('need_help', null);
			$status = $this->gget('status', null);

			if ($status !== null) {
				$this->session->set_userdata('field_status', $status);
			} else {
				$status = $this->session->userdata('field_status', null);
			}


			$filter[] = 'status = 0';

			if (trim($user->locations) != '')
				$filter[] = 'add_level_1 IN (' . $user->locations . ')';
			$resp['status'] = $status;
		}

		$filter[] = 'bds_id != 0';

		$uri_segment_page = 4;
		$cfg_page = $this->load_pagination("front/items/manage_bds", $uri_segment_page);
		$cfg_page["per_page"] = 15;
		$page = ($this->uri->segment($uri_segment_page)) ? $this->uri->segment($uri_segment_page) : 0;
		$resp['items'] = $this->item_model->getItemByUser($filter, $cfg_page["per_page"], $page, $cfg_page["total_rows"]);
		$this->pagination->initialize($cfg_page);
		$resp['pagination'] = $this->pagination->create_links();

		if ($user->user_role == UserRole::$MANAGER)
			$this->template->load('front/item/bds_man_manage', $resp);
		else {
			User_Helper::ins()->setMessage($this->lang->line('warning_permission'), 'warning');
			redirect(base_url('/'));
		}
	}

	public function form_bds($iid = 0)
	{
		$this->load->model('report_model');
		User_Helper::ins()->checkPermission();
		$user = User_Helper::ins()->get();

		if (count($_POST)) {

			$iid = Item::item_save($this);
			if ($iid) {
				User_Helper::ins()->setMessage($this->lang->line('msg_item_save_successfully'), 'success');
			} else
				User_Helper::ins()->setMessage($this->lang->line('msg_item_save_unsuccessfully'), 'danger');
			if ($this->gpost('approved', 0) == 1) {

				$phone = $this->gpost('contact_phone');
				$email = $this->gpost('contact_email', '');
				$this->report_model->save($this->gpost('user_id'), $user->id, $phone, false);

				if (!empty($email)) {
					$subject = 'Bất động sản Việt Nam';
					$message = $this->load->view('front/mailtmpl/notify_post_bdsvn', $_POST, true);


					User_Helper::ins()->sendMail($email, $message, $subject);
				}

				redirect(base_url('front/items/manage_bds/'));
			} else
				redirect(base_url('front/items/form_bds/' . $iid));
		}

		$item = $this->item_model->getById($iid);

		if ($iid != 0 && $item == null)
			show_404();


		$resp['user'] = $user;
		$resp['properties_proportion'] = $this->properties_proportion_model->getList();
		$resp['type_items'] = Constant::$ITEM_TYPE;
		$resp['cities'] = $this->city_model->getList();
		$resp['districts'] = $this->district_model->getByCity($item->add_level_1);
		$resp['wards'] = $this->ward_model->getByDistrict($item->add_level_2);
		$resp['streets'] = $this->street_model->getByDistrict($item->add_level_2);
		$resp['projects'] = $this->project_model->getList();

		$resp['item'] = $item;

		$this->template->load('front/item/bds_item_form', $resp);
	}

	public function cancel_bds($item_id)
	{
		$this->cancel_item($item_id);
		redirect(base_url('front/items/manage_bds'));
	}

	public function cancel($item_id)
	{
		$this->cancel_item($item_id);
		redirect(Links::manageItem());
	}

	private function cancel_item($item_id)
	{
		$params['id'] = $item_id;
		$params['status'] = -1;
		$rlt = $this->item_model->save($params);

		User_Helper::ins()->setMessage('Tin đăng đã bị hủy', 'success');
	}

	public function edit_image($item_id)
	{
		$url = $this->gget('url', '');
		$resp = array('image' => $url, 'item_id' => $item_id);
		echo Mhtml::loadView('front/item/crop_image', $resp);
	}

	public function crop()
	{
		// Original image
		$original_image = PUBLIC_PATH . $_POST['image'];
		$image_quality = 100;

		// Get dimensions of the original image
		list($current_width, $current_height) = getimagesize($original_image);

		// Get coordinates x and y on the original image from where we
		// will start cropping the image, the data is taken from the hidden fields of form.
		$scale_w =  round($current_width / $_POST['w_c'], 1);
		$scale_h =  round($current_height / $_POST['h_c'], 1);
		$tmp = array();
		$tmp['x1'] = $_POST['x1'];
		$tmp['y1'] = $_POST['y1'];
		$tmp['x2'] = $_POST['x2'];
		$tmp['y2'] = $_POST['y2'];


		// Define the final size of the image here ( cropped image )
		$tmp['c_w'] = floor(($_POST['x2'] - $tmp['x1']) * $scale_w);
		$tmp['c_h'] = floor(($_POST['y2'] - $tmp['y1']) * $scale_h);


		$tmp['x1'] = $tmp['x1'] * $scale_w;
		$tmp['y1'] = $tmp['y1'] * $scale_h;

		$tmp['x2'] = $tmp['x2'] * $scale_w;
		$tmp['y2'] = $tmp['y2'] * $scale_h;



		// Create our small image
		$background = imagecreatetruecolor($tmp['c_w'], $tmp['c_h']);
		$whiteBackground = imagecolorallocate($background, 255, 255, 255);
		imagefill($background, 0, 0, $whiteBackground);
		// Create original image
		$current_image = imagecreatefromjpeg($original_image);
		// resampling ( actual cropping )
		$a = imagecopyresampled($background, $current_image, 0, 0, $tmp['x1'], $tmp['y1'], $tmp['c_w'], $tmp['c_h'], $tmp['c_w'], $tmp['c_h']);

		// this method is used to create our new image
		imagejpeg($background, $original_image, $image_quality);
		echo Mhtml::loadView('front/item/review_image_crop', array('image' => $_POST['image']));
	}
}