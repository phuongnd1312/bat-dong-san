<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Items extends MY_Controller{

	public function __construct() {
		parent::__construct ();
		$this->load->model('user_item_model');
	}

	function update_item($item_id = 0){
		$user = User_Helper::ins()->get(); 
		if($user != null && $item_id != 0){
			$rlt = $this->user_item_model->getItem($user->id, $item_id);
			if($rlt == null)
			{
				$this->user_item_model->save(array('user_id'=>$user->id, 'item_id'=>$item_id));
				Util::response(0, 'Đã lưu dự án của bạn', array('lbl'=>'Xóa lưu tin', 'class'=>'remove'));
			}	
			else
			{
				$this->user_item_model->delete($user->id, $item_id);
				Util::response(0, 'Đã xóa dự án của bạn', array('lbl'=>'Lưu tin', 'class'=>'save'));
			}	
		}
		Util::response(1, 'UnSuccess');
	}

	function index(){
		$this->load->helper('item_helper');
		$user = User_Helper::ins()->get();
		$items = array();
		$uri_segment_page = 4;
		$cfg_page = $this->load_pagination('front/user_items/index', $uri_segment_page);
		$cfg_page["per_page"] = 8;
		$page = ($this->uri->segment($uri_segment_page)) ? $this->uri->segment($uri_segment_page) : 0;
		if($user != null){
			$items = $this->user_item_model->getUserItems($user->id, $cfg_page["per_page"], $page, $cfg_page["total_rows"]);
		}
		$this->pagination->initialize($cfg_page);

		$resp['pagination'] = $this->pagination->create_links();	
		$resp['items'] = $items;
		$this->template->load('front/user_item/list', $resp);
	}
}