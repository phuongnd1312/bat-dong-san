<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller{

	public function __construct() {
		parent::__construct ();
	}

	private function response($error_code, $message, $data = null){
		$resp = array('error_code'=>$error_code, 'message'=>$message, 'data'=>$data);
		echo json_encode($resp);
		exit;
	}

	public function get_district_by_city($city = 0){
		$this->load->model ('district_model');
		$districts = $this->district_model->getByCity($city);
		$this->response(0, 'Success', $districts);
	}

	public function get_ward_by_district($district = 0){
		$this->load->model ('ward_model');
		$wards = $this->ward_model->getByDistrict($district);
		$this->response(0, 'Success', $wards);
	}

	public function get_street_by_district($district = 0){
		$this->load->model ('street_model');
		$streets = $this->street_model->getByDistrict($district);
		$this->response(0, 'Success', $streets);
	}

	public function show_captcha(){
		$this->load->helper('captcha_helper');
		Captcha_Helper::show();
		exit();
	}

	public function project_properties(){
		$id = $this->gget('id', 0);
		$item_id = $this->gget('item_id', 0);

		$city = $this->gget('city', 0);
		$district = $this->gget('district', 0);

		$filter_project = array();
		if($city)
			$filter_project['city'] = 'city_id = '.intval($city);
		//if($district)
		//	$filter_project['district'] = 'district = '.intval($district);

		$this->load->model ('properties_proportion_model');
		$this->load->model('item_properties_model');
		$this->load->model('project_model');
		

		$properties = $this->properties_proportion_model->getById($id);
	
		$html = '';
		if($properties != null){
			$item_properties = $this->item_properties_model->getById($item_id);
			if($item_properties == null)
				$item_properties = new item_properties_model();

			$resp['orientation_houses'] = Constant::$ORIENTATION_HOUSE;
			$resp['projects'] = $this->project_model->getList($filter_project);
			$resp['properties'] = $properties;
			$resp['item_properties'] = $item_properties;
			$html = MHtml::loadView('front/item/partials/project_properties', $resp, true);
		}

		$this->response(0, 'Success', $html);
	}

	public function filter_project_properties(){
		$id = $this->gget('id', 0);
		$item_id = $this->gget('item_id', 0);

		$city = $this->gget('city', 0);
		$district = $this->gget('district', 0);
		$search_params = $this->gget('search_params', array());

		$filter_project = array();
		if($city)
			$filter_project['city'] = 'city_id = '.intval($city);

		$this->load->model ('filter_properties_model');
		$this->load->model('item_properties_model');
		$this->load->model('project_model');
	

		$properties = $this->filter_properties_model->getById($id);
	
		$html = '';
		if($properties != null){
			$item_properties = new item_properties_model();

			$resp['orientation_houses'] = Constant::$ORIENTATION_HOUSE;
			$resp['projects'] = $this->project_model->getList($filter_project);
			$resp['properties'] = $properties;
			$resp['search_params'] = $search_params;
			$html = MHtml::loadView('front/item/partials/filter_project_properties', $resp, true);
		}

		$this->response(0, 'Success', $html);
	}



	public function get_projects(){
		$this->load->model('project_model');
		$city = $this->gget('city', 0);
		$district = $this->gget('district', 0);

		$filter_project = array();
		if($city)
			$filter_project['city'] = 'city_id = '.intval($city);

		if($district)
			$filter_project['district'] = 'district_id = '.intval($district);
		
		$projects = $this->project_model->getList($filter_project);
		$html = '<option value="">--Dự án--</option>';
		foreach ($projects as $key => $project) {
			
			$html.='<option value="'.$project->id.'">'.$project->name.'</option>';
		}
		$this->response(0, 'Success', $html);
	}

	/*------------------------- Items ---------------------------*/
	public function get_location_items(){

		$this->load->helper('item_helper');
		$this->load->model('item_model');

		$location = $this->gpost('location', null);
		$filter = $this->gpost('filter', null);

		if($location == null)
			$this->response(1, 'No filter', array());

		$general_filter = array();

		//Filter address first

		if(!empty($filter['city']))
			$general_filter[] = 'add_level_1 = '.intval($filter['city']);

		if(!empty($filter['district']))
			$general_filter[] = 'add_level_2 = '.intval($filter['district']);

		if(!empty($filter['wards'])){
			$general_filter[] = 'add_level_3 IN ('.str_replace('-',',', $filter['wards']).')';
		}
		
		if(!empty($filter['streets']))
			$general_filter[] = 'add_level_4 IN ('.str_replace('-',',', $filter['streets']).')';

		

		//filter by bound location
		if(count($general_filter) == 0){
			$ne_lat = floatval($location['ne_lat']);
			$ne_lng = floatval($location['ne_lng']);
			$sw_lng = floatval($location['sw_lng']);
			$sw_lat = floatval($location['sw_lat']);
		
			$scaleX = 1/20;
	        $scaleY = 1/20;
	        $ne_lat1 = $ne_lat + ($sw_lat - $ne_lat) * $scaleX;
	        $sw_lat1 = $ne_lat + ($sw_lat - $ne_lat) * (1 - $scaleX);

	        $ne_lng1 = $ne_lng + ($sw_lng - $ne_lng) * $scaleY;
	        $sw_lng1 = $ne_lng + ($sw_lng - $ne_lng) * (1 - $scaleY);
	        
	        // ____ location filter
	        $general_filter[] = "lat > $sw_lat1";
	        $general_filter[] = "lat < $ne_lat1";
	        $general_filter[] = "lng > $sw_lng1";
	        $general_filter[] = "lng < $ne_lng1";
		}

		if(!empty($filter['form'])){
			$general_filter[] = 'type_item = '.intval($filter['form']);
		}

		if(!empty($filter['type'])){
			$general_filter[] = 'project_type = '.intval($filter['type']);
		}

		/*if(!empty($filter['num_beds'])){
			$general_filter[] = 'num_bedrooms = '.$filter['num_beds'];
		}

		if(!empty($filter['orientation'])){
			$general_filter[] = 'orientation_house = '.$filter['orientation'];
		}

		if(!empty($filter['project'])){
			$general_filter[] = 'belong_project = '.$filter['project'];
		}*/

		if(!empty($filter['price'])){
			$filter_price = Item::getFilterPrice($filter['price']);
			if(!empty($filter_price)){
				$general_filter[] = $filter_price;
			}
		}
	
		/*if(!empty($filter['area'])){
			$filter_area = Item::getFilterArea($filter['area']);
			if(!empty($filter_area)){
				$general_filter[] = 'area'.$filter_area;
			}
		}*/
		foreach(Configuration::$advance_filter as $key=>$val){
			if(!empty($filter[$val])){
				$tmp = $filter[$val];
				if(is_numeric($tmp)){
					$options[$val] = $tmp;
					$general_filter[] = $val.' = '.intval($options[$val]);
				}
			}			
		}
		
		foreach(Configuration::$advance_filter1 as $key=>$val){
			if(!empty($filter[$val])){
			$tmp = $filter[$val];
				$arr = explode('-', $tmp);
				if(isset($arr[1]) && $arr[1] >1)
				{
					$options[$val] = $tmp;
				
					$f='filter_'.$val; 
					$$f= Item::select_func($arr[0],$arr[1],$val);

					if(!empty($$f)){
					$general_filter[] = $$f;
					}
				}	
			}		
		}
       
		//chi show item da dc approvel
		$general_filter[] = 'status='.ItemStatus::$Approved;
		
        $items = $this->item_model->getByLocation($general_filter);

        $length = count($items);
        for($i = 0; $i < $length; $i++){
        	$items[$i]->link = Item::getLink($items[$i]);
        }
      
        $this->response(0, 'Success', $items);
	}

	public function check_captcha(){
		$this->load->helper('captcha_helper');
		if(Captcha_Helper::check($this->input->post('captcha', '')))
		{
			$token = md5(time());
			$this->session->set_userdata('token', md5(time()));
			$this->response(0, 'Success', $token);
		}	
		else
			$this->response(1, $this->lang->line('warning_captcha'));
	}

	public function get_type_item(){
		$this->load->model('properties_proportion_model');
		$type = $this->gget('type', 0);
		$properties_proportion = $this->properties_proportion_model->getList();
		$html = '<option value="">--Chọn loại tin đăng--</option>';
		foreach ($properties_proportion as $key => $value) {
			if( ($type == 1 || $type == 2) &&  $value->id == 1)
				continue;
			
			$html.='<option value="'.$value->id.'">'.$this->lang->line($value->type).'</option>';
		}
		$this->response(0, 'Success', $html);
	}
}
