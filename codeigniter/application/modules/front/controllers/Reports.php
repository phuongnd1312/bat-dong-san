<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('report_model');
		
	}

	function index(){
		User_Helper::ins()->checkPermission();
		if(count($_POST)){
			$this->notify();
		}
		$cfg_page = $this->load_pagination('front/reports', null);
		$cfg_page["per_page"] = 20;
		$cfg_page['page_query_string'] = TRUE;
		$page = $this->gget('per_page', 0);

		$resp['users'] = $this->report_model->getListWP($cfg_page["per_page"], $page, $cfg_page["total_rows"]);
		$this->pagination->initialize($cfg_page);
		$resp['pagination'] = $this->pagination->create_links();

		$this->template->load('front/report/list', $resp);
	}

	private function notify(){
		User_Helper::ins()->checkPermission();
		/*$data = &$_POST;

		if(isset($data['users'])){
			$users_notify = array();

			foreach ($data['users'] as $key => $user) {
				$resp['full_name'] = $data['full_name'][$user];

				$content = Mhtml::loadView('front/report/sms_notify', $resp);
				debug($content);
				if(Util::sendActivationPhone($data['phone'][$user], $content))
					$users_notify[] = $user;

			}
			if(count($users_notify))
				$this->report_model->updateNotify($users_notify);
		}*/
		redirect(base_url('/front/reports'));
		// var_dump($users_notify);
		// debug($_POST);
	}

	public function notify_users(){
		if(NOTIFY_DEBUG)
		{
			//Util::sendSMS('0985467919', 'notify_users');
			return;
		}
		$users = $this->report_model->getUsersNotify();
		
		if(count($users) < 200){
			echo 'SMS notify khi đủ số lương 200 users';
			return;
		}

		$users_notify = array();
		foreach ($users as $key => $user) {
			$resp['user'] = $user;
			$content = Mhtml::loadView('front/report/sms_notify', $resp);
		
			if(Util::sendSMS($user->mobile, $content)){
				$users_notify[] = $user->user_id;
				var_dump('notify');
			}

		}
		if(count($users_notify))
			$this->report_model->updateNotify($users_notify);

		echo 'Đã gửi SMS đến hơn 200 user';
	}

	public function test_notify_users(){
			Util::sendSMS('0985467919', 'test brand name');
	}
}