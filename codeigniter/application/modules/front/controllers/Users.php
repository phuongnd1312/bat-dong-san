<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller{

	public function __construct() {
		parent::__construct ();
		$this->load->model('user_model');
	}

	public function logout(){
		User_Helper::ins()->logout();
		redirect(base_url('/'));
	}

	public function login(){
		
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$user = $this->user_model->sign_in($email , $password );
	
		if($user != null)
		{
			if($user->active == 1){
				User_Helper::ins()->setInfo($user);
				Util::response(0, 'Success');
			}
			else{
				Util::response(1, $this->lang->line('msg_your_account_not_activated'));
			}
		}	
		else{
			Util::response(1, $this->lang->line('msg_your_loign_failed'));
		}
	}
	public function register(){
		$user = new user_model();
		if($this->gpost('email',null) != null)
		{
			if(!is_numeric($this->gpost('email','')))
			{
				$params['username'] = $params['email'] = $this->gpost('email','');
				$params['full_name'] = $this->gpost('full_name','');
				$params['password'] = password_hash($this->gpost('password',''), PASSWORD_DEFAULT);
				$params['created_timestamp'] = date('Y-m-d H:i:s');
				$params['activation_code'] = md5($params['email'].'_'.time());
				$params['user_role'] = UserRole::$REGISTER;
				if(trim($params['email']) == '')
				{
					User_Helper::ins()->setMessage($this->lang->line('msg_warning_miss_info'), 'warning');
				}else{
					$rlt = $this->user_model->getByEmail($params['email']);
					if($rlt == null){
						$rlt = $this->user_model->save($params);
						if($rlt)
						{
							$this->send_activation((object)$params);
							User_Helper::ins()->setMessage($this->lang->line('msg_sign_up_success'));
						}	
						else
							User_Helper::ins()->setMessage($this->lang->line('msg_sign_up_unsuccess'), 'warning');
						redirect(base_url('/'));
					}else{					
							User_Helper::ins()->setMessage($this->lang->line('msg_email_exists'), 'warning');
					}
				}
			}
			else
			{
				$phone = $this->gpost('email','');
				$params['username'] = $phone;
				$params['full_name'] = $this->gpost('full_name','');
				$params['password'] = password_hash($this->gpost('password',''), PASSWORD_DEFAULT);
				$params['mobile'] = $phone;
				$params['created_timestamp'] = date('Y-m-d H:i:s');
				$params['activation_code'] = rand(100000, 999999);
				$params['user_role'] = UserRole::$REGISTER;
				if(trim($params['username']) == '')
				{
					User_Helper::ins()->setMessage($this->lang->line('msg_warning_miss_info'), 'warning');
				}else{
					$rlt = $this->user_model->getByUsername($params['username']);
					if($rlt == null){
						$rlt = $this->user_model->save($params);
						if($rlt)
						{
							$this->send_activation_phone((object)$params);
							User_Helper::ins()->setMessage($this->lang->line('msg_sign_up_success'));
						}	
						else
							User_Helper::ins()->setMessage($this->lang->line('msg_sign_up_unsuccess'), 'warning');
						redirect(base_url('/'));
					}else{					
							User_Helper::ins()->setMessage($this->lang->line('msg_mobile_exists'), 'warning');
					}
				}
				$params['email'] = $phone;
			}

		}
		else{
			$params = new user_model();
			if($this->gpost('email_register',null) != '')
				$params->email = $this->gpost('email_register');
		}
		$resp['user'] = (object)$params;
    	$this->template->load('front/user/register', $resp);
	}

	public function activate($code){
		$user = $this->user_model->activate($code);
		if($user === null)
			User_Helper::ins()->setMessage($this->lang->line('msg_code_activation_invalid'), 'warning');
		else{
			User_Helper::ins()->setInfo($user);
			User_Helper::ins()->setMessage($this->lang->line('msg_your_account_activated'));
		}
		redirect(base_url('/'));
	}

	public function forgot_pass(){
		Document::ins()->setTitle($this->lang->line('lbl_submit_forgot'));
		if($this->gpost('email_or_phone') != null){
			$user = $this->user_model->getByUsername($this->gpost('email_or_phone'));
			if($user == null){
				User_Helper::ins()->setMessage($this->lang->line('msg_email_phone_not_existed'), 'warning');
			}else{
				$code = md5($user->email.'_'.rand(100,999));
				$params['id'] = $user->id;
				$params['activation_code'] = $code;
				$this->user_model->save($params);

				$this->send_forgot_message($user, $code);
			}
			redirect(base_url('/'));
		}else
			$this->template->load('front/user/forgot_pass');
	}

	public function reset_password($code = ''){
		if($this->gpost('password') != null){

			$user = $this->user_model->getByCode($code);
			if($user == null)
			{
				User_Helper::ins()->setMessage($this->lang->line('msg_code_reset_pass_not_right'), 'warning');
			}else{
				$params['password'] = password_hash($this->gpost('password',''), PASSWORD_DEFAULT);
				$params['id'] = $user->id;
				$params['activation_code'] = '';
			
				if($this->user_model->save($params)){
					User_Helper::ins()->setInfo($user);
					User_Helper::ins()->setMessage($this->lang->line('msg_password_reset_success'));
				}
				redirect(base_url('/'));
			}	
		}

		$resp['code'] = $code;
		$this->template->load('front/user/reset_password', $resp);
	}

	public function profile(){
		User_Helper::ins()->checkPermission();
		$resp['user'] = User_Helper::ins()->get();
		$this->template->load('front/user/profile', $resp);
	}
	public function edit(){
		User_Helper::ins()->checkPermission();
		$user = User_Helper::ins()->get();
		if(count($_POST)){
			$params['id'] = $user->id;
			$params['username'] = $user->username;
			$params['email'] = $this->gpost('email','');
			$params['full_name'] = $this->gpost('full_name','');
			$params['zalo']=Util::clean($this->gpost('zalo',''));
			$params['viber']=$this->gpost('viber','');
			$params['deskphone'] = Util::clean($this->gpost('deskphone',''));
			$params['birthday'] = Util::formatDateYMD($this->gpost('birthday',0));
			$params['gender'] = $this->gpost('gender',0);
			$params['address'] = $this->gpost('address','');
			$params['mobile'] = Util::clean($this->gpost('mobile',''));
     		if(isset($_FILES['lefile']) && $_FILES['lefile']['size'] > 0){
				$_FILES['userfile'] = &$_FILES['lefile'];
				$name_file = $user->id.'_'.time();
				$path = '/images/users/avatars';
				$upload = Mhtml::do_upload_img($this, PUBLIC_URL.$path, $name_file);
				if(isset($upload['upload_data'])){
					$params['image'] = $path.'/'.$upload['upload_data']['file_name'];
				}else
					User_Helper::ins()->setMessage($upload['error'],'warning');
			}
			$rlt = $this->user_model->getByEmail($params['email']);
			if($rlt != null && $rlt->id != $user->id){
				$user = (object) $params;
				User_Helper::ins()->setMessage($this->lang->line('msg_email_exists'), 'warning');
			}
			else{
				//debug($params['email'].$user->email);
				if($params['email']==$user->email){
					$rlt = $this->user_model->save($params);
					User_Helper::ins()->setMessage($this->lang->line('msg_update_profile_success'));
					redirect(Links::account('profile'));
				}
				else
				{
					$params['active']=0;
					$params['activation_code']=md5($params['email'].'_'.time());
					$this->send_activation((object)$params);
					$rlt = $this->user_model->save($params);
					User_Helper::ins()->setMessage($this->lang->line('msg_update_profile_success'));
					$this->logout();
				}
			}
		}
		$resp['user'] = $user;
		$this->template->load('front/user/update_profile', $resp);
	}
	public function change_password(){
		User_Helper::ins()->checkPermission();
		$user = User_Helper::ins()->get();
		$old_password = $this->gpost('old_password', null);

		if($old_password != null){

			$rlt = $this->user_model->sign_in($user->email, $old_password);
			if($rlt == null){
				User_Helper::ins()->setMessage($this->lang->line('msg_password_not_match'), 'warning');
			}else{
				$new_password = $this->gpost('password');
				if($old_password == $new_password)
					User_Helper::ins()->setMessage($this->lang->line('warn_new_old_password_is_match'), 'warning');
				else{
					$params['id'] = $user->id;
					$params['password'] = password_hash($new_password, PASSWORD_DEFAULT);
					$rlt = $this->user_model->save($params);
					if($rlt)
						User_Helper::ins()->setMessage($this->lang->line('msg_change_pass_success'));
					else
						User_Helper::ins()->setMessage($this->lang->line('msg_change_pass_unsuccess'), 'warning');
				}
			
			}
		}
		$this->template->load('front/user/change_password');
	}

	private function send_activation($user){
		$subject = $this->lang->line('lbl_email_active');
		$message = $this->load->view('front/mailtmpl/user_activation', array('user'=>$user), true);
		$sent = User_Helper::ins()->sendMail($user->email, $message, $subject);
		if ($sent)
	        User_Helper::ins()->setMessage($this->lang->line('msg_email_activation'));
	    else
	        User_Helper::ins()->setMessage($this->lang->line('msg_cant_send_email'), 'warning');
	}

	// send sms confrim user
	private function send_activation_phone($user){
		$content = 'Ma xac nhan tai khoan cua ban la '.$user->activation_code .'. Hoac click vao link de kich hoat '.Links::account('activate/'.$user->activation_code);

		if(Util::sendSMS($user->mobile, $content))
			User_Helper::ins()->setMessage($this->lang->line('msg_sms_activation'));
		else
			User_Helper::ins()->setMessage($this->lang->line('msg_cant_send_sms'));
	}

	private function send_forgot_message($user, $code){
		
		$subject = $this->lang->line('lbl_email_reset_pass');
		$message = $this->load->view('front/mailtmpl/user_forgot_msg', array('user'=>$user, 'code'=>$code), true);
		$sent = User_Helper::ins()->sendMail($user->email, $message, $subject);

		if ($sent)
	        User_Helper::ins()->setMessage($this->lang->line('msg_email_reset_pass'));
	    else
	        User_Helper::ins()->setMessage($this->lang->line('msg_cant_send_email'), 'warning');
	
	}
}
