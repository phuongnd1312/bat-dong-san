<?php
set_time_limit(86400);
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') or exit('No direct script access allowed');

class Crawl extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
	}
	private function response($error_code, $message, $data = null)
	{
		$resp = array('error_code' => $error_code, 'message' => $message, 'data' => $data);
		echo json_encode($resp);
		exit;
	}
	public function save()
	{
		$this->load->model('crawl_model');
		$city_n = $this->gpost('city');
		$district_n = $this->gpost('district');
		$streets = $this->gpost('streets');
		if ($city_n == null || $district_n == null || $streets == null)
			$this->response(3, 'No post');


		if (count($streets) == 0) {
			$this->response(3, 'No Streets');
			$this->crawl_model->log($city_n, $district_n, 3, 'No Streets');
		}

		$data = $this->crawl_model->getDistrict($city_n, $district_n);
		if ($data == null) {
			$this->response(1, 'District ' . $district_n . ' không tồn tại');
			$this->crawl_model->log($city_n, $district_n, 1, 'District không tồn tại');
		}

		$rlt = $this->crawl_model->insertStreet($data->district, $data->city, $streets);
		if ($rlt == -1)
			$this->response(2, 'Đã insert street trước đây');
		else
			$this->response(0, 'Insert thành công');
	}

	public function format_projects()
	{
		$this->load->model('crawl_model');
		$projects = $this->crawl_model->getProjects();
		foreach ($projects as $key => $project) {
			$arr = explode(',', $project->address);
			$city = trim($arr[count($arr) - 1]);
			if ($city == 'Việt Nam' || $city == 'Vietnam')
				$city = trim($arr[count($arr) - 2]);

			$find = array("Tỉnh", 'tỉnh', 'T.', 'Q7', 'TP', 'Tp.', '- ', 'thành phố', ".");

			$city = trim(str_replace($find, '', $city));

			if ($city == 'Tp.HCM' || $city == 'HCM' || $city == 'TpHCM' || $city == 'Nhà Bè' || $city == 'Quận 7') {
				$city = 'Hồ Chí Minh';
			} else if ($city == 'Núi Thành')
				$city = 'Đà Nẵng';
			else if ($city == 'Tây Hồ')
				$city = 'Hà Nội';
			$sql = '';

			$rlt = $this->crawl_model->getCityByName($city, $sql);
			if ($rlt == null) {
					var_dump($city);
					echo $project->address;
					exit;
				} else {
				$params = array();
				$params['id'] = $project->id;
				$params['city_id'] = $rlt->id;

				$this->crawl_model->updateProject($params);
			}
		}
		echo 'Done';
		exit;
	}

	public function normalizationDistrict()
	{
		$this->load->model('crawl_model');
		$projects = $this->crawl_model->getProjects();
		foreach ($projects as $key => $project) {
			$arr = explode(',', $project->address);
			$district = trim($arr[count($arr) - 2]);


			$find = array("Quận ", "Q.", "quận ", "quận ", "H.", "Huyện ", "huyện ", "TP.", "Thành phố ", 'thành phố ', 'thị xã ', 'Thị xã ', 'Tp ', 'tp. ');

			$district = trim(str_replace($find, '', $district));
			if ($district == 'Kiến Hưng (Hà Đông) và xã Cự Khê (Thanh Oai)')
				$district = 'Hà Đông';
			else if (is_numeric($district))
				$district = "Quận " . $district;

			$sql = '';

			$rlt = $this->crawl_model->getDistrictByName($project->city_id, $district, $sql);

			if ($rlt == null) {
					var_dump($project->city_id);
					var_dump($district);
					var_dump($project->id);
					echo $project->address;
					exit;
				} else {
				$params = array();
				$params['id'] = $project->id;
				$params['district_id'] = $rlt->id;

				$this->crawl_model->updateProject($params);
			}
		}
		echo 'Done';
		exit;
	}

	public function pushItemsCrawl()
	{
		$this->load->helper('item_helper');
		$this->load->model('crawl_model');
		$this->load->model('user_model');
		$this->load->model('item_model');
		$this->load->model('item_properties_model');
		$this->load->model('properties_proportion_model');

		$items = $this->crawl_model->getItemsCrawl();

		foreach ($items as $key => $item) {

			$params = $this->crawl_model->getItemByBdsId($item->id);

			if ($params != null) {
				$params = (array)$params;
				if ($params['status'] != 0)
					continue;
			}


			$email = $item->email;
			$item->phone = str_replace(' ', '', $item->phone);
			if (empty($email)) {
				$email = $item->phone . '@realestatevietnam.vn';
			}


			//get user
			$user = $this->user_model->getByEmail($email);
			if ($user == null) {
				$user = array();
				$user['username'] 	= $email;
				$user['email'] 		= $email;
				$user['full_name'] 	= $item->author;
				$user['user_role'] 	= 1;
				$user['mobile'] = $item->phone;
				$user['password'] = md5(Util::generatePass($item->phone));
				$user['active'] = 1;
				$user_id = $this->user_model->save($user);
				if (!$user_id) {
					echo 'user';
					continue;
				}
			} else
				$user_id = $user->id;

			//get city 
			$city = $this->crawl_model->getCityByName($item->city);

			if ($city == null) {
				echo 'city';
				var_dump($city);
				continue;
			}

			//get district
			$district = $this->crawl_model->getDistrictByName($city->id, $item->district);

			if ($district == null) {
				echo 'district';
				var_dump($district);
				continue;
			}

			//get district
			$ward = $this->crawl_model->getWardByName($district->id, $item->ward);
			if ($ward != null)
				$params['add_level_3'] = $ward->id;

			$params['user_id'] = $user_id;
			$params['add_level_1'] = $city->id;
			$params['add_level_2'] = $district->id;
			$params['type_item'] = $item->type_item;
			$params['project_type'] = $item->project_type;
			$params['title'] = $item->title;
			$params['full_address'] = $item->address;
			$params['lat'] = $item->latitude;
			$params['lng'] = $item->longitude;
			$params['description'] = $item->description;
			$params['area'] = $item->area;

			$params['date_started'] = $item->date_started;
			$params['date_ended'] = $item->date_ended;

			$params['contact_name'] = $item->author;
			$params['contact_phone'] = $item->phone;
			$params['contact_email'] = $item->email;

			$params['price_total'] = $item->price;
			$params['status'] = 0;
			$params['bds_id'] = $item->id;
			$params['created_date'] = $item->date_started . date('H:i:s');

			$rlt = $this->crawl_model->saveItem($params);
			if ($rlt) {
				$item_prop = array();
				$this->setTotalArea($params, $item_prop);

				$item_prop['item_id'] = $rlt;

				if ($item->numbed)
					$item_prop['num_bedrooms'] = $item->numbed;

				if ($item->numtolet)
					$item_prop['bath_room'] = $item->numtolet;

				$item_prop['orientation_house'] = $this->getOrientation($item->orientation_house);
				$item_prop['belong_project'] = $this->getBelongProject($item->belong_project);

				$j = $this->crawl_model->saveItemPropertise($item_prop);

				//move image
				$params['id'] = $rlt;
				$this->moveImages($item, $params);
				echo "<pre>-----------------------$rlt----------------------------";
				//exit;
			}

			// if($key > 40)
			// 	break;

		}
	}

	private function setTotalArea($params, &$item_prop)
	{
		$rlt = $this->properties_proportion_model->getList();
		$type = null;
		foreach ($rlt as $key => $prop) {
			if ($params['project_type'] == $prop->id)
				$type = $prop;
		}
		if ($type->lbli_total_area > 0 && $type->lbli_total_area_building > 0) {
			$item_prop['total_area_building'] = $item_prop['total_area'] = $params['area'];
		} else if ($type->lbli_total_area > $type->lbli_total_area_building)
			$item_prop['total_area'] = $params['area'];
		else
			$item_prop['total_area_building'] = $params['area'];
	}
	private function getOrientation($name)
	{
		$name = str_replace('-', ' ', $name);
		foreach (Constant::$ORIENTATION_HOUSE as $key => $val) {
			if ($name == $val['name']) {
				return $val['id'];
			}
		}
		echo "---------------------$name------------------------";
		return null;
	}

	private function getBelongProject($name)
	{
		$rlt = $this->crawl_model->getBelongProjectByName($name);
		if ($rlt)
			return $rlt->id;
		return null;
	}

	private function moveImages($item, &$params)
	{

		$images = explode(',', trim($item->image));
		if (count($images) == 0)
			return;

		if (empty($params['created_date']))
			$created_date = date('Y-m-d');
		else
			$created_date = $params['created_date'];


		$path = 'public/images/items/';
		$sub = date('Y/m/d', strtotime($created_date)) . '/' . $params['id'];
		if (!file_exists($path . $sub)) {
			if (!Util::createDirectoryDate($path, $sub)) {
				return false;
			}
		}

		$arr_photos = array();
		$root_img_tmp = SITE_LOCAL_PATH . '/crawl_items/';
		foreach ($images as $key => $image) {
			if (preg_match("/\b(\.jpg|\.JPG|\.png|\.PNG)\b/", $image)) {
				$path_file = $root_img_tmp . $image;

				$new_file = '/images/items/' . $sub . '/' . basename($path_file);

				if (is_file($path_file)) {
					if (copy($path_file, 'public' . $new_file)) {
						$arr_photos[] = $new_file;
					}
					//unlink($path_file);
				}
			}
		}

		$path_dir = $root_img_tmp . 'images/' . $item->id;
		if (!count($arr_photos)) {
			var_dump($path_dir);
		}
		//if (file_exists($path_dir) && !count(scandir($path_dir)))
		//	rmdir($path_dir);

		if (count($arr_photos)) {
			$tmp['photos'] = implode(';', $arr_photos);
			$tmp['id'] = $params['id'];
			$this->item_model->save($tmp);
		}
		//var_dump($arr_photos);
		//exit;
	}

	public function generatePass()
	{
		$this->load->model('crawl_model');
		$this->load->model('user_model');
		$users = $this->crawl_model->getUsers();
		foreach ($users as $key => $user) {
			$params = array();
			$params['id'] = $user->id;
			$params['password'] = password_hash(Util::generatePass($user->mobile), PASSWORD_DEFAULT);
			$params['active'] = 1;
			$this->user_model->save($params);
		}
	}
}
