<div id='wrap_man_manage_items' class='bound_content'>
	<div class='main_content' style="width: 100%;max-width: 900px;float: none;margin: auto;">
		<div class='center items'>
			<div class='list_items' id='user_manage_form'>
				<ul class="s-list">
					<?php 
					$length = count($items);
					for($i = 0; $i < $length; $i++){
						$link = base_url('front/items/form_bds/'.$items[$i]->id);?>

						<li id='user_item_<?php echo $items[$i]->id; ?>' class="item_ads">

							<a href='<?php echo $link; ?>'>
								<h3 class="hh-sr">
									<span class="date"><?php echo $items[$i]->id; ?>.</span> 
									<span class='title'><?php echo $items[$i]->title; ?></span>			                    
								</h3>
							</a>

							<div class="padding">
								<div class="sr-image">
									<a href='<?php echo Item::getLink($items[$i]); ?>'><?php echo Item::photofst($items[$i]); ?></a>
								</div>
								<div class="date">
									<div class="font_pad">
										<span class="sp-info pull-right" style="padding-left: 25px"><b class="text-blue">Ngày kết thúc: </b><strong style="color: red;font-size: 16px">
											<?php echo Util::formatDateDMY($items[$i]->date_ended); ?></strong>
										</span>

										<span class="sp-info pull-right" style="padding-left: 25px"><b class="text-blue ">Ngày bắt đầu: </b><strong style="color: black;">
											<?php echo Util::formatDateDMY($items[$i]->date_started); ?></strong>
										</span>
									</div>
									<div class="font_pad">
										<span class="sp-info pull-right" style="padding-left: 25px"><b class="text-blue ">Lượt xem: </b><strong style="color: black;">
											<?php echo $items[$i]->hits; ?></strong>
										</span>
										<span class="sp-info pull-right" ><b class="text-blue ">Người đăng: </b><strong style="color: black;">
											<?php echo $items[$i]->email; ?></strong>
										</span>
										
									</div>

									<span class='sp-info pull-right'>
										<a href='<?php echo base_url('/front/items/cancel_bds/'.$items[$i]->id); ?>' onclick='return onCancelItem();' class='btn btn-danger'>Hủy</a>
									</span>
								</div>
							</div>
						</li>
					<?php
				}
				if($length == 0)
					echo '<span class="message">Không có bất động sản nào được tìm thấy</span>';
				?>
				</ul>
			</div>
		</div>
		<div class='clear'></div>

		<div class='bound_pagination'>
			<ul class='pagination'>
				<?php echo $pagination; ?>
			</ul>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#frm_search_global').attr('action', '<?php echo base_url(uri_string()); ?>');
$('#field_filter_status').change(function(){
	$('#frm_item_filter').submit();
});
$('#field_need_help').change(function(){
	$('#frm_item_filter').submit();
});
$('select.item_status').change(function(){
	var status = $(this).val();
	var item_id = $(this).attr('for');
	API.updateItemStatus({item_id: item_id, status: status}, function(resp){
		console.log(resp);
	});
});

function onCancelItem(){
	return confirm("Bạn chắc chắn muốn hủy tin này?");
}
</script>