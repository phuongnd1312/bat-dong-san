
<div id='wrap_item_detail' class='bound_content'>
<div class='bound_slidebar_left'>
			<?php echo MHtml::loadView('front/partials/slide_filter'); ?>
		</div>
	<div class='main_content'>
		<div class='center bound_item_detail'>
			<div class='clear hotline' >
				<a style="color: red;font-weight: 700; " href='tel:01227555111'>
					<img style="height: 35px;"  src='<?php echo Util::loadImg('/images/stories/hotline.png'); ?>' />
				</a>
			</div>
			<div class='breadcrumb'>
				<?php echo Mhtml::loadView('front/item/partials/breadcrumb'); ?>
			</div>

			<div class='bound_status'>
				<ul>
					<li class='element price'><?php echo Item::formatCurrency($item->price_total); echo Item::rentMonth($item->type_item);?></li>
					<li class='element location'><?php echo $item->district.' - '.$item->city; ?></li>
					<li class='element item_id'>Mã tin <?php echo $item->id; ?></li>
					<li class='element created_date'>Ngày đăng <?php echo Util::formatDate($item->date_started); ?></li>
				</ul>
				<?php if($item->post_status){?>
					<span class='status<?php if(PostStatus::$Detail[$item->post_status]=='ps_available') echo 'green'; else echo 'red';?>'><?php echo $this->lang->line(PostStatus::$Detail[$item->post_status]); ?></span>
				<?php } ?>
			</div>

			<div class='bound_title'>
				<h3 class='title'><?php echo $item->title; ?></h3>
				<?php 
				if(isset($item_saved)){ 
					if($item_saved){
						$text_usersitem = 'Xóa lưu tin';
						$link_remove_item = '';
						$class ='remove';
					}else{
						$text_usersitem = 'Lưu tin';
						$link_remove_item = '';
						$class ='save';
					}
				?>
				<div class='control'>
					<a href='javascript:;' id='user_item' class='btn_item <?php echo $class; ?>' onclick='onSaveItem(<?php echo $item->id; ?>)'>
					<?php echo $text_usersitem ?>
					</a>
				</div>
				<?php } ?>
				<div class="clear"></div>
			</div>

			<div class='bound_tabs'>
				<div class='bound_nav_menu'>
					<a href='#tab_item_photo' class='nav_menu left active'>Hình ảnh</a>
					<?php if($item->video){?>
					<a href='#tab_item_video' class='nav_menu middle'>Xem video</a>
					<?php } ?>
					<a href='#tab_map' class='nav_menu middle'>Xem bản đồ</a>
					<a href='#tab_map_ultility' class='nav_menu right'>Tiện ích xung quanh</a>
				</div>

				<div class="bound_tab_content">
				  <div id="tab_item_photo" class="tab_pane active">
						<div class='bound_photos'>
							<div id='wrapper_slide_photo' class="jcarousel-wrapper">
								<div class="jcarousel">
								    <ul>
								    <?php 
								    if(count($item->photos)){
									    foreach ($item->photos as $photo) {?>
											<li>
												<img src="<?php echo Util::loadImg($photo); ?>" style="margin-top: 30px;"/>
											</li>
										<?php
										}
									}
									else {?>
										<li>
											<img src="<?php echo Util::loadImg('/images/stories/no-thumbnail.png')?>" style="margin-top: 30px;"/>
										</li>
								     <?php } ?>
								    </ul>
								</div>
								<a href="#" class="navigation jcarousel-control-prev"><i class='fa fa-angle-left'></i></a>
								<a href="#" class="navigation jcarousel-control-next"><i class='fa fa-angle-right'></i></a>
								<p class="jcarousel-pagination"></p>
							</div>
						</div>
					<?php 
						
					?>
				  </div>
				   <div id="tab_item_video" class="tab_pane">
				  	<div id='item_video'>
				  		<iframe style="width: 100%;"title="YouTube video player" class="youtube-player" type="text/html" 
						width="640" height="390" src="http://www.youtube.com/embed/<?php echo $item->video;?>"
						frameborder="0" allowFullScreen></iframe>
				  	</div>
				  </div>
				  <div id="tab_map" class="tab_pane">
				  	<div id='map_canvas'></div>
				  </div>
				  <div id="tab_map_ultility" class="tab_pane">
				    <div id='map_ultility_canvas'></div>
				    <div class='tab_info'>
				    <label class="label_noti">Dịch vụ trong vòng bán kính 1km</label>
				    <div id='list_facility_places'>				    	
				    </div>
				    </div>	
				    <div class='clear'></div>
				  </div>
				</div>
			</div>
			
		

			<div class='bound_information'>
				<?php echo Mhtml::loadView('front/item/partials/item_properties'); ?>
			</div>

			<div class='bound_description'>
				<?php echo nl2br($item->description); ?>
			</div>
		</div>
		
		<div class='slide_right'>
			<div class='bound_contact'>
				<h3 class="subtitle">Liên hệ</h3>
				<div class="detail_contact">
					<table>
						<?php if($item->contact_name){ ?>
						<tr>
							<td class='lbl'>Liên hệ</td>
							<td class='val'><?php echo $item->contact_name; ?></td>
						</tr>
						<?php }
						 if($item->contact_company){ ?>
						<tr>
							<td class='lbl'>Công ty</td>
							<td class='val'><?php echo $item->contact_company; ?></td>
						</tr>
						<?php }?>
						</table>
						<table >
						<?php
						 if($item->contact_email){ ?>
						<tr>
							<td class='lbl'><span class='icon email'></span></td>
							<td class='val email'>
								<span><?php echo $item->contact_email; ?></span>
							</td>
						</tr>
						<?php }
						 if($item->contact_phone){ ?>
						<tr>
							<td class='lbl'><span class='icon phone'></span></td>
							<td class='val'><?php echo Util::formatPhone($item->contact_phone); ?></td>
						</tr>
						<?php }
						 if($item->contact_zalo){ ?>
						<tr>
							<td class='lbl'><span class='icon zalo'></span></td>
							<td class='val'><?php echo $item->contact_zalo; ?></td>
						</tr>
						<?php }
						 if($item->contact_viber){ ?>
						<tr>
							<td class='lbl'><span class='icon viber'></span></td>
							<td class='val'><?php echo $item->contact_viber; ?></td>
						</tr>
						<?php }?>
					</table>
				</div>
				<div class='approve_by'>
					<span class='txt'>Tin được duyệt bởi</span> <span class='by'><?php echo $item->approval_name; ?></span>
				</div>
			</div>
		</div>
		<div class='clear'></div>
	</div>
</div>
<input type="hidden" id='lat_item' value="<?php echo $item->lat; ?>" />
<input type="hidden" id='lng_item' value="<?php echo $item->lng; ?>" />
<script type="text/javascript" src="<?php echo base_url('/public/templates/front/js'); ?>/item_detail.js?v=1.1"></script>
<script type="text/javascript">
var save_item_second = false;
function onSaveItem(item_id){
	if(save_item_second ==  true) return;
	save_item_second =  true;
	API.updateUserItem(item_id, function(resp){
		if(resp.error_code == 0){
			var user_item = $('#user_item');
			user_item.html(resp.data.lbl);

		
			if(resp.data.class == 'save')
				user_item.removeClass('remove');
			else
				user_item.removeClass('save');

			user_item.addClass(resp.data.class);
		}else{

		}
		save_item_second = false;
	});
}
SlidePhoto.init();
</script>