<?php if(0){?>
<div id='wrap_man_manage_items' class='bound_content manage_items'>
	<div class='main_content'>
		<div class='center items'>
			<form id='frm_item_filter' action='' method="get" class='form-inline' style="margin-bottom: 10px;">
				<div class='form-group'>
					<select class='form-control' name='status' id='field_filter_status'>
						<?php
						foreach (ItemStatus::$Detail as $key => $val) {
							$selected = ($status == $key) ? ' selected ' : '';
							if($key == 0)
								echo '<option value="">--Chọn trạng thái--</option>';
							else{
								?>
								<option value="<?php echo $key; ?>" <?php echo  $selected; ?>><?php echo $this->lang->line($val) ?></option>
								<?php
							}
						}
						?>
					</select>
				</div>
			</form>
			<div class='list_items' id='user_manage_form'>

				<ul class="s-list">
					<?php
					$length = count($items);
					for($i = 0; $i < $length; $i++){
						$link = Links::pushItem($items[$i]->id);?>

						<li id='user_item_<?php echo $items[$i]->id; ?>' class="item_ads">

							<a href='<?php echo $link; ?>'>
								<h3 class="hh-sr">
									<span class="date"><?php echo $items[$i]->id; ?>.</span>
									<span class='title'><?php echo $items[$i]->title; ?></span>
								</h3>
							</a>

							<div class="padding">
								<div class="sr-image">
									<a href='<?php echo Item::getLink($items[$i]); ?>'><?php echo Item::photofst($items[$i]); ?></a>
								</div>
								<div class="date">
									<div class="font_pad">
										<span class="sp-info pull-right" style="padding-left: 25px"><b class="text-blue">Ngày kết thúc: </b><strong style="color: red;font-size: 16px">
											<?php echo Util::formatDateDMY($items[$i]->date_ended); ?></strong>
										</span>

										<span class="sp-info pull-right" style="padding-left: 25px"><b class="text-blue ">Ngày bắt đầu: </b><strong style="color: black;">
											<?php echo Util::formatDateDMY($items[$i]->date_started); ?></strong>
										</span>
									</div>
									<div class="font_pad">
										<span class="sp-info pull-right" style="padding-left: 25px"><b class="text-blue ">Tình trạng: </b><strong style="color: black;">
											<?php if($items[$i]->status < ItemStatus::$Approved)
											echo $this->lang->line(ItemStatus::$Detail[$items[$i]->status]);
											else{ ?>
												<select name='status' class='item_status status' for='<?php echo $items[$i]->id; ?>'>
													<?php
													for($k = 3; $k < count(ItemStatus::$Detail); $k++){
														$selected = ($items[$i]->status == $k) ? ' selected="selected" ' : '';
														?>
														<option <?php echo $selected; ?> value='<?php echo $k; ?>'>
															<?php echo $this->lang->line(ItemStatus::$Detail[$k]); ?>
														</option>
														<?php
													}
													?>
												</select>
												<?php
											}?>
										</span>
										<span class="sp-info pull-right" style="padding-left: 25px"><b class="text-blue ">Lượt xem: </b><strong style="color: black;">
											<?php echo $items[$i]->hits; ?></strong>
										</span>
										<span class="sp-info pull-right" ><b class="text-blue ">Người đăng: </b><strong style="color: black;">
											<?php echo $items[$i]->email; ?></strong>
										</span>
									</div>

									<span class='sp-info pull-right'>
										<a href='<?php echo base_url('/front/items/cancel/'.$items[$i]->id); ?>' onclick='return onCancelItem();' class='btn btn-danger'>Hủy</a>
									</span>
								</div>
							</div>
						</li>
					<?php
				}
				if($length == 0)
					echo '<span class="message">Không có bất động sản nào được tìm thấy</span>';
				?>
				</ul>
			</div>
		</div>
	</div>

	<div class='bound_pagination'>
		<ul class='pagination'>
			<?php echo $pagination; ?>
		</ul>
	</div>
</div>
<?php }?>

<div id="content" class="cs-content">
	<div class="container">
		<div class="row">
			<div class="span3">
				<?php echo Mhtml::loadView('front/partials/user_menu'); ?>

			</div>
			<div class="span9 user_area">
				<h3 class="title">Danh sách tin</h3>
                <div class="row">
                    <div class="span12">
	                    <form id='frm_item_filter' action='' method="get" class='form-inline' style="margin-bottom: 10px;">
	                        <div class='form-group'>
	                            <select class='form-control' name='status' id='field_filter_status'>
	                                <?php
	                                foreach (ItemStatus::$Detail as $key => $val) {
	                                    $selected = ($status == $key) ? ' selected ' : '';
	                                    if($key == 0)
	                                        echo '<option value="">--Chọn trạng thái--</option>';
	                                    else{
	                                        ?>
	                                        <option value="<?php echo $key; ?>" <?php echo  $selected; ?>><?php echo $this->lang->line($val) ?></option>
	                                        <?php
	                                    }
	                                }
	                                ?>
	                            </select>
	                        </div>
	                    </form>
                    </div>
                </div>
                <div class="properties-rows">
				    <div class="row">
				    	<?php
							$length = count($items);
							for($i = 0; $i < $length; $i++){?>
				        <div class="property span9 item_ads" id='user_item_<?php echo $items[$i]->id; ?>'>
				            <div class="row">
				                <div class="image span3">
				                    <div class="content">
				                        <a href="<?php echo Links::pushItem($items[$i]->id); ?>"></a>
				                        <?php echo Item::photofst($items[$i]); ?>
				                    </div><!-- /.content -->
				                </div><!-- /.image -->

				                <div class="body span6">
				                    <div class="title-price row">
				                        <div class="title span4">
				                            <h2><a href="detail.html"><?php echo $items[$i]->id; ?>.<?php echo $items[$i]->title; ?></a></h2>
				                        </div><!-- /.title -->

				                        <div class="price">
				                           
				                        </div><!-- /.price -->
				                    </div><!-- /.title -->

				                    <div class="location"><?php echo $items[$i]->full_address; ?></div><!-- /.location -->				                    
				                    <div class="area">
				                        <span class="key">Ngày kết thúc:</span><!-- /.key -->
				                        <span class="value"><?php echo Util::formatDateDMY($items[$i]->date_ended); ?></span><!-- /.value -->
				                    </div><!-- /.area -->

				                    <div class="area">
				                        <span class="key">Ngày bắt đầu:</span><!-- /.key -->
				                        <span class="value"><?php echo Util::formatDateDMY($items[$i]->date_started); ?></span><!-- /.value -->
				                    </div><!-- /.area -->

				                    <div class="area">
				                        <span class="key">Lượt xem:</span><!-- /.key -->
				                        <span class="value"><?php echo $items[$i]->hits; ?></span><!-- /.value -->
				                    </div><!-- /.area -->
				                    <?php if($items[$i]->status < ItemStatus::$Approved)
								echo $this->lang->line(ItemStatus::$Detail[$items[$i]->status]);
							else{ ?>
								<select name='status' class='item_status status' for='<?php echo $items[$i]->id; ?>'>
								<?php
									for($k = 3; $k < count(ItemStatus::$Detail); $k++){
										$selected = ($items[$i]->status == $k) ? ' selected="selected" ' : '';
										?>
										<option <?php echo $selected; ?> value='<?php echo $k; ?>'>
											<?php echo $this->lang->line(ItemStatus::$Detail[$k]); ?>
										</option>
								<?php
									}
								?>
								</select>

								<select name='post_status' class='post_status status' for='<?php echo $items[$i]->id; ?>'>
								<?php
									for($j = 1; $j < count(PostStatus::$Detail); $j++){
										$selected = ($items[$i]->post_status == $j) ? ' selected="selected" ' : '';
										?>
										<option <?php echo $selected; ?> value='<?php echo $j; ?>'>
											<?php echo $this->lang->line(PostStatus::$Detail[$j]); ?>
										</option>
								<?php
									}
								?>
								</select>
							<?php
								}?>
				                    
				                </div><!-- /.body -->
				            </div><!-- /.property -->
				        </div><!-- /.row -->
				    <?php }
				    if($length == 0)
					echo '<span class="message">Không có bất động sản nào được tìm thấy</span>';
					?>
				    <div class='bound_pagination'>
	                    <ul class='pagination'>
	                    <?php echo $pagination; ?>
	                    </ul>
				    </div>
				    </div>
				</div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$('#frm_search_global').attr('action', '<?php echo base_url(uri_string()); ?>');
$('#field_filter_status').change(function(){
	$('#frm_item_filter').submit();
});
$('#field_need_help').change(function(){
	$('#frm_item_filter').submit();
});
$('select.item_status').change(function(){
	var status = $(this).val();
	var item_id = $(this).attr('for');
	API.updateItemStatus({item_id: item_id, status: status}, function(resp){
	});
});

function onCancelItem(){
	return confirm("Bạn chắc chắn muốn hủy tin này?");
}
</script>
