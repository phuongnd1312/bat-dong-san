<!-- CONTENT -->
<div class="nat-client-detail" id="content">
    <div class="container">
        <div id="main">
            <div class="row">
                <div class='span12'>
                    <div class="breadcrumb">
                        <?php echo Mhtml::loadView('front/item/partials/breadcrumb'); ?>
                    </div>

                </div>

                <div class="sidebar span3">

                    <div class="widget our-agents">
                        <div class="title">
                            <h2>Tìm Kiếm</h2>
                        </div><!-- /.title -->
                        <?php echo MHtml::loadView('front/partials/slide_filter_list'); ?>
                    </div><!-- /.widget -->

                </div>




                <div class="span6">


                    <div class='bound_status'>
                        <div class="title">
                            <h2><?php echo $item->title; ?></h2>
                        </div><!-- /.title -->
                    </div>

                    <div class='bound_status'>

                        <ul>
                            <li class='element price'>
                                <?php echo Item::formatCurrency($item->price_total);
                                echo Item::rentMonth($item->type_item); ?>
                            </li>
                            <li class='element location'>
                                <?php echo $item->district . ' - ' . $item->city; ?>
                            </li>
                            <li class='element item_id'>Mã tin
                                <?php echo $item->id; ?>
                            </li>
                            <li class='element created_date'>Ngày đăng
                                <?php echo Util::formatDate($item->date_started); ?>
                            </li>
                        </ul>
                        <?php if ($item->post_status) { ?>
                            <span class='status<?php if (PostStatus::$Detail[$item->post_status] == ' ps_available') echo
                                                    'green';
                                                else echo 'red'; ?>'>
                                <?php echo $this->lang->line(PostStatus::$Detail[$item->post_status]); ?></span>
                        <?php
                    } ?>
                    </div>


                    <?php
                    if (isset($item_saved)) {
                        if ($item_saved) {
                            $text_usersitem = 'Xóa lưu tin';
                            $link_remove_item = '';
                            $class = 'remove';
                        } else {
                            $text_usersitem = 'Lưu tin';
                            $link_remove_item = '';
                            $class = 'save';
                        }
                        ?>
                        <div class='control'>
                            <a href='javascript:;' id='user_item' class='btn btn-primary btn_item <?php echo $class; ?>' onclick='onSaveItem(<?php echo $item->id; ?>)' style="margin-bottom: 5px;    float: right;">
                                <?php echo $text_usersitem ?>
                            </a>
                        </div>
                    <?php
                } ?>
                    <ul class="nav nav-tabs bound_nav_menu" id="myTab">
                        <li class="active"><a href="#picture">Hình ảnh</a></li>
                        <li><a href="#map">Xem bản đồ</a></li>
                        <li><a href="#utilities">Tiện ích xung quanh</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="picture">
                            <div class="carousel-wrapper">
                                <div class="carousel property">
                                    <div class="preview">
                                        <img src="<?php echo Util::loadImg($item->photos[0]); ?>" />
                                    </div><!-- /.preview -->

                                    <div class="content">

                                        <a class="carousel-prev" href="#">Previous</a>
                                        <a class="carousel-next" href="#">Next</a>
                                        <ul>
                                            <?php
                                            $i = 0;
                                            $len = count($item->photos);
                                            if ($len) {
                                                foreach ($item->photos as $photo) {
                                                    if ($i == 0) {
                                                        ?>

                                                        <li class="active">
                                                            <img src="<?php echo Util::loadImg($photo); ?>" />
                                                        </li>
                                                    <?php

                                                } else if ($i == $len - 1) { ?>

                                                        <li>
                                                            <img src="<?php echo Util::loadImg($photo); ?>" />
                                                        </li>
                                                    <?php

                                                }

                                                $i++;
                                            }
                                        } else { ?>
                                                <li>
                                                    <img src="<?php echo Util::loadImg('/images/stories/no-thumbnail.png') ?>" />
                                                </li>
                                            <?php
                                        } ?>
                                        </ul>
                                    </div>
                                    <!-- /.content -->
                                </div>
                            </div>
                            <!-- /.carousel -->
                            <div class="property-detail" style="display: table-cell;">
                                <div class="pull-left overview">
                                    <p>
                                        <h2>Thông tin chung</h2>
                                        <?php echo Mhtml::loadView('front/item/partials/item_properties'); ?>

                                    </p>

                                </div>

                                <p>
                                    <?php echo nl2br($item->description); ?>
                                </p>
                            </div>
                        </div>
                        <div class="tab-pane" id="map">
                            <div class="property-detail">
                                <div id='map_canvas'></div>
                            </div>

                        </div>
                        <div class="tab-pane" id="utilities">
                            <div class="property-detail">

                                <p id='property-map'>

                                </p>
                                <h2>Dịch vụ trong vòng bán kính 1km</h2>

                                <p id='list_facility_places'>


                                </p>



                            </div>
                        </div>
                    </div>

                </div>
                <div class="sidebar span3">
                    <div class="widget our-agents">
                        <div class="title">
                            <h2>Liên Hệ</h2>
                        </div><!-- /.title -->

                        <div class="content">
                            <div class="agent">
                                <?php if ($item->contact_name) { ?>

                                    <div class="name">
                                        <?php echo $item->contact_name; ?>
                                    </div>

                                <?php
                            }
                            if ($item->contact_company) { ?>
                                    <div class="name">
                                        <p>Công ty: </p>
                                        <?php echo $item->contact_company; ?>
                                    </div>

                                <?php
                            } ?>


                                <?php
                                if ($item->contact_email) { ?>
                                    <div class="email"><a href="mailto:" <?php echo $item->contact_email; ?>>
                                            <?php echo $item->contact_email; ?></a></div><!-- /.email -->

                                <?php
                            }
                            if ($item->contact_phone) { ?>
                                    <div class="phone"><span class='icon phone'>
                                            <?php echo Util::formatPhone($item->contact_phone); ?>
                                    </div><!-- /.phone -->

                                <?php
                            }
                            if ($item->contact_zalo) { ?>
                                    <div class="phone"><span class='icon zalo'>
                                            <?php echo $item->contact_zalo; ?>
                                    </div><!-- /.phone -->

                                    }
                                    if ($item->contact_viber) { ?>
                                    <div class="phone"><span class='icon viber'>
                                            <?php echo $item->contact_viber; ?>
                                    </div><!-- /.phone -->

                                <?php
                            } ?>
                            </div>

                        </div><!-- /.content -->
                    </div><!-- /.our-agents -->
                    <div class='approve_by'>
                        <span class='txt'>Tin được duyệt bởi</span> <span class='by'><?php echo $item->approval_name; ?></span>
                    </div>
                    <div class="widget our-agents">
                        <div class="title">
                            <h2>Our Agents</h2>
                        </div><!-- /.title -->

                        <div class="content">
                            <?php
                            $length = count($agents);
                            for ($i = 0; $i < $length; $i++) { ?>

                                <div class="agent">
                                    <div class="image">
                                        <img src="../public/<?php echo $agents[$i]->image; ?>" alt="">
                                    </div><!-- /.image -->
                                    <div class="name">
                                        <?php echo $agents[$i]->fullname; ?>
                                    </div><!-- /.name -->
                                    <div class="phone">
                                        <?php echo $agents[$i]->mobile; ?>
                                    </div><!-- /.phone -->
                                    <div class="email"><a href="mailto:<?php echo $agents[$i]->email; ?>">
                                            <?php echo $agents[$i]->email; ?></a></div><!-- /.email -->
                                </div><!-- /.agent -->
                            <?php

                        }
                        if ($length == 0)
                            echo '<span class="message">Không có bất động sản nào được tìm thấy</span>';
                        ?>
                        </div><!-- /.content -->
                    </div><!-- /.our-agents -->

                    <div class="widget contact">

                        <div class="title">
                            <h2 class="block-title">Subscribe for news</h2>
                        </div><!-- /.title -->

                        <div class="content">
                            <form method="post">
                                <p>Stay informed on our latest news!</p>
                                <div class="control-group">
                                    <label class="control-label" for="inputEmail">
                                        Email
                                        <span class="form-required" title="This field is required.">*</span>
                                    </label>
                                    <div class="controls">
                                        <input type="text" id="inputEmail">
                                    </div><!-- /.controls -->
                                </div><!-- /.control-group -->

                                <div class="form-actions">
                                    <input type="submit" class="btn btn-primary arrow-right" value="Subscribe">
                                </div><!-- /.form-actions -->
                            </form>
                        </div><!-- /.content -->
                    </div><!-- /.widget -->
                </div>
            </div>
        </div>
    </div>
</div><!-- /#content -->
<input type="hidden" id='lat_item' value="<?php echo $item->lat; ?>" />
<input type="hidden" id='lng_item' value="<?php echo $item->lng; ?>" />
<script type="text/javascript" src="<?php echo base_url('/public/templates/front/js/core'); ?>/item_detail.js?v=1.1">
</script>
<script type="text/javascript">
    $('#myTab a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    })

    var save_item_second = false;

    function onSaveItem(item_id) {
        if (save_item_second == true) return;
        save_item_second = true;
        API.updateUserItem(item_id, function(resp) {
            if (resp.error_code == 0) {
                var user_item = $('#user_item');
                user_item.html(resp.data.lbl);


                if (resp.data.class == 'save')
                    user_item.removeClass('remove');
                else
                    user_item.removeClass('save');

                user_item.addClass(resp.data.class);
            } else {

            }
            save_item_second = false;
        });
    }
    // MapUtil.init();
    // MapHouse.init();
</script>
<style>
    .nat-client-detail .tab-content {
        padding: 0px !important;
    }

    .bound_status {
        padding: 0 0 15px 0;
        border-bottom: 1px solid #d9e7fd;
        margin-bottom: 15px;
        position: relative;
    }

    .bound_status ul {
        list-style: none;
        margin: 0;
        padding: 0;
        /* width: calc(100% - 125px); */
    }

    .bound_status ul li {
        margin: 0;
        padding: 0;
        display: inline-block;
        padding: 0 10px 0 10px;
        position: relative;
    }

    .bound_status ul li.element:before {
        content: '';
        background: url(../images/icon_detail.png) no-repeat;
        width: 18px;
        height: 16px;
        display: inline-block;
        position: absolute;
        left: 0;
        top: 2;
    }

    .bound_status ul li.location:before {
        background-position-x: -22px;
    }

    .bound_status ul li.item_id:before {
        background-position-x: -42px;
    }

    .bound_status ul li.created_date:before {
        background-position-x: -63px;
    }

    .bound_nav_menu a:hover,
    .bound_nav_menu a.active {
        color: #555 !important;
        border-bottom: 3px solid #4285f4 !important;
    }

    .bound_nav_menu a:focus {
        background-color: none !important;
        border: none !important;
    }

    .bound_nav_menu a {
        color: #777 !important;
    }
</style>