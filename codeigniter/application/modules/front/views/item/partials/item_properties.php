<table class='table table-striped'>
    <tr>
        <td class='lbl'>Loại</td>
        <td class='val'><?php echo $this->lang->line($item->type_of_project); ?></td>
    </tr>

    <?php
	renderRow($this, $item, 'total_area', 'm²');
	renderRow($this, $item, 'building_density', '%');
	renderRow($this, $item, 'total_area_building', 'm²');
	renderRow($this, $item, 'num_floor', 'tầng');
	renderRow($this, $item, 'num_bedrooms', 'phòng');
	renderRowCheck($this, $item, 'red_book');
	renderRowCheck($this, $item, 'pink_book');
	renderRowSelect($this, $item, 'investment_certificate');
	renderRowSelect($this, $item, 'implementation_planning');
	renderRowSelect($this, $item, 'finished_planning');
	renderRowSelect($this, $item, 'during_compensation');
	renderRowSelect($this, $item, 'done_compensation');
	renderRowSelect($this, $item, 'done_red_book');
	renderRowSelect($this, $item, 'done_obligation_finance');
	renderRowSelect($this, $item, 'under_construction');
	renderRowSelect($this, $item, 'done_building');
	renderRowSelect($this, $item, 'on_marketing');
	?>

    <?php
	if (is_numeric($item->orientation_house)) { ?>
    <tr>
        <td class='lbl'><?php echo $this->lang->line('orientation_house'); ?></td>
        <td class='val'><?php echo Item::getOrientation($item->orientation_house - 1); ?></td>
    </tr>
    <?php 
} ?>
    <?php
	if (is_numeric($item->belong_project)) { ?>
    <tr>
        <td class='lbl'><?php echo $this->lang->line('belong_project'); ?></td>
        <td class='val'><?php foreach ($projects as $key => $value) {
							if ($value->id == $item->belong_project)
								echo $value->name;
						} ?></td>
    </tr>
    <?php 
} ?>
    <tr>
        <td class='lbl'>Địa chỉ</td>
        <td class='val'><?php echo $item->full_address; ?></td>
    </tr>

    <tr>
        <td class='lbl'>Tình trạng</td>
        <td class='val'><?php echo $this->lang->line(PostStatus::$Detail[$item->post_status]); ?></td>
    </tr>

    <tr>
        <td class='lbl'>Giá</td>
        <td class='val'><?php echo Item::formatCurrency($item->price_total);
						echo Item::rentMonth($item->type_item); ?></td>
    </tr>
</table>

<?php
function renderRow(&$ctx, &$item, $key, $suffix = '')
{
	if (!empty($item->$key)) { ?>
<tr>
    <td class='lbl'><?php echo $ctx->lang->line($key); ?></td>
    <td class='val'><?php echo Item::formatPrice($item->$key, ' '); ?> <?php echo $suffix; ?></td>
</tr>
<?php 
}
}
function renderRowCheck(&$ctx, &$item, $key)
{
	if (!empty($item->$key)) { ?>
<tr>
    <td class='lbl'><?php echo $ctx->lang->line($key); ?></td>
    <td class='val'>
        <?php 
		$class = 'fa fa-square';
		if ($item->$key == 1)
			$class = 'fa fa-check-square'; ?>

        <i class='<?php echo $class; ?>'></i>
    </td>
</tr>
<?php 
}
}
function renderRowSelect(&$ctx, &$item, $key)
{
	if (!empty($item->$key)) { ?>
<tr>
    <td class='lbl'><?php echo $ctx->lang->line($key); ?></td>
    <td class='val'>
        <?php 
		$data = isset(Configuration::$$key) ? Configuration::$$key : array();
		if ($ctx->lang->line($data[$item->$key]))
			echo $ctx->lang->line($data[$item->$key]); ?>

        <i class='<?php echo $class; ?>'></i>
    </td>
</tr>
<?php 
}
}