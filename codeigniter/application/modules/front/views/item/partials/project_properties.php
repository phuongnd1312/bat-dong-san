<div class='project_properties'>
	<?php
	unset($properties->id);
	unset($properties->type);
	$set_main_area = false;
	foreach ($properties as $key => $opt) {
		if ($opt == 0)
			continue;
		?>

		<?php if (preg_match('/^lbli_(.*)/', $key, $matches)) {
			input($matches[1], $item_properties->$matches[1], $opt, $this, $set_main_area);
		} else if (preg_match('/^lbls_(.*)/', $key, $matches)) {
			if ($matches[1] == 'orientation_house')
				select_orientation_house($matches[1], $item_properties->$matches[1], $this, $orientation_houses);
			else if ($matches[1] == 'belong_project') {
				select_belong_project($matches[1], $item_properties->$matches[1], $this, $projects);
			}
		} else if (preg_match('/^lblc_(.*)/', $key, $matches)) {
			checkbox($matches[1], $item_properties->$matches[1], $this);
		} else if (preg_match('/^lblp_(.*)/', $key, $matches)) {
			select_project($matches[1], $item_properties->$matches[1], $this);
		}
		?>

	<?php
}
?>
	<script type="text/javascript">
		$('select.select_advance_cus').select2();
	</script>
</div>

<?php
function input($key, $val, $opt, &$ctx, &$set_main_area)
{
	$class = '';
	$main_area = '';
	if (!$set_main_area && $opt >= 2) {
		$class = 'cal_equivalent_base';
		$main_area = '<input type="hidden" id="who_main_area" name="main_area" value="' . $key . '" />';
	}
	$class_required = $text_required = '';
	if ($opt != 1) {
		$text_required = '<span>*</span>';
		$class_required = 'required';
	}

	$suffix = '';
	if ($key == 'building_density')
		$suffix = '%';
	else if (strpos($key, 'total_area') !== false)
		$suffix = 'm²';
	?>
	<div class='property <?php echo $class_required; ?>'>
		<label class='lbl'><?php echo $ctx->lang->line($key); ?> <?php echo $text_required; ?></label>
		<input type="text" id='field_<?php echo $key; ?>' value="<?php echo Util::zero2Null(number_format($val, 0, ',', '.')); ?>" name='<?php echo $key; ?>' class='type_number <?php echo $class; ?> val form-control' onkeypress="Util.validate(event)" onkeyup="this.value=Util.numberWithCommas(this.value)">
		<span class='suffix'><?php echo $suffix; ?></span>
		<?php echo $main_area; ?>
	</div>
<?php
}

function select_orientation_house($key, $val, &$ctx, $orientation_houses)
{
	if (isset($val) || $val == NULL)
		$val = 1;
	?>
	<div class='property'>
		<label class='lbl'><?php echo $ctx->lang->line($key); ?></label>
		<select id='field_<?php echo $key; ?>' name='<?php echo $key; ?>' class='val form-control select_advance_cus'>
			<option>--Chọn hướng--</option>
			<?php foreach ($orientation_houses as $orientation) :
				$select = ($orientation['id'] == $val) ? ' selected="selected" ' : '';
				?>
				<option <?php echo $select; ?> value="<?php echo $orientation['id']; ?>"><?php echo $ctx->lang->line($orientation['code']); ?></option>
			<?php endforeach; ?>
		</select>
	</div>
<?php
}
function select_belong_project($key, $val, &$ctx, $projects)
{
	if (isset($val) || $val == NULL)
		$val = 1;
	?>
	<div class='property'>
		<label class='lbl'><?php echo $ctx->lang->line($key); ?></label>
		<select id='sel_<?php echo $key; ?>' name='<?php echo $key; ?>' class='val form-control select_advance_cus'>
			<option value="">--Dự án--</option>
			<?php foreach ($projects as $project) :
				$select = ($project->id == $val) ? ' selected="selected" ' : '';
				?>
				<option <?php echo $select; ?> value="<?php echo $project->id; ?>"><?php echo $project->name; ?></option>
			<?php endforeach; ?>
		</select>
	</div>
<?php
}

function select_project($key, $val, &$ctx)
{
	if (isset($val) || $val == NULL)
		$val = 1;

	$data = isset(Configuration::$$key) ? Configuration::$$key : array();
	?>

	<div class='property'>
		<label class='lbl'><?php echo $ctx->lang->line($key); ?></label>
		<select id='field_<?php echo $key; ?>' name='<?php echo $key; ?>' class='val form-control select_advance_cus'>
			<?php foreach ($data as $index => $el) :
				$select = ($index == $val) ? ' selected="selected" ' : '';
				?>
				<option <?php echo $select; ?> value="<?php echo $index; ?>"><?php echo $ctx->lang->line($el); ?></option>
			<?php endforeach; ?>
		</select>
	</div>
<?php
}

function checkbox($key, $val, &$ctx)
{
	?>
	<div class='property'>
		<label class='lbl'><?php echo $ctx->lang->line($key); ?></label>
		<div class='bound_chk'>
			<input type="checkbox" value="1" name='<?php echo $key; ?>' <?php echo ($val) ? 'checked="checked"' : ''; ?> class='val form-control' style="float: left;" />
		</div>
	</div>
<?php
}
