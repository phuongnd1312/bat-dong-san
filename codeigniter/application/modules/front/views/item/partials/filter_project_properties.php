<div class='project_properties'>
    <?php
    unset($properties->id);
    unset($properties->type);
    foreach ($properties as $key => $opt) {
        if ($opt == 0)
            continue;
        ?>

        <?php
        if (preg_match('/^lbli_(.*)/', $key, $matches)) {
            if ($opt != 1) {
                $val = isset($search_params[$matches[1]]) ? $search_params[$matches[1]] : null;
                select($matches[1], $opt, $val);
            }
        } else if (preg_match('/^lbls_(.*)/', $key, $matches)) {
            $val = isset($search_params[$matches[1]]) ? $search_params[$matches[1]] : null;

            if ($matches[1] == 'orientation_house') {
                select_orientation_house($matches[1], $val, $this, $orientation_houses);
            } else if ($matches[1] == 'belong_project')
                select_belong_project($matches[1], $val, $this, $projects);
        } else if (preg_match('/^lblc_(.*)/', $key, $matches)) {
            $val = isset($search_params[$matches[1]]) ? $search_params[$matches[1]] : null;

            checkbox($matches[1], $val, $this);
        } else if (preg_match('/^lblp_(.*)/', $key, $matches)) {

            $val = isset($search_params[$matches[1]]) ? $search_params[$matches[1]] : null;
            select_project($matches[1], $val, $this);
        }
        ?>
    <?php } ?>
</div>

<!-- Chọn tổng điện tích đất|-Chọn mật độ xây dựng|Chọn số tầng|Chọn phòng tắm, vệ sinh|Chọn phòng ngủ -->
<?php
function select($field_name, $index, $val)
{
    ?>
    <div class="control-group">
        <div class="controls">
            <select name='<?php echo $field_name; ?>' class="advance_combobox">
                <?php $tt = 'FILTER_' . strtoupper($index);
                foreach (Constant::$$tt as $key => $value) {
                    $val_field = $index . '-' . $key;
                    $select = ($val == $val_field) ? ' selected ' : '';
                    ?>
                    <option <?php echo $select; ?> value='<?php echo $val_field; ?>'>
                        <?php echo $value[1]; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
    </div>
<?php
}
function select_orientation_house($key, $val, &$ctx, $orientation_houses)
{
    ?>
    <div class="control-group">
        <div class="controls">
            <select name='<?php echo $key; ?>' class="advance_combobox">
                <option value=''>--Chọn hướng--</option>
                <?php foreach ($orientation_houses as $orientation) :
                    $select = ($orientation['id'] == $val) ? ' selected="selected" ' : '';
                    ?>
                    <option <?php echo $select; ?> value="?php echo $orientation['id']; ?>">
                        <?php echo $ctx->lang->line($orientation['code']); ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div><!-- /.controls -->
    </div><!-- /.control-group -->

<?php
}
function select_belong_project($key, $val, &$ctx, $projects)
{
    ?>
    <div class="control-group">
        <div class="controls">
            <select id='sel_<?php echo $key; ?>' name='<?php echo $key; ?>' class="advance_combobox">
                <option value="">--Chọn dự án--</option>
                <?php foreach ($projects as $project) :
                    $select = ($project->id == $val) ? ' selected="selected" ' : '';
                    ?>
                    <option <?php echo $select; ?> value="<?php echo $project->id; ?>">
                        <?php echo $project->name; ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div><!-- /.controls -->
    </div><!-- /.control-group -->
<?php
}

function select_project($key, $val, &$ctx)
{

    $data = isset(Configuration::$$key) ? Configuration::$$key : array();
    ?>
    <div class="control-group">
        <div class="controls">

            <select name='<?php echo $key; ?>' class="advance_combobox">
                <option value="">
                    <?php echo '--Chọn ' . $ctx->lang->line($key) . '--'; ?>
                </option>
                <?php foreach ($data as $index => $el) :
                    $select = (is_numeric($val) && $index == $val) ? ' selected="selected" ' : '';
                    ?>
                    <option <?php echo $select; ?> value="
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <?php echo $index; ?>">
                        <?php echo $ctx->lang->line($el); ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div><!-- /.controls -->
    </div><!-- /.control-group -->
<?php
}
function checkbox($key, $val, &$ctx)
{
    ?>
    <div class="control-group">
        <div class="controls">
            <select name='<?php echo $key; ?>' class="advance_combobox">
                <option value="">
                    <?php echo '--Chọn ' . $ctx->lang->line($key) . '--'; ?>
                </option>
                <option <?php if (is_numeric($val) && $val == 0) echo "selected"; ?> value="0">Không</option>
                <option <?php if (is_numeric($val) && $val == 1) echo "selected"; ?> value="1">Có</option>
            </select>
        </div>
    </div>
<?php
} ?>