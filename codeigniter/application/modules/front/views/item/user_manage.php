<?php if(0){?>
<div id='wrap_manage_items' class='bound_content manage_items'>
	<div class='main_content'>
		<div class='center items'>
			<div class='list_items' id='user_manage_form'>

		        <ul class="s-list">
				<?php
					$length = count($items);
					for($i = 0; $i < $length; $i++){?>
					<li id='user_item_<?php echo $items[$i]->id; ?>' class="item_ads">

		                <a href='<?php echo Links::pushItem($items[$i]->id); ?>'>
			                <h3 class="hh-sr">
			                 	<span class="date"><?php echo $items[$i]->id; ?>.</span>
			                    <span class='title'><?php echo $items[$i]->title; ?></span>
			                </h3>
		                </a>

		                <div class="padding">
		                    <div class="sr-image">
		                        <a href='<?php echo Item::getLink($items[$i]); ?>'><?php echo Item::photofst($items[$i]); ?></a>
		                    </div>
		                    <div class="date">
		                        <div class="font_pad">
		                            <span class="sp-info pull-right" style="padding-left: 25px"><b class="text-blue">Ngày kết thúc: </b><strong style="color: red;font-size: 16px">
		                               <?php echo Util::formatDateDMY($items[$i]->date_ended); ?></strong>
		                            </span>

		                            <span class="sp-info pull-right" style="padding-left: 25px"><b class="text-blue ">Ngày bắt đầu: </b><strong style="color: black;">
		                                 <?php echo Util::formatDateDMY($items[$i]->date_started); ?></strong>
		                            </span>
		                            <span class="sp-info pull-right" ><b class="text-blue ">Lượt xem: </b><strong style="color: black;">
		                                 <?php echo $items[$i]->hits; ?></strong>
		                            </span>
		                        </div>
		                    </div>
								<?php if($items[$i]->status < ItemStatus::$Approved)
								echo $this->lang->line(ItemStatus::$Detail[$items[$i]->status]);
							else{ ?>
								<select name='status' class='item_status status' for='<?php echo $items[$i]->id; ?>'>
								<?php
									for($k = 3; $k < count(ItemStatus::$Detail); $k++){
										$selected = ($items[$i]->status == $k) ? ' selected="selected" ' : '';
										?>
										<option <?php echo $selected; ?> value='<?php echo $k; ?>'>
											<?php echo $this->lang->line(ItemStatus::$Detail[$k]); ?>
										</option>
								<?php
									}
								?>
								</select>

								<select name='post_status' class='post_status status' for='<?php echo $items[$i]->id; ?>'>
								<?php
									for($j = 1; $j < count(PostStatus::$Detail); $j++){
										$selected = ($items[$i]->post_status == $j) ? ' selected="selected" ' : '';
										?>
										<option <?php echo $selected; ?> value='<?php echo $j; ?>'>
											<?php echo $this->lang->line(PostStatus::$Detail[$j]); ?>
										</option>
								<?php
									}
								?>
								</select>
							<?php
								}?>
							<!--
							<div class=''>
								<a href='<?php echo base_url('front/items/remove/'.$items[$i]->id) ?>' onclick='return onRemove(this);'>Xóa tin</a>
							</div>-->
		                </div>
		            </li>
				<?php
					}
					if($length == 0)
						echo '<span class="message">Không có bất động sản nào được tìm thấy</span>';
				?>
		        </ul>

			</div>

			<div class='bound_pagination'>
				<ul class='pagination'>
				<?php echo $pagination; ?>
				</ul>
			</div>
		</div>
		<div class='clear'></div>
	</div>
</div>
<?php }?>
<script type="text/javascript">
	$('select.item_status').change(function(){
		var status = $(this).val();
		var item_id = $(this).attr('for');
		API.updateItemStatus({item_id: item_id, status: status}, function(resp){
			console.log(resp);
		});
	});
	$('select.post_status').change(function(){
		var status = $(this).val();
		var item_id = $(this).attr('for');
		API.updatePostStatus({item_id: item_id, status: status}, function(resp){
			console.log(resp);
		});
	});

	function onRemove(event){
		return confirm(jlang.js_are_you_sure);
	}
</script>

<div id="content" class="cs-content">
	<div class="container">
		<div class="row">
			<div class="span3">
				<?php echo Mhtml::loadView('front/partials/user_menu'); ?>

			</div>
			<div class="span9 user_area">
				<h3 class="title">Danh sách tin</h3>
				<?php
					$length = count($items);
					for($i = 0; $i < $length; $i++){?>
						<!--item -->
						<div class="properties-rows" id='user_item_<?php echo $items[$i]->id; ?>'>
						    <div class="row">
						        <div class="property span9">
						            <div class="row">
						                <div class="image span3">
						                    <div class="content">
						                        <a href='<?php echo Links::pushItem($items[$i]->id); ?>'></a>
						                        <?php echo Item::photofst($items[$i]); ?>
						                    </div><!-- /.content -->
						                </div><!-- /.image -->

						                <div class="body span6">
						                    <div class="title-price row">
						                        <div class="title span4">
						                            <h2><a href="<?php echo Item::getLink($items[$i]); ?>"><?php echo $items[$i]->id; ?>.<?php echo $items[$i]->title; ?></a></h2>
						                        </div><!-- /.title -->

						                        <div class="price">

						                        </div><!-- /.price -->
						                    </div><!-- /.title -->
						                     <div class="location"><?php echo $items[$i]->full_address; ?></div><!-- /.location -->
						                    <div>
						                    	<?php if($items[$i]->status < ItemStatus::$Approved)
												echo '<span class="item-status">'.$this->lang->line(ItemStatus::$Detail[$items[$i]->status]).'</span>';
											else{ ?>
												<select name='status' class='item_status status' for='<?php echo $items[$i]->id; ?>'>
												<?php
													for($k = 3; $k < count(ItemStatus::$Detail); $k++){
														$selected = ($items[$i]->status == $k) ? ' selected="selected" ' : '';
														?>
														<option <?php echo $selected; ?> value='<?php echo $k; ?>'>
															<?php echo $this->lang->line(ItemStatus::$Detail[$k]); ?>
														</option>
												<?php
													}
												?>
												</select>

												<select name='post_status' class='post_status status' for='<?php echo $items[$i]->id; ?>'>
												<?php
													for($j = 1; $j < count(PostStatus::$Detail); $j++){
														$selected = ($items[$i]->post_status == $j) ? ' selected="selected" ' : '';
														?>
														<option <?php echo $selected; ?> value='<?php echo $j; ?>'>
															<?php echo $this->lang->line(PostStatus::$Detail[$j]); ?>
														</option>
												<?php
													}
												?>
												</select>
											<?php
												}?>
						                    </div>


						                    <div class="area">
						                        <span class="key">Ngày kết thúc:</span><!-- /.key -->
						                        <span class="value">
						                        	<?php echo Util::formatDateDMY($items[$i]->date_ended); ?>

					                        	</span><!-- /.value -->
						                    </div><!-- /.area -->
						                    <div class="area">
						                        <span class="key">Ngày bắt đầu:</span><!-- /.key -->
						                        <span class="value">
						                        	<?php echo Util::formatDateDMY($items[$i]->date_started); ?>

					                        	</span><!-- /.value -->
						                    </div><!-- /.area -->
						                    <div class="area">
						                        <span class="key">Lượt xem:</span><!-- /.key -->
						                        <span class="value">
						                        	<?php echo $items[$i]->hits; ?>

					                        	</span><!-- /.value -->
						                    </div><!-- /.area -->

						                </div><!-- /.body -->
						            </div><!-- /.property -->
						        </div><!-- /.row -->
						    </div>
						</div>
						<!-- end item-->

					<?php }
					if($length == 0)
						echo '<span class="message">Không có bất động sản nào được tìm thấy</span>';
					?>

                <div class='bound_pagination'>
                    <ul class='pagination'>
                    <?php echo $pagination; ?>
                    </ul>
			    </div>
			</div>
		</div>
	</div>
</div>