<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel='stylesheet' type='text/css' href="../public/templates/front/old/css/select2.min.css?v=1" />
<script type="text/javascript" src="../public/templates/front/old/js/lib/select2.min.js"></script>
<div id="content" class="cs-content">
    <div class="container">
        <div class="row">
            <div class="span3">
                <?php echo Mhtml::loadView('front/partials/user_menu'); ?>

            </div>
            <div class="span9 user_area">
                <div id='wrap_post_item' class='bound_content'>
                    <form action='' name='frm_post_item' method="post" id='frm_post_item'>
                        <div class='main_container'>
                            <h1 class='title'>
                                ĐĂNG TIN RAO BÁN , CHO THUÊ NHÀ ĐẤT
                                <span class='note'>Điền các thông tin của tin đăng</span>
                            </h1>

                            <div class='block info'>
                                <div class='block_title'><span>Thông tin chung</span></div>
                                <table style="width: 100%">
                                    <tr class='required'>
                                        <td class='lbl'>Chọn hình thức <span>*</span></td>
                                        <td class='val'>
                                            <select name='type_item' class='form-control advance_custom' id='field_type_item'>
                                                <option value=''>--Chọn hình thức--</option>
                                                <?php 
												foreach ($type_items as $val) {
													$select = ($val['id'] == $item->type_item) ? ' selected="selected" ' : '';
													?>
                                                <option <?php echo $select; ?> value='<?php echo $val['id']; ?>' placeholder='<?php echo $val['default_days']; ?>'>
                                                    <?php echo $this->lang->line($val['code']); ?>
                                                </option>
                                                <?php

											} ?>
                                            </select>
                                        </td>
                                        <td class='suffix'></td>
                                    </tr>

                                    <tr class='required'>
                                        <td class='lbl'>Chọn loại tin đăng <span>*</span></td>
                                        <td class='val'>
                                            <select name='project_type' class='form-control advance_custom' id='field_project_type'>
                                                <option value=''>--Chọn loại tin đăng--</option>
                                                <?php foreach ($properties_proportion as $val) {
													$select = ($val->id == $item->project_type) ? ' selected="selected" ' : '';
													?>
                                                <option <?php echo $select; ?>value='<?php echo $val->id; ?>'><?php echo $this->lang->line($val->type); ?></option>
                                                <?php

											} ?>
                                            </select>
                                        </td>
                                        <td class='suffix'></td>
                                    </tr>

                                    <tr class='required'>
                                        <td class='lbl'>Tiêu đề <span>*</span></td>
                                        <td class='val' colspan="2">
                                            <input name='title' class='form-control' id='field_title' maxlength="99" value='<?php echo $item->title; ?>' placeholder='Nhập tiêu đề' />
                                            <span id='num_of_characters'></span>
                                        </td>
                                    </tr>

                                    <tr class='bound_chkbox'>
                                        <td class='lbl'>Giúp tôi hoàn thiện tin này</td>
                                        <td class='val' colspan="2">
                                            <input type="checkbox" value='1' name='need_help' <?php echo ($item->need_help) ? ' checked="checked" ' : ''; ?> class='chk_box form-control' style="float: left;" />
                                            <p style="float: left;">Khi bạn chọn chức năng này, nhân viên của chúng tôi sẽ giúp bạn hoàn thiện tin.</p><br>
                                        </td>
                                    </tr>

                                    <tr class='required'>
                                        <td class='lbl'>Thời gian đăng tin <span>*</span></td>
                                        <td class='val'>
                                            <input type="text" name='time_to_expire' id='field_time_to_expire' value="<?php echo Util::zero2Null(Util::distance2Days($item->date_ended, $item->date_started)); ?>" class='type_number form-control' placeholder='Nhập thời gian đăng tin' /> Ngày
                                        </td>
                                        <td class='suffix'>
                                            <span class='suffix'></span>
                                        </td>
                                    </tr>

                                    <tr class='required'>
                                        <td class='lbl'>Ngày bắt đầu <span>*</span></td>
                                        <td class='val'>
                                            <input type='text' name='date_started' id='picker_date_started' value='<?php echo Util::formatDateDMY($item->date_started); ?>' class='form-control' placeholder='Nhập ngày bắt đầu' />
                                        </td>
                                        <td class='suffix'></td>
                                    </tr>
                                </table>
                            </div>

                            <div class='block location'>
                                <div class='block_title'><span>Vị trí</span></div>
                                <table>
                                    <tr class='required'>
                                        <td class='lbl'>Tỉnh/Thành Phố <span>*</span></td>
                                        <td class='val'>
                                            <select name='add_level_1' class='form-control advance_custom' id='select_city'>
                                                <option value="">--Tỉnh/Thành Phố--</option>
                                                <?php foreach ($cities as $city) :
													$select = ($city->id == $item->add_level_1) ? ' selected="selected" ' : '';
													?>
                                                <option <?php echo $select; ?> value="<?php echo $city->id; ?>"><?php echo $city->name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </td>
                                        <td class='suffix'></td>
                                    </tr>

                                    <tr class='required'>
                                        <td class='lbl'>Quận/Huyện <span>*</span></td>
                                        <td class='val'>
                                            <select name='add_level_2' class='form-control advance_custom' id='select_district'>
                                                <option value="">--Quận/Huyện--</option>
                                                <?php foreach ($districts as $district) :
													$select = ($district->id == $item->add_level_2) ? ' selected="selected" ' : '';
													?>
                                                <option <?php echo $select; ?> value="<?php echo $district->id; ?>"><?php echo $district->full_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr class='required'>
                                        <td class='lbl'>Phường/Xã <span>*</span></td>
                                        <td class='val'>
                                            <select name='add_level_3' class='form-control advance_custom' id='select_ward'>
                                                <option value="">--Phường/Xã--</option>
                                                <?php foreach ($wards as $ward) :
													$select = ($ward->id == $item->add_level_3) ? ' selected="selected" ' : '';
													?>
                                                <option <?php echo $select; ?> value="<?php echo $ward->id; ?>"><?php echo $ward->name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr class=''>
                                        <td class='lbl'>Đường/Phố</td>
                                        <td class='val'>
                                            <select name='add_level_4' class='form-control advance_custom' id='select_street'>
                                                <option value="">--Đường/Phố--</option>
                                                <?php foreach ($streets as $street) :
													$select = ($street->id == $item->add_level_4) ? ' selected="selected" ' : '';
													?>
                                                <option <?php echo $select; ?> value="<?php echo $street->id; ?>"><?php echo $street->name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr class=''>
                                        <td class='lbl'>Số nhà</td>
                                        <td class='val'>
                                            <input type="text" name='num_house' value='<?php echo $item->num_house; ?>' id='field_num_house' class='form-control' placeholder='Nhập số nhà' />
                                        </td>
                                    </tr>

                                    <tr class=''>
                                        <td class='lbl'>Địa chỉ</td>
                                        <td class='val' colspan="2">
                                            <input type="text" name='full_address' value='<?php echo $item->full_address; ?>' id='field_full_address' readonly="true" class='form-control' placeholder='Nhập địa chỉ' style="width: 150%;" />
                                        </td>
                                    </tr>

                                </table>
                            </div>

                            <div class='block propertise'>
                                <div class='block_title'><span>Thuộc tính</span></div>
                                <div id='project_properties'>

                                </div>
                            </div>

                            <div class='block content'>
                                <div class='block_title'><span>Nội dung tin đăng</span></div>
                                <table>
                                    <tr class='required'>
                                        <td colspan="2" class='lbl'>Nội dung tin đăng <span>*</span></td>
                                    </tr>
                                    <tr>
                                        <td class="required">
                                            <textarea name='description' rows="8" class='form-control' placeholder='Nhập nội dung tin đăng'><?php echo $item->description; ?></textarea>
                                        </td>
                                        <td class='suffix'>
                                            <span class='suffix'>Tối đa 3000 ký tự</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>


                            <div class='block content'>
                                <div class='block_title'><span>Giá</span></div>
                                <table>
                                    <tr class=''>
                                        <td class='lbl'>Tổng giá tiền</td>
                                        <td class='val'>
                                            <input type="text" name='price_total' value='<?php echo Item::formatPrice($item->price_total, ''); ?>' id='field_price_total' class='type_number form-control' placeholder='Nhập giá tiền' />
                                            <p id='field_price_total_string'></p>

                                        </td>
                                        <td class='suffix'><span class='suffix'>VND</span></td>
                                    </tr>

                                    <tr class='bound_chkbox'>
                                        <td class='lbl'>Giá có thể thương lượng</td>
                                        <td class='val'>
                                            <input type="checkbox" name='price_equivalent' value='1' <?php echo ($item->price_equivalent) ? ' checked="checked" ' : ''; ?> class='chk_box form-control' placeholder='Nhập giá thương lượng' />
                                        </td>
                                        <td class='suffix'></td>
                                    </tr>

                                    <tr class=''>
                                        <td class='lbl'>Đơn giá tương đương</td>
                                        <td class='val'>
                                            <input type="text" name='price_square' value='<?php echo Item::formatPrice($item->price_square, ''); ?>' id='field_price_square' class='type_number form-control' placeholder='' readonly />
                                        </td>
                                        <td class='suffix'><span class='suffix'>VND/m²</span></td>
                                    </tr>

                                </table>
                            </div>

                            <div class='block content'>
                                <div class='block_title'><span>Liên hệ</span></div>
                                <table>
                                    <tr class=''>
                                        <td class='lbl'>Tên liên hệ</td>
                                        <td class='val'>
                                            <input type="text" name='contact_name' value='<?php echo $item->contact_name; ?>' class='form-control' placeholder='Nhập tên liên hệ' />
                                        </td>
                                    </tr>

                                    <tr class=''>
                                        <td class='lbl'>Tên công ty</td>
                                        <td class='val'>
                                            <input type="text" name='contact_company' value='<?php echo $item->contact_company; ?>' class='form-control' placeholder='Nhập tên công ty' />
                                        </td>
                                    </tr>

                                    <tr class=''>
                                        <td class='lbl'>Phone</td>
                                        <td class='val'>
                                            <input type="text" name='contact_phone' value='<?php echo $item->contact_phone; ?>' class='form-control' placeholder='Nhập số điện thoại' />
                                        </td>
                                    </tr>

                                    <tr class=''>
                                        <td class='lbl'>Zalo</td>
                                        <td class='val'>
                                            <input type="text" name='contact_zalo' value='<?php echo $item->contact_zalo; ?>' class='form-control' placeholder='Nhập nick zalo' />
                                        </td>
                                    </tr>

                                    <tr class=''>
                                        <td class='lbl'>Viber</td>
                                        <td class='val'>
                                            <input type="text" name='contact_viber' value='<?php echo $item->contact_viber; ?>' class='form-control' placeholder='Nhập nick viber' />
                                        </td>
                                    </tr>

                                    <tr class=''>
                                        <td class='lbl'>Email</td>
                                        <td class='val'>
                                            <input type="text" name='contact_email' value='<?php echo $item->contact_email; ?>' class='form-control' placeholder='Nhập email' />
                                        </td>
                                    </tr>

                                    <tr class='bound_chkbox'>
                                        <td class='lbl'>Nhận email phản hồi</td>
                                        <td class='val'>
                                            <input type="checkbox" name='subscribe' class='chk_box form-control' checked />
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class='block photos'>
                                <div class='block_title'><span>Hình ảnh</span></div>
                                <input id="field_file_upload" type="file" accept="image/*" name="file_upload" multiple='multiple' url='<?php echo base_url('front/items/upload_img/' . $item->id); ?>' />
                                <div class='tips'>Tin rao có ảnh sẽ được xem nhiều hơn gấp 10 lần và được nhiều người gọi gấp 5 lần so với tin rao không có ảnh. Hãy đăng ảnh để được giao dịch nhanh chóng!</div>
                                <div class="bound_photos" style="display: inline-block;">
                                    <?php
									$photos = Item::photos($item);

									$length = count($photos);

									for ($i = 0; $i < $length; $i++) {
										$class = ($i == 0) ? 'default' : '';
										?>
                                    <div class='bound_item_file photo <?php echo $class; ?>'>
                                        <div class='bound_middle'>
                                            <img class='photo_item' src='<?php echo Util::loadImg($photos[$i]); ?>' url='<?php echo $photos[$i]; ?>' />
                                        </div>
                                        <a href="javascript:;" onclick="removePhoto(this, '<?php echo $photos[$i]; ?>');" class="remove"></a>
                                    </div>
                                    <?php

								}
								?>
                                    <div class='bound_item_file' id='control_upload'>
                                        <span class='text'>Tải hình ảnh</span>
                                    </div>

                                    <div class='clear'></div>
                                </div>
                                <div class='resolution'>Hình ảnh tối đa 5Mb.</div>
                            </div>
                            <div class='block video'>
                                <div class='block_title'><span>Link Video Youtube</span>
                                </div>
                                <span class='text'>Chèn đường dẫn video của bạn</span>
                                <input type="text" name='video' class='input_video form-control' value='<?php if (isset($item->video)) echo $item->video; ?>' id='field_input_video' />
                                <br />
                                <!--<span class='text'>Hoặc đăng tải video (có dung lượng < 100MB)</span>
								<input type='file' id='file_video' class='input' type="text" name='file_video' />-->

                            </div>
                            <div class='block other'>
                                <div class='block_title'><span>Thông tin khác</span></div>
                                <div class='note'>
                                    Vị trí của nhà đất được chúng tôi thiết lập một cách tự động dựa vào thông tin đường phố, phường, quận
                                    mà bạn cung cấp.<br />Nếu bạn không rõ về bản đồ, nhân viên của chúng tôi sẽ giúp bạn.
                                </div>
                                <div id='map_canvas' style="height: 350px;"></div>

                                <table class='bound_control'>
                                    <tr class='required captcha'>
                                        <td class='lbl' valign="top">Mã an toàn <span>*</span>:</td>
                                        <td class='val'>
                                            <img style="max-width: 75%;" id='siimage' title="Click to refresh" src="<?php echo base_url('/front/ajax/show_captcha'); ?>" />
                                            <i class='button fa fa-refresh' onclick="onRefreshCaptcha();"></i>
                                            <input type="text" name='captcha' class='input_captcha form-control' id='field_input_captcha' />
                                            <span class='text-error' id='captcha_message'></span>
                                        </td>
                                        <td class='suffix'>Trong trường hợp hiện ra thông báo "Mã an toàn bạn nhập vào chưa đúng" bạn vui lòng nhấn vào nút
                                            <i class='fa fa-refresh'></i>để tạo mã an toàn mới và thử lại.</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <?php 
										$title_btn_post = 'Đăng tin';
										if ($user->user_role == UserRole::$MANAGER)
											$title_btn_post = 'Cập nhật';
										?>
                                        <td>
                                            <input type="button" value='<?php echo $title_btn_post; ?>' class='btn btnc btn_post_new' onclick="return onSubmitItem(0);" />
                                            <a href="<?php echo base_url('front/items/manage'); ?>" class='btn btn-danger btn_cancel_post'>Hủy đăng tin</a>
                                        </td>
                                        <td class='suffix'>
                                            <?php if ($user->user_role == UserRole::$MANAGER) { ?>
                                            <input type="button" value='Duyệt tin' onclick="return onSubmitItem(2);" class='btn btnc btn_post_new' />
                                            <?php

										} else { ?>
                                            <input type="button" value='Lưu web vào nháp mà chưa đăng tin' onclick="return onSubmitItem(1);" class='btn btnc btn_post_new' />
                                            <?php 
										} ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <input type="hidden" name='id' value='<?php echo $item->id; ?>' id='field_item_id' />
                            <input type="hidden" name='lat' id='field_latitude' value='<?php echo $item->lat; ?>' />
                            <input type="hidden" name='lng' id='field_longitude' value='<?php echo $item->lng; ?>' />
                            <input type='hidden' name='photos' id='field_photos' value='' />
                            <input type='hidden' name='token' id='field_token' value='' />
                            <input type="checkbox" style="display:none;" class="cb-hd" name='draft' id='field_draft' value='1' />
                            <input type="checkbox" style="display:none;" class="cb-hd" name='approved' id='field_approval' value='1' />
                        </div>
                    </form>
                </div>
                <script type="text/javascript">
                    if ($(window).width() < 979) {
                        $('table').each(function() {
                            $(this).replaceWith($(this).html()
                                .replace(/<tbody/gi, "<div role='form' class='form-horizontal'")
                                .replace(/<tr/gi, "<div ")
                                .replace(/<\/tr>/gi, "</div>")
                                .replace(/<td/gi, "<div class='col-sm-12'")
                                .replace(/<\/td>/gi, "</div>")
                                .replace(/<\/tbody/gi, "<\/div")
                            );
                        });
                    }
                </script>
                <script type="text/javascript" src="<?php echo base_url('/public/templates/front/old'); ?>/js/item_form.js?v=1.1"></script>
            </div>
        </div>
    </div>
</div> 