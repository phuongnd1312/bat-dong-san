<style type="text/css">
#photo{
    float: left; margin-right: 10px;
    max-width:500px; 
}
#photo img{
    height: 100%;
    max-width: 100%;
}
#preview{
    float: left;
    position: relative;
    overflow: hidden;
    width: 300px;
    height: 225px;
    margin-right: 20px;
    border:1px solid #ccc;
}
.btn_crop{
    color: #fff;
    background-color: #d9534f;
    border-color: #d43f3a;
    padding: 6px 15px; 
    border: 0;
}
</style>
<link rel='stylesheet' type='text/css' href="<?php echo base_url('/public/templates/front/css'); ?>/imgareaselect-default.css?v=1.0" />
<script type="text/javascript" src="<?php echo base_url('/public/templates/front/js/lib'); ?>/jquery-1.11.3.min.js?v=1.1"></script>   
<script type="text/javascript" src="<?php echo base_url('/public/templates/front/js'); ?>/jquery.imgareaselect.pack.js?v=1.1"></script>   

<?php $url_image = Util::loadImg($image); ?>
<div class='wrap_contain_crop'>
   
    <img id="photo" src='<?php echo $url_image; ?>'/>

</div>

<form action="<?php echo base_url('/front/items/crop'); ?>" method="post" id='frm_crop_photo'>
    <input type="hidden" name="image" value="<?php echo $image; ?>" />
    <input type="hidden" name="item_id" value="<?php echo $item_id; ?>" />
    <input type="hidden" name="y1" value="" />
    <input type="hidden" name="x1" value="" />
    <input type="hidden" name="y1" value="" />
    <input type="hidden" name="x2" value="" />
    <input type="hidden" name="y2" value="" />
    <input type="hidden" name="w" value="" />
    <input type="hidden" name="h" value="" />
    <input type="hidden" name="w_c" value="" />
    <input type="hidden" name="h_c" value="" />
    <input type="button" value="Crop" class='btn_crop' onclick="onCrop()"; />
</form>

<script>
    var photo_crop = jQuery('#photo'); 
    var photo_height = photo_width = null;
    function preview(img, selection) { 
        var scaleX = 300 / (selection.width || 1); 
        var scaleY = 225 / (selection.height || 1); 
        jQuery('#photo + div > img').css({ 
            width: Math.round(scaleX * photo_width) + 'px', 
            height: Math.round(scaleY * photo_height) + 'px', 
            marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px', 
            marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
        }); 
    } 

    function render(){
		$('#photo').attr('src','<?php echo $url_image; ?>');
        jQuery('<div id="preview"><img src="<?php echo $url_image; ?>" style="position: relative;" /><div>') .insertAfter(photo_crop); 

        var y2 = photo_width*150/200;
        photo_crop.imgAreaSelect({ 
            aspectRatio: '200:150', 
            handles: true,
            persistent: true,
            x1: 0, y1: 0, x2: 200, y2: 150,
            onInit: preview,
            onSelectChange: preview, 
            onSelectEnd: function ( image, selection ) {
                jQuery('input[name=x1]').val(selection.x1); 
                jQuery('input[name=y1]').val(selection.y1); 
                jQuery('input[name=x2]').val(selection.x2); 
                jQuery('input[name=y2]').val(selection.y2);
                jQuery('input[name=w]').val(selection.width);
                jQuery('input[name=h]').val(selection.height);  
            } 
        }); 
    } 

    

    photo_crop.load(function() {
        if(photo_width == null){
            photo_width = $(this).width();
            photo_height = $(this).height();
            jQuery('input[name=h_c]').val($(this).height());
            jQuery('input[name=w_c]').val($(this).width());
            render();
        }
       
    });

    function onCrop(){
        var x2 = jQuery('input[name=x2]').val();
        var y2 = jQuery('input[name=y2]').val();
        if(x2 && y2)
            $('#frm_crop_photo').submit();
        else
            alert('Bạn cần điều chỉnh lại khung crop');
    }

</script>