<div id='wrap_list_view' class='bound_content contain_homepage'>
	<div class='bound_slidebar_left'>
		<?php echo MHtml::loadView('front/partials/slide_filter'); ?>
	</div>

	<div class='main_content'>

		<?php echo Mhtml::loadView('front/partials/control_map_list', array('selected'=>1)); ?>
		<div class='list_items'>
		
		<table style='margin-top: -12px;'>
		<?php 
			$length = count($items);
			for($i = 0; $i < $length; $i++){?>
			<tr class='item'>
		        <td class='photo'>
					<a href='<?php echo Item::getLink($items[$i]); ?>'><?php echo Item::photofst($items[$i]); ?></a>
		        </td>
		        <td class='left'>	
		        <table style="width: 100%">	
					<tr class='save_item' id='user_item_<?php echo $items[$i]->id; ?>' >
					<td class='content'>
						<h3 class='title'>
							<a href='<?php echo Item::getLink($items[$i]); ?>'><?php echo $items[$i]->title; ?></a>
						</h3>
						</td>
								<?php
								if(isset($user_items)){
									if(isset($user_items[$items[$i]->id])){
										$text_usersitem = 'Xóa lưu tin';
										$link_remove_item = '';
										$class ='remove';
									}else{
										$text_usersitem = 'Lưu tin';
										$link_remove_item = '';
										$class ='save';
									}
								?>
									<td class='lbl'><?php echo $text_usersitem; ?></td>
									<td class='property'>
										<a href='javascript:;' onclick="onSaveItem(<?php echo $items[$i]->id; ?>)" class='btn_item <?php echo $class; ?>'></a>
									</td>
								<?php } ?>
					</tr>
					<tr class='price_item' >
					<td rowspan="2">
						<div class='description'>
							<?php echo nl2br(Util::truncateWords($items[$i]->description,3,25, '...')); ?>
						</div>
						</td>
								<td class='lbl'>Giá</td>
								<td class='property'>
									<?php echo Item::formatCurrency($items[$i]->price_total);?>
								</td>
					</tr>
					<tr class='area_item' >
								<td class='lbl'>Diện tích</td>
								<td class='property'>
									<?php echo Item::getArea($items[$i], true); ?>
								</td>
					</tr>
					<tr class='date_item' >
					<td class='content'>
						<div class='address'>
							<?php echo $items[$i]->full_address; ?>
						</div>
						</td>
								<td class='lbl'><div class="border_date1">Ngày đăng</div></td>
								<td class='property'>
									<div class="border_date2"><?php echo Util::formatDate($items[$i]->created_date); ?></div>
								</td>
					</tr>
					</table>		
		        </td>
      		</tr>
		<?php
			}
			if($length == 0)
				echo '<span class="message">Không có bất động sản nào được tìm thấy</span>';
		?>
		</table>
		</div>

		<div class='bound_pagination'>
			<ul class='pagination'>
			<?php echo $pagination; ?>
			</ul>
		</div>
	</div>
	<div class='clear'></div>
</div>
<script type="text/javascript">
var save_item_second = false;
function onSaveItem(item_id){
	if(save_item_second ==  true) return;
	save_item_second =  true;
	API.updateUserItem(item_id, function(resp){
		if(resp.error_code == 0){
			var user_item = $('#user_item_'+item_id);
			user_item.find('td.lbl').html(resp.data.lbl);

			var link = user_item.find('a.btn_item');
			if(resp.data.class == 'save')
				link.removeClass('remove');
			else
				link.removeClass('save');

			link.addClass(resp.data.class);
		}else{

		}
		save_item_second = false;
	});
}
</script>