<div id='wrap_search_view' class='bound_content contain_homepage'>
    <div class='bound_slidebar_left'>
        <?php echo MHtml::loadView('front/partials/slide_filter'); ?>
    </div>

    <div class='main_content'>
        <div class='list_items'>
            <?php
            $length = count($items);
            for ($i = 0; $i < $length; $i++) { ?>
                <tr class='item'>
                    <td class='photo'>
                        <a href='<?php echo Item::getLink($items[$i]); ?>'><?php echo Item::photofst($items[$i]); ?></a>
                    </td>
                    <td class='left'>
                        <table style="width: 100%">
                            <tr class='save_item' id='user_item_<?php echo $items[$i]->id; ?>'>
                                <td class='content'>
                                    <h3 class='title'>
                                        <a href='<?php echo Item::getLink($items[$i]); ?>'><?php echo $items[$i]->title; ?></a>
                                    </h3>
                                </td>
                                <?php
                                if (isset($user_items)) {
                                    if (isset($user_items[$items[$i]->id])) {
                                        $text_usersitem = 'Xóa lưu tin';
                                        $link_remove_item = '';
                                        $class = 'remove';
                                    } else {
                                        $text_usersitem = 'Lưu tin';
                                        $link_remove_item = '';
                                        $class = 'save';
                                    }
                                    ?>
                                    <td class='lbl'><?php echo $text_usersitem; ?></td>
                                    <td class='property'>
                                        <a href='javascript:;' onclick="onSaveItem(<?php echo $items[$i]->id; ?>)" class='btn_item <?php echo $class; ?>'></a>
                                    </td>
                                <?php
                            } ?>
                            </tr>
                            <tr class='price_item'>
                                <td rowspan="2">
                                    <div class='description'>
                                        <?php echo nl2br(Util::truncateWords($items[$i]->description, 3, 45, '...')); ?>
                                    </div>
                                </td>
                                <td class='lbl'>Giá</td>
                                <td class='property'>
                                    <?php echo Item::formatCurrency($items[$i]->price_total); ?>
                                </td>
                            </tr>
                            <tr class='area_item'>
                                <td class='lbl'>Diện tích</td>
                                <td class='property'>
                                    <?php echo Item::getArea($items[$i], true); ?>
                                </td>
                            </tr>
                            <tr class='date_item'>
                                <td class='content'>
                                    <div class='address'>
                                        <?php echo $items[$i]->full_address; ?>
                                    </div>
                                </td>
                                <td class='lbl'>Ngày đăng</td>
                                <td class='property'>
                                    <?php echo Util::formatDate($items[$i]->created_date); ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            <?php

        }
        if (isset($message))
            echo '<span class="message">' . $message . '</span>';
        else if ($length == 0)
            echo '<span class="message">Không có bất động sản nào được tìm thấy</span>';
        ?>
        </div>
        <?php if (isset($pagination)) { ?>
            <div class='bound_pagination'>
                <ul class='pagination'>
                    <?php echo $pagination; ?>
                </ul>
            </div>
        <?php
    } ?>
    </div>
    <div class='clear'></div>
</div>