<div id="content" class="nat-client-list">
    <div class="container">
        <div id="main">
            <div class="row">

                <div class="sidebar span3">
                    <div class="widget our-agents">
                        <div class="title">
                            <h2>Tìm Kiếm</h2>
                        </div><!-- /.title -->
                        <?php echo MHtml::loadView('front/partials/slide_filter_list'); ?>
                    </div><!-- /.ad -->
                    <div class="widget our-agents">
                        <div class="title">
                            <h2>Our Agents</h2>
                        </div><!-- /.title -->

                        <div class="content">
                            <?php 
                            $length = count($agents);
                            for ($i = 0; $i < $length; $i++) { ?>

                            <div class="agent">
                                <div class="image">
                                    <img src="../public/<?php echo $agents[$i]->image; ?>" alt="">
                                </div><!-- /.image -->
                                <div class="name">
                                    <?php echo $agents[$i]->fullname; ?>
                                </div><!-- /.name -->
                                <div class="phone">
                                    <?php echo $agents[$i]->mobile; ?>
                                </div><!-- /.phone -->
                                <div class="email"><a href="mailto:<?php echo $agents[$i]->email; ?>">
                                        <?php echo $agents[$i]->email; ?></a></div><!-- /.email -->
                            </div><!-- /.agent -->
                            <?php

                        }
                        if ($length == 0)
                            echo '<span class="message">Không có bất động sản nào được tìm thấy</span>';
                        ?>
                        </div><!-- /.content -->
                    </div><!-- /.our-agents -->

                </div>
                <div class="span9">
                    <h1 class="page-header">Listing rows</h1>

                    <div class="properties-rows">
                        <div class="row">
                            <?php 
                            $length = count($items);
                            for ($i = 0; $i < $length; $i++) { ?>
                            <?php
                            if (isset($user_items)) {
                                if (isset($user_items[$items[$i]->id])) {
                                    $text_usersitem = 'Xóa lưu tin';
                                    $link_remove_item = '';
                                    $class = 'remove';
                                } else {
                                    $text_usersitem = 'Lưu tin';
                                    $link_remove_item = '';
                                    $class = 'save';
                                }
                                ?>
                            <span class="pull-right iconsave">
                                <a href='javascript:;' id='user_item_<?php echo $items[$i]->id; ?>'
                                    onclick="onSaveItem(<?php echo $items[$i]->id; ?>)"
                                    class='btn_item <?php echo $class; ?>'> <label class="btn btn-primary lbl"
                                        style="    margin-bottom: 5px;">
                                        <?php echo $text_usersitem; ?></label></a>

                            </span>

                            <?php 
                        } ?>

                            <div class="property span9" id='user_item_<?php echo $items[$i]->id; ?>'>
                                <div class="row">
                                    <div class="image span3">
                                        <div class="content">
                                            <a href='<?php echo Item::getLink($items[$i]); ?>' class="nat-list-item">
                                                <?php echo Item::photofst($items[$i]); ?></a>
                                        </div><!-- /.content -->
                                        <div class="nat-price">
                                            <?php echo Item::formatCurrency($items[$i]->price_total); ?>
                                        </div><!-- /.price -->
                                    </div><!-- /.image -->

                                    <div class="body span6">
                                        <div class="title-price row">
                                            <div class="title span6">
                                                <h2><a class="nat-title"
                                                        href='<?php echo Item::getLink($items[$i]); ?>'>
                                                        <?php echo $items[$i]->title; ?>
                                                        (
                                                        <?php if (isset($items[$i]->date_started)) echo Util::formatDate($items[$i]->date_started); ?>)</a>

                                                </h2>
                                            </div><!-- /.title -->


                                        </div><!-- /.title -->
                                        <div class="location"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <?php echo $items[$i]->full_address; ?>
                                        </div><!-- /.location -->
                                        <p class="nat-content">
                                            <?php echo $items[$i]->description ?>
                                        </p>
                                        <div class="area">
                                            <span class="key">Area:</span><!-- /.key -->
                                            <span class="value">
                                                <?php echo Item::getArea($items[$i], true); ?></span><!-- /.value -->
                                        </div><!-- /.area -->
                                        <div class="nat-date">
                                            <div class="content">
                                                <?php echo Util::formatDate($items[$i]->created_date); ?> </div>
                                        </div>
                                        <!-- /.date -->

                                    </div><!-- /.body -->

                                </div><!-- /.property -->
                            </div><!-- /.row -->

                            <?php	
                        }
                        if ($length == 0)
                            echo '<span class="message">Không có bất động sản nào được tìm thấy</span>';
                        ?>
                        </div><!-- /.row -->
                    </div><!-- /.properties-rows -->
                    <div class="pagination pagination-centered">
                        <?php if (isset($pagination)) echo $pagination; ?>
                    </div><!-- /.pagination -->
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('/public/templates/front/js/core'); ?>/list_page.js?v=1.1">
</script>
<script>
var save_item_second = false;

function onSaveItem(item_id) {
    if (save_item_second == true) return;
    save_item_second = true;
    API.updateUserItem(item_id, function(resp) {
        if (resp.error_code == 0) {
            var user_item = $('#user_item_' + item_id);
            user_item.find('label.lbl').html(resp.data.lbl);

            var link = user_item.find('a.btn_item');
            if (resp.data.class == 'save')
                link.removeClass('remove');
            else
                link.removeClass('save');

            link.addClass(resp.data.class);
        } else {

        }
        save_item_second = false;
    });
}
</script>

<style>
/* .nat-list-item img {
    width: 270px;
    height: 200px;
} */

.nat-title {
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    overflow: hidden;
    font-size: 16px !important;
    text-transform: uppercase;
    padding-bottom: 10px;
    padding-right: 15px;
    line-height: 1.5em !important;
    height: 2.5em
}

.nat-content {
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 3;
    overflow: hidden;
    font-size: 11px !important;
    text-transform: inherit;
    padding-bottom: 15px;
    line-height: 1.5em !important;
    height: 2.7em
}


.nat-price {
    background-color: #ffffff;
    color: #313131;
    font-size: 18px;
    padding: 5px 16px;
    position: absolute;
    right: 15px;
    bottom: 30px;
}

.nat-client-list .location {
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 3;
    overflow: hidden;
    font-size: 12px !important;
    text-transform: uppercase;
    padding-bottom: 10px !important;
    line-height: 2em !important;
    height: 1.5em
}

.nat-date {
    float: right;
    margin-right: 10px;
}

.nat-date .content {
    background-image: url(../public/templates/front/img/icons/date.png);
    background-position: left center;
    background-repeat: no-repeat;
    background-size: 15px 15px;
    color: #313131;
    padding-left: 18px;
    padding-top: 1px;
}
</style>