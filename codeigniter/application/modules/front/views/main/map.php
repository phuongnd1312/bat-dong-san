<div class="nat-client-map" id="content">

    <div class="map-wrapper">
        <div class="map">
            <div class="search_list">
                <?php echo Mhtml::loadView('front/partials/control_map_list', array('selected' => 1)); ?>
            </div>

            <div id='map_canvas' class="map-inner"></div>

            <div class="container">
                <?php echo MHtml::loadView('front/partials/slide_filter'); ?>
            </div><!-- /.container -->
        </div><!-- /.map -->
    </div><!-- /.map-wrapper -->

    <div class="container">
        <div id="main">
            <div class="row">
                <div class="span9">
                    <h1 class="page-header">Featured properties</h1>
                    <div class="properties-grid">
                        <div class="row">

                            <?php
                            $length = count($items);
                            for ($i = 0; $i < $length; $i++) { ?>
                                <div class="property span3">
                                    <div class="image">
                                        <div class="content">
                                            <a href='<?php echo Item::getLink($items[$i]); ?>'></a>
                                            <?php if (isset($items[$i]->video)) echo Item::videoImage($items[$i]->video);
                                            else echo Item::photofst($items[$i]); ?>
                                        </div><!-- /.content -->

                                        <div class="price">
                                            <?php echo Item::formatCurrency($items[$i]->price_total);
                                            if (isset($items[$i]->type_item)) echo Item::rentMonth($items[$i]->type_item); ?>
                                        </div><!-- /.price -->

                                    </div><!-- /.image -->

                                    <div class="title">
                                        <h2><a href="<?php echo Item::getLink($items[$i]); ?>">
                                                <?php echo $items[$i]->title; ?>
                                            </a></h2>
                                    </div><!-- /.title -->

                                    <div class="location">
                                        <?php echo $items[$i]->full_address; ?>
                                    </div><!-- /.location -->
                                    <div class="area">
                                        <span class="key">Area:</span><!-- /.key -->
                                        <span class="value">
                                            <?php echo Item::getArea($items[$i], true); ?></span><!-- /.value -->
                                    </div><!-- /.area -->
                                    <div class="date">
                                        <div class="content">
                                            <?php if (isset($items[$i]->date_started)) echo Util::formatDate($items[$i]->date_started); ?>
                                        </div>
                                    </div><!-- /.date -->
                                </div><!-- /.property -->

                            <?php

                        }
                        if ($length == 0)
                            echo '<span class="message">Không có bất động sản nào được tìm thấy</span>';
                        ?>


                        </div><!-- /.row -->
                    </div><!-- /.properties-grid -->
                </div>
                <div class="sidebar span3">
                    <div class="widget our-agents">
                        <div class="title">
                            <h2>Our Agents</h2>
                        </div><!-- /.title -->

                        <div class="content">
                            <?php
                            $length = count($agents);
                            for ($i = 0; $i < $length; $i++) { ?>

                                <div class="agent">
                                    <div class="image">
                                        <img src="public/<?php echo $agents[$i]->image; ?>" alt="">
                                    </div><!-- /.image -->
                                    <div class="name">
                                        <?php echo $agents[$i]->fullname; ?>
                                    </div><!-- /.name -->
                                    <div class="phone">
                                        <?php echo $agents[$i]->mobile; ?>
                                    </div><!-- /.phone -->
                                    <div class="email"><a href="mailto:<?php echo $agents[$i]->email; ?>">
                                            <?php echo $agents[$i]->email; ?></a></div><!-- /.email -->
                                </div><!-- /.agent -->

                                <div class="agent">
                                    <div class="image">
                                        <img src="../public/templates/front/img/photos/john-small.png" alt="">
                                    </div><!-- /.image -->
                                    <div class="name">John Doe</div><!-- /.name -->
                                    <div class="phone">111-222-333</div><!-- /.phone -->
                                    <div class="email"><a href="mailto:john.doe@example.com">victoria@example.com</a></div>
                                    <!-- /.email -->
                                </div><!-- /.agent -->
                            <?php

                        }
                        if ($length == 0)
                            echo '<span class="message">Không có bất động sản nào được tìm thấy</span>';
                        ?>
                        </div><!-- /.content -->
                    </div><!-- /.our-agents -->
                </div>
            </div>
            <div class="carousel-wrapper">
                <div class="carousel">
                    <h2 class="page-header">All properties</h2>
                    <a href="/danh-sach" class="btn btn-primary" style="float: right;margin-top: -50px;">View
                        more</a>
                    <div class="content">
                        <a class="carousel-prev" href="#">Previous</a>
                        <a class="carousel-next" href="#">Next</a>
                        <ul>
                            <?php
                            $length = count($itemsAll);
                            for ($i = 0; $i < $length; $i++) { ?>

                                <li>
                                    <div class="image">

                                        <a href="<?php echo Item::getLink($itemsAll[$i]); ?>"></a>
                                        <?php if (isset($itemsAll[$i]->video)) echo Item::videoImage($itemsAll[$i]->video);
                                        else echo Item::photofst($itemsAll[$i]); ?>
                                    </div><!-- /.image -->
                                    <div class="title">
                                        <h2><a href="<?php echo Item::getLink($itemsAll[$i]); ?>">
                                                <?php echo $itemsAll[$i]->title; ?>
                                            </a></h2>
                                    </div><!-- /.title -->
                                    <div class="location">
                                        <?php echo $itemsAll[$i]->full_address; ?>
                                    </div><!-- /.location-->
                                    <div class="price">
                                        <?php echo Item::formatCurrency($itemsAll[$i]->price_total); ?>
                                    </div><!-- .price -->
                                    <div class="area">
                                        <span class="key">Area:</span>
                                        <span class="value">
                                            <?php echo Item::getArea($itemsAll[$i], true); ?></span>
                                    </div><!-- /.area -->
                                    <div class="date">
                                        <div class="inner">
                                            <?php if (isset($items[$i]->date_started)) echo Util::formatDate($items[$i]->date_started); ?>
                                        </div>
                                    </div><!-- /.date -->
                                </li>


                            <?php

                        }
                        if ($length == 0)
                            echo '<span class="message">Không có bất động sản nào được tìm thấy</span>';
                        ?>
                        </ul>
                    </div><!-- /.content -->
                </div><!-- /.carousel -->
            </div>

        </div>
    </div>

    <div class="bottom-wrapper">
        <div class="bottom container">
            <div class="bottom-inner row">
                <div class="item span4">
                    <div class="address decoration"></div>
                    <h2><a>List your property</a></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla accumsan dui ac nunc imperdiet
                        rhoncus. Aenean vitae imperdiet lectus</p>
                    <a href="#" class="btn btn-primary">Read more</a>
                </div><!-- /.item -->

                <div class="item span4">
                    <div class="gps decoration"></div>
                    <h2><a>Advertise rentals</a></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla accumsan dui ac nunc imperdiet
                        rhoncus. Aenean vitae imperdiet lectus</p>
                    <a href="#" class="btn btn-primary">Read more</a>
                </div><!-- /.item -->

                <div class="item span4">
                    <div class="key decoration"></div>
                    <h2><a>Guidance</a></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla accumsan dui ac nunc imperdiet
                        rhoncus. Aenean vitae imperdiet lectus</p>
                    <a href="#" class="btn btn-primary">Read more</a>
                </div><!-- /.item -->
            </div><!-- /.bottom-inner -->
        </div><!-- /.bottom -->
    </div><!-- /.bottom-wrapper -->
</div><!-- /#content -->

<script type="text/javascript">
    CMap.init();
</script>

<style>
    .search_list {
        position: absolute;
        z-index: 1;
        text-align: center;
        height: 40px;
        vertical-align: middle;
        color: rgb(86, 86, 86);
        top: 10px;
        left: 168px;
        background: #fff;
    }

    .search_list .item-url {
        direction: ltr;
        overflow: hidden;
        text-align: center;
        height: 40px;
        display: table-cell;
        vertical-align: middle;
        position: relative;
        color: rgb(86, 86, 86);
        font-family: Roboto, Arial, sans-serif;
        user-select: none;
        font-size: 18px;
        background-color: rgb(255, 255, 255);
        border-bottom-right-radius: 2px;
        border-top-right-radius: 2px;
        background-clip: padding-box;
        box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px;
        min-width: 56px;
        border-left: 0px;
    }

    .search_list div:hover {
        direction: ltr;
        overflow: hidden;
        text-align: center;
        height: 40px;
        display: table-cell;
        vertical-align: middle;
        position: relative;
        color: rgb(0, 0, 0);
        font-family: Roboto, Arial, sans-serif;
        user-select: none;
        font-size: 18px;
        background-color: rgb(235, 235, 235);
        border-bottom-right-radius: 2px;
        border-top-right-radius: 2px;
        background-clip: padding-box;
        box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px;
        min-width: 56px;
        border-left: 0px;
    }

    .search_list .item-url a {
        color: rgb(86, 86, 86);
        text-decoration: none !important;
    }

    .search_list .item-url a:hover {
        color: rgb(0, 0, 0);
        text-decoration: none !important;
    }

    .gm-style-mtc div {
        padding: 0px 10px !important;
    }
</style>