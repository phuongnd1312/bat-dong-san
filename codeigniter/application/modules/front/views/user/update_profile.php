<div id="content" class="cs-content">
	<div class="container">
		<div class="row">
			<div class="span3">
				<?php echo Mhtml::loadView('front/partials/user_menu'); ?>

			</div>
			<div class="span9 user_area">
<div id='wrap_user_update_profile' class='theme_register bound_content'>
	<div class='main_container row'>

		<?php echo User_Helper::ins()->getMessage(); ?>
		<form id='frm_update_profile' name='register_form' action='' method="post"  enctype="multipart/form-data">
		<h3 class='title'>Cập nhật thông tin</h3>
			<div >
			<div class="form-group">
				<label class='required'>Hình ảnh</label>
				<div class="col-md-12" style="margin-bottom: 10px;">
					<div class="col-md-3 col-lg-3 " align="center" style="width: 200px"> <img alt="User Pic" src="<?php if($user->image!=NULL)echo Util::loadImg($user->image); else echo Util::loadImg('/images/stories/user_logo.png'); ?>" class="img-circle img-responsive" id="photoCover"> </div>
				</div>
				<input id="lefile" name='lefile'type="file" style="display:none">
				<div class="input-append">
				<a class="btn btn_input" style="margin-left: 30px"onclick="$('input[id=lefile]').click();">Cập nhật ảnh</a>
				</div>
				<script type="text/javascript">
				$('#lefile').change( function(event) {
    			$("#photoCover").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
				});
				</script>
            </div>
			<div class='form-group'>
					<label class='required'>Họ và tên <span class='required'>*</span></label>
					<input type="text" name="full_name" value="<?php echo $user->full_name; ?>" id='field_full_name' class="form-control required" placeholder='' />
					<div class='msg_warning'></div>
				</div>
				<div class='form-group'>
					<label class='required'>ID (Email hoặc số điện thoại)<span class='required'>*</span></label>
					<input type="text" value="<?php echo $user->email; ?>"  name='email' id='field_email' class="form-control" placeholder='' />
				</div>

				<div class='form-group'>
					<label class='required'>Ngày sinh</label>
					<input type="text" name="birthday" value='<?php echo Util::formatDateDMY($user->birthday); ?>'  id='field_birthday' class="form-control" />
					<div class='msg_warning'></div>
				</div>

				<div class='form-group'>
					<label class='required'>Giới tính </label>
					<select name="gender" id='field_gender' class="form-control">
						<option value=''>--Chọn giới tính--</option>
						<?php
							foreach (Gender::$Detail as $key => $val) {
								$selected = ($key == $user->gender) ? ' selected="selected" ' : '';
							?>
								<option <?php echo $selected; ?> value='<?php echo $key; ?>'><?php echo $this->lang->line(Gender::$Detail[$key]); ?></option>
						<?php
							}
						?>
					</select>
				</div>

				<div class='form-group'>
					<label class='required'>Điện thoại di động<span class='required'>*</span></label>
					<input type="text" name="mobile" value="<?php echo Util::formatPhone($user->mobile); ?>" id='field_mobile' class="form-control required" />
					<div class='msg_warning'></div>
				</div>

				<div class='form-group'>
					<label class='required'>Điện thoại bàn</label>
					<input type="text" name="deskphone" value="<?php echo Util::formatPhone($user->deskphone); ?>" id='field_deskphone' class="form-control" />
					<div class='msg_warning'></div>
				</div>

				<div class='form-group'>
					<label class='required'>Địa chỉ</label>
					<input type="text" name="address" value="<?php echo $user->address; ?>" id='field_address' class="form-control" />
					<div class='msg_warning'></div>
				</div>

				<div class='form-group'>
					<label class='required'>Zalo</label>
					<input type="text" name="zalo" value="<?php echo Util::formatPhone($user->zalo); ?>" id='field_zalo' class="form-control" />
					<div class='msg_warning'></div>
				</div>

				<div class='form-group'>
					<label class='required'>Viber</label>
					<input type="text" name="viber" value="<?php echo $user->viber; ?>" id='field_viber' class="form-control" />
					<div class='msg_warning'></div>
				</div>

				<div class='form-group control'>
					<a href="<?php echo base_url('/'); ?>" class='btn btn-default'>Hủy</a>
					<input type="button" value="Cập nhật" class='btn btn_search' name='signup' onclick='onSubmitRegister();' />
					<a href='<?php echo Links::account('change_password'); ?>'>Đổi mật khẩu</a>
				</div>
			</div>

		</form>
	</div>
</div>
</div>
</div>
</div>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$('#field_birthday').datepicker();
$('#field_birthday').datepicker( "option", "dateFormat", 'dd/mm/yy')


function onSubmitRegister(){
	$('#frm_update_profile .form-control.required').each(function(index){
		var val = $(this).val();
		if($.trim(val) == '')
			$(this).addClass('error');
		else
			$(this).removeClass('error');
	});

	var email = $('#field_email');
	var mobile = $('#field_mobile');
	var deskphone=$('#field_deskphone');
	var zalo=$('#field_zalo');
	if(zalo.val())
	if(!Util.validatePhone(zalo.val())){
		zalo.addClass('error');
		zalo.next().html(jlang.js_phone_not_correct);
	}
	if(mobile.val())
	if(!Util.validatePhone(mobile.val())){
		mobile.addClass('error');
		mobile.next().html(jlang.js_phone_not_correct);
	}
	if(deskphone.val())
	if(!Util.validateDeskphone(deskphone.val())){
		deskphone.addClass('error');
		deskphone.next().html(jlang.js_phone_not_correct);
	}
	if(!Util.validateEmail(email.val())){
		email.addClass('error');
		email.next().html(jlang.js_email_not_correct);
	}else
		email.next().html('');

	if($('#frm_update_profile .form-control.error').length == 0){
		$('#frm_update_profile').submit();
	}
}
</script>