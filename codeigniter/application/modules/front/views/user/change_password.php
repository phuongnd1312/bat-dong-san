<div id="content" class="cs-content">
	<div class="container">
		<div class="row">
			<div class="span3">
				<?php echo Mhtml::loadView('front/partials/user_menu'); ?>

			</div>
			<div class="span9 user_area">
<div id='wrap_register_form' class='theme_register bound_content'>
	<div class='main_container row'>
		<h3 class='title'>Cập nhật thông tin</h3>
		<?php echo User_Helper::ins()->getMessage(); ?>
		<form id='frm_edit_password' name='edit_edit_password' action='' method="post">
			<div class='form-group'>
				<label class=''><?php echo $this->lang->line('lbl_old_password'); ?></label>
				<input type="password" name="old_password" id='field_old_password' class="form-control required" placeholder='<?php echo $this->lang->line('txt_enter_old_password'); ?>' />
			</div>

			<div class='form-group'>
				<label class=''><?php echo $this->lang->line('txt_enter_password_new'); ?></label>
				<input type="password" name="password" id='field_password' class="form-control required" placeholder='<?php echo $this->lang->line('txt_enter_password'); ?>' />
				<div class='msg_warning'></div>
			</div>

			<div class='form-group'>
				<label class=''><?php echo $this->lang->line('txt_enter_repassword_new'); ?></label>
				<input type="password" name="repassword" id='field_repassword' class="form-control required" placeholder='<?php echo $this->lang->line('txt_enter_repassword'); ?>' />
			</div>

			<div class='form-group'>
				<a class='btn btn-default' href='<?php echo Links::account('edit'); ?>'><?php echo $this->lang->line('lbl_cancel'); ?></a>
				<input type="button" value="<?php echo $this->lang->line('lbl_update'); ?>" class='btn btn_search' name='signup' onclick='onSubmitUpdate();' />
			</div>
		</form>
			
	</div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
function onSubmitUpdate(){
	$('input.required').each(function(index){
		var val = $(this).val();
		if($.trim(val) == '')
			$(this).addClass('error');
		else
			$(this).removeClass('error');
	});

	var pass1 = $('#field_password');
	var pass2 = $('#field_repassword');
	pass1.next().html('');
	if(pass1.val() != pass2.val()){
		pass1.addClass('error');
		pass2.addClass('error');
		pass1.next().html(jlang.js_password_not_match);
	}else if(!Util.validatePassword(pass1.val())){
		pass1.addClass('error');
		pass2.addClass('error');
		pass1.next().html(jlang.js_password_validate_wrong);
	}else
		pass1.next().html('');
	
	if($('input.error').length == 0){
		$('#frm_edit_password').submit();
	}
}
</script>
