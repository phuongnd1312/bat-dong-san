<div id="content" class="cs-content">
	<div class="container">
		<div class="row">
			<div class="span3">
				<?php echo Mhtml::loadView('front/partials/user_menu'); ?>
				
			</div>
			<div class="span9 user_area">
				<div id='wrap_user_update_profile' class='theme_register bound_content'>
					<div class='main_container'>

						<h3 class='title'>Thông tin tài khoản</h3>
						<div class="col-md-12" style="margin-bottom: 10px;">
						<div class="col-md-3 col-lg-3 " align="center" style="width: 200px"> <img alt="User Pic" src="<?php if($user->image!=NULL)echo Util::loadImg($user->image); else echo Util::loadImg('/images/stories/user_logo.png'); ?>" class="img-circle img-responsive"> </div>
						</div>
						<table class='table profile'>
							<thead>
								<h4 class="border_font"><span>Thông tin cơ bản</span></h4>
							</thead>
							<tr>
								<td class='lbl'>Họ và tên:</td>
								<td><?php echo $user->full_name; ?></td>
							</tr>
							<tr>
								<td class='lbl'>Email:</td>
								<td><?php echo $user->email; ?></td>
							</tr>
							</table>
							<table class='table profile'>
								<h4 class="border_font"><span>Thông tin nâng cao</span></h4>
						
							<tr>
								<td class='lbl'>Ngày sinh:</td>
								<td><?php echo Util::formatDateDMY($user->birthday); ?></td>
							</tr>	

							<tr>
								<td class='lbl'>Giới tính:</td>
								<td><?php if($user->gender)echo $this->lang->line(Gender::$Detail[$user->gender]); ?></td>
							</tr>

							<tr>
								<td class='lbl'>Điện thoại di động:</td>
								<td><?php echo Util::formatPhone($user->mobile); ?></td>
							</tr>

							<tr>
								<td class='lbl'>Điện thoại bàn:</td>
								<td><?php echo Util::formatPhone($user->deskphone); ?></td>
							</tr>

							<tr>
								<td class='lbl'>Địa chỉ:</td>
								<td><?php echo $user->address; ?></td>
							</tr>

							<tr>
								<td class='lbl'>Zalo:</td>
								<td><?php echo $user->zalo; ?></td>
							</tr>

							<tr>
								<td class='lbl'>Viber:</td>
								<td><?php echo $user->viber; ?></td>
							</tr>

						</table>
						<a class='btn btn-primary' href='<?php echo Links::account('edit'); ?>'>Chỉnh sửa</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>