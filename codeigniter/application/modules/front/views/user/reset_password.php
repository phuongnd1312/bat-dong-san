<div id='wrap_reset_pass' class='theme_register'>
	<div class='bound_content'>
		<h3 class='title'><?php echo $this->lang->line('lbl_email_reset_pass'); ?></h3>
		<?php echo User_Helper::ins()->getMessage(); ?>
		<form id='frm_reset_password' name='edit_reset_password' action='<?php echo Links::account('reset_password/'.$code); ?>' method="post">

			<div class='form-group'>
				<label class=''><?php echo $this->lang->line('txt_enter_password_new'); ?></label>
				<input type="password" name="password" id='field_password' class="form-control required" placeholder='<?php echo $this->lang->line('txt_enter_password_new'); ?>' />
				<div class='msg_warning'></div>
			</div>

			<div class='form-group'>
				<label class=''><?php echo $this->lang->line('txt_enter_repassword_new'); ?></label>
				<input type="password" name="repassword" id='field_repassword' class="form-control required" placeholder='<?php echo $this->lang->line('txt_enter_repassword'); ?>' />
			</div>

			<div class='form-group'>
				<input type="button" value="<?php echo $this->lang->line('lbl_update'); ?>" class='btn btn_search' name='signup' onclick='onSubmitReset();' />
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
function onSubmitReset(){
	var pass1 = $('#field_password');
	var pass2 = $('#field_repassword');
	$('input.error').removeClass('error');
	if(pass1.val() != pass2.val()){
		pass1.addClass('error');
		pass2.addClass('error');
		pass1.next().html(jlang.js_password_not_match);
	}else if(!Util.validatePassword(pass1.val())){
		pass1.addClass('error');
		pass2.addClass('error');
		pass1.next().html(jlang.js_password_validate_wrong);
	}else
		pass1.next().html('');
	
	if($('input.error').length == 0){
		$('#frm_reset_password').submit();
	}
}
</script>
