<?php if (0) { ?>
  <!-- CONTENT -->
  <div id="content" class="nat-forget-pass">
    <div class="container">
      <div>
        <div id="main">
          <div class="login-register">
            <div class="row">
              <div class="span4 offset4">
                <div id='wrap_forget_pass' class='theme_register'>
                  <div class='bound_content'>
                    <h3 class='title'>Quên mật khẩu</h3>
                    <?php echo User_Helper::ins()->getMessage(); ?>
                    <form id='frm_forgot_pass' name='forgot_passoword' action='<?php echo Links::account('forgot_pass'); ?>' method="post">
                      <div class='form-group'>
                        <label class=''><?php echo $this->lang->line('lbl_email'); ?></label>
                        <input type="email" name="email_or_phone" id='field_email' class="form-control required" placeholder='<?php echo $this->lang->line('txt_enter_email_or_phone'); ?>' />
                        <div class='msg_warning'></div>
                      </div>

                      <div class='form-group'>
                        <input type="button" value="<?php echo $this->lang->line('lbl_submit_forgot'); ?>" class='btn btn-success' name='signup' onclick='onSubmitForgot();' />
                      </div>
                    </form>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php } ?>
<div id="content" class="nat-forget-pass">
  <div class="container">
    <div>
      <div id="main">
        <div class="login-register">
          <div class="row">
            <div class="span4 offset4">
              <ul class="tabs nav nav-tabs">
                <li class="active"><a href="#register">Quên mật khẩu</a></li>
              </ul>
              <!-- /.nav -->
              <div class="tab-content">
                <div class="tab-pane active" id="register">
                  <form id='frm_forgot_pass' name='forgot_passoword' action='<?php echo Links::account('forgot_pass'); ?>' method="post">
                    <?php echo User_Helper::ins()->getMessage(); ?>
                    <div class="control-group">
                      <label class="control-label" for="inputRegisterFirstName">
                        Tên đăng nhập
                        <span class="form-required" title="This field is required.">*</span>
                      </label>

                      <div class="controls">
                        <input style="width: 100%" type="email" name="email_or_phone" id='field_email' class="form-control required" placeholder='<?php echo $this->lang->line('txt_enter_email_or_phone'); ?>' />
                        <div class='msg_warning'></div>
                      </div>
                      <!-- /.controls -->
                    </div>
                    <!-- /.control-group -->
                    <div class="form-actions" style="text-align: right;">
                      <a href="<?php echo base_url('/'); ?>" class='btn btn-default'>Hủy</a>
                      <input type="button" value="Gửi" class="btn btn-primary arrow-right" name='signup' onclick='onSubmitForgot();'>
                    </div>
                    <!-- /.form-actions -->
                  </form>
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- /.span12-->
          </div>
          <!-- /.row -->
        </div><!-- /.login-register -->
      </div>
    </div>
  </div>

</div><!-- /#content -->




<script type="text/javascript">
  function onSubmitForgot() {
    var email = $('#field_email');
    if ($.trim(email.val()) == '') {
      email.addClass('error');
    }

    if ($('input.error').length == 0) {
      $('#frm_forgot_pass').submit();
    }
  }
</script>