 <!-- CONTENT -->
 <div id="content" class="nat-register">
   <div class="container">
     <div>
       <div id="main">
         <div class="login-register">
           <div class="row">
             <div class="span4 offset4">
               <ul class="tabs nav nav-tabs">
                 <li class="active"><a href="#register">Đăng Ký</a></li>
               </ul>
               <!-- /.nav -->
               <div class="tab-content">
                 <div class="tab-pane active" id="register">
                   <form id='frm_register' name='register_form' action='<?php echo Links::account('register'); ?>' method="post">
                     <?php echo User_Helper::ins()->getMessage(); ?>
                     <div class="control-group">
                       <label class="control-label" for="inputRegisterFirstName">
                         Họ và tên
                         <span class="form-required" title="This field is required.">*</span>
                       </label>

                       <div class="controls">
                         <input type="text" name="full_name" value="<?php echo $user->full_name; ?>" id='field_full_name' class="form-control required" placeholder=''>
                         <div class='msg_warning'></div>
                       </div>
                       <!-- /.controls -->
                     </div>
                     <!-- /.control-group -->

                     <div class="control-group">
                       <label class="control-label" for="inputRegisterSurname">
                         Email hoặc điện thoại
                         <span class="form-required" title="This field is required.">*</span>
                       </label>

                       <div class="controls">
                         <input type="text" name="email" autocomplete="off" value="<?php echo $user->email; ?>" id='field_email' class="form-control required" placeholder=''>
                         <div class='msg_warning'></div>
                       </div>
                       <!-- /.controls -->
                     </div>
                     <!-- /.control-group -->


                     <!-- /.control-group -->

                     <div class="control-group">
                       <label class="control-label" for="inputRegisterPassword">
                         Mật khẩu
                         <span class="form-required" title="This field is required.">*</span>
                       </label>

                       <div class="controls">
                         <input type="password" name="password" autocomplete="off" value="" id='field_password' class="form-control required" />
                         <div class='msg_warning'></div>
                       </div>
                       <!-- /.controls -->
                     </div>
                     <!-- /.control-group -->

                     <div class="control-group">
                       <label class="control-label" for="inputRegisterRetype">
                         Mật khẩu nhập lại
                         <span class="form-required" title="This field is required.">*</span>
                       </label>

                       <div class="controls">
                         <input type="password" name="repassword" value="" id='field_repassword' class="form-control required" />
                         <div class='msg_warning'></div>
                       </div>
                       <!-- /.controls -->
                     </div>
                     <!-- /.control-group -->

                     <div class="form-actions" style="text-align: right;">
                       <a href="<?php echo base_url('/'); ?>" class='btn btn-default'>Hủy</a>
                       <input type="button" value="Đăng ký" class="btn btn-primary arrow-right" name='signup' onclick='onSubmitRegister();'>
                     </div>
                     <!-- /.form-actions -->
                   </form>
                 </div>
                 <!-- /.tab-pane -->
               </div>
               <!-- /.tab-content -->
             </div>
             <!-- /.span12-->
           </div>
           <!-- /.row -->
         </div><!-- /.login-register -->
       </div>
     </div>
   </div>

 </div><!-- /#content -->



 <script type="text/javascript">
   function onSubmitRegister() {
     $('.form-control.required').each(function(index) {
       var val = $(this).val();
       if ($.trim(val) == '')
         $(this).addClass('error');
       else
         $(this).removeClass('error');
     });
     //var username = $('#field_username');
     var email = $('#field_email');
     var pass1 = $('#field_password');
     var pass2 = $('#field_repassword');

     /*if(!Util.validateName(username.val())){
     	username.addClass('error');
     	username.next().html(jlang.js_username_not_include_spec_char);
     }else
     	username.next().html('');*/
     if (!Util.isNumber(email.val())) {
       if (!Util.validateEmail(email.val())) {
         email.addClass('error');
         email.next().html(jlang.js_email_not_correct);
       } else
         email.next().html('');
     } else {
       if (!Util.validatePhone(email.val())) {
         email.addClass('error');
         email.next().html(jlang.js_phone_not_correct);
       } else
         email.next().html('');
     }
     if (pass1.val() != pass2.val()) {
       pass1.addClass('error');
       pass2.addClass('error');
       pass1.next().html(jlang.js_password_not_match);
     } else if (!Util.validatePassword(pass1.val())) {
       pass1.addClass('error');
       pass2.addClass('error');
       pass1.next().html(jlang.js_password_validate_wrong);
     } else
       pass1.next().html('');

     if ($('.form-control.error').length == 0) {
       $('#frm_register').submit();
     }
   }
 </script>