<!-- Menu người dùng khi đăng nhập -->
<section id="sidebar">
  <header>
    <h3>Người dùng</h3>
  </header>
  <aside>
    <ul class="sidebar-navigation">
      <li><a href="/account/profile"><i class="fa fa-user-circle" aria-hidden="true"></i> Tài khoản</a></li>
      <li><a href="/tin-da-luu"><i class="fa fa-home"></i>Tin đã lưu</a></li>
      <li><a href="/dang-tin"><i class="fa fa-upload" aria-hidden="true"></i>Đăng tin</a></li>
      <li><a href="/quan-tri-tin"><i class="fa fa-list" aria-hidden="true"></i>Quản lý tin</a></li>
      <li><a href="/front/items/manage_bds"><i class="fa fa-list" aria-hidden="true"></i>Quản lý tin BDSVN</a></li>
      <li><a href="/account/logout"><i class="fa fa-sign-out" aria-hidden="true"></i>Đăng xuất</a></li>
    </ul>
  </aside>
</section>