<link rel="icon" href="<?php echo $favicon; ?>" sizes="32x32" />
<title><?php echo $title; ?></title>
<meta name='description' content='<?php echo $description; ?>' />
<!--Facebook-->
<meta property="og:url" content="<?php echo $url; ?>" />
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:description" content="<?php echo $description; ?>" />
<meta property="og:image" content="<?php echo $image; ?>" />
<!-- twitter -->
<meta name="twitter:title" content="<?php echo $title; ?>">
<meta name="twitter:url" content="<?php echo $url; ?>"">
<meta name=" twitter:image" content="<?php echo $image; ?>">
<meta name="twitter:description" content="<?php echo $description; ?>">