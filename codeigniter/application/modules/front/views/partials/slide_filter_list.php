<?php
$sel_type_item = isset($filter['form']) ? $filter['form'] : 0;
$sel_project_type = isset($filter['type']) ? $filter['type'] : array();
$sel_city = isset($filter['city']) ? $filter['city'] : 0;
$sel_district = isset($filter['district']) ? $filter['district'] : 0;
$sel_area = isset($filter['area']) ? $filter['area'] : null;
$sel_price = isset($filter['price']) ? $filter['price'] : null;
$sel_wards = isset($filter['wards']) ? $filter['wards'] : array();
$sel_streets = isset($filter['streets']) ? $filter['streets'] : array();
$sel_project = isset($filter['project']) ? $filter['project'] : array();
$sel_orientation = isset($filter['orientation']) ? $filter['orientation'] : array();
$sel_num_beds = isset($filter['num_beds']) ? $filter['num_beds'] : array();
$advance_search_class = 'active';
$btn_advanced_val = $this->lang->line('lbl_ignore_search_advanced');
if (isset($has_advance_filter))
  if (!$has_advance_filter) {
    $advance_search_class = 'deactive';
    $btn_advanced_val = $this->lang->line('lbl_search_advanced');
  }

?>

<form id='frm_filter_item' action='<?php echo base_url(Link::$list); ?>' method='get'>
  <div class='row nat-slide-filter-left'>
    <div class='span3'>
      <div class='property-filter'>
        <div class='content nat-selection'>

          <!--Chọn hình thức -->
          <div class='control-group'>
            <div class='controls'>
              <select name='form' id='sbl_form_item' class="advance_combobox">
                <option value=''>--Chọn hình thức--</option>
                <?php
                foreach ($type_items as $val) {
                  $select = ($sel_type_item == $val['id']) ? ' selected' : '';
                  ?>
                  <option <?php echo $select; ?> value='<?php echo $val['id']; ?>'>
                    <?php echo $this->lang->line($val['code']); ?>
                  </option>
                <?php } ?>
              </select>
            </div>
          </div>

          <!-- Chọn loại tin đăng -->
          <div class='control-group'>
            <div class='controls'>
              <select name='type' id='sbl_project_type' class="advance_combobox">
                <option value=''>--Chọn loại tin đăng--</option>
                <?php
                foreach ($properties_proportion as $val) {
                  $select = ($val->id == $sel_project_type) ? ' selected ' : '';
                  ?>
                  <option <?php echo $select; ?>value='<?php echo $val->id; ?>'>
                    <?php echo $this->lang->line($val->type); ?>
                  </option>
                <?php } ?>
              </select>
            </div>
          </div>

          <!-- Tỉnh/Thành Phố -->
          <div class='control-group'>
            <div class='controls'>
              <select name='city' id='sbl_select_city' class="advance_combobox">
                <option value=''>--Tỉnh/Thành Phố--</option>
                <?php foreach ($cities as $city) :
                  $select = ($city->id == $sel_city) ? ' selected ' : '';
                  ?>
                  <option <?php echo $select; ?> value='<?php echo $city->id; ?>'>
                    <?php echo $city->name; ?>
                  </option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>

          <!-- Quận/Huyện -->
          <div class='control-group'>
            <div class='controls'>
              <select name='district' id='sbl_select_district' class="advance_combobox">
                <option value=''>--Quận/Huyện--</option>
              </select>
            </div>
          </div>

          <!-- Chọn mức giá -->
          <div class='control-group'>
            <div class='controls'>
              <select name='price' id='sbl_select_price' class="advance_combobox">
                <?php
                foreach (Constant::$RANGE_PRICE as $key => $value) {
                  if ($key == 0) {
                    $key = '';
                  }
                  $select = ($sel_price == $key) ? ' selected ' : '';
                  ?>
                  <option <?php echo $select; ?> value='<?php echo $key; ?>'>
                    <?php echo $value[1]; ?>
                  </option>
                <?php } ?>
              </select>
            </div>
          </div>

          <!-- Tìm kiếm nâng cao -->
          <div class='advance_search  <?php echo $advance_search_class; ?>' id='group_advanced_search'>

            <!-- Chọn phường/xã -->
            <div class='control-group'>
              <div class='controls'>
                <select name='wards[]' id='sbl_select_ward' multiple="multiple" class="advance_combobox">
                  <?php foreach ($wards as $ward) :
                    $select = in_array($ward->id, $sel_wards) ? ' selected ' : '';
                    ?>
                    <option <?php echo $select; ?> value="<?php echo $ward->id; ?>" lat='<?php echo $ward->lat; ?>' lng='<?php echo $ward->lng; ?>'><?php echo $ward->full_name; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <!-- Chọn đường -->
            <div class='control-group'>
              <div class='controls'>
                <select name='streets[]' id='sbl_select_street' multiple="multiple" class="advance_combobox">
                  <?php foreach ($streets as $street) :
                    $select = in_array($street->id, $sel_streets) ? ' selected ' : '';
                    ?>
                    <option <?php echo $select; ?> value="<?php echo $street->id; ?>"><?php echo $street->name; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
          </div>

          <!-- Tìm kiếm mở rộng khi chọn (Hình thức|Loại tin đăng)  -->
          <div id='adv_filter_more'></div>

          <div class='form-actions'>
            <input type='button' value='Tìm kiếm' class='btn btn-primary btn-large' onclick='onSearchData();' />
            <p id='show_hide_advanced_seach' class='advanced-seach' onclick='onAdvancedSearch();'>
              <?php echo $btn_advanced_val; ?></p>
          </div>

        </div>
      </div>
    </div>
  </div>
</form>

<script type='text/javascript'>
  <?php if (isset($ppfa_val)) { ?>
    var properties_pro_filter = <?php echo json_encode($ppfa_val); ?>;
  <?php
} ?>
</script>
<script type='text/javascript' src='<?php echo base_url('/public/templates/front/js/partials'); ?>/slide_filter.js?v=1.2'></script>
<style>
  .nat-selection .chzn-container .chzn-single {
    height: 32px !important;
    line-height: 32px !important;
  }

  .property-filter .nat-selection input[type='text'] {
    height: 32px !important;
  }

  .nat-selection .chzn-container-multi .chzn-choices .search-field input {
    min-height: 30px;
  }

  .nat-client-list .chzn-container-multi .chzn-choices {
    background-image: none;
  }

  .nat-client-list .chzn-container-active .chzn-choices {
    box-shadow: none;
    border: none;
  }

  .nat-client-list .chzn-container-multi .chzn-choices {
    border: none;
  }

  .advanced-seach {
    text-align: -webkit-center;
    padding-top: 8px;
    cursor: pointer;
  }
</style>