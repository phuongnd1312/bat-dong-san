<?php
$menus = array(
	array('title' => 'List', 'id' => 'btn_show_list', 'link' => Link::$list)
);
?>
<div class='item-url'>
    <?php
	foreach ($menus as $key => $item) {
		$class = $item['class'];
		$link = base_url($item['link']);
		?>
    <a href='<?php echo $link; ?>' id='<?php echo $item['id']; ?>'><?php echo $item['title']; ?></a>
    <?php

}
?>

</div>