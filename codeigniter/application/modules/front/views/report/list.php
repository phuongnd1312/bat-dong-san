<style type="text/css">
#wrap_users_notify{
	width: 900px;
	margin: 0 auto;
}
</style>
<div id='wrap_users_notify'>
	<h3 class='title'>Danh sách người dùng</h3>
<?php if(count($users)): 
?>
	<form action='<?php echo base_url('/front/reports'); ?>' method='post'>
		<table class='table'>
			<thead>
				<tr>
					<td width="20px">
						<input type="checkbox" id='check_all' />
					</td>
					<td width="80px">ID</td>
					<td>Tên</td>
					<td width="120px">Số điện thoại</td>
				</tr>
			</thead>
			<tbody>
				
					<?php
					foreach ($users as $key => $user) {?>
					<tr>
						<td>
							<input type="checkbox" class='check_item' name='users[]' value='<?php echo $user->user_id; ?>'/>
						</td>
						<td><?php echo $user->user_id; ?></td>
						<td>
							<?php echo $user->full_name; ?>
							<input type="hidden" value="<?php echo $user->full_name; ?>" name='full_name[<?php echo $user->user_id; ?>]' />
						</td>
						<td>
							<?php echo $user->phone; ?>
							<input type="hidden" value="<?php echo $user->phone; ?>" name='phone[<?php echo $user->user_id; ?>]' />
						</td>
					</tr>
					<?php
					}
					?>
				
				<tr>
					<td colspan="5" align="center">
						<input type="submit" value="Gửi thông báo" class='btn btn-danger' />
					</td>
				</tr>
			</tbody>
		</table>
	</form>

	<div class='bound_pagination'>
		<ul class='pagination'>
		<?php if(isset($pagination)) echo $pagination; ?>
		</ul>
	</div>
<?php endif; ?>
</div>
<script type="text/javascript">
	$('#check_all').click(function(){
		$('input.check_item').prop('checked', $(this).prop('checked'));
	})
</script>