<!-- BREADCRUMB -->
<div class="breadcrumb-wrapper">
    <div class="container">
        <div class="row">
            <div class="span12">
                <!-- <ul class="breadcrumb pull-left">
                    <li><a href="index.html">Home</a></li>
                </ul>/.breadcrumb -->

                <div class="account pull-right">
                    <?php
                    //print_r($_SESSION["BDS_USER_STATE"]);exit;
                    $user = User_Helper::ins()->get();
                    if ($user == null) {
                        ?>
                        <ul class="nav nav-pills">
                            <li><a href="#modal_frm_login" data-toggle="modal" id='btn_student_login'>
                                    <?php echo $this->lang->line('lbl_sign_in'); ?></a></li>
                            <li><a href='<?php echo Links::account('register'); ?>'>
                                    <?php echo $this->lang->line('lbl_sign_up'); ?></a></li>
                        </ul>
                    <?php

                } else { ?>
                        <ul class="nav nav-pills">
                            <li>
                                <a href="/account/profile"><i class="fa fa-user-circle" aria-hidden="true"></i>
                                    <?php echo $user->full_name; ?></a>
                            </li>
                            <li><a href="/account/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Đăng xuất</a>
                            </li>
                        </ul>
                    <?php

                } ?>
                </div>
            </div><!-- /.span12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.breadcrumb-wrapper -->

<!-- HEADER -->
<div id="header-wrapper">
    <div id="header">
        <div id="header-inner" class="nat-header">
            <div class="container">
                <div class="navbar">
                    <div class="navbar-inner">
                        <div class="row">
                            <div class="logo-wrapper span4">
                                <a href="#nav" class="hidden-desktop" id="btn-nav">Toggle navigation</a>

                                <div class="logo">
                                    <a class="nav-logo" href="/" title="Home"></a>
                                </div><!-- /.logo -->

                                <div class="site-name">
                                    <!-- <a href="/" title="Home" class="brand">Sweet Home</a> -->
                                </div><!-- /.site-name -->

                                <div class="site-slogan">
                                    <span>Tìm nơi<br>Tổ Ấm</span>
                                </div><!-- /.site-slogan -->
                            </div><!-- /.logo-wrapper -->

                            <div class="info">
                                <div class="site-email">
                                    <a href="mailto:info@byaviators.com">info@byaviators.com</a>
                                </div><!-- /.site-email -->

                                <div class="site-phone">
                                    <span>0.777.555.111</span>
                                </div><!-- /.site-phone -->
                            </div><!-- /.info -->
                            <form class="list-your-property arrow-right" id='frm_search_global' action='<?php echo Links::homepage('bds-tim-kiem'); ?>' method="get" class="site-search">
                                <div class="input-append">
                                    <input title="Enter the terms you wish to search for." name='keywords' value='<?php echo Util::gget('keywords'); ?>' class="search-query span2 form-text" placeholder="Search" type="text" name="">
                                    <button type="submit" class="btn"><i class="icon-search"></i></button>
                                </div><!-- /.input-append -->
                            </form><!-- /.site-search -->

                        </div><!-- /.row -->
                    </div><!-- /.navbar-inner -->
                </div><!-- /.navbar -->
            </div><!-- /.container -->
        </div><!-- /#header-inner -->
    </div><!-- /#header -->
</div><!-- /#header-wrapper -->

<!-- NAVIGATION -->
<div id="navigation">
    <div class="container">
        <div class="navigation-wrapper">
            <div class="navigation clearfix-normal">

                <ul class="nav">
                    <li class="menuparent  nolink">
                        <a href="/" class="haslink ">Home</a>
                    </li>
                    <li class="menuparent">
                        <span class="menuparent nolink">Tin Tức</span>
                        <ul>
                            <li><a href="/tin-thi-truong" class="haslink ">Tin thị trường</a></li>
                            <li><a href="/phan-tich-nhan-dinh" class="haslink ">Phân tích - nhận định</a>
                            </li>
                            <li><a href="/chinh-sach-quan-ly" class="haslink ">Chính sách - Quản lý</a></li>
                            <li><a href="/thong-tin-quy-hoach" class="haslink ">Thông tin quy hoạch</a></li>
                            <li><a href="/bat-dong-san-the-gioi" class="haslink ">Bất động sản thế giới</a>
                            </li>
                            <li><a href="/tai-chinh-chung-khoan-bat-dong-san" class="haslink ">Tài chính -
                                    Chứng khoán - BĐS</a></li>
                            <li><a href="/tu-van-luat-bat-dong-san" class="haslink indent">Tư vấn luật</a>

                            </li>
                            <li><a href="/loi-khuyen" class="haslink indent">Lời khuyên</a>

                            </li>
                        </ul>
                    </li>
                    <li class="menuparent">
                        <span class="menuparent nolink">Phong Thủy</span>
                        <ul>
                            <li><a href="/phong-thuy-toan-canh" class="haslink ">Phong thủy toàn cảnh</a>
                            </li>
                            <li><a href="/tu-van-phong-thuy" class="haslink ">Tư vấn phong thủy</a></li>
                            <li><a href="/phong-thuy-nha-o" class="haslink indent">Phong thủy nhà ở</a>

                            </li>
                            <li><a href="/phong-thuy-van-phong" class="haslink ">Phong thủy văn phòng</a>
                            </li>
                            <li><a href="/tin-tuc-phong-thuy-theo-tuoi" class="haslink ">Phong thủy theo
                                    tuổi</a></li>
                        </ul>
                    </li>
                    <li class="menuparent">
                        <span class="menuparent nolink">Kiến Trúc</span>
                        <ul>
                            <li><a href="/tu-van-thiet-ke" class="haslink ">Tư vấn thiết kế</a></li>
                            <li><a href="/kien-truc-xua-va-nay" class="haslink ">Kiến trúc xưa và nay</a>
                            </li>
                            <li><a href="/the-gioi-kien-truc" class="haslink ">Thế giới kiến trúc</a></li>
                            <li><a href="/nha-dep" class="haslink ">Nhà đẹp</a></li>
                            <li><a href="/thu-vien-mau-nha" class="haslink indent">Thư viện mẫu nhà</a>

                            </li>
                        </ul>
                    </li>
                    <li class="menuparent">
                        <span class="menuparent nolink">Xậy Dựng</span>
                        <ul>
                            <li><a href="/vat-lieu-xay-dung" class="haslink ">Vật liệu xây dựng</a></li>
                            <li><a href="/kien-thuc-xay-dung" class="haslink ">Kiến thức xây dựng</a></li>
                            <li><a href="/giai-phap-xay-dung" class="haslink ">Giải pháp xây dựng</a></li>
                        </ul>
                    </li>
                    <li class="menuparent">
                        <span class="menuparent nolink">Nhà Đẹp</span>
                        <ul>
                            <li><a href="#">Ngoại Thất</a></li>
                            <li><a href="#">Nội Thất</a>
                                <ul>
                                    <li><a href="/toan-canh-ngoi-nha" class="haslink ">Toàn cảnh ngôi
                                            nhà</a></li>
                                    <li><a href="/phong-khach" class="haslink ">Phòng khách</a></li>
                                    <li><a href="/phong-ngu" class="haslink ">Phòng ngủ</a></li>
                                    <li><a href="/phong-bep" class="haslink ">Phòng bếp</a></li>
                                    <li><a href="/phong-tam" class="haslink ">Phòng tắm</a></li>
                                    <li><a href="/phong-tre-em" class="haslink ">Phòng trẻ em</a></li>
                                    <li><a href="/noi-that-van-phong" class="haslink ">Nội thất văn
                                            phòng</a></li>
                                    <li><a href="/khach-san-nha-hang-showroom" class="haslink ">Khách sạn,
                                            Nhà hàng</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul><!-- /.nav -->

                <div class="language-switcher">
                    <div id="google_translate_element"></div>
                </div><!-- /.language-switcher -->

            </div><!-- /.navigation -->
        </div><!-- /.navigation-wrapper -->
    </div><!-- /.container -->
</div><!-- /.navigation -->

<!-- Modal -->
<div class="modal hide fade" id="modal_frm_login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo $this->lang->line('lbl_sign_in'); ?>
                </h4>
            </div>
            <form action='' method='' id='frm_user_login'>
                <div class="modal-body">
                    <div class="alert alert-danger ehide" id='login_message' role="alert">
                    </div>
                    <div class='control-group'>
                        <label>
                            <?php echo $this->lang->line('lbl_email'); ?></label>
                        <div class="controls">
                            <input type="email" name='email' autocomplete="off" id='field_email_login' class='form-control lfrequired my-input' placeholder='<?php echo $this->lang->line('txt_enter_email_or_phone_user'); ?>' />
                        </div>
                    </div>
                    <div class='control-group'>
                        <label>
                            <?php echo $this->lang->line('lbl_password'); ?></label>
                        <div class="controls">
                            <input type="password" autocomplete="off" name='password' id='field_pass_login' class='form-control lfrequired my-input' placeholder='<?php echo $this->lang->line('txt_enter_password'); ?>' />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href='<?php echo Links::account('register'); ?>' class="btn btn-default" style='font-weight:bold;'>
                        <?php echo $this->lang->line('txt_dont_have_account'); ?></a>
                    <a href='<?php echo Links::account('forgot_pass'); ?>' class="btn btn-default" style='font-weight:bold;'>
                        <?php echo $this->lang->line('txt_forgot_password'); ?></a>
                    <input type="button" class="btn btn-primary" value='<?php echo $this->lang->line('lbl_sign_in'); ?>' id='btn_user_login' onclick="return onUserLogin();" />
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            pageLanguage: 'vi',
            layout: google.translate.TranslateElement.InlineLayout.SIMPLE
        }, 'google_translate_element');
    }
</script>

<style>
    .goog-te-gadget {
        color: #fff;
        width: 100%;
    }

    .goog-te-gadget-simple {
        width: 100%;
        height: 28px;
        line-height: 28px;
        background-color: #fff;
        border: none;
        font-size: 10pt;
        display: inline-block;
        padding-top: 1px;
        padding-bottom: 2px;
        cursor: pointer;
        zoom: 1;
    }

    .goog-te-menu-value img {
        display: none;
    }

    .goog-te-menu-value span:nth-child(3) {
        display: none;
    }

    .goog-te-menu-value span:nth-child(5) {
        padding-right: 4px;
        padding-left: 4px;
    }

    @media (min-width: 1200px) {
        .goog-te-menu-value span:nth-child(1) {
            padding-right: 45px;
        }
    }

    @media (max-width: 1199px) {
        .goog-te-menu-value span:nth-child(1) {
            padding-right: 18px;
        }
    }

    .skiptranslate goog-te-gadget {}

    .nav-logo {
        border: 1px solid #fff;
        border-radius: 100%;
        display: inline-block;
        height: 50px !important;
        width: 50px !important;
        background-image: url(../public/images/stories/logo.png);
        background-repeat: no-repeat;
        vertical-align: middle;
        background-position: center;
        background-size: contain;
    }

    .nat-header {
        background-color: #02344a;

    }
</style>