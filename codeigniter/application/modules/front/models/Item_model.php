<?php
class Item_model extends CI_Model{

	protected $_name = 'bds_items';
	var $id;
	var $title;
	var $need_help;
	var $date_started;
	var $date_ended;
	var $description;
	var $type_item;
	var $add_level_1;
	var $add_level_2;
	var $add_level_3;
	var $add_level_4;
	var $project_id;
	var $project_type;
	var $num_house;
	var $full_address;
	var $price_total;
	var $price_square;
	var $contact_name;
	var $contact_phone;
	var $contact_company;
	var $contact_email;
	var $contact_zalo;
	var $contact_viber;
	var $price_equivalent;
	var $lat;
	var $lng;
	var $photos;

	function Item_model(){
		parent::__construct();
		$this->load->database();
	}

	function getById($id){
		$sql = "SELECT * FROM {$this->_name} WHERE id = ".intval($id);
		$query = $this->db->query($sql);

		return $query->row();
	}

	function getListWP($limit = 0, $start = 0, &$total=0){
		if($limit){
			$select = 'count(i.id) as total';
			$query = $this->db->query($this->queryItems($select));
			$total = $query->row()->total;
		}

		$select = 'i.*';
		$query = $this->db->query($this->queryItems($select, array($start, $limit)));
		return $query->result();
	}

	function queryItems($select, $pagination = null){

		$order = 'i.id desc';

		$sql = "SELECT {$select} FROM {$this->_name} as i";

		if($pagination[1] != 0){
			$sql.=" order by {$order}  limit {$pagination[0]}, {$pagination[1]}";
		}
		return $sql;
	}

	function save($params = null){

		if(empty($params['id'])){
			$params['created_date'] = date('Y-m-d H:i:s');
			$this->db->insert($this->_name, $params);
			return $this->db->insert_id();
		}else{
			$this->db->where('id', $params['id']);
			if($this->db->update($this->_name, $params)) 
				return $params['id'];
			else
				return 0;
		}
	}

	function delete($id){
		return $this->db->delete($this->_name, array('id' => $id)); 
	}
	/*------------------Front end-----------------*/
	function getDetailById($id){
		$select = "i.*, ip.*, pp.lbli_total_area, pp.lbli_total_area_building, pp.type as type_of_project, 
		lo.name as city, IF(lt.name REGEXP '^[0-9]+$', concat('Quận ',lt.name), lt.name) as district, IF(lr.name REGEXP '^[0-9]+$', concat('Phường ',lr.name), lr.name) as ward, u.full_name as approval_name";
		$sql = "SELECT {$select} FROM {$this->_name} as i 
		INNER JOIN bds_item_properties as ip on i.id = ip.item_id
		LEFT JOIN bds_properties_proportion as pp on i.project_type = pp.id
		LEFT JOIN bds_add_level_1 as lo on lo.id = i.add_level_1
		LEFT JOIN bds_add_level_2 as lt on lt.id = i.add_level_2
		LEFT JOIN bds_add_level_3 as lr on lr.id = i.add_level_3
		LEFT JOIN bds_users as u on u.id = i.approval_id
		WHERE i.id = ".intval($id);

    	$query = $this->db->query($sql);
    	return $query->row();
	}
	function getItemByUser($filter, $limit = 0, $start = 0, &$total=0){

		if($limit){
			$select = 'count(i.id) as total';
			$query = $this->db->query($this->queryItemByUser($select, $filter));
			$total = $query->row()->total;
		}

		$select = 'i.*,u.email';
		$query = $this->db->query($this->queryItemByUser($select, $filter, array($start, $limit)));

		return $query->result(); 
	}

	function queryItemByUser($select, $filter, $pagination = null){

		$order = 'created_date desc, i.id desc';

		$sql = "SELECT {$select} FROM {$this->_name} as i
		INNER JOIN bds_users as u on i.user_id = u.id";
		if(count($filter)){
			$where = array();
			foreach ($filter as $key => $value) {
				$where[] = $value;
			}
			$sql.=" WHERE ".implode(" AND ", $where);
		}

		if($pagination[1] != 0){
			$sql.=" order by {$order}  limit {$pagination[0]}, {$pagination[1]}";
		}
		return $sql;
	}

	function getByLocation($filters){
		$filters_str = implode(" AND ", $filters);
		$select = "i.id,i.type_item, i.title, i.full_address, i.price_total, i.lat, i.lng, i.photos, ip.total_area, ip.total_area_building, lbli_total_area, lbli_total_area_building";
		$sql = "SELECT {$select} FROM {$this->_name} as i 
		LEFT JOIN bds_item_properties as ip on i.id = ip.item_id
		LEFT JOIN bds_properties_proportion as pp on ip.belong_project = pp.id
		WHERE $filters_str
    	ORDER BY created_date DESC LIMIT 0, 200";

    	$query = $this->db->query($sql);
    	return $query->result();
	}


	function getListItems($filter, $limit = 0, $start = 0, &$total=0){
		if($limit){
			$select = 'count(i.id) as total';
			$query = $this->db->query($this->queryListItems($select, $filter));
			$total = $query->row()->total;
		}

		$select = "i.id, i.title,i.type_item, i.full_address, i.description, i.price_total, i.lat, i.lng, i.photos, i.video,
		i.created_date, i.date_started, ip.total_area, ip.total_area_building, lbli_total_area, lbli_total_area_building";
		$query = $this->db->query($this->queryListItems($select, $filter, array($start, $limit)));

		return $query->result();
	}

	
	function getNewsList($filter, $limit = 0, $start = 0, &$total=0){
		if($limit){
			$select = 'count(i.id) as total';
			$query = $this->db->query($this->queryListItems($select, $filter));
			$total = $query->row()->total;
		}

		$select = "i.id, i.title,i.type_item, i.full_address, i.description, i.price_total, i.lat, i.lng, i.photos, i.video,
		i.created_date, i.date_started, ip.total_area, ip.total_area_building, lbli_total_area, lbli_total_area_building";
		$query = $this->db->query($this->queryListItems($select, $filter, array($start, $limit)));

		return $query->result();
	}

	function queryListItems($select, $filter, $pagination = null){

		$order = 'i.created_date desc';

		$sql = "SELECT {$select} FROM {$this->_name} as i";

		$sql = "SELECT {$select} FROM {$this->_name} as i 
		LEFT JOIN bds_item_properties as ip on i.id = ip.item_id
		LEFT JOIN bds_properties_proportion as pp on i.project_type = pp.id";

		if(count($filter)){
			$where = array();
			foreach ($filter as $key => $value) {
				$where[] = $value;
			}
			$sql.=" WHERE ".implode(" AND ", $where);
		}

		if($pagination[1] != 0){
			$sql.=" order by {$order}  limit {$pagination[0]}, {$pagination[1]}";
		}

		return $sql;
	}

	function searchItems($kewords, $limit = 0, $start = 0, &$total=0){
		if($limit){
			$select = 'count(i.id) as total';
			$query = $this->db->query($this->querySearchItems($select, $kewords));
			$total = $query->row()->total;
		}

		$select = "i.id, i.type_item, i.title, i.full_address, i.description, i.price_total, i.lat, i.lng, i.photos, i.video, 
		i.created_date, i.date_started, ip.total_area, ip.total_area_building, lbli_total_area, lbli_total_area_building";
		$query = $this->db->query($this->querySearchItems($select, $kewords, array($start, $limit)));

		return $query->result();
	}

	function querySearchItems($select, $kewords, $pagination = null){

		$order = 'i.created_date desc';

		$sql = "SELECT {$select} FROM {$this->_name} as i";

		$sql = "SELECT {$select} FROM {$this->_name} as i 
		LEFT JOIN bds_item_properties as ip on i.id = ip.item_id
		LEFT JOIN bds_properties_proportion as pp on i.project_type = pp.id
		WHERE status = %s AND (title like '%%%s%%' OR full_address like '%%%s%%' OR description like '%%%s%%')";
		$sql = sprintf($sql, ItemStatus::$Approved, $kewords, $kewords, $kewords);


		if($pagination[1] != 0){
			$sql.=" order by {$order}  limit {$pagination[0]}, {$pagination[1]}";
		}
		return $sql;
	}

}