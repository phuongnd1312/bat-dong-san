<?php
class Report_model extends CI_Model{

	protected $_name = 'tbl_users_notify';
	function Report_model(){
		parent::__construct();
		$this->load->database();
	}

	function getUsersNotify(){
		$sql = "SELECT n.*, u.full_name, u.mobile FROM {$this->_name} as n
				INNER JOIN bds_users as u on u.id = n.user_id
				WHERE notify = 0";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function getListWP($limit = 0, $start = 0, &$total=0){
		if($limit){
			$select = 'count(n.user_id) as total';
			$query = $this->db->query($this->queryItems($select));
			$total = $query->row()->total;
		}

		$select = 'n.*, u.full_name';
		$query = $this->db->query($this->queryItems($select, array($start, $limit)));
		return $query->result();
	}

	function queryItems($select, $pagination = null){

		$order = 'n.user_id desc';

		$sql = "SELECT {$select} FROM {$this->_name} as n
				INNER JOIN bds_users as u on u.id = n.user_id
				WHERE notify = 0";

		if($pagination[1] != 0){
			$sql.=" order by {$order}  limit {$pagination[0]}, {$pagination[1]}";
		}
		return $sql;
	}

	function getUser($user_id){
		$sql = "SELECT * FROM {$this->_name} WHERE user_id = ".$user_id;
		$query = $this->db->query($sql);
		return $query->row();
	}

	function save($contact_user, $user_review, $iid, $phone, $update = true){
		$params = array();
		$params['user_id'] = $contact_user;
		$params['phone'] = $phone;
		$params['item_id'] = $iid;
		$params['user_review'] = $user_review;
		$params['created_date'] = date('Y-m-d H:i:s');

		if($this->getUser($contact_user) == null){
			$this->db->insert($this->_name, $params);
			return $this->db->insert_id();
		}else if($update){
			return $this->db->update($this->_name, $params, array('user_id'=>$contact_user));
		}
		
	}

	function updateNotify($users){
		$this->db->trans_start();
		foreach ($users as $key => $user) {
			$sql = "UPDATE %s SET notify = 1 WHERE user_id = %s";
			$sql = sprintf($sql, $this->_name, $user);
			$this->db->query($sql);
		}
		
		$this->db->trans_complete();
		return $this->db->trans_status();
	}
}