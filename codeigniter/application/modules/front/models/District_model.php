<?php
class District_model extends CI_Model{

	protected $_name = 'bds_add_level_2';
	var $id;

	function District_model(){
		parent::__construct();
		$this->load->database();
	}

	function getById($id){
		$sql = "SELECT *, IF(name REGEXP '^[0-9]+$', concat('Quận ',name), name) as full_name FROM {$this->_name} as d WHERE id = ".$this->db->escape($id);
		$query = $this->db->query($sql);

		return $query->row();
	}

	function getByCity($city){
		$sql = "SELECT *, IF(name REGEXP '^[0-9]+$', concat('Quận ',name), name) as full_name FROM {$this->_name} as d WHERE lvl1_id = ".$this->db->escape($city)." ORDER BY name asc";
		$query = $this->db->query($sql);
		return $query->result();
	}
}