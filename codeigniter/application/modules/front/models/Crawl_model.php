<?php
class Crawl_model extends CI_Model
{

	function Crawl_model()
	{
		parent::__construct();
		$this->load->database();
	}

	function getDistrict($city, $district)
	{
		$sql = "SELECT d.id as district, c.id as city from bds_add_level_2 as d
				INNER JOIN bds_add_level_1 as c on c.id = d.lvl1_id
				WHERE c.name = %s AND d.name = %s";
		$sql = sprintf($sql, $this->db->escape($city), $this->db->escape(trim($district)));
		$query = $this->db->query($sql);

		return $query->row();
	}

	function insertStreet($district, $city, $streets)
	{
		$sql = "SELECT id FROM bds_add_level_4 WHERE lvl2_id = $district";
		$query = $this->db->query($sql);
		if ($query->row() != null)
			return -1;
		$data = array();
		foreach ($streets as $key => $street) {
			$data[] = array('name' => $street, 'lvl1_id' => $city, 'lvl2_id' => $district);
		}

		return $this->db->insert_batch('bds_add_level_4', $data);
	}

	function log($city, $district, $success, $msg)
	{
		$params = array();
		$params['city'] = $city;
		$params['district'] = $district;
		$params['success'] = $success;
		$params['message'] = $msg;
		$this->db->insert('bds_logs', $params);
	}

	function getCityByName($city, &$sql = null)
	{
		$sql = "SELECT id FROM bds_add_level_1 WHERE name = '" . trim($city) . "'";
		$query = $this->db->query($sql);

		return $query->row();
	}

	function getProjects()
	{

		$sql = "SELECT * FROM bds_projects";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function updateProject($params = null)
	{

		$this->db->where('id', $params['id']);
		if ($this->db->update('bds_projects', $params))
			return $params['id'];
		else
			return 0;

	}

	function getDistrictByName($city, $district)
	{
		$district = trim(str_replace(array('Quận', 'Huyện'), '', $district));
		$sql = "SELECT id FROM bds_add_level_2 WHERE lvl1_id = {$city} AND name = '" . $district . "'";
		$query = $this->db->query($sql);

		return $query->row();
	}

	function getWardByName($district, $ward)
	{
		$sql = "SELECT id FROM bds_add_level_3 WHERE lvl2_id = {$district} AND name = '" . $ward . "'";
		$query = $this->db->query($sql);

		return $query->row();
	}

	function getItemsCrawl()
	{
		$sql = "SELECT * FROM tb_room ";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function getItemByBdsId($id)
	{
		$sql = "SELECT * FROM bds_items WHERE bds_id = " . $id;
		$query = $this->db->query($sql);
		return $query->row();
	}
	function getBelongProjectByName($name)
	{
		$sql = "SELECT id FROM bds_projects WHERE name = '" . $name . "'";
		$query = $this->db->query($sql);

		return $query->row();
	}

	function getItemProp($id)
	{
		$sql = "SELECT * FROM bds_item_properties WHERE item_id = " . intval($id);
		$query = $this->db->query($sql);
		return $query->row();
	}

	function saveItemPropertise($params)
	{
		if ($this->getItemProp($params['item_id']) == null) {
			$this->db->insert('bds_item_properties', $params);
			return $this->db->insert_id();
		} else {
			$this->db->where('item_id', $params['item_id']);
			if ($this->db->update('bds_item_properties', $params))
				return $params['item_id'];
			else
				return 0;
		}
	}

	function saveItem($params = null)
	{

		if (empty($params['id'])) {
			$this->db->insert('bds_items', $params);
			return $this->db->insert_id();
		} else {
			$this->db->where('id', $params['id']);
			if ($this->db->update('bds_items', $params))
				return $params['id'];
			else
				return 0;
		}
	}

	function getUsers()
	{
		$sql = "SELECT * FROM bds_users WHERE length(password) < 40 ";

		$query = $this->db->query($sql);
		return $query->result();
	}

	function getUsersByRole($role = null, $limit = 0)
	{
		$sql = "SELECT * FROM bds_users WHERE user_role = " . $role . "  limit " . $limit;

		$query = $this->db->query($sql);
		return $query->result();
	}
}