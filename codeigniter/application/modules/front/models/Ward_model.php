<?php
class Ward_model extends CI_Model{

	protected $_name = 'bds_add_level_3';
	var $id;

	function Ward_model(){
		parent::__construct();
		$this->load->database();
	}

	function getById($id){
		$sql = "SELECT * ,IF(name REGEXP '^[0-9]+$', concat('Phường ',name), name)  as full_nameFROM {$this->_name} WHERE id = ".$this->db->escape($id);
		$query = $this->db->query($sql);
		return $query->row();
	}

	function getByDistrict($district){
		$sql = "SELECT *, IF(name REGEXP '^[0-9]+$', concat('Phường ',name), name)  as full_name FROM {$this->_name} WHERE lvl2_id = %s ORDER BY name asc";
		$sql = sprintf($sql, intval($district));
	
		$query = $this->db->query($sql);
		return $query->result();
	}
}