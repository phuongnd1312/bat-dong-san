<?php
class User_model extends CI_Model{
	
	protected $_name = 'bds_users';
	var $id;
	var $full_name;
	var $name_alias;
	var $birthday;
	var $gender;
	var $address;
	var $username;
	var $password;
	var $user_role;
	var $email;
	var $mobile;
	var $viber;
	var $zalo;
	var $deskphone;

	function User_model(){
		parent::__construct();
		$this->load->database();
	}

	function getByLoginField($email, $mobile){
		$sql = "SELECT * FROM {$this->_name} where email= %s OR mobile = %s";
		$sql = sprintf($sql, $this->db->escape($email), $this->db->escape($mobile));

		$query = $this->db->query($sql);
		return $query->row();
	}

	function getByEmail($email){
		$sql = "SELECT * FROM {$this->_name} where email=".$this->db->escape($email);

		$query = $this->db->query($sql);
		return $query->row();
	}

	function getByUsername($username){
		$sql = "SELECT * FROM {$this->_name} where username=".$this->db->escape($username);

		$query = $this->db->query($sql);
		return $query->row();
	}

	function getByEmailOrPhone($email_phone){
		$sql = "SELECT * FROM {$this->_name} where email= %s OR mobile = %s";
		$sql = sprintf($sql, $this->db->escape($email_phone), $this->db->escape($email_phone));

		$query = $this->db->query($sql);
		return $query->row();
	}

	function getById($id){
		$sql = "SELECT * FROM {$this->_name} where id=".$this->db->escape($id);

		$query = $this->db->query($sql);
		return $query->row();
	}

	function save($params = null){
		
		if(empty($params['id'])){
			$this->db->insert($this->_name, $params);
			return $this->db->insert_id();
		}else{
			$this->db->where('id', $params['id']);

			return $this->db->update($this->_name, $params); 
		}
		
	}

	function sign_in($email, $password){
		$sql = "SELECT * FROM {$this->_name} where email= %s OR username = %s";
		$sql = sprintf($sql, $this->db->escape($email), $this->db->escape($email));

		$query = $this->db->query($sql);
		$user = $query->row();
		if($user != null && password_verify($password, $user->password))
			return $user;
		else
			return null;

	}

	function activate($code){
		$sql = "SELECT * FROM {$this->_name} where activation_code=".$this->db->escape($code);
		$query = $this->db->query($sql);
		$user = $query->row();
		if($user == null)
			return null;
		else
		{
			$params = array();
			$params['activation_code'] = '';
			$params['active'] = 1;
			$this->db->where('id', $user->id);
			if($this->db->update($this->_name, $params))
				return $user;
			else
				return null;
		}
	}

	function getByCode($code){
		$sql = "SELECT * FROM {$this->_name} where activation_code=".$this->db->escape($code);

		$query = $this->db->query($sql);
		return $query->row();
	}
}