<?php
class Project_model extends CI_Model{

	protected $_name = 'bds_projects';
	var $id;
	var $name;
	var $description;

	function Project_model(){
		parent::__construct();
		$this->load->database();
	}

	function getById($id){
		$sql = "SELECT * FROM {$this->_name} WHERE id = ".$this->db->escape($id);
		$query = $this->db->query($sql);
		return $query->row();
	}

	function getList($filter = null){
		$sql = "SELECT * FROM {$this->_name}";
		if(count($filter))
		{
			$sql.=' WHERE '.implode(' AND ', $filter);
		}

		$query = $this->db->query($sql);
		return $query->result();
	}


	function save($params = null){
		
		if(empty($params['id'])){
			$this->db->insert($this->_name, $params);
			return $this->db->insert_id();
		}else{
			$this->db->where('id', $params['id']);
			if($this->db->update($this->_name, $params)) 
				return $params['id'];
			else
				return 0;
		}
	}

	function delete($id){
		return $this->db->delete($this->_name, array('id' => $id)); 
	}
}