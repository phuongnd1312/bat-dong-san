<?php
class User_item_model extends CI_Model{
	
	protected $_name = 'bds_user_items';

	function User_item_model(){
		parent::__construct();
		$this->load->database();
	}

	function save($params = null){
		return $this->db->insert($this->_name, $params);
	}

	function delete($user_id, $item_id){
		return $this->db->delete($this->_name, array('user_id' => $user_id, 'item_id'=>intval($item_id))); 
	}

	function getItem($user_id, $item_id){
		$query = $this->db->get_where($this->_name, array('user_id' => $user_id, 'item_id'=>intval($item_id)));
		return $query->row();
	}

	function getByUser($user_id, $hashmap = true){
		$query = $this->db->get_where($this->_name, array('user_id' => $user_id));
		$rlt = $query->result();
		if($rlt != null){
			$arr = array();
			foreach ($rlt as $key => $val) {
				$arr[$val->item_id] = $val->item_id;
			}
			return $arr;
		}
		return $rlt;
	}

	function getUserItems($user_id, $limit = 0, $start = 0, &$total=0){
		if($limit){
			$select = 'count(i.id) as total';
			$query = $this->db->query($this->queryListItems($select, $user_id));
			$total = $query->row()->total;
		}

		$select = "i.id, i.title, i.full_address, i.description, i.price_total, i.lat, i.lng, i.photos, 
		i.created_date, ip.total_area, ip.total_area_building, lbli_total_area, lbli_total_area_building";
		$query = $this->db->query($this->queryListItems($select, $user_id, array($start, $limit)));

		return $query->result();
	}

	function queryListItems($select, $user_id, $pagination = null){

		$order = 'i.created_date desc';

		$sql = "SELECT {$select} FROM {$this->_name} as i";

		$sql = "SELECT {$select} FROM bds_items as i 
		LEFT JOIN bds_item_properties as ip on i.id = ip.item_id
		LEFT JOIN bds_properties_proportion as pp on i.project_type = pp.id 
		WHERE i.id IN (SELECT item_id FROM {$this->_name} WHERE user_id = {$user_id})";

		if($pagination[1] != 0){
			$sql.=" order by {$order}  limit {$pagination[0]}, {$pagination[1]}";
		}
	
		return $sql;
	}
}