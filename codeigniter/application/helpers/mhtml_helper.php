<?php 
class Mhtml{
	public static function breakNum($num){
		$arr = str_split((string)$num);

		$length = count($arr);
		$html = '';
		for ($i=0; $i < $length; $i++) { 
			$html.='<span>'.$arr[$i].'</span>';
		}

		return $html;
	}

	public static function tinymce(){
		$editor = '
		<script>
			var Editor = {
			basic: function(id){
				tinymce.init({
				selector: "#"+id,
				relative_urls: false,
				remove_script_host: false,
					plugins: [
				        "advlist autolink lists image link charmap print preview anchor",
				        "searchreplace visualblocks code",
				        "insertdatetime table contextmenu paste responsivefilemanager"
				    ],
				    toolbar: "undo redo | bold italic | sub sup | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
					external_filemanager_path: BASE_URL+"/filemanager/",
					filemanager_title:"Responsive Filemanager" ,
					external_plugins: { "filemanager" : BASE_URL+"/filemanager/plugin.min.js"}
					});
				},
			}
		</script>';

		return $editor;
	}

	public static function gpost($key, $defaul = null){
		if(!isset($_POST[$key]))
			return $defaul;
		else 
			return $_POST[$key];
	}

	public static function gget($key, $defaul = null){
		if(!isset($_GET[$key]))
			return $defaul;
		else 
			return $_GET[$key];
	}

	public static function loadView($view, $data=null){
		$ci = &get_instance();
		return $ci->load->view($view, $data, true);
	}

	public static function jslang(){
		$ci = &get_instance();
		$tmp = $ci->lang->language;
		$jslang = array();
		foreach ($tmp as $key => $value) {
			if(strpos($key,'js_') === 0)
				$jslang[$key] = $value;
			else
				break;
		}
		echo '
			<script>
				var jlang = '.json_encode($jslang).';
			</script>
		';
	}

	public static function do_upload_img($ctx, $upload_path, $name_file = null)
	{
		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = Constant::$ALLOW_PIC_UPLOAD;
		$config['max_size']	= Constant::$LIMIT_PIC_UPLOAD;
		if(!empty($name_file))
			$config['file_name'] = $name_file;
		else
			$config['encrypt_name'] = true;

		$ctx->load->library('upload', $config);

		if ( ! $ctx->upload->do_upload())
		{
			$error = array('error' => $ctx->upload->display_errors());
			return $error;
		}
		else
		{
			$data = array('upload_data' => $ctx->upload->data());
			return $data;
		}
	}

	public static function sorting($name, $field){
	    $class = '';
	    $opt = 'asc';
	    if(!empty($_GET['field'])){
	        $opt = (isset($_GET['sort']) && $_GET['sort'] == 'asc') ? 'desc' : 'asc'; 
	        if($_GET['field'] == $field)
	            $class='active';

	    }
	    
	    $query_string = Util::addQueryString(array('field'=>$field, 'sort'=>$opt));
	 
	    echo "<a class='{$class} sorting' href='".base_url(uri_string().'?'.$query_string)."'>{$name}</a>";
	}

}