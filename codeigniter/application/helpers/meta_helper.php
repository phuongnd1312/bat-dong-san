<?php 
class Meta{
	private static $intance = null;
	private $ci;
	public $image = null;
	public $title = null;
	public $favicon = null;
	public $description = null;
	public $url = null;
	public function Meta(){
		$this->ci = &get_instance();
		$this->title = 'Tìm nơi tổ ấm';
		$this->description = 'Bất động sản Việt Nam chuyên cung cấp tin về mua bán nhà đất, cho thuê nhà đất, văn phòng, căn hộ, biệt thự, chung cư.';
		$this->url = current_url();
		$this->image = Util::loadImg('/images/stories/logo_share_fb.png');
		$this->favicon = Util::loadImg('/images/stories/favicon.png');

	}
	public static function ins(){
		if(self::$intance == null)
			self::$intance = new Meta();
		return self::$intance;
	} 

	public function setMeta($title, $description = null, $image = null){
		$this->title = $title;

		if(!empty($image))
			$this->image = $image;

		if(!empty($description))
			$this->description = $description;
		
	}

	public function showMeta(){
		$resp = array();
		$resp['title'] = $this->title;
		$resp['image'] = $this->image;
		$resp['favicon'] = $this->favicon;
		$resp['description'] = $this->description;
		$resp['url'] = $this->url;
		echo Mhtml::loadView('front/partials/meta', $resp);
	}
}