<?php
set_include_path(APPLICATION_PATH.'/third_party' . PATH_SEPARATOR . get_include_path());
require_once 'Google/Client.php';
require_once 'Google/Service/Oauth2.php';
require_once 'facebook/auth_facebook.php';

class Register_Helper{


	public static function byfacebook(){
    
		
		$facebook = new Auth_Facebook();

		$user_profile = null;
		$user_face = $facebook->getUser($user_profile);

		if($user_face==0)
		{
			redirect($facebook->getUrlLogin());
			return null;
		}
		else
		{
			$resp = array();
			if(!empty($user_profile['email']))
				$resp['email'] = $user_profile['email'];
			$resp['lname'] = empty($user_profile['last_name']) ? '' : $user_profile['last_name'];
			$resp['mname'] = empty($user_profile['middle_name']) ? '' : $user_profile['middle_name'];
			$resp['fname'] = empty($user_profile['first_name']) ? '' : $user_profile['first_name'];
			return $resp;
		}
  	}

  	public static function bygoogle()
	{
		$redirect=base_url('main/bygoogle');
		if(ENVIRONMENT == 'development'){
			$client_key = Constant::$DEV_GCLIENT_ID;
			$client_secret = Constant::$DEV_GCLIENT_SECRECT;
		}
		else
		{
			$client_key = Constant::$PRO_GCLIENT_ID;
			$client_secret = Constant::$PRO_GCLIENT_SECRECT;
		}
		$gclient = new Google_Client(); 
		$gclient->setClientId($client_key);
		$gclient->setClientSecret($client_secret);
		$gclient->setScopes('email');
		

		$gclient->setRedirectUri($redirect);

		if (isset($_GET['code'])) {
			  $gclient->authenticate($_GET['code']);
			  User_Helper::ins()->setSession('access_token', $gclient->getAccessToken());
			  header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
		}

		if (User_Helper::ins()->getSession('access_token', null) != null) {
		  	$gclient->setAccessToken(User_Helper::ins()->getSession('access_token'));
		} else {
		  	$authUrl = $gclient->createAuthUrl();
		  	redirect($authUrl); 
		}

		if ($gclient->getAccessToken()) {
			  User_Helper::ins()->setSession('access_token', $gclient->getAccessToken());
			  //$token_data = $gclient->verifyIdToken();

			  $userInfoService = new Google_Service_Oauth2($gclient);
			  try {
					$userInfo = $userInfoService->userinfo->get();
				
					if(isset($userInfo->email)){
						$resp = array();
						$resp['email'] = $userInfo->email;
						$resp['fname'] = $userInfo->familyName;
						$givenName = explode(" ", $userInfo->givenName);

						$length = count($givenName);
						if($length > 1)
						{
							$resp['lname'] = $givenName[$length-1];
							unset($givenName[$length-1]);
							$resp['mname'] = implode(' ', $givenName);
						}	
						else
						{
							$resp['mname'] = '';
							$resp['lname'] = $givenName;
						}
						return $resp;
					}

				
			  } catch (Google_Exception $e) {
			  	User_Helper::ins()->setSession('access_token',null);
			    redirect('index.php');
			  }
		}

	    return null;
	}
	
}