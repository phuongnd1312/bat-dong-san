<?php
class User_Helper{
	private static $intance = null;
	private $user = null;
	private $ci;
	public function User_Helper(){
		$this->ci = &get_instance();

	}
	public static function ins(){
		if(self::$intance == null)
			self::$intance = new User_Helper();
		return self::$intance;
	}
	public function isLogin(){
		$isLoggedIn = $this->ci->session->userdata(Constant::$KEY_USER_STATE, null);
		return  $isLoggedIn != null ? true : false;
	}
	public function setInfo($data){
		$this->user = $data;

		$tmp = (object)array('id'=>$data->id, 'username'=>$data->username);
        $this->ci->session->set_userdata(Constant::$KEY_USER_STATE, $tmp);
	}

	public function get(){


		if($this->user != null)
			return $this->user;

		$user = $this->ci->session->userdata(Constant::$KEY_USER_STATE, null);
		if($user != null){
			$this->ci->load->model('user_model');
			$this->user = $this->ci->user_model->getById($user->id);
			return $this->user;
		}
		else
			return null;
	}

	public function logout(){
		return $this->ci->session->unset_userdata(Constant::$KEY_USER_STATE);
	}

	public function setMessage($msg, $code='info'){
		//code = succ - info - warn - err
		$arr = $this->ci->session->userdata(Constant::$KEY_MESSAGE, array());
		$arr[] = array('msg'=>$msg, 'code'=>$code);
		return $this->ci->session->set_userdata(Constant::$KEY_MESSAGE, $arr);
	}
	public function getMessage($only_msg = false){
		$msgs = $this->ci->session->userdata(Constant::$KEY_MESSAGE, null);
		$this->ci->session->unset_userdata(Constant::$KEY_MESSAGE);

		if($msgs != null){
			$message = '';
			if($only_msg)
				$message = $msgs;
			else{
				foreach ($msgs as $key => $msg) {
					$class = $msg['code'];
					$message.= "<div class='alert alert-".$class."'>
							<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
						  	<strong></strong>".$msg['msg']."
						</div>";
				}

			}
			return $message;
		}
		return null;
	}

	public function checkPermission(){
		if($this->isLogin()){
			$request = sprintf('%s/%s', $this->ci->router->fetch_class(), $this->ci->router->fetch_method());
			$user = $this->get();
			if(in_array($request, UserRole::$ActPermit[$user->user_role]))
				return true;
		}
		$this->setMessage($this->ci->lang->line('warning_permission'));
		redirect(base_url('/'));
		exit;
	}

	public function sendMail($to, $message, $subject){
        $this->ci->load->library('email');

        $this->ci->email->from('info@bdsvn.com', 'Bất động sản Việt Nam');
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
        return $this->ci->email->send();
    }

}