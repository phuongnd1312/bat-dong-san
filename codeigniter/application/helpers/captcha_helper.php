<?php
set_include_path(APPLICATION_PATH.'/third_party' . PATH_SEPARATOR . get_include_path());

require_once 'captcha/securimage.php';

class Captcha_Helper{
	public static function show(){
		$img = new Securimage();
		$img->code_length =4;
		$img->show();
	}

	public static function check($code){
		$img = new Securimage();
		return $img->check($code);
	}
}
