<?php
class Links{
	static function cur_url(){
		return base_url(uri_string());
	}
	static function homepage($segments = ''){
		return base_url('/'.$segments);
	}

	static function account($segments = ''){
		return base_url('account/'.$segments);
	}

	static function itemSaved(){
		return base_url('tin-da-luu');
	}

	static function pushItem($segments = ''){
		return base_url('dang-tin/'.$segments);
	}

	static function manageItem(){
		return base_url('quan-tri-tin');
	}
}