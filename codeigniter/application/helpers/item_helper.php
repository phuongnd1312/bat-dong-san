<?php 
class Item{
	
	public static function currency2Num($str){
		$tmp = str_replace(App::ins()->thousands_sep,'',$str);
		return is_numeric($tmp) ? $tmp : 0;
	}
	public static function rentMonth($number){
		if($number=='1' or $number=='2')
		return '/tháng';
	}
	public static function formatPrice($price, $suffix ='đ'){
		if($price==0)
			return null;
		else

			return number_format($price,0,App::ins()->dec_point,App::ins()->thousands_sep).$suffix;
	}
	public static function formatCurrency($price){
        if ($price == 0)
            return "Thỏa thuận";
        else if ($price < 1000000){
            $tmp = $price/ 1000;
            return number_format($tmp,0,App::ins()->dec_point,App::ins()->thousands_sep)." nghàn";
        } else if($price < 1000000000){
            $tmp = $price/ 1000000;
            if (floor($tmp) == $tmp) 
                return number_format($tmp,0,App::ins()->dec_point,App::ins()->thousands_sep)." triệu";
            else
                return number_format($tmp,1,App::ins()->dec_point,App::ins()->thousands_sep)." triệu";
        }else{
            $tmp = $price/ 1000000000;
            if (floor($tmp) == $tmp) 
                return number_format($tmp,0,App::ins()->dec_point,App::ins()->thousands_sep)." tỉ";
            else
                return number_format($tmp,1,App::ins()->dec_point,App::ins()->thousands_sep)." tỉ";
        }
    }

	public static function photofst($item, $class=''){
		if(empty($item->photos))
			return '<img class="photos"src="'.Util::loadImg('/images/stories/no-thumbnail.png').'" />';
		else{
			$arr = explode(';', $item->photos);
			return '<img class="photos" src="'.Util::loadImg($arr[0]).'" class='.$class.' />';
		}
	}
	public static function videoImage($video){

		return '<img class="photos" src="http://img.youtube.com/vi/'.$video.'/default.jpg" /><img style="margin-left: -75px;"src='. Util::loadImg('/images/stories/youtube.png') .'>';

	}
	public static function replaceLinkVideo($video)
	{
		if($video)
		{
			$arr = explode("=", $video);
			return $arr[1];
		}

	}
	public static function photo($item){
		if(empty($item->photos))
			return '';

		$photos = explode(';', $item->photos);
		return $photos[0];
	}

	public static function photos($item){
		if(empty($item->photos))
			return array();

		return explode(';', $item->photos);
	}

	public static function getArea($item, $subfix = false){
		if($item->lbli_total_area == 2 || $item->lbli_total_area == 3)
			return $subfix ? Item::unitArea($item->total_area,' m²') : $item->total_area;
		else
			return $subfix ? Item::unitArea($item->total_area_building,' m²') : $item->total_area_building;
	}
	public static function unitArea($item,$subfix){
		if((int)($item/10000)>0){
			$item=$item/10000;
			$subfix=' ha';
		}
		return $item.$subfix;
	}
	public static function do_upload_pricture($item, &$ctx)
	{
		if($item == null)
		{
			$image_path = $path = '/images/temp';
			$full_path = PUBLIC_URL.$path;
		}else{
			$path = '/images/items';
			$sub = date('Y/m/d/', strtotime($item->created_date)).'/'.$item->id;
			$full_path = PUBLIC_URL.$path.'/'.$sub;
			$image_path = $path.'/'.$sub;
		}
		
		if(!file_exists($full_path)){
			if(!Util::createDirectoryDate('public'.$path, $sub))
				return array('error' => 'Can not make directory '.$path);
		}

		$config['upload_path'] = $full_path;
		$config['allowed_types'] = Constant::$ALLOW_PIC_UPLOAD;
		$config['max_size']	= Constant::$LIMIT_PIC_UPLOAD;
		$config['encrypt_name'] = true;


		$ctx->load->library('upload', $config);

		if ( ! $ctx->upload->do_upload())
			return array('error' => $ctx->upload->display_errors());
		else
			return array('upload_data' => $ctx->upload->data(), 'path' => $image_path);
	}

	public static function format_picture(&$ctx, $path){
		$ctx->load->library('image_lib');
		$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
		$path = PUBLIC_URL.$path;

		if($ext == 'png')
			$im = imagecreatefrompng($path);
		else
			$im = imagecreatefromjpeg($path);

		$ini_x_size = imagesx($im);
		if($ini_x_size > Constant::$RES_IMG['width']){
			$config['image_library'] = 'gd2';
			$config['source_image']	= $path;
			$config['create_thumb'] = FALSE;
			$config['width'] = Constant::$RES_IMG['width'];
			
			$ctx->image_lib->initialize($config); 
			$ctx->load->library('image_lib', $config); 

			$ctx->image_lib->resize();

			//load img sau khi resize
			if($ext == 'png')
				$im = imagecreatefrompng($path);
			else
				$im = imagecreatefromjpeg($path);
		}
	

		$new_width = $ini_x_size = imagesx($im);
		$new_height = $ini_y_size = imagesy($im);


		// Load
		//$new_width = $new_height = 0;
		if($ini_x_size < Constant::$RES_IMG['width'])
		{
			$new_width = Constant::$RES_IMG['width'];
			$new_height = ($ini_y_size*$new_width) / $ini_x_size;
		}	

		if($new_height < Constant::$RES_IMG['height']){
			$new_height = Constant::$RES_IMG['height'];
			$new_width = ($ini_x_size*$new_height) / $ini_y_size;
		}

		$thumb = imagecreatetruecolor($new_width, $new_height);

		// Resize
		imagecopyresized($thumb, $im, 0, 0, 0, 0, $new_width, $new_height, $ini_x_size, $ini_y_size);

		if($new_height != Constant::$RES_IMG['height'] || $new_width != Constant::$RES_IMG['width'] ){
			$to_crop_array = array('x' =>0 , 'y' => 0, 'width' => Constant::$RES_IMG['width'], 'height'=> Constant::$RES_IMG['height']);
			$thumb_im = imagecrop($thumb, $to_crop_array);

			if($ext == 'png')
				imagepng($thumb_im,  $path);
			else
				imagejpeg($thumb_im,  $path);
			imagedestroy($im);
		}
	}

	public static function movePhotos($item){
		$arr_photos = explode(";", trim($item->photos));

		$path = 'public/images/items/';
		$sub = date('Y/m/d', strtotime($item->created_date)).'/'.$item->id;
		if(!file_exists($path.$sub)){
			if(!Util::createDirectoryDate($path, $sub)){
				return false;
			}
		}

		$length = count($arr_photos);
		for($i = 0; $i < $length; $i++){
			if(preg_match("/^\/images\/temp/", $arr_photos[$i], $matches)){

				$path_file = 'public'.$arr_photos[$i];
				$new_file = '/images/items/'.$sub.'/'.basename($path_file);

				if(is_file($path_file)){
					if(copy($path_file, 'public'.$new_file)){
						$arr_photos[$i] = $new_file;
					}
					unlink($path_file);
				}
			}
		}

		return $arr_photos;
	}

	
	public static function item_save(&$ctx){
		$user =  User_Helper::ins()->get();

		$params['id'] = $ctx->gpost('id', null);
		//user post
		if($params['id'] == null){ 
			$params['user_id'] = $user->id;
		}

		$params['title'] = $ctx->gpost('title', '');
		$params['need_help'] = $ctx->gpost('need_help', '');
		$params['description'] = $ctx->gpost('description', '');
		$params['type_item'] = $ctx->gpost('type_item', 0);
		$params['add_level_1'] = $ctx->gpost('add_level_1', 0);
		$params['add_level_2'] = $ctx->gpost('add_level_2', 0);
		$params['add_level_3'] = $ctx->gpost('add_level_3', 0);
		$params['add_level_4'] = $ctx->gpost('add_level_4', 0);
		$params['price_total'] = Item::currency2Num($ctx->gpost('price_total', 0));
		$params['price_square'] = Item::currency2Num($ctx->gpost('price_square', 0));
		$params['price_equivalent'] = $ctx->gpost('price_equivalent', 0);

		$params['project_type'] = $ctx->gpost('project_type', 0);
		$params['num_house'] = $ctx->gpost('num_house', '');
		$params['full_address'] = $ctx->gpost('full_address', '');
		$params['contact_name'] = $ctx->gpost('contact_name', '');
		$params['contact_phone'] = $ctx->gpost('contact_phone', '');
		$params['contact_company'] = $ctx->gpost('contact_company', '');
		$params['video'] = Item::replaceLinkVideo($ctx->gpost('video', ''));

		$params['contact_email'] = $ctx->gpost('contact_email', '');
		$params['contact_zalo'] = $ctx->gpost('contact_zalo', '');
		$params['contact_viber'] = $ctx->gpost('contact_viber', '');
		$params['contact_company'] = $ctx->gpost('contact_company', '');
		$params['status'] = ItemStatus::$New;
		
		if($ctx->gpost('draft', null))
			$params['status'] = ItemStatus::$Draft;

		//main area
		if($ctx->gpost('main_area', '') == ''){
			$params['area'] = $ctx->gpost($ctx->gpost('main_area'), 0);
		}

		//user approval
		if($ctx->gpost('approved', 0) && $user->user_role == UserRole::$MANAGER)
		{
			$params['status'] = ItemStatus::$Approved;
			$params['approval_id'] = $user->id;
			$params['created_date'] = date("Y-m-d H:i:s");
		}	

		$params['lat'] = $ctx->gpost('lat', 0);
		$params['lng'] = $ctx->gpost('lng', 0);
		$params['photos'] = $ctx->gpost('photos', '');
		if(!$ctx->gpost('date_started')){
			$params['date_started']=date("Y-m-d H:i:s");
		}
		else
		{
			$params['date_started'] = Util::formatDateYMD($ctx->gpost('date_started', 0));
		}
		$time_to_expire = $ctx->gpost('time_to_expire', 30);

		$params['date_ended'] = Util::parseEndDate($params['date_started'], $time_to_expire);

		$subscribe = $ctx->gpost('subscribe', 0);

	
		$id = $ctx->item_model->save($params);
		if($id){
			//save item properties
			$ctx->item_properties_model->save($id, $params['project_type']);

			//Move images[]
			if(empty($params['id'])){
				$item = $ctx->item_model->getById($id);
				$photos = Item::movePhotos($item);
			
				if(count($photos)){
					$tmp['photos'] = implode(';', $photos);
					$tmp['id'] = $id;
					$ctx->item_model->save($tmp);
				}
			}
		}
		
		$item = $ctx->item_model->getById($id);
	
		return $id;
	}

	public static function getOrientation($key){
		if(isset(Constant::$ORIENTATION_HOUSE[$key]))
			return Constant::$ORIENTATION_HOUSE[$key]['name'];
		return '';
	}

	public static function getType($key){
		if(isset(Constant::$ITEM_TYPE[$key]))
			return Constant::$ITEM_TYPE[$key];
		return '';
	}

	public static function getLink(&$item){
		$alias = Util::stringURLSafe($item->title).'-'.$item->id;
		return base_url('/chi-tiet/'.$alias);
	}

	/*-------------- Filter ---------------------------*/
	public static function getFilterPrice($index){

		if($index != 0 && isset(Constant::$RANGE_PRICE[$index]))
		{
			$val = Constant::$RANGE_PRICE[$index][0];

			if($val === '0')
				return 'price_equivalent = 1';

			$arr = explode('-', $val);
			if(count($arr) == 2)
				return "price_total BETWEEN {$arr[0]}000000 AND {$arr[1]}000000 ";
			else
				return 'price_total '.$val.'000000';
		}
		return '';
	}

	public static function select_func($index,$val, $field_name){
	
			$t='FILTER_'.strtoupper($index);
			$tt=Constant::$$t;
		if($val != 0 && isset($tt[$val]))
		{
			$vall = $tt[$val][0];

			if($vall === '1')
				return ''.$val.' = 1';

			$arr = explode('-', $vall);
			if(count($arr) == 2)
				return " ".$field_name." BETWEEN {$arr[0]} AND {$arr[1]} ";
			else
				return $field_name.' '.$vall;
		}
		
			return '';
	}		
	public static function getRangePrice($index){
		$index=$index/1000000;

		$val = Constant::$RANGE_PRICE;

		$arr=explode('<', $val[2][0]);
												
		if($arr[1]>= $index)
		{
			return 2;
		}


		$arr=explode('>', $val[count($val)-1][0]);
										
		if($arr[1]<= $index)
		{
			return count($val)-1;
		}

		for($i=2;$i<count($val);$i++) {
				
				if(strpos($val[$i][0],'-'))
				{
					$arr=explode('-', $val[$i][0]);
					if($arr[0]<= $index && $arr[1]>= $index)
					{
						return $i;
					}
				}
		}
	}
	public static function getRangeFilter($index, $opt){
		$opt='FILTER_'.$opt;
		$val = Constant::$$opt;
		for($i=1;$i<count($val);$i++) {
				if(preg_match('/>/',$val[$i][0]))
				{

					$arr=explode('>', $val[$i][0]);
					if($arr[1]< $index)
					{
						return $i;
					}
				}
				if(preg_match('/</',$val[$i][0]))
				{
					$arr=explode('<', $val[$i][0]);
					if($arr[1]> $index)
					{
						return $i;
					}
				}
				if(preg_match('/=/',$val[$i][0]))
				{
					$arr=explode('=', $val[$i][0]);
					if($arr[1]== $index)
					{
						return $i;
					}
				}
				if(preg_match('/-/',$val[$i][0]))
				{
					$arr=explode('-', $val[$i][0]);
					if($arr[0]<= $index && $arr[1]>= $index)
					{
						return $i;
					}
				}
		}
	}
	public static function getRangeArea($index){

		$val = Constant::$RANGE_AREA;

		$arr=explode('<=', $val[1][0]);
												
		if($arr[1]>= $index)
		{
			return 1;
		}


		$arr=explode('>=', $val[count($val)-1][0]);
										
		if($arr[1]<= $index)
		{
			return count($val)-1;
		}

		for($i=1;$i<count($val);$i++) {
				
				if(strpos($val[$i][0],'-'))
				{
					$arr=explode('-', $val[$i][0]);
					if($arr[0]<= $index && $arr[1]>= $index)
					{
						return $i;
					}
				}
		}
	}

	public static function getFilterArea($index){
		if($index != 0 && isset(Constant::$RANGE_AREA[$index]))
		{
			$val = Constant::$RANGE_AREA[$index][0];
			if($val === '1')
				return '';

			$arr = explode('-', $val);
			if(count($arr) == 2)
				return " BETWEEN {$arr[0]} AND {$arr[1]} ";
			else
				return $val;
		}
		return '';
	}
	public static function youtube_id_from_url($url) {
		$pattern = 
		        '%^# Match any youtube URL
		        (?:https?://)?  # Optional scheme. Either http or https
		        (?:www\.)?      # Optional www subdomain
		        (?:             # Group host alternatives
		          youtu\.be/    # Either youtu.be,
		        | youtube\.com  # or youtube.com
		          (?:           # Group path alternatives
		            /embed/     # Either /embed/
		          | /v/         # or /v/
		          | /watch\?v=  # or /watch\?v=
		          )             # End path alternatives.
		        )               # End host alternatives.
		        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
		        $%x'
		        ;
		$result = preg_match($pattern, $url, $matches);
		    if (false !== $result && isset($matches[1])) {
		        return $matches[1];
		    }
		    return false;
	}
}