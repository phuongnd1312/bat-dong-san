<?php
class MY_Controller extends MX_Controller{
	
	public function __construct($template = 'front'){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('links_helper');
		$this->load->helper('security');
		$this->load->helper('user_helper');
		$this->load->helper('mhtml_helper');
		$this->load->helper('meta_helper');
		$this->lang->load("js",App::ins()->getLangName()); 
		$this->lang->load("main",App::ins()->getLangName());
		$this->loadTemplate($template);
	}

	protected function loadTemplate($template = 'front'){
		$document = null;
		switch ($template) {
			case 'front':
				$document = $this->frontEnd();
				break;
			
			case 'admin':
				$document = $this->backEnd();
				break;
		}
		
		if($document != null)
			$this->template->setTemplate($document['template'], $document);
	}

	protected function frontEnd(){
		$options=array();
		$this->addNavigation();
		$options['template']='templates/front/tmpl';
		$options['template_url'] = base_url().TEMPLATE_URL. '/front';
		$options['css_url'] = $options['template_url'] . '/css/';
		$options['js_url'] = $options['template_url'] . '/js/';
		$options['img_url'] = $options['template_url'] . '/images/';
		return $options;
	}

	protected function backEnd(){
		$options=array();
		$this->addAdminNav();
		$options['template']='templates/admin/tmpl';
		$options['template_url'] = base_url().TEMPLATE_URL. '/admin';
		$options['css_url'] = $options['template_url'] . '/css/';
		$options['js_url'] = $options['template_url'] . '/js/';
		$options['img_url'] = $options['template_url'] . '/images/';
		return $options;
	}

	public function gpost($key, $defaul = null){
		if(!isset($_POST[$key]))
			return $defaul;
		else 
			return $_POST[$key];
	}

	public function gget($key, $defaul = null){
		if(!isset($_GET[$key]))
			return $defaul;
		else 
			return $_GET[$key];
	}

	private function addNavigation(){
		$action = $this->uri->segment(2, '');
		$controler = $this->uri->segment(1, 'main');
		$link = '/'.$controler. ($action != '' ? '/'.$action: '');
		$resp =  array('controler'=>$controler, 
					'action'=>$action, 
					'link'=>$link);

		$navigation = $this->load->view('front/navigation', $resp, true);
		$this->template->set('navigation', $navigation);
	}

	private function addAdminNav(){
		$action = $this->uri->segment(2, 'index');
	
		$navigation = $this->load->view('admin/navigation',  array('action'=>$action), true);
		$this->template->set('navigation', $navigation);
	}

	public function load_pagination($url, $uri_segment = 4){
		$this->load->library("pagination");
		$config["base_url"] = base_url().$url;
		$config["per_page"] = Constant::$PER_ROW_PAGE;
		$config["uri_segment"] = $uri_segment;
		$config['num_links'] = Constant::$NUM_LINK_PAGE;
		$config['total_rows'] = 0;
		$config['prev_link'] = $this->lang->line('nav_previous');
		$config['next_link'] = $this->lang->line('nav_next');
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="javascript:;">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		return $config;
	}
}